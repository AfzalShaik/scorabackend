'use strict';
var empEventValidation = require('../commonValidation/all-models-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var employerEventJson = require('../commonValidation/all-models-json').employerEvent;
var empEventLookups = require('../commonValidation/lookupMethods').typeCodeFunction;
var addressValidation = require('../commonValidation/allmethods').addressFunction;
var employerEvent = server.models.EmployerEvent;
  /**
   *updateEmpEventService-function deals with updating employee event
   *@constructor
   * @param {object} eventData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateEmpEventService(eventData, cb) {
  if (eventData) {
    if (eventData.empDriveId && eventData.companyId && eventData.empEventId) {
      var validValue, eventType, eventStatus;
      var eventTypeObj = {};
      var eventStatusObj = {};
      var eventTypeCode = 'EMPLOYER_EVENT_TYPE_CODE';
      var eventStatusCode = 'EMPLOYER_EVENT_STATUS_CODE';
      eventTypeObj['lookupValueId'] = eventData.eventTypeValueId;
      eventStatusObj['lookupValueId'] = eventData.eventStatusValueId;
      empEventLookups(eventTypeObj, eventTypeCode, function(typeVal) {
        if (typeVal == true) {
          empEventLookups(eventStatusObj, eventStatusCode, function(statusVal) {
            if (statusVal == true) {
              empEventValidation.validateModelsJson(eventData, employerEventJson, function(err, validValue) {
                validValue = validValue;
                if (validValue == false) {
                  throwError(err, cb);
                  logger.error('validation error');
                } else {
                  addressValidation(eventData, function(eventAddVal) {
                    if (eventAddVal == false) {
                      throwError('Invalid CityId or PostalCode or CountryCode or StateCode', cb);
                    } else {
                      var inputFilterObject = {};
                      inputFilterObject['empDriveId'] = eventData.empDriveId;
                      inputFilterObject['companyId'] = eventData.companyId;
                      inputFilterObject['empEventId'] = eventData.empEventId;

                      findEntity.entityDetailsById(inputFilterObject, employerEvent, function(error, response) {
                        if (error) {
                          throwError(error, cb);
                          logger.error('Error while fetching employer campus list Program record');
                        } else if (response) {
                          response.data.updateAttributes(eventData, function(err, info) {
                            if (err) {
                              throwError('error while updating emp campus Program', cb);
                            } else {
                              logger.info('emp campus Program record Updated Successfully');
                              info['requestStatus'] = true;
                              info['updateDatetime'] = new Date();
                              cb(null, info);
                            }
                          });
                        } else {
                          throwError('Invalid companyId  or empEventId or empDriveId', cb);
                          logger.error('Invalid companyId or empEventId or empDriveId');
                        }
                      });
                    }
                  });
                }
              });
            } else {
              throwError('Invalid Employer_Event_Status_code', cb);
            }
          });
        } else {
          throwError('Invalid Employer_Event_Type_code', cb);
        }
      });
    } else {
      throwError('empDriveId, empEventId and companyId are required', cb);
      logger.error('empDriveId, empEventId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateEmpEventService = updateEmpEventService;

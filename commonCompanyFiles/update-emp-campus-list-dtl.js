'use strict';
var empProgramValidation = require('../commonValidation/all-models-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var employerCampusListProgramJson = require('../commonValidation/all-models-json').employerCampusListProgramJson;
var employerCampusListPrograms = server.models.EmployerCampusListDtl;
  /**
   *updateEmpCampusProgramService-function deals with updating the campus program service
   *@constructor
   * @param {object} empProgramData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateEmpCampusProgramService(empProgramData, cb) {
  if (empProgramData) {
    if (empProgramData.listCampusId && empProgramData.companyId && empProgramData.listId && empProgramData.campusId) {
      var validValue;
      empProgramValidation.validateModelsJson(empProgramData, employerCampusListProgramJson, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
          logger.error('validation error');
        } else {
          var inputFilterObject = {};
          inputFilterObject['campusId'] = empProgramData.campusId;
          inputFilterObject['companyId'] = empProgramData.companyId;
          inputFilterObject['listCampusId'] = empProgramData.listCampusId;
          inputFilterObject['listId'] = empProgramData.listId;
          findEntity.entityDetailsById(inputFilterObject, employerCampusListPrograms, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching employer campus list Program record');
            } else if (response) {
              response.data.updateAttributes(empProgramData, function(err, info) {
                if (err) {
                  throwError('error while updating emp campus Program', cb);
                } else {
                  logger.info('emp campus Program record Updated Successfully');
                  info['requestStatus'] = true;
                  info['updateDatetime'] = new Date();
                  cb(null, info);
                }
              });
            } else {
              throwError('Invalid companyId or campusId or listCampusId or listId', cb);
              logger.error('Invalid companyId or campusId or listCampusId or listId');
            }
          });
        }
      });
    } else {
      throwError('listId, listCampusId, campusId and companyId are required', cb);
      logger.error('listId, listCampusId, campusId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateEmpCampusProgramService = updateEmpCampusProgramService;

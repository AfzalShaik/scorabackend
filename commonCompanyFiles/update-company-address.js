'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validation = require('../commonValidation/addressValidation');
var updateAddress = require('../commonValidation/updateAddress');
var allmethods = require('../commonValidation/allmethods');
var lookupValidation = require('../commonValidation/lookupMethods');
var updateAddService = require('../commonValidation/update-service-address');
var CompanyAddress = server.models.CompanyAddress;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *updateCompany-function deals with updating the company data
   *@constructor
   * @param {object} companyData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateCompany(companyData, cb) {
  if (companyData) {
    if (companyData.companyId && companyData.addressId) {
      CompanyAddress.findOne({
        'where': {
          'and': [{
            'companyId': companyData.companyId,
          }, {
            'addressId': companyData.addressId,
          }],
        },
      }, function(err, companyList) {
        if (err) {
          //throws error
          cb(err, companyData);
        } else if (companyList) {
          logger.info('record found for companyid,addressid');
          updateAddService.updateAddresService(companyData, function(err, companyAddressList) {
            if (err) {
              cb(err, companyData);
            } else {
              logger.info('company address updated successfully');
              cb(null, companyAddressList);
            }
          });
        } else {
          //throws error incase of invalid address_id or companyId
          throwError('Invalid address_id or companyId ', cb);
          logger.error('Invalid address_id or companyId');
        }
      });
    } else {
      throwError('companyId and  addressId are required', cb);
    }
  } else {
    logger.error("Input can't be Null");
    throwError('Input is Null: ', cb);
  }
}
exports.updateCompany = updateCompany;

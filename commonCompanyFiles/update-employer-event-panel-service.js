'use strict';
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var employerEventPanelJson = require('../common/models/EMPLOYER_EVENT_PANEL.json').properties;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var employerEventPanel = server.models.EmployerEventPanel;
var count = 0;
var panelArray = [];
/**
 *updatePanel-function deals with updating the panel details
 *@constructor
 * @param {object} panelData - contains all the data need to get updated
 * @param {any} cb - deals with the response
 */
function updatePanel(panelData, cb) {
  if (panelData) {
    validateModel(panelData, employerEventPanelJson, function(err, resp) {
      if (resp.valid == false) {
        cb(resp.errors, resp);
      } else {
        panelData.map(function(itemObj) {
          if (itemObj.empEventPanelId && itemObj.companyId && itemObj.empEventId && itemObj.empDriveId && itemObj.employerPersonId) {
            var inputFilterObject = {};
            inputFilterObject['empEventPanelId'] = itemObj.empEventPanelId;
            inputFilterObject['companyId'] = itemObj.companyId;
            inputFilterObject['empEventId'] = itemObj.empEventId;
            inputFilterObject['empDriveId'] = itemObj.empDriveId;
            findEntity(inputFilterObject, employerEventPanel, function(error, response) {
              if (error) {
                throwError(error, cb);
                logger.error('Error while fetching emp event panel record');
              } else if (response) {
                response.data.updateAttributes(itemObj, function(err, info) {
                  if (err) {
                    throwError('error while updating emp event panel', cb);
                  } else {
                    logger.info('emp event panel item record Updated Successfully');
                    info['requestStatus'] = true;
                    info['updateDatetime'] = new Date();
                    count++;
                    panelArray.push(info);
                    panelArray['employerEventPanel'] = panelArray;
                    if (panelData.length == count) {
                      count = 0;
                      // console.log('child arrayyyyyyyyyyyyyyyyyyyy ', panelArray);
                      cb(null, panelArray);
                    }
                  }
                  // }
                });
              } else {
                throwError('Invalid empEventId, empDriveId, empEventPanelId and CompanyId', cb);
                logger.error('Invalid empEventId, empDriveId,  empEventPanelId and CompanyId');
              }
            });
          } else {
            throwError('empEventPanelId, empDriveId, empEventId, employerPersonId and CompanyId are required', cb);
            logger.error('empEventPanelId, empDriveId, empEventId, employerPersonId and CompanyId are required');
          }
        });
      }
    });
  } else {
    cb(null, 'done');
  }
}
exports.updatePanel = updatePanel;

'use strict';
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var empListHdrJson = require('../common/models/EMPLOYER_CAMPUS_LIST_HDR.json').properties;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var employerCampusListHdr = server.models.EmployerCampusListHdr;
var updateEmpDtl = require('./update-emp-campus-list-dtl-service').updateDtl;
var updateEmpList = require('./update-emp-campus-list-compPkg-service').updateList;
var count = 0;
var empArray = [];
var hdrArray = {};
  /**
   *updateEmployerCampusListService-function deals with updating the employer campus list service
   *@constructor
   * @param {object} hdrData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateEmployerCampusListService(hdrData, cb) {
  validateModel(hdrData.empHdr, empListHdrJson, function(err, resp) {
    if (resp.valid == false) {
      cb(resp.errors, resp);
    } else {
      hdrData.empHdr.map(function(hdrObj) {
        if (hdrObj.listId && hdrObj.companyId && hdrObj.jobRoleId) {
          var compLookups = [{
            'lookupValueId': hdrObj.compApprovalStatusValueId,
            'lookupTypeId': 'COMP_APPROVAL_STATUS_CODE',
          }];
          lookupValidation(compLookups, function(lookupValue) {
            if (lookupValue == true) {
              var inputFilterObject = {};
              inputFilterObject['listId'] = hdrObj.listId;
              inputFilterObject['companyId'] = hdrObj.companyId;
              // inputFilterObject['jobRoleId'] = hdrObj.jobRoleId;
              findEntity(inputFilterObject, employerCampusListHdr, function(error, response) {
                if (error) {
                  throwError(error, cb);
                  logger.error('Error while fetching Emp Campus Hdr record');
                } else if (response) {
                  response.data[0].updateAttributes(hdrObj, function(err, info) {
                    if (err) {
                      throwError(err, cb);
                    } else {
                      logger.info('Emp Campus Hdr record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      count++;
                      empArray.push(info);

                      if (hdrData.empHdr.length == count) {
                        count = 0;
                        updateEmpDtl(hdrData.empDtl, function(error, dtlResponse) {
                          if (error) {
                            throwError(error, cb);
                            logger.error('Error while updating emp dtl');
                          } else if (dtlResponse) {
                            hdrArray['empHdr'] = empArray;
                            hdrArray['empDtl'] = dtlResponse;
                            updateEmpList(hdrData.empListPkg, function(error, listResponse) {
                              if (error) {
                                throwError(error, cb);
                                logger.error('Error while updating emp dtl');
                              } else if (listResponse) {
                                hdrArray['empListPkg'] = listResponse;
                                cb(null, hdrArray);
                              } else {
                                throwError('Invalid emp list Input', cb);
                                logger.error('Invalid emp list Input');
                              }
                            });
                          } else {
                            throwError('Invalid emp dtl Input', cb);
                            logger.error('Invalid emp dtl Input');
                          }
                        });
                      }
                    }
                  });
                } else {
                  throwError('Invalid listId,jobRoleId and CompanyId', cb);
                  logger.error('Invalid listId,jobRoleId and CompanyId');
                }
              });
            } else {
              throwError('Invalid compApprovalStatusValueId', cb);
              logger.error('Invalid compApprovalStatusValueId');
            }
          });
        } else {
          throwError('listId,jobRoleId and CompanyId are required', cb);
          logger.error('listId,jobRoleId and CompanyId are required');
        }
      });
    }
  });
}
exports.updateEmployerCampusListService = updateEmployerCampusListService;

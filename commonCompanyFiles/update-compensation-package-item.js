'use strict';
var compensationValidation = require('../commonValidation/compensation-package-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var compensationPackageItem = server.models.CompensationPackageItem;
  /**
   *updatePackageItemService-function deals with updating the company pakage item
   *@constructor
   * @param {object} pakageData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updatePackageItemService(packageData, cb) {
  if (packageData) {
    if (packageData.compPackageId && packageData.companyId && packageData.compPackageItemId) {
      var validValue;
      compensationValidation.validateCompensationPackageJson(packageData, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
          logger.error('validation error');
        } else {
          var inputFilterObject = {};
          inputFilterObject['compPackageId'] = packageData.compPackageId;
          inputFilterObject['companyId'] = packageData.companyId;
          inputFilterObject['compPackageItemId'] = packageData.compPackageItemId;
          findEntity.entityDetailsById(inputFilterObject, compensationPackageItem, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching compensation package item record');
            } else if (response) {
              compensationValidation.validateCompensationPackageLookup(packageData, function(lookupErr, lookupValue) {
                if (lookupErr) {
                  throwError(lookupErr, cb);
                  logger.error('error while validating package lookups');
                } else if (lookupValue == true) {
                  response.data.updateAttributes(packageData, function(err, info) {
                    if (err) {
                      throwError('error while updating compensation package item', cb);
                    } else {
                      logger.info('compensation package item record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      cb(null, info);
                    }
                  });
                } else {
                  throwError('invalid  compPackageTypeValueId', cb);
                }
              });
            } else {
              throwError('Invalid companyId or compPackageId or compPackageItemId', cb);
              logger.error('Invalid companyId or compPackageId or compPackageItemId');
            }
          });
        }
      });
    } else {
      throwError('compPackageId, compPackageItemId and companyId are required', cb);
      logger.error('compPackageId, compPackageItemId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updatePackageItemService = updatePackageItemService;

'use strict';
var cleanArray = require('../commonValidation/common-mass-upload').cleanArray;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var async = require('async');
var deleteSingleEntityDetails = require('../ServicesImpl/CommonImpl/deleteEntityRecord.js')
  .deleteSingleEntityDetails;
var errorArray = [];
var finalArray1 = [];
var key;
function createMassUpload(fileInput, inputData, callBc) {
  var companyUploadLog = server.models.CompanyUploadLog;
  var details = inputData.fileDetails;
  var container = inputData.fileDetails.container;
  var pathForm = require('path');
  var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
    // csvFileLocation = pathForm.join(__dirname, '../../../', csvFileLocation);
  var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + details.name;
    // errorFileLocation = pathForm.join(__dirname, '../../../', errorFileLocation);
  var logInput = {
    'compUploadTypeValueId': inputData.compUploadTypeValueId,
    'companyId': inputData.companyId,
    'inputParameters': 'string',
    'createUserId': 1,
    'createDatetime': new Date(),
    'csvFileLocation': csvFileLocation,
    'errorFileLocation': errorFileLocation,
    'totalNoRecs': fileInput.length,
  };
  companyUploadLog.create(logInput, function(err, logResponse) {
    if (err) {
      throwError(err, callBc);
    } else {
      inputData.companyUploadLogId = logResponse.companyUploadLogId;
      async.map(fileInput, validateInput, function(validateErr, validateOutput) {
        if (validateErr) {
          throwError(validateErr, callBc);
        } else {
          // console.log('2222222222222222222222222222222222');
          var inputArray = cleanArray(validateOutput);
          if (inputArray.length > 0) {
            async.map(inputArray, getStudentId, getStudentCB);

            function getStudentId(obj, studentCB) {
              var scoraUser = server.models.ScoraUser;
              var eventTestScoreWork = server.models.EventTestScoreWork;
              scoraUser.findOne({
                where: {
                  email: obj.studentEmail,
                },
              },
                function(userErr, userOutput) {
                  if (userErr) {
                    obj.error = 'Invalid Email';
                    obj.companyUploadLogId = logResponse.companyUploadLogId;
                    eventTestScoreWork.create(obj, function(testErr, testOutput) {
                      studentCB(null, null);
                    });
                  } else if (userOutput) {
                    obj.userId = userOutput.id;
                    studentCB(null, obj);
                  } else {
                    obj.error = 'Invalid Email';
                    obj.companyUploadLogId = logResponse.companyUploadLogId;
                    eventTestScoreWork.create(obj, function(testErr, testOutput) {
                      studentCB(null, null);
                    });
                  }
                }
              );
            }

            function getStudentCB(studentError, userInfo) {
              var userResponse = cleanArray(userInfo);
              if (userResponse.length > 0) {
                async.map(userResponse, validateStudent, validateStudentCB);
              } else {
                commonErrorRead(undefined, function(err, output) {
                  callBc(null, output);
                });
              }
            }

            function validateStudent(object, stdCB) {
              var student = server.models.Student;
              var eventTestScoreWork = server.models.EventTestScoreWork;
              student.findOne({
                where: {
                  id: object.userId,
                },
              },
                function(stdErr, studentOut) {
                  if (stdErr) {
                    object.error = 'Invalid Student Id';
                    object.companyUploadLogId = logResponse.companyUploadLogId;
                    eventTestScoreWork.create(object, function(
                      testErr,
                      testOutput
                    ) {
                      stdCB(null, null);
                    });
                  } else if (studentOut) {
                    object.studentId = studentOut.studentId;
                    stdCB(null, object);
                  } else {
                    object.error = 'Invalid Student Id';
                    object.companyUploadLogId = logResponse.companyUploadLogId;
                    eventTestScoreWork.create(object, function(
                      testErr,
                      testOutput
                    ) {
                      stdCB(null, null);
                    });
                  }
                }
              );
            }

            function validateStudentCB(error, studentInfo) {
              var studentResponse = cleanArray(studentInfo);
              async.map(studentResponse, validate, validationCB);
            }

            function validate(obj, validateCallBC) {
              var eventTestScoreWork = server.models.EventTestScoreWork;
              if (
                obj.score > 0 &&
                obj.maxScore > 0 &&
                parseInt(obj.maxScore) > parseInt(obj.score)
              ) {
                validateCallBC(null, obj);
              } else {
                obj.error = 'Validation Failed';
                obj.companyUploadLogId = logResponse.companyUploadLogId;
                eventTestScoreWork.create(obj, function(testErr, testOutput) {
                  validateCallBC(null, null);
                });
              }
            }

            function validationCB(err, validateInfo) {
              var validateResponse = cleanArray(validateInfo);
              var eventTestScore = server.models.EventTestScore;
              var eventTestScoreWork = server.models.EventTestScoreWork;
              // var readError = require('../commonValidation/read-error-into-file').readErrorRecords;
              var createCompanyLog = require('./create-company-log')
                .createCompanyLog;
              var inputFilterObject = {};
              inputFilterObject['employerEventId'] = inputData.empEventId;
              deleteSingleEntityDetails(inputFilterObject, eventTestScore, function(
                error,
                destroy
              ) {
                eventTestScore.create(validateResponse, function(
                  createErr,
                  createOutput
                ) {
                  if (createErr) {
                    throwError(validateResponse, callBc);
                  } else {
                    eventTestScoreWork.find({}, function(failedErr, failedOutput) {
                      readErrorRecords(failedOutput, inputData, function(
                        readError,
                        readResponse
                      ) {
                        // var logInput = {
                        //   'noFailRecs': cleanArray(failedOutput).length,
                        //   'noSuccessRecs': createOutput.length,
                        // };
                        var container = inputData.fileDetails.container;
                        var logInput = {
                          'totalNoRecs': fileInput.length,
                          'noFailRecs': cleanArray(finalArray1).length,
                          'noSuccessRecs': createOutput.length,
                          'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + key,
                        };
                        // console.log('.............................. ', logInput);
                        companyUploadLog.updateAll({'companyUploadLogId': logResponse.companyUploadLogId},
                        logInput, function(logErr, logOutput) {
                          eventTestScoreWork.destroyAll({'companyUploadLogId': logResponse.companyUploadLogId},
                          function(
                            destroyError,
                            destroyOutput
                          ) {
                            callBc(null, logOutput);
                          });
                        });
                        // console.log('ddddddddddddddddddddddddddd ', logInput);
                        // createCompanyLog(logInput, function(logError, logOutput) {
                        // console.log(logError, logOutput);
                        // });
                      });
                    });
                  }
                });
              });
            }
          } else {
            commonErrorRead(undefined, function(err, output) {
              callBc(null, output);
            });
          }
        }
      });

      function validateInput(validateObj, validateCB) {
        // console.log('111111111111111111111111111111111111111111111111111111');
        var eventTestScoreValidation = require('../commonValidation/event-test-score-validation').validateJson;
        var eventTestScoreWork = server.models.EventTestScoreWork;
        eventTestScoreValidation(validateObj, function(validationErr, validationResponse) {
          // console.log('........................................ ', validationErr, validationResponse);
          if (validationErr) {
            validateObj.error = validationErr.err;
            eventTestScoreWork.create(validateObj, function(testErr, testOutput) {
              if (testErr) {
                validateObj.error = validationErr.err;
                // console.log('........................................ ', validationErr.err, validateObj);
                errorArray.push(validateObj);
                validateCB(null, null);
              } else {
                validateCB(null, null);
              }
            });
          } else {
            validateObj.companyUploadLogId = logResponse.companyUploadLogId;
            validateCB(null, validateObj);
          }
        });
      }

      function readErrorRecords(finalArray, stDataa, cb) {
        var ErrorArray = [];
        var failedArray = [];
        var finalArray1 = [];
        failedArray = finalArray;
        // console.log('failedArrayfailedArrayfailedArrayfailedArrayfailedArray ', failedArray);
        // console.log('errorzrrayyyyyyyyyyyyyyyyyyyyyy ', errorArray);
        if (errorArray.length > 0) {
          for (var j = 0; j < errorArray.length; j++) {
            // console.log();
            failedArray.push(errorArray[j]);
            // console.log(failedArray);
          }
        } else {
          failedArray = finalArray;
        }
        for (var i = 0; i < finalArray.length; i++) {
          var obj = {
            rowNumber: finalArray[i].rowNumber,
            testName: finalArray[i].testName,
            testVendorName: finalArray[i].testVendorName,
            description: finalArray[i].description,
            studentEmail: finalArray[i].studentEmail,
            error: finalArray[i].error,
            score: finalArray[i].score,
            maxScore: finalArray[i].maxScore,
            companyId: finalArray[i].companyId,
          };
          finalArray1.push(obj);
        }
        var fs = require('fs');
        var csv = require('fast-csv');
        var name = stDataa.fileDetails.name;
        var container = stDataa.fileDetails.container;
        // var pathForm = require('path');
        // var errorCsv = './attachments/' + container + '/' + name;
        // errorCsv = pathForm.join(__dirname, '../../../', errorCsv);
        // // console.log('errorCSV', errorCsv);
        // var ws = fs.createWriteStream(errorCsv);
        // csv
        //   .writeToPath(errorCsv, finalArray1, {
        //     headers: true,
        //   })
        //   .on('finish', function() {
        //     // console.log('done!');
        //   });
        // cb(null, 'success');
        // errorArray = [];
        var uploadCsv = require('../commonCampusFiles/commonCSVUpload.js').uploadCSV;
        var company = server.models.Company;
        company.findOne({'where': {'companyId': inputData.companyId}}, function(companyErr, companyOut) {
          var fs = require('fs');
          var csv = require('fast-csv');
          var json2csv = require('json2csv');
          var fileName;
          fileName = companyOut.name;
          var randomNumber = Math.random().toString(20).slice(-10);
          var errorFile = companyOut.name + '_' + randomNumber + '.csv';
          fileName = './commonValidation/' + companyOut.name + '.csv';
          var fields = ['rowNumber', 'testName', 'testVendorName', 'description', 'studentEmail', 'score', 'maxScore', 'error'];
          var csv = json2csv({
            data: finalArray1,
            fields: fields,
          });
          fs.writeFile(fileName, csv, function(err) {
            if (err) throw err;
            uploadCsv(fileName, container, errorFile, function(fileErr, fileOut) {
              key = fileOut;
              cb(null, 'success');
              errorArray = [];
              // failedArray = []
              // finalArray1 = [];
            });
          });
        });
      }

      function commonErrorRead(responseCreated, cb) {
        var eventTestScoreWork = server.models.EventTestScoreWork;
        var companyUploadLog = server.models.CompanyUploadLog;
        //var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
        eventTestScoreWork.find({}, function(workErr, workResponse) {
          if (workErr) {
            eventTestScoreWork.destroyAll({'companyUploadLogId': logResponse.companyUploadLogId}, function(error, destroy) {
              throwError(error, cb);
            });
          } else {
            var campusUploadLog = server.models.CampusUploadLog;
            var details = inputData.fileDetails;
            var container = inputData.fileDetails.container;
            readErrorRecords(workResponse, inputData, function(err, failedArray) {
              var pathForm = require('path');
              var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
              // csvFileLocation = pathForm.join(__dirname, '../../../', csvFileLocation);
              var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + key;
              // errorFileLocation = pathForm.join(__dirname, '../../../', errorFileLocation);
              var logInput = {
                'totalNoRecs': fileInput.length,
                'noFailRecs': workResponse.length,
                'noSuccessRecs': responseCreated ? cleanArray(responseCreated).length : 0,
              };
              // console.log('.............................. ', logInput);
              companyUploadLog.updateAll({'companyUploadLogId': logResponse.companyUploadLogId},
              logInput, function(logErr, logOutput) {
                // console.log(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ', logErr, logOutput);
                eventTestScoreWork.destroyAll({'companyUploadLogId': logResponse.companyUploadLogId}, function(error, destroy) {
                  cb(null, logOutput);
                });
              });
            });
          }
        });
      }
    }
  });
}
exports.createMassUpload = createMassUpload;

'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validation = require('../commonValidation/addressValidation');
var updateAddress = require('../commonValidation/updateAddress');
var allmethods = require('../commonValidation/allmethods');
var OrganizationAddress = server.models.OrganizationAddress;
var updateAddService = require('../commonValidation/update-service-address');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *organizationAddressUpdate-function deals with updating the organization address
   *@constructor
   * @param {object} organizationData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function organizationAddressUpdate(organizationData, cb) {
  if (organizationData) {
    //  console.log("validating fields: ",organizationData);
    if (organizationData.companyId && organizationData.organizationId && organizationData.addressId) {
      OrganizationAddress.findOne({
        'where': {
          'and': [{
            'companyId': organizationData.companyId,
          }, {
            'addressId': organizationData.addressId,
          }, {
            'organizationId': organizationData.organizationId,
          }],
        },
      }, function(err, OrgList) {
        if (err) {
          //throws error
          cb(err, organizationData);
        } else if (OrgList) {
          logger.info('record found for organization address');
          updateAddService.updateAddresService(organizationData, function(err, orgAddressList) {
            if (err) {
              cb(err, organizationData);
            } else {
              logger.info('organization address updated successfully');
              cb(null, orgAddressList);
            }
          });
        } else {
          //throws error incase of invalid address_id or companyId
          throwError('Invalid address_id or companyId or organizationId', cb);
          logger.error('Invalid address_id or companyId or organizationId');
        }
      });
    } else {
      throwError('companyId and organizationId and addressId is required ', cb);
    }
  } else {
    logger.error("Input can't be Null");
    throwError('Input is Null: ', cb);
  }
}

exports.organizationAddressUpdate = organizationAddressUpdate;

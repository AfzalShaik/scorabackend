'use strict';
var empDriveValidation = require('../commonValidation/all-models-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var employerDriveJson = require('../commonValidation/all-models-json').employerDrive;
var empDriveLookups = require('../commonValidation/lookupMethods').typeCodeFunction;
var employerDrive = server.models.EmployerDrive;
  /**
   *updateEmpDriveService-function deals with updating the drive service
   *@constructor
   * @param {object} driveData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateEmpDriveService(driveData, cb) {
  if (driveData) {
    if (driveData.empDriveId && driveData.companyId  && driveData.jobRoleId) {
      var validValue, driveType, driveStatus;
      var driveTypeObj = {};
      var driveStatusObj = {};
      var driveTypeCode = 'EMPLOYER_DRIVE_TYPE_CODE';
      var driveStatusCode = 'EMPLOYER_DRIVE_STATUS_CODE';
      driveTypeObj['lookupValueId'] = driveData.driveTypeValueId;
      driveStatusObj['lookupValueId'] = driveData.driveStatusValueId;
      empDriveLookups(driveTypeObj, driveTypeCode, function(typeVal) {
        if (typeVal == true) {
          empDriveLookups(driveStatusObj, driveStatusCode, function(statusVal) {
            if (statusVal == true) {
              empDriveValidation.validateModelsJson(driveData, employerDriveJson, function(err, validValue) {
                validValue = validValue;
                if (validValue == false) {
                  throwError(err, cb);
                  logger.error('validation error');
                } else {
                  var inputFilterObject = {};
                  inputFilterObject['empDriveId'] = driveData.empDriveId;
                  inputFilterObject['companyId'] = driveData.companyId;
                 // inputFilterObject['organizationId'] = driveData.organizationId;
                  inputFilterObject['jobRoleId'] = driveData.jobRoleId;
                  findEntity.entityDetailsById(inputFilterObject, employerDrive, function(error, response) {
                    if (error) {
                      throwError(error, cb);
                      logger.error('Error while fetching employer campus list Program record');
                    } else if (response) {
                      response.data[0].updateAttributes(driveData, function(err, info) {
                        if (err) {
                          throwError('error while updating emp campus Program', cb);
                        } else {
                          logger.info('emp campus Program record Updated Successfully');
                          info['requestStatus'] = true;
                          info['updateDatetime'] = new Date();
                          cb(null, info);
                        }
                      });
                    } else {
                      throwError('Invalid companyId or jobRoleId or organizationId or empDriveId', cb);
                      logger.error('Invalid companyId or jobRoleId or organizationId or empDriveId');
                    }
                  });
                }
              });
            } else {
              throwError('Invalid Employer_Drive_Status_code', cb);
            }
          });
        } else {
          throwError('Invalid Employer_Drive_Type_code', cb);
        }
      });
    } else {
      throwError('empDriveId, organizationId, jobRoleId and companyId are required', cb);
      logger.error('empDriveId, organizationId, jobRoleId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateEmpDriveService = updateEmpDriveService;

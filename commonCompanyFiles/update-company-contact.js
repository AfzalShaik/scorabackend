'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var contactValidation = require('../commonValidation/contactValidation');
var updateContact = require('../commonValidation/updateContact');
var allmethods = require('../commonValidation/allmethods');
var updateContactService = require('../commonValidation/update-service-contact');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var CompanyContact = server.models.CompanyContact;
  /**
   *updateCompanyContact-function deals with updating company contact details
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateCompanyContact(contactData, cb) {
  //validating the input
  if (contactData.companyId && contactData.contactId) {
    CompanyContact.findOne({
      'where': {
        'and': [{
          'companyId': contactData.companyId,
        }, {
          'contactId': contactData.contactId,
        }],
      },
    }, function(err, companyList) {
      if (err) {
        cb(err, contactData);
      } else if (companyList) {
        updateContactService.updateContactService(contactData, function(error, info) {
          if (error) {
            logger.error('error while performing update company contact');
            cb(error, contactData);
          } else {
            logger.info('successfully company contact updated');
            cb(null, info);
          }
        });
      } else {
        //throws error when invalid contatcId or companyId
        throwError('Invalid contactId or companyId ', cb);
        logger.error('Invalid contactid or companyId');
      }
    });
  } else {
    throwError('required contactId and companyId ', cb);
    logger.error('required contactId and companyId');
  }
}
exports.updateCompanyContact = updateCompanyContact;

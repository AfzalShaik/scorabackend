  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var contactValidation = require('../commonValidation/contactValidation');
  var updateContact = require('../commonValidation/updateContact');
  var allmethods = require('../commonValidation/allmethods');
  var updateContactService = require('../commonValidation/update-service-contact');
  var EmployerPersonContact = server.models.EmployerPersonContact;
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *employeePersonContactUpdate-function deals with updating the employee person contact
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function employeePersonContactUpdate(contactData, cb) {
    if (contactData.employerPersonId && contactData.contactId && contactData.companyId) {
      //validating the input
      EmployerPersonContact.findOne({
        'where': {
          'and': [{
            'companyId': contactData.companyId,
          }, {
            'employerPersonId': contactData.employerPersonId,
          }, {
            'contactId': contactData.contactId,
          }],
        },
      }, function(err, employerPersonList) {
        if (err) {
          cb(err, contactData);
        } else if (employerPersonList) {
          logger.info('record found for employee person contact');
          updateContactService.updateContactService(contactData, function(error, info) {
            if (error) {
              logger.error('error while updating the employee person contact');
              cb(error, contactData);
            } else {
              logger.info('successfully employee person contact updated');
              cb(null, info);
            }
          });

        } else {
          //throws error when invalid contatcId or employerPersonId or companyId
          throwError('Invalid contactId or companyId or employerPersonId ', cb);
          logger.error('Invalid contactId or companyId or employerPersonId');
        }
      });
    } else {
      throwError('employerPersonId, contactId and companyId required', cb);
      logger.error('employerPersonId, contactId and companyId are required ');
    }
  }
  exports.employeePersonContactUpdate = employeePersonContactUpdate;

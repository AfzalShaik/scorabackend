'use strict';
var server = require('../server/server');
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var throwError = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var deleteSingleEntityDetails = require('../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;
var errorArray = [];
var key;
function createCompanyHiringAggregate(fileInput, inputData, callBc) {
  var logInput = {
    'compUploadTypeValueId': inputData.compUploadTypeValueId,
    'companyId': inputData.companyId,
    'container': inputData.fileDetails.container,
    'name': inputData.fileDetails.name,
    'originalFileName': inputData.fileDetails.originalFileName,
    'totalNoRecs': fileInput.length,
  };
  var createCompanyLog = require('./create-company-log').createCompanyLog;
  createCompanyLog(logInput, function(logError, logOutput) {
    inputData.companyUploadLogId = logOutput.companyUploadLogId;
    async.map(fileInput, validateInput, function(validateErr, validateOutput) {
      if (validateErr) {
        throwError(validateErr, callBc);
      } else {
        // console.log('fileeeeeeeeeeeeeeeeee', fileInput);
        var inputArray = cleanArray(validateOutput);
        // console.log('0000000000000000000000000000000000000 ', inputArray);
        if (inputArray.length > 0) {
          async.map(inputArray, verifyOrganization, organizationCB);

          function verifyOrganization(obj, orgCB) {
            var organization = server.models.Organization;
            var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
            organization.findOne({
              'where': {
                'and': [{
                  'name': obj.organizationName,
                },
                {
                  'companyId': inputData.companyId,
                },
                ],
              },
            }, function(orgErr, orgOutput) {
              if (orgErr) {
                obj.error = 'ORGANIZATION does not exist';
                obj.companyUploadLogId = inputData.companyUploadLogId;
                companyHiringAggregatesWork.create(obj, function(err, errResp) {
                  orgCB(null, null);
                });
              } else if (orgOutput) {
                obj.organizationId = orgOutput.organizationId;
                obj.companyId = orgOutput.companyId;
                orgCB(null, obj);
              } else {
                obj.error = 'ORGANIZATION does not exist';
                obj.companyUploadLogId = inputData.companyUploadLogId;
                companyHiringAggregatesWork.create(obj, function(err, errResp) {
                  orgCB(null, null);
                });
              }
            });
          }

          function organizationCB(orgError, orgOut) {
            // console.log('orggggggggggggggggggggggggg', orgError, orgOut);
            var organizationResponse = cleanArray(orgOut);
            if (organizationResponse.length > 0) {
              async.map(organizationResponse, verifyCampus, campusCallBack);
            } else {
              commonErrorRead(undefined, function(err, res) {
                callBc(null, res);
              });
            }
          }

          function verifyCampus(object, campusCB) {
            var campus = server.models.Campus;
            var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
            campus.findOne({
              'where': {
                'name': object.campusName,
              },
            }, function(error, campusOut) {
              if (error) {
                object.error = 'CAMPUS does not exist';
                object.companyUploadLogId = inputData.companyUploadLogId;
                companyHiringAggregatesWork.create(object, function(err, errResp) {
                  campusCB(null, null);
                });
              } else if (campusOut) {
                object.campusId = campusOut.campusId;
                campusCB(null, object);
              } else {
                object.companyUploadLogId = inputData.companyUploadLogId;
                object.error = 'CAMPUS does not exist';
                companyHiringAggregatesWork.create(object, function(err, errResp) {
                  campusCB(null, null);
                });
              }
            });
          }

          function campusCallBack(error, campusOut) {
            var campusResponse = cleanArray(campusOut);
            if (campusResponse.length > 0) {
              async.map(campusResponse, verifyJobRole, jobRoleCallBack);
            } else {
              commonErrorRead(undefined, function(err, res) {
                callBc(null, res);
              });
            }
          }

          function verifyJobRole(obj, jobroleCb) {
            var jobRole = server.models.JobRole;
            var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
            jobRole.findOne({
              'where': {
                'and': [{
                  'jobRoleName': obj.jobRoleName,
                }, {
                  'companyId': inputData.companyId,
                }],
              },
            }, function(err, jobOut) {
              if (err) {
                obj.error = 'Job Role does not exist';
                obj.companyUploadLogId = inputData.companyUploadLogId;
                companyHiringAggregatesWork.create(obj, function(err, errResp) {
                  jobroleCb(null, null);
                });
              } else if (jobOut) {
                obj.jobRoleId = jobOut.jobRoleId;
                jobroleCb(null, obj);
              } else {
                obj.error = 'Job Role does not exist';
                obj.companyUploadLogId = inputData.companyUploadLogId;
                companyHiringAggregatesWork.create(obj, function(err, errResp) {
                  jobroleCb(null, null);
                });
              }
            });
          }

          function jobRoleCallBack(error, roleOut) {
            var roleResponse = cleanArray(roleOut);
            //console.log(roleResponse.length);
            if (roleResponse.length > 0) {
              async.map(roleResponse, verifyValidation, validationCallBack);
            } else {
              commonErrorRead(undefined, function(err, res) {
                callBc(null, res);
              });
            }
          }

          function verifyValidation(obj, validationCB) {
            // console.log('objobjobjobjobj ', obj);
            var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
            if (parseInt(obj.noOfOffers) > 0 && parseInt(obj.sumOfOffers) > 0 && parseInt(obj.minimumOffer) > 0 && parseInt(obj.maximumOffer) > 0 && (parseInt(obj.sumOfOffers) > parseInt(obj.maximumOffer)) && (parseInt(obj.maximumOffer) >= parseInt(obj.minimumOffer))) {
              validationCB(null, obj);
            } else {
              obj.error = 'Validation Failed';
              obj.companyUploadLogId = inputData.companyUploadLogId;
              companyHiringAggregatesWork.create(obj, function(err, resp) {
                // console.log('...................... ', resp);
                if (err) {
                  errorArray.push(obj);
                  validationCB(null, null);
                } else {
                  validationCB(null, null);
                }
              });
            }
          }

          function validationCallBack(error, validationOut) {
            // console.log('in');
            var validationResponse = cleanArray(validationOut);
            var companyHiringAggregates = server.models.CompanyHiringAggregates;
            var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
            var createCompanyLog = require('./create-company-log').createCompanyLog;
            var inputFilterObject = {};
            inputFilterObject['companyId'] = inputData.companyId;
            deleteSingleEntityDetails(inputFilterObject, companyHiringAggregates, function(error, destroy) {
              companyHiringAggregates.create(validationResponse, function(createErr, createOut) {
                companyHiringAggregatesWork.find({}, function(failerdErr, failedOutput) {
                  // console.log(failedOutput);
                  readErrorRecords(failedOutput, inputData, function(readError, readResponse) {
                    // console.log('createeeeeeeeeee errr', createErr);
                    var container = inputData.fileDetails.container;
                    var logInput = {
                      'totalNoRecs': fileInput.length,
                      'noFailRecs': cleanArray(failedOutput).length,
                      'noSuccessRecs': createOut.length,
                      'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + key,
                    };
                    var companyUploadLogModel = server.models.CompanyUploadLog;
                    companyUploadLogModel.updateAll({
                      'companyUploadLogId':
                        inputData.companyUploadLogId,
                    }, logInput, function(logError, logOutput) {
                      companyHiringAggregatesWork.destroyAll({
                        'companyUploadLogId':
                          inputData.companyUploadLogId,
                      }, function(destroyError, destroyOutput) {
                        callBc(null, logOutput);
                      });
                    });
                  });
                });
              });
            });
          }
        } else {
          commonErrorRead(undefined, function(err, output) {
            callBc(null, output);
          });
        }
      }
    });

    function validateInput(validateObj, validateCB) {
      // console.log('one by one object', validateObj);
      var eventTestScoreValidation = require('../commonValidation/company-hiring-aggregate-validation').validateJson;
      var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
      eventTestScoreValidation(validateObj, function(validationErr, validationResponse) {
        if (validationErr) {
          // validateObj.error = validationErr.err;
          // console.log('this i validate object', validateObj);
          // companyHiringAggregatesWork.create(validateObj, function(err, resp) {
          //   if (err) {
          // console.log('err occured', validationErr);
          validateObj.error = validationErr.err;
          errorArray.push(validateObj);
          validateCB(null, null);
          //   } else {
          //     validateCB(null, null);
          //   }
          // });
        } else {
          validationResponse.companyUploadLogId = inputData.companyUploadLogId;
          validateCB(null, validationResponse);
          // validateObj.error = 'Invalid Acedemic Year';
          // companyHiringAggregatesWork.create(validateObj, function(err, resp) {
          //   if (err) {
          //     errorArray.push(validateObj);
          //     validateCB(null, null);
          //   } else {
          //     validateCB(null, null);
          //   }
          // });
        }
      });
    }

    function readErrorRecords(finalArray, stDataa, cb) {
      var ErrorArray = [];
      var failedArray = [];
      var finalArray1 = [];
      failedArray = finalArray;
      // console.log('this is error array ', errorArray);
      if (errorArray.length > 0) {
        for (var j = 0; j < errorArray.length; j++) {
          // console.log();
          failedArray.push(errorArray[j]);
          // console.log(failedArray);
        }
      } else {
        failedArray = finalArray;
      }
      for (var i = 0; i < finalArray.length; i++) {
        var obj = {
          'rowNumber': failedArray[i].rowNumber,
          'campusName': failedArray[i].campusName,
          'organizationName': failedArray[i].organizationName,
          'jobRoleName': failedArray[i].jobRoleName,
          'error': failedArray[i].error,
          'noOfOffers': failedArray[i].noOfOffers,
          'sumOfOffers': failedArray[i].sumOfOffers,
          'minimumOffer': failedArray[i].minimumOffer,
          'maximumOffer': failedArray[i].maximumOffer,
          'calendarYear': failedArray[i].calendarYear,
        };
        finalArray1.push(obj);
      }
      var fs = require('fs');
      var csv = require('fast-csv');
      var name = stDataa.fileDetails.name;
      var container = stDataa.fileDetails.container;
      //   var pathForm = require('path');
      //   var errorCsv = './attachments/' + container + '/' + name;
      // // errorCsv = '../../../' + errorCsv;
      //   errorCsv = pathForm.join(__dirname, '../../../', errorCsv);
      // // console.log('errorCSV', errorCsv);
      //   var ws = fs.createWriteStream(errorCsv);
      //   csv
      //   .writeToPath(errorCsv, finalArray1, {
      //     headers: true,
      //   })
      //   .on('finish', function() {
      //     // console.log('done!');
      //   });
      //   cb(null, 'success');
      //   errorArray = [];
      var uploadCsv = require('../commonCampusFiles/commonCSVUpload.js').uploadCSV;
      var company = server.models.Company;
      company.findOne({ 'where': { 'companyId': inputData.companyId } }, function(companyErr, companyOut) {
        var fs = require('fs');
        var csv = require('fast-csv');
        var json2csv = require('json2csv');
        var fileName;
        fileName = companyOut.name;
        var randomNumber = Math.random().toString(20).slice(-10);
        var errorFile = companyOut.name + '_' + randomNumber + '.csv';
        fileName = './commonValidation/' + companyOut.name + '.csv';
        var fields = ['rowNumber', 'campusName', 'organizationName', 'jobRoleName', 'noOfOffers', 'sumOfOffers', 'minimumOffer', 'maximumOffer', 'calendarYear', 'error'];
        var csv = json2csv({
          data: finalArray1,
          fields: fields,
        });
        fs.writeFile(fileName, csv, function(err) {
          if (err) throw err;
          uploadCsv(fileName, container, errorFile, function(fileErr, fileOut) {
            key = fileOut;
            // var details = inputData.fileDetails;
            // var container = inputData.fileDetails.container;
            // var logInput = {
            //   'csvFileLocation': './attachments/' + container + '/' + details.name + '/' + details.name,
            //   'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + key,
            // };
            // var companyUploadLog = server.models.CompanyUploadLog;
            // var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;

            // companyUploadLog.updateAll({
            //   'companyUploadLogId':
            //     inputData.companyUploadLogId,
            // }, logInput, function(logErr, logOutput) {
            //   console.log('dddddddddddddddddddddddddddddd ', logErr, logOutput);
            //   companyHiringAggregatesWork.destroyAll({
            //     'companyUploadLogId':
            //       inputData.companyUploadLogId,
            //   }, function(error, destroy) {
            //     // cb(null, logOutput);
            cb(null, 'success');
            errorArray = [];

            //   });
            // });
          });
        });
      });
    }

    function commonErrorRead(responseCreated, cb) {
      var companyUploadLog = server.models.CompanyUploadLog;
      var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
      companyHiringAggregatesWork.find({}, function(workErr, workResponse) {
        console.log(workResponse);
        if (workErr) {
          companyHiringAggregatesWork.destroyAll({
            'companyUploadLogId':
              inputData.companyUploadLogId,
          }, function(error, destroy) {
            throwError(error, cb);
          });
        } else {
          var campusUploadLog = server.models.CampusUploadLog;
          var details = inputData.fileDetails;
          var container = inputData.fileDetails.container;
          readErrorRecords(workResponse, inputData, function(err, failedArray) {
            // console.log('resssssssssssssssssssssssss created', key);
            var pathForm = require('path');
            var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
            // csvFileLocation = pathForm.join(__dirname, '../../../', csvFileLocation);
            var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + key;
            // errorFileLocation = pathForm.join(__dirname, '../../../', errorFileLocation);
            var logInput = {
              'csvFileLocation': csvFileLocation,
              'errorFileLocation': errorFileLocation,
              'totalNoRecs': fileInput.length,
              'noFailRecs': workResponse.length,
              'noSuccessRecs': (responseCreated) ? cleanArray(responseCreated).length : 0,
            };
            companyUploadLog.updateAll({
              'companyUploadLogId':
                inputData.companyUploadLogId,
            }, logInput, function(logErr, logOutput) {
              // console.log('dddddddddddddddddddddddddddddd ', logErr, logOutput);
              companyHiringAggregatesWork.destroyAll({
                'companyUploadLogId':
                  inputData.companyUploadLogId,
              }, function(error, destroy) {
                cb(null, logOutput);
              });
            });
          });
        }
      });
    }

    function getlength(number) {
      return number.length;
    }

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
  });
}
exports.createCompanyHiringAggregate = createCompanyHiringAggregate;

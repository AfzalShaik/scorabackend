'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
var Joi = require('joi');
var campus = server.models.Campus;
var getCampus = campus.getCampus;
chai.use(chaiHttp);
describe('Unit Test Cases', function() {
  it('Get Campus Function', function(done) {
    var campusId = 113914986;
    getCampus(campusId, function(err, resp) {
      expect(resp.data[0].campusId).to.equal(113914986);

      done();
    });
  });
});

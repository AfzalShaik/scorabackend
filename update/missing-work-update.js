'use strict';
var server = require('../server/server');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var lookupMethods = require('../commonValidation/lookupMethods');
var findCampus = require('../common/models/STUDENT_MISSING_PROGRAM_WORK.js').findCampus;
var findDepartment = require('../common/models/STUDENT_MISSING_PROGRAM_WORK.js').findDepartment;
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var missingWork = server.models;
function updateMissingWork(input, cb) {
  if (input) {
    var inputData = input;
    var StudentMissingProgramWork = server.models.StudentMissingProgramWork;
    findCampus(data, function(campusErr, campusResp) {
      if (campusErr) {
        errorFunction('invalid campus id or university id', cb);
      } else if (campusResp == 'true') {
        findDepartment(data, function(departerr, departResp) {
          if (departerr) {
            errorFunction('invalid department id', cb);
          } else if (departResp == 'true') {
            console.log('in correct place only');
            var programTypeCode = 'PROGRAM_TYPE_CODE';
            var programClassCode = 'PROGRAM_CLASSIFICATION_CODE';
            var programCatCode = 'PROGRAM_CATEGORY_CODE';
            var programMajorCode = 'PROGRAM_MAJOR';
            var programCatVal, programTypeVal, programMajorVal, programClassVal;
            var lookupValueObj = {};
            var lookupValueObj1 = {};
            var lookupValueObj2 = {};
            var lookupValueObj3 = {};
            lookupValueObj.lookupValueId = data.programTypeValueId;
            lookupValueObj1.lookupValueId = data.programClassValueId;
            lookupValueObj2.lookupValueId = data.programCatValueId;
            lookupValueObj3.lookupValueId = data.programMajorValueId;
            lookupMethods.typeCodeFunction(lookupValueObj, programTypeCode, function(programTypeVal) {
              programTypeVal = programTypeVal;
              lookupMethods.typeCodeFunction(lookupValueObj1, programClassCode, function(programClassVal) {
                programClassVal = programClassVal;
                lookupMethods.typeCodeFunction(lookupValueObj2, programCatCode, function(programCatVal) {
                  programCatVal = programCatVal;
                  lookupMethods.typeCodeFunction(lookupValueObj3, programMajorCode, function(programMajorVal) {
                    programMajorVal = programMajorVal;
                    console.log('this is val', programTypeVal, 'cat', programCatVal, 'major', programMajorVal, 'this is class value', programClassVal);
                    if (programTypeVal == false || programClassVal == false || programCatVal == false || programMajorVal == false) {
                      errorFunction(' Invalid programTypeVal or programClassVal or programCatVal or programMajorVal', cb);
                      logger.error('Invalid programTypeVal or programClassVal or programCatVal or programMajorVal');
                    } else {
                      StudentMissingProgramWork.create(data, function(err, createResponse) {
                        cb(null, data);
                      });
                      findAndUpdate(inputData, function(error, output) {
                        if (error) {
                          throwError(JSON.stringify(error), cb);
                        } else {
                          cb(null, output);
                        }
                      });
                    }
                  });
                });
              });
            });
          } else {
            cb(null, data);
          }
        });
      } else {
        errorFunction('invalid Input Params', cb);
      }
    });
  } else {
    throwError('Invalid Input', cb);
  }
};
function findAndUpdate(input, cb) {
  var inputFilterObject = {};
  var StudentMissingProgramWork = server.models.StudentMissingProgramWork;
  inputFilterObject['studentId'] = input.studentId;
  findEntity(inputFilterObject, StudentMissingProgramWork, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response) {
      response.data.updateAttributes(input, function(err, info) {
        if (err) {
          throwError(err, cb);
        } else {
          logger.info('StudentAdditionalInfo record Updated Successfully');
          info['requestStatus'] = true;
          info['updateDatetime'] = new Date();
          cb(null, info);
        }
      });
    } else {
      throwError('Invalid academicsId', cb);
    }
  });
};
exports.updateMissingWork = updateMissingWork;

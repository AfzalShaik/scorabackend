var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validation = require("../commonValidation/addressValidation");
var updateAddress = require("./updateAddress");
var allmethods = require("./allmethods");
var Joi = require('joi');
 /**
   *updateCampus-function deals with updating the campus data
   *@constructor
   * @param {object} campusData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateCampus (campusData,cb){

    if(campusData){
      if(campusData.createDatetime || campusData.updateDatetime || campusData.createUserId || campusData.updateUserId){
        var error = new Error("You cannot perform update operation: ");
        error.statusCode = 422;
        error.requestStatus=false;
         cb(error, campusData);
      }
      else{
      
            if(campusData.campusId &&  campusData.addressId){

              //validating input json fields

          validation.validateJson(campusData,function(err,validVal){
                if(err){
                  cb(err, campusData);
                }else if(validVal==true){
                  var cityValue;
                  var stateValue;
                  var countryValue;
                  var typeCodeVal;
                   var CampusAddress = server.models.CampusAddress;
                   var Address = server.models.Address;
                   //addressTypeCodeFunction to verify addressTypeValueId
                     allmethods.addressTypeCodeFunction(campusData,function(typeCodeCheck){
                       typeCodeVal = typeCodeCheck;

                  //addressFunction function verifies city id , countryCode and stateCode
                    allmethods.addressFunction(campusData,function(cityCheck){
                     cityValue = cityCheck;
                      if(cityValue==false){
                        var error = new Error(" cityId and stateCode and countryCode required and should be valid");
                        error.statusCode = 422;
                        error.requestStatus=false;
                        cb(error, campusData);
                      }else{
                      if(typeCodeVal == true && cityValue==true){

                        CampusAddress.findOne({"where":{"and":[{"campusId": campusData.campusId},{"addressId": campusData.addressId}]}}, function (err, campusList) {
                          if(err){
                            //throws error

                             cb(err, campusData);
                          }else if (campusList){
                            //checking record in address table with addressId
                          updateAddress.updateAddress(campusData,function(error,info){
                            if(error){
                                cb(error, campusData);
                            }else{
                                cb(null, info);
                            }
                          })

                          }
                          else{
                            //throws error incase of invalid address_id or campusId
                            var error = new Error("Invalid address_id or campusId ");
                            logger.error("Invalid address_id or campusId");
                            error.statusCode = 422;
                            error.requestStatus=false;
                             cb(error, campusData);
                          }
                        })
                      }else{
                        //throws error in case of Invalid input
                        logger.error("Invalid addressTypeValueId");
                        var error = new Error("Invalid addressTypeValueId ");
                        error.statusCode = 422;
                        error.requestStatus=false;
                         cb(error, campusData);
                      }
                    }
                })
                 })
                }
                else{
                  var error = new Error("You cannot perform update operation: ");
                  error.statusCode = 422;
                  error.requestStatus=false;
                   cb(error, campusData);
                }
              })
            }
              else{
                var error = new Error("campusId and  addressId are required");
                error.statusCode = 422;
                error.requestStatus=false;
                 cb(error, campusData);

            }
      }
    }else{
      logger.error("Input can't be Null");
      var error = new Error("Input is Null: ");
        error.statusCode = 422;
        error.requestStatus=false;
         cb(error, campusData);
    }






}
exports.updateCampus = updateCampus;

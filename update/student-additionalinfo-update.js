'use strict';
var server = require('../server/server');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var additionalinfo = server.models.StudentAdditionalInfo;

/**
 * updateAcademics -- called by remote method which deals with updating the data in the database
 * @constructor
 * @param {object} input -holds the data which want to get updated
 * @param {function} cb -call back function which sends back the data to the called function
 */
function updateAcademics(input, cb) {
  if (input) {
    var inputData = input;
    findAndUpdate(inputData, function(error, output) {
      if (error) {
        throwError(JSON.stringify(error), cb);
      } else {
        cb(null, output);
      }
    });
  } else {
    throwError('Invalid Input', cb);
  }
};
function findAndUpdate(input, cb) {
  var inputFilterObject = {};
  inputFilterObject['academicsId'] = input.academicsId;
  findEntity(inputFilterObject, additionalinfo, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response) {
      response.data[0].updateAttributes(input, function(err, info) {
        if (err) {
          throwError(err, cb);
        } else {
          logger.info('StudentAdditionalInfo record Updated Successfully');
          info['requestStatus'] = true;
          info['updateDatetime'] = new Date();
          cb(null, info);
        }
      });
    } else {
      throwError('Invalid academicsId', cb);
    }
  });
};

/*'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var Joi = require('joi');

/**
 * updateAcademics - function called by updateStudentInfo
 * @constructor
 * @param {object} academicsData- contains data which need to get updated
 * @param {function} cb - acts as a callback function deals with response

function updateAcademics(academicsData, cb) {
  if (academicsData) {
    if (academicsData.createDatetime || academicsData.updateDatetime ||
      academicsData.createUserId || academicsData.updateUserId || academicsData.catcode) {
      var error = new Error('You cannot perform update operation: ');
      error.statusCode = 422;
      error.requestStatus = false;
      cb(error, academicsData);
    } else {
      if (academicsData.academicsId) {
        console.log('academicsId received');
        var schema = Joi.object().keys({
          academicsId: Joi.number().integer().max(99999999).required(),
          instituteName: Joi.string().max(100),
          url: Joi.string().max(200),
          programName: Joi.string().max(100),
          scoreGrade: Joi.number().max(100),
          comment: Joi.string().max(16),
          Duration: Joi.number().integer().max(2),
        });
        Joi.validate(academicsData, schema, function(err, value) {
          if (err) {
            cb(err, null);
          }
          else {
            console.log('inside else part');
            var academicsId, instituteName, url,
              programName, scoreGrade, comment, Duration;
            academicsCode(academicsData, function(err, res) {
              if (err) {
                var error = new Error('error while updating');
                error.statusCode = 422;
                error.requestStatus = false;
                cb(error, res);
              }
              else if (res == 0) {
                var error = new Error('Your response was null make sure with the provided details');
                error.statusCode = 422;
                error.requestStatus = false;
                cb(null, res);
               // callbackAssessmentFunction(null, res, null);
              }
              else {
                academicsData.requestStatus = true;
                //error.requestStatus = true;
                cb(null, res);
              }
            });
          }
        });
      } else {
        var error = new Error('You need to provide academicsId to update any operation: ');
        error.statusCode = 422;
        error.requestStatus = false;
        cb(error, academicsData);
      }
    }
  }
}
/**
 * academicsCode-function where updates in the database where done
 * @constructor
 * @param {object} academicsData-contains data which need to get updated
 * @param {function} callbackAcademicsFunction-callback function deals with response

function academicsCode(academicsData, callbackAcademicsFunction) {
  console.log('inside function part');
  var academicsValue = false;
  if (academicsData.academicsId) {
    var academicsModel = server.models.StudentAdditionalInfo;
    academicsModel.find({
      'where': {
        'academicsId': academicsData.academicsId,
      },
    }, function(err, res) {
      console.log('done with finding the AcademicId id');
      console.log(res);
      if (res == 0) {
        console.log('response was null\n');
        academicsValue = false;
        res.statusCode = 422;
        res.requestStatus = false;
        callbackAcademicsFunction(null, res);
      }
      else if (err) {
        console.log('there was an error');
        academicsValue = false;
        callbackAcademicsFunction(err, res);
      }
      else {
        academicsData['catcode'] = res[0].catcode;
        console.log(academicsData);
        console.log('came to update area');
        academicsModel.updateAll({academicsId: academicsData.academicsId}, academicsData, function(err, r) {
          console.log('going in function');
          console.log(r);
          if (err) {
            console.log('error if where err');
            callbackAcademicsFunction(err, null);
          }
          else {
            console.log('no error');
            console.log('data verified, final data below:\n');
            console.log(academicsData);
            academicsValue = true;
            callbackAcademicsFunction(null, academicsData);
          }
        }
          );
      }
    }
    );
  }
}
*/
exports.updateAcademics = updateAcademics;

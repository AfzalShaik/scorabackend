var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require("./allmethods");
var Joi = require('joi');
 /**
   *updateCompany-function deals with updating the company address
   *@constructor
   * @param {object} companyData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateCompany (companyData,cb){
  if(companyData){
    if(companyData.createDatetime || companyData.updateDatetime || companyData.createUserId || companyData.updateUserId){
      var error = new Error("You cannot perform update operation: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, companyData);
    }else{
        if(companyData.companyId &&  companyData.addressId){
          //validating input json fields
          var schema = Joi.object().keys({
            companyId: Joi.number().integer().max(999999999999999).required(),
            addressId: Joi.number().max(999999999999999).required(),
            addressTypeValueId:Joi.number().max(999999999999999),
            addressLine1:Joi.string().max(100),
            addressLine2:Joi.string().max(100),
            addressLine3:Joi.string().max(100),
            postalCode:Joi.string().max(16),
            stateCode:Joi.string().max(16),
            cityId: Joi.number().max(999999999999999),
            countryCode:Joi.string().max(16),
        });

        Joi.validate(companyData, schema, function (err, value) {
          if(err){
             cb(err, companyData);
          }else{
            var cityValue;
            var stateValue;
            var countryValue;
              var typeCodeVal;
             var CompanyAddress = server.models.CompanyAddress;
             var Address = server.models.Address;
             //addressTypeCodeFunction to verify addressTypeValueId
               allmethods.addressTypeCodeFunction(companyData,function(typeCodeCheck){
                 typeCodeVal = typeCodeCheck;

                 //addressFunction function verifies city id , countryCode and stateCode
                   allmethods.addressFunction(companyData,function(cityCheck){
                    cityValue = cityCheck;
                     if(cityValue==false){
                       var error = new Error(" cityId and stateCode and countryCode required and should be valid");
                       error.statusCode = 422;
                       error.requestStatus=false;
                       cb(error, companyData);
                     }else{
                if(cityValue==true && typeCodeVal==true ){

                  CompanyAddress.findOne({"where":{"and":[{"companyId": companyData.companyId},{"addressId": companyData.addressId}]}}, function (err, companyList) {
                    if(err){
                      //throws error
                       cb(err, companyData);
                    }else if (companyList){
                      //checking record in address table with addressId
                      Address.findOne({'where':{"addressId": companyData.addressId}}, function (err, AddressResp) {
                        //if record found it will the  updateAttributes
                        AddressResp.updateAttributes(companyData,function(err, info){

                          if(err){
                            cb(err,info)
                          }else{
                            //making requestStatus is true;
                            logger.info("Company Address Updated Successfully");
                            info["requestStatus"] = true;
                            info["updateDatetime"] = new Date();
                            cb(null, info);
                          }

                        })
                      })

                    }
                    else{
                      //throws error incase of invalid address_id or companyId
                      var error = new Error("Invalid address_id or companyId ");
                      logger.error("Invalid address_id or companyId");
                      error.statusCode = 422;
                      error.requestStatus=false;
                       cb(error, companyData);
                    }
                  })
                }else{
                  //throws error in case of Invalid input
                  logger.error("Invalid addressTypeValueId");
                  var error = new Error("Invalid addressTypeValueId ");
                  error.statusCode = 422;
                  error.requestStatus=false;
                   cb(error, companyData);
                }
              }

            })
              })
          }
        })
        }else{
          var error = new Error("companyId and  addressId are required");
          error.statusCode = 422;
          error.requestStatus=false;
           cb(error, companyData);
        }
    }
  }else{
    logger.error("Input can't be Null");
    var error = new Error("Input is Null: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, companyData);
  }

}
exports.updateCompany = updateCompany;

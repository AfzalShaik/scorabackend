var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require("./allmethods");
var Joi = require('joi');
 /**
   *updateCompanyContact-function deals with updating the company contact id
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateCompanyContact (contactData,cb){
  //validating the input
  if(contactData.createDatetime || contactData.updateDatetime || contactData.createUserId || contactData.updateUserId){
    var error = new Error("You cannot perform update operation: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, contactData);
  }else{
    //validating the input json
    var schema = Joi.object().keys({
        companyId: Joi.number().integer().max(999999999999999).required(),
        contactId: Joi.number().max(999999999999999).required(),
        contactTypeValueId:Joi.number().max(999999999999999),
        contactInfo:Joi.string().max(100),
    });
    Joi.validate(contactData, schema, function (err, value) {
      if(err){
        cb(err,contactData)
      }else{
        var typeCodeValue;
         var CompanyContact = server.models.CompanyContact;
          var Contact = server.models.Contact;
          //checking record with companyId and contactId
          allmethods.contactTypeCodeFunction(contactData,function(typeCodeCheck){
            typeCodeValue = typeCodeCheck;
            if(typeCodeValue==false){
              var error = new Error("contactTypeValueId is not valid");
              error.statusCode = 422;
              error.requestStatus=false;
              cb(error, contactData);
            } else{
               CompanyContact.findOne({'where':{"and":[{"companyId": contactData.companyId},{"contactId": contactData.contactId}]}}, function (err, companyList) {
                 if(err){
                    cb(err, contactData);
                 }else if (companyList){
                   //checking record with contactId in contact table
                     Contact.findOne({'where':{"contactId": contactData.contactId}}, function (err, CompanyResp) {
                       //update the company table
                       CompanyResp.updateAttributes(contactData,function(err, infoCompany){
                         if(err){
                           cb(err,infoCompany)
                         }else{
                           //if success requestStatus will becomes true
                           infoCompany["requestStatus"] = true;
                           infoCompany["updateDatetime"] = new Date();
                           cb(null, infoCompany);
                         }

                       })
                     })

                 }
                 else{
                   //throws error when invalid contatcId or companyId
                   var error = new Error("Invalid contactId or companyId ");
                   error.statusCode = 422;
                   error.requestStatus=false;
                    cb(error, contactData);
                 }
               })
             }
           })
      }
    })
  }

}
exports.updateCompanyContact = updateCompanyContact;

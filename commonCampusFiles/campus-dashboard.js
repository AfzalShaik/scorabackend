'use strict';
var server = require('../server/server');
var async = require('async');
var selectedStudents = [];
var selectedStudentsCountArray = [];
var shortListedCountArray = [];
var departmentSelectedStudents = [];
var totalComp = 0;
var totalSelectedStudents = 0;
var allStudentsOfDepartment = 0;
var totalSelected = 0;
var maxComp = 0;
var totalStudentsFinalYear = 0;
var totalStudentsInvolvedInEvents = [];
var depComp = 0;
var campusDashboardData = function(campusId, cb) {
  selectedStudents = [];
  depComp = 0;
  totalSelectedStudents = 0;
  selectedStudentsCountArray = [];
  shortListedCountArray = [];
  departmentSelectedStudents = [];
  totalComp = 0;
  totalStudentsFinalYear = 0;
  allStudentsOfDepartment = 0;
  totalSelected = 0;
  totalStudentsInvolvedInEvents = [];
  var department = server.models.Department;
  department.find({'where': {'campusId': campusId}},
  function(err, departmentsOfCampus) {
    if (err) {
      cb(err, null);
    } else {
      var endDate = new Date();
      var startDate = new Date();
      var currentYear = new Date().getFullYear();
      var month = new Date().getMonth() + 1;
      if (month >= 6) {
        startDate.setFullYear((currentYear), 5, 1);
        endDate.setFullYear((currentYear + 1), 4, 31);
      } else if (month < 6) {
        startDate.setFullYear((currentYear - 1), 5, 1);
        endDate.setFullYear((currentYear), 4, 31);
      }
      async.map(departmentsOfCampus, getCountsForDepartmentData,
            function(err, departmentResponse) {
              if (err) {
                cb(err, null);
              } else {
                var totalVisited = 'select count(distinct company_id) visited from CAMPUS_EVENT where scheduled_date < ' + JSON.stringify(new Date()) + ' and ' +
                ' campus_id = ' + campusId + ' and  (event_status_value_id = 244 or event_status_value_id = 239) ' +
                ' and update_datetime between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + '';
                MySql(totalVisited, function(er, countResponse) {
                  if (er) {
                    cb(er, null);
                  } else {
                    var data = {};
                    data.graphsDataForGraph = departmentSelectedStudents;
                    data.totalStudentsFinalYear = totalStudentsFinalYear;
                    data.totalSelected = totalSelected;
                    data.averageCompForCampus = totalComp / departmentSelectedStudents.length;
                    data.maxComp = maxComp;
                    data.visited = countResponse[0].visited;
                    departmentSelectedStudents = [];
                    totalComp = 0;
                    maxComp = 0;
                    totalSelected = 0;
                    totalStudentsFinalYear = 0;
                    cb(null, data);
                  }
                });
              }
            });
    }
  });
};
function getCountsForDepartmentData(departmentObject, callback) {
  selectedStudents = [];
  selectedStudentsCountArray = [];
  shortListedCountArray = [];
  departmentSelectedStudents = [];
  totalComp = 0;
  totalStudentsFinalYear = 0;
  allStudentsOfDepartment = 0;
  totalSelected = 0;
  totalStudentsInvolvedInEvents = [];
  var campusDrive = server.models.CampusDrive;
  var endDate = new Date();
  var startDate = new Date();
  var currentYear = new Date().getFullYear();
  var month = new Date().getMonth() + 1;
  if (month >= 6) {
    startDate.setFullYear((currentYear), 5, 1);
    endDate.setFullYear((currentYear + 1), 4, 31);
  } else if (month < 6) {
    startDate.setFullYear((currentYear - 1), 5, 1);
    endDate.setFullYear((currentYear), 4, 31);
  }
  allStudentsOfDepartment = 0;
  campusDrive.find({'where': {'departmentId': departmentObject.departmentId,
      'createDateTime':
      {
        between: [startDate, endDate],
      }}},
      function(err, driveResponse) {
        if (err) {
          callback(err, null);
        } else {
          async.map(driveResponse, getEventsOfCampus, function(err, response) {
            if (err) {
              callback(err, null);
            } else {
              getStudentsCounts(departmentObject, function(err, responseData) {
                if (err) {

                } else {
                  callback(null, 'done');
                }
              });
            }
          });
        }
      });
}
function getStudentsCounts(departmentObject, callback) {
  async.map(selectedStudents, getTotalComp,
    function(err, averageComp) {
      if (err) {
        callback(err, null);
      } else {
        var program = server.models.Program;
        program.find({'where': {'departmentId': departmentObject.departmentId}},
    function(err, allPrograms) {
      if (err) {
        callback(err, null);
      } else {
        // console.log('these area all programs', allPrograms);
        async.map(allPrograms, getAllStudents, function(err, allStudents) {
          if (err) {
            callback(err, null);
          } else {
            // console.log('total involved', totalStudentsInvolvedInEvents,
            // shortListedCountArray, selectedStudentsCountArray);
            // console.log('depppppppppppp obj', departmentObject);
            var data = {};
            data.departmentName = departmentObject.shortName;
            data.selected = selectedStudentsCountArray.length;
            data.shortListed = shortListedCountArray.length;
            data.totalStudentsOfDepartment = allStudentsOfDepartment;
            data.totalStudentsInvolvedInEvents = totalStudentsInvolvedInEvents.length;
            data.unAttendedStudents = allStudentsOfDepartment - (totalStudentsInvolvedInEvents.length);
            data.averageComp = (depComp / (selectedStudents.length));
            console.log('final data', selectedStudentsCountArray.length);
            departmentSelectedStudents.push(data);
            totalSelected = selectedStudentsCountArray.length + totalSelected;
            totalStudentsFinalYear = allStudentsOfDepartment + totalStudentsFinalYear;
            allStudentsOfDepartment = 0;
            selectedStudents = [];
            selectedStudentsCountArray = [];
            shortListedCountArray = [];
            depComp = 0;
            totalStudentsInvolvedInEvents = [];
            if (data !== {}) { callback(null, data); }
          }
        });
      }
    });
      }
    });
}
function getAllStudents(obj, studentCallback) {
  // console.log('obbbbbbbbbbbbbbbbb', obj);
  var endDate = new Date();
  var startDate = new Date();
  var currentYear = new Date().getFullYear();
  var month = new Date().getMonth() + 1;
  if (month >= 6) {
    startDate.setFullYear((currentYear), 5, 1);
    endDate.setFullYear((currentYear + 1), 4, 31);
  } else if (month < 6) {
    startDate.setFullYear((currentYear - 1), 5, 1);
    endDate.setFullYear((currentYear), 4, 31);
  }
  var enroll = server.models.Enrollment;
  enroll.find({'where': {'programId': obj.programId,
  'planedCompletionDate':
  {
    between: [startDate, endDate],
  }}},
    function(err, programResponse) {
      if (err) {
        studentCallback(err, null);
      } else {
        allStudentsOfDepartment = programResponse.length + allStudentsOfDepartment;
        // console.log('all', allStudentsOfDepartment);
        studentCallback(null, 'done');
      }
    });
}
function getEventsOfCampus(campusDriveObject, callBc) {
  console.log('drive object', campusDriveObject);
  var campusEvent = server.models.CampusEvent;
  var endDate = new Date();
  var startDate = new Date();
  var currentYear = new Date().getFullYear();
  var month = new Date().getMonth() + 1;
  if (month >= 6) {
    startDate.setFullYear((currentYear), 5, 1);
    endDate.setFullYear((currentYear + 1), 4, 31);
  } else if (month < 6) {
    startDate.setFullYear((currentYear - 1), 5, 1);
    endDate.setFullYear((currentYear), 4, 31);
  }
  campusEvent.find({'where': {'campusDriveId': campusDriveObject.campusDriveId,
  'createDateTime':
  {
    between: [startDate, endDate],
  }}},
    function(err, campusEventResponse) {
      if (err) {
        callBc(err, null);
      } else {
        console.log('campus event response', campusEventResponse);
        async.map(campusEventResponse, allEventsOfCampus,
            function(err, eventResponse) {
              if (err) {
                callBc(err, null);
              } else {
                callBc(null, eventResponse);
              }
            });
      }
    });
}
function allEventsOfCampus(eventResponseData, callBack) {
  // console.log('all events', eventResponseData);
  var eventStudentList = server.models.EventStudentList;
  eventStudentList.find({'where': {'campusEventId': eventResponseData.campusEventId},
  'include': 'eventStudentListIbfk7rel'},
  function(err, response) {
    if (err) {
      callBack(err, null);
    } else {
      async.map(response, getStudentCounts, function(err, studentResponse) {
        if (err) {
          callBack(err, null);
        } else {
          callBack(null, studentResponse);
        }
      });
    }
  });
  function getStudentCounts(eventStudentObject, eventStudentCallBack) {
    // console.log('these are arrayssss', selectedStudentsCountArray, shortListedCountArray,
    // totalStudentsInvolvedInEvents,
    //               selectedStudentsCountArray.length, shortListedCountArray.length,
    //               totalStudentsInvolvedInEvents.length);
    // console.log('student list object', eventStudentObject);
    if (eventStudentObject.candidateStatusValueId == 381) {
      selectedStudents.push(eventStudentObject);
      if (!(selectedStudentsCountArray.includes(parseInt(eventStudentObject.studentId)))) {
        selectedStudentsCountArray.push(parseInt(eventStudentObject.studentId));
        if ((shortListedCountArray.includes(parseInt(eventStudentObject.studentId)))) {
          var index = shortListedCountArray.indexOf(parseInt(eventStudentObject.studentId));
          shortListedCountArray.splice(index, 1);
          eventStudentCallBack(null, 'done');
        } else {
          totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject.studentId));
          eventStudentCallBack(null, 'done');
        }
      } else {
        eventStudentCallBack(null, 'done');
      }
    } else if (eventStudentObject.updateDatetime > eventResponseData.scheduledDate) {
      if (!(selectedStudentsCountArray.includes(parseInt(eventStudentObject.studentId))) &&
        !(shortListedCountArray.includes(parseInt(eventStudentObject.studentId)))) {
        shortListedCountArray.push(parseInt(eventStudentObject.studentId));
        if (!(totalStudentsInvolvedInEvents.includes(parseInt(eventStudentObject.studentId)))) {
          totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject.studentId));
          eventStudentCallBack(null, 'done');
        } else {
          eventStudentCallBack(null, 'done');
        }
      } else {
        eventStudentCallBack(null, 'done');
      }
    } else {
      if (!(totalStudentsInvolvedInEvents.includes(parseInt(eventStudentObject.studentId)))) {
        totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject.studentId));
        eventStudentCallBack(null, 'done');
      } else {
        eventStudentCallBack(null, 'done');
      }
    }
  }
}
function getTotalComp(studentObject, compCallBack) {
  var eventStudentData = studentObject.toJSON();
  if (studentObject.compPackageId !== null) {
    depComp = Number((eventStudentData.eventStudentListIbfk7rel.totalCompPkgValue) ?
    eventStudentData.eventStudentListIbfk7rel.totalCompPkgValue : 0
    ) + depComp;
    totalComp = Number((eventStudentData.eventStudentListIbfk7rel.totalCompPkgValue) ?
    eventStudentData.eventStudentListIbfk7rel.totalCompPkgValue : 0
      ) + totalComp;
    maxComp = Math.max(maxComp, Number((eventStudentData.eventStudentListIbfk7rel.totalCompPkgValue)
      ? eventStudentData.eventStudentListIbfk7rel.totalCompPkgValue : 0));
    compCallBack(null, 'done');
  } else {
    depComp = Number((studentObject.totalCompValue) ?
    studentObject.totalCompValue : 0
    ) + depComp;
    totalComp = Number((studentObject.totalCompValue) ? studentObject.totalCompValue : 0) + totalComp;
    maxComp = Math.max(maxComp, Number((studentObject.totalCompValue) ?
    studentObject.totalCompValue : 0));
    compCallBack(null, 'done');
  }
}
function cleanArray(array, cb) {
  var sampleArray = [];
  for (var i = 0; i < array.length; i++) {
    if (array[i] !== [] || array[i] !== 0 || array[i] !== null) {
      sampleArray.push(array[i]);
    }
    if (i == array.length - 1) {
      cb(null, sampleArray);
    }
  }
}
var async = require('async');
function campusDashboard(campusId, cb) {
  selectedStudents = [];
  selectedStudentsCountArray = [];
  shortListedCountArray = [];
  departmentSelectedStudents = [];
  totalComp = 0;
  totalSelectedStudents = 0;
  totalStudentsFinalYear = 0;
  totalSelected = 0;
  maxComp = 0;
  totalStudentsInvolvedInEvents = [];
  var department = server.models.Department;
  department.find({'where': {'campusId': campusId}}, function(err, responseOfDepartments) {
    if (err) {
      cb(err, null);
    } else {
      async.map(responseOfDepartments, getDepartmentCounts, function(err, responseOfCounts) {
        if (err) {
          cb(err, null);
        } else {
          var endDate = new Date();
          var startDate = new Date();
          var currentYear = new Date().getFullYear();
          var month = new Date().getMonth() + 1;
          if (month >= 6) {
            startDate.setFullYear((currentYear), 5, 1);
            endDate.setFullYear((currentYear + 1), 4, 31);
          } else if (month < 6) {
            startDate.setFullYear((currentYear - 1), 5, 1);
            endDate.setFullYear((currentYear), 4, 31);
          }
          var totalVisited = 'select count(distinct company_id) visited from CAMPUS_EVENT where scheduled_date <= ' + JSON.stringify(new Date()) + ' and ' +
          ' campus_id = ' + campusId + ' and  (event_status_value_id = 244 or event_status_value_id = 239) ' +
          ' and update_datetime between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + '';
          MySql(totalVisited, function(er, countResponse) {
            if (er) {
              cb(er, null);
            } else {
              var data = {};
              data.graphsDataForGraph = departmentSelectedStudents;
              data.totalStudentsFinalYear = totalStudentsFinalYear;
              data.totalSelected = totalSelected;
              data.averageCompForCampus = Math.floor(totalComp / totalSelectedStudents);
              data.maxComp = maxComp;
              data.visited = countResponse[0].visited;
              departmentSelectedStudents = [];
              totalComp = 0;
              maxComp = 0;
              totalSelected = 0;
              selectedStudents = [];
              totalStudentsFinalYear = 0;
              cb(null, data);
            }
          });
        }
      });
    }
  });
}
function getDepartmentCounts(object, callBack) {
  var endDate = new Date();
  var startDate = new Date();
  var currentYear = new Date().getFullYear();
  var month = new Date().getMonth() + 1;
  if (month >= 6) {
    startDate.setFullYear((currentYear), 5, 1);
    endDate.setFullYear((currentYear + 1), 4, 31);
  } else if (month < 6) {
    startDate.setFullYear((currentYear - 1), 5, 1);
    endDate.setFullYear((currentYear), 4, 31);
  }
  var totalStudents = 'select count(distinct student_id) departmentStudents from ENROLLMENT en, PROGRAM pr where ' +
  ' pr.department_id = ' + object.departmentId + ' and en.program_id = pr.program_id and en.planed_completion_date ' +
  ' between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + '';
  MySql(totalStudents, function(err, responseOfStudents) {
    if (err) {
      callBack(err, null);
    } else {
      var totalDepComp = 'select cast(sum(comp.total_comp_pkg_value) as unsigned) totalDepComp , ' +
      ' cast(max(comp.total_comp_pkg_value) as unsigned) maxComp ' +
      ' from COMPENSATION_PACKAGE comp, EVENT_STUDENT_LIST evst, ' +
      ' CAMPUS_EVENT ce where ' +
      ' evst.comp_package_id = comp.comp_package_id and ' +
      ' evst.campus_event_id = ce.campus_event_id and ' +
      ' evst.department_id = ' + object.departmentId + ' and ' +
      ' evst.candidate_status_value_id = 381 and ' +
      ' evst.update_datetime between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + '';
      MySql(totalDepComp, function(err, depCom) {
        if (err) {
          callBack(err, null);
        } else {
          var unregComp = 'select cast(sum(evst.total_comp_value) as unsigned) totalDepComp , ' +
          ' cast(max(evst.total_comp_value) as unsigned) maxComp ' +
          ' from EVENT_STUDENT_LIST evst, ' +
          ' CAMPUS_EVENT ce where ' +
          ' evst.campus_event_id = ce.campus_event_id and ' +
          ' evst.department_id = ' + object.departmentId + ' and ' +
          ' evst.candidate_status_value_id = 381 and ' +
          ' evst.update_datetime between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + '';
          MySql(unregComp, function(err, unregComp) {
            if (err) {
              callBack(err, null);
            } else {
              var eventStudentListCount = 'select evst.*,  ce.scheduled_date from EVENT_STUDENT_LIST evst, ' +
              ' CAMPUS_EVENT ce where evst.campus_event_id = ce.campus_event_id ' +
              ' and ' +
              ' evst.department_id = ' + object.departmentId + ' and evst.update_datetime between ' +
              ' ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + ' ';
              MySql(eventStudentListCount, function(err, eventStudentResponse) {
                if (err) {
                  callBack(err, null);
                } else {
                  async.map(eventStudentResponse, getStudentCounts, function(err, studentDetailsResponse) {
                    if (err) {
                      callBack(err, null);
                    } else {
                      var unregDepComp = (unregComp[0].totalDepComp === null) ? 0 : unregComp[0].totalDepComp;
                      var unregMaxComp = unregComp[0].maxComp;
                      var unregAndRegComp = Math.max(Number(depCom[0].maxComp) ? depCom[0].maxComp : 0,
                      Number(unregComp[0].maxComp) ? unregComp[0].maxComp : 0);
                      var unregAndRegTotalComp = Number((depCom[0].totalDepComp) ? depCom[0].totalDepComp : 0) +
                      Number((unregDepComp) ? unregDepComp : 0);
                      var data = {};
                      data.departmentName = object.shortName;
                      data.selected = selectedStudentsCountArray.length;
                      data.shortListed = shortListedCountArray.length;
                      data.totalStudentsOfDepartment = responseOfStudents[0].departmentStudents;
                      data.totalStudentsInvolvedInEvents = totalStudentsInvolvedInEvents.length;
                      data.unAttendedStudents = responseOfStudents[0].departmentStudents - (totalStudentsInvolvedInEvents.length);
                      data.averageComp = (unregAndRegTotalComp === 0) ? 0 : Math.floor(unregAndRegTotalComp / (selectedStudents.length));
                      departmentSelectedStudents.push(data);
                      maxComp = Math.max(maxComp, unregAndRegComp);
                      totalSelectedStudents = selectedStudents.length + totalSelectedStudents;
                      totalComp = unregAndRegTotalComp + totalComp;
                      console.log('tt', data, totalComp, data.averageComp === null, data.averageComp === NaN, data.averageComp === undefined);
                      totalSelected = selectedStudentsCountArray.length + totalSelected;
                      totalStudentsFinalYear = responseOfStudents[0].departmentStudents + totalStudentsFinalYear;
                      selectedStudentsCountArray = [];
                      selectedStudents = [];
                      shortListedCountArray = [];
                      totalStudentsInvolvedInEvents = [];
                      callBack(null, 'done');
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
function getStudentCounts(eventStudentObject, eventStudentCallBack) {
  if (eventStudentObject['candidate_status_value_id'] == 381) {
    selectedStudents.push(eventStudentObject);
    if (!(selectedStudentsCountArray.includes(parseInt(eventStudentObject['student_id'])))) {
      selectedStudentsCountArray.push(parseInt(eventStudentObject['student_id']));
      if ((shortListedCountArray.includes(parseInt(eventStudentObject['student_id'])))) {
        var index = shortListedCountArray.indexOf(parseInt(eventStudentObject['student_id']));
        shortListedCountArray.splice(index, 1);
        eventStudentCallBack(null, 'done');
      } else {
        totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject['student_id']));
        eventStudentCallBack(null, 'done');
      }
    } else {
      eventStudentCallBack(null, 'done');
    }
  } else if (eventStudentObject['update_datetime'] > eventStudentObject['scheduled_date']) {
    if (!(selectedStudentsCountArray.includes(parseInt(eventStudentObject['student_id']))) &&
      !(shortListedCountArray.includes(parseInt(eventStudentObject['student_id'])))) {
      shortListedCountArray.push(parseInt(eventStudentObject['student_id']));
      if (!(totalStudentsInvolvedInEvents.includes(parseInt(eventStudentObject['student_id'])))) {
        totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject['student_id']));
        eventStudentCallBack(null, 'done');
      } else {
        eventStudentCallBack(null, 'done');
      }
    } else {
      eventStudentCallBack(null, 'done');
    }
  } else {
    if (!(totalStudentsInvolvedInEvents.includes(parseInt(eventStudentObject['student_id'])))) {
      totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject['student_id']));
      eventStudentCallBack(null, 'done');
    } else {
      eventStudentCallBack(null, 'done');
    }
  }
}
function MySql(statment, sqlCb) {
  var app = require('../server/server.js');
  var connector = app.dataSources.ScoraXChangeDB.connector;
  connector.execute(statment, undefined, function(err, result) {
    if (err) {
      sqlCb(null, null);
    } else {
      sqlCb(null, result);
    }
  });
}
exports.campusDashboard = campusDashboard;
exports.campusDashboardData = campusDashboardData;

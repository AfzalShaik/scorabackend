'use strict';
var departmentJson = require('../commonValidation/all-models-json.js');
var departmentValidation = require('../commonValidation/all-models-validation');
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var department = server.models.Department;
var validValue;
  /**
   *updateDepartmentProfile-function deals with updating department profile data
   *@constructor
   * @param {object} departmentdata - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateDepartmentProfile(departmentdata, cb) {
  if (departmentdata) {
    if (departmentdata.campusId && departmentdata.departmentId) {
      departmentValidation.validateModelsJson(departmentdata, departmentJson, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
        } else {
          //checking record for provided input
          department.findOne({
            'where': {
              'and': [{
                'campusId': departmentdata.campusId,
              }, {
                'departmentId': departmentdata.departmentId,
              }],
            },
          }, function(err, departmentList) {
            if (err) {
              //throws error
              cb(err, departmentList);
            } else if (departmentList) {
              //if record found it will the  updateAttributes
              departmentList.updateAttributes(departmentdata, function(err, info) {
                if (err) {
                  cb(err, info);
                } else {
                  //making requestStatus is true;
                  logger.info('Campus Department Updated Successfully');
                  info['requestStatus'] = true;
                  info['updateDatetime'] = new Date();
                  cb(null, info);
                }
              });
            } else {
              //throws error incase of invalid address_id or campusId
              var error = new Error('Invalid departmentId or campusId');
              logger.error('Invalid departmentId or campusId');
              error.statusCode = 422;
              error.requestStatus = false;
              cb(error, departmentdata);
            }
          });
        }
      });
    } else {
      //throws error if input is not valid
      throwError('CampusId and Department are required ', cb);
      logger.error('CampusId and Department are required:');
    }
  } else {
    //throws error if input is null
    throwError('Input Cannot be Blank: ', cb);
    logger.error("Input Can't be Null");
  }
};
exports.updateDepartmentProfile = updateDepartmentProfile;

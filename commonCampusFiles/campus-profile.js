
'use strict';
var server = require('../server/server');
var errorFunction = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var getLookups = require('./get-campus-dashboard').getLookups;
var campus = server.models.Campus;
var enrolledStudents = 0;
var placed = 0;
var offerd = [];
var totalEnrollStudents = [];
var finayYearStudents = 0;
var selectedPeople = [];
var totalOfferedJobs = 0;
var compvisted = 0;
var fnlYr = [];
function getCampusProfile(campusId, callBc) {
  campus.findOne({'where': {'campusId': campusId}}, function(err, campusResponse) {
    if (err) {
      callBc(err, null);
    } else if (campusResponse == 0) {
      errorFunction('cant find the campus details', callBc);
    } else {
      getDepartment(campusResponse, campusId, function(err, depRes) {
        if (err) {
          errorFunction('err', callBc);
        } else {
          var departmentId = false;
          if (departmentId) {
            getDepartmentDetails(campusId, departmentId, function(err, responseFinal) {
              if (err) {
                errorFunction('err occured', callBc);
              } else {
                depRes.totalSelectedPeople = totalOfferedJobs;
                depRes.selectedPeople = selectedPeople;
                callBc(null, depRes);
              }
            });
          } else {
            callBc(null, depRes);
          }
        }
      });
    }
  });
}
function getDepartment(campusResponse, campusId, depCallback) {
  var dep = server.models.Department;
  dep.find({'where': {'campusId': campusId}}, function(error1, depRes) {
    if (error1) {
      callBc(error1, null);
    } else {
      getUniversity(campusResponse, depRes, campusId, function(err, univRes) {
        if (err) {
          errorFunction('err', depCallback);
        } else {
         // console.log(depRes);
          depCallback(null, univRes);
        }
      });
    }
  });
}
function getUniversity(campusResponse, depRes, campusId, univCallback) {
  var university = server.models.University;
  university.findOne({'where': {'universityId': campusResponse.universityId}},
  function(error, universityResponse) {
    if (error) {
      callBc(error, null);
    } else {
      getLookUp(campusResponse, depRes, universityResponse, campusId,
            function(err, lookupResponse) {
              if (err) {
                errorFunction('err', univCallback);
              } else {
                univCallback(null, lookupResponse);
              }
            });
    }
  });
}
function getLookUp(campusResponse, depRes, universityResponse, campusId, lookupCb) {
  lookup('UNIVERSITY_TYPE_CODE', universityResponse.universityTypeValueId,
    function(er, lookUpRes) {
      if (er) {
        callBc(er, null);
      } else {
        getContact(campusResponse, depRes, universityResponse, campusId, lookUpRes,
            function(err, contactRes) {
              if (err) {
                errorFunction('err', lookupCb);
              } else {
                lookupCb(null, contactRes);
              }
            });
      }
    });
}
function getContact(campusResponse, depRes, universityResponse,
                    campusId,
                    lookUpRes,
                    contactCallback) {
  var campusContact = server.models.CampusContact;
  campusContact.findOne({'where': {'campusId': campusId, 'primaryInd': 'Y'}},
    function(error2, contactResponse) {
      if (error2) {
        contactCallback(error2, null);
      } else if (contactResponse == null) {
        var contactInfo = [];
        getTotalStudentsEnrolled(campusResponse, campusId,
          contactInfo,
          depRes,
          universityResponse, lookUpRes,
          function(err, enrollmentRes) {
            if (err) {
              errorFunction('err', contactCallback);
            } else {
              contactCallback(null, enrollmentRes);
            }
          });
      } else {
        var contact = server.models.Contact;
        contact.findOne({'where': {'contactId': contactResponse.contactId}},
        function(error3, contactInfo) {
          if (error3) {
            contactCallback(error3, null);
          } else {
            getTotalStudentsEnrolled(campusResponse, campusId,
                contactInfo,
                depRes,
                universityResponse, lookUpRes,
                function(err, enrollmentRes) {
                  if (err) {
                    errorFunction('err', contactCallback);
                  } else {
                    contactCallback(null, enrollmentRes);
                  }
                });
          }
        });
      };
    });
}
function getTotalStudentsEnrolled(campusResponse, campusId,
                                    contactInfo,
                                    depRes,
                                    universityResponse, lookUpRes,
                                    enrollcb) {
  var enroll = server.models.Enrollment;
  enroll.find({'where': {'campusId': campusId}}, function(err, enrollRes) {
    if (err) {
      errorFunction('err', enrollcb);
    } else {
      var async = require('async');
      enrollRes.filter(function(ob) {
        var date = new Date().getFullYear();
        var date1 = JSON.stringify(new Date());
        var compYear = date1.substr(6, 2);
        var final = ob.createDatetime;
        var parse = JSON.stringify(final);
        var p = parse.substr(1, 4);
        if (p == date) { enrolledStudents++; }
        var finalYear = JSON.stringify(ob.actualCompletionDate);
        var finalYr = finalYear.substr(1, 4);
        if (compYear < 6) { if (finalYr == date) { finayYearStudents++; } }
        if (compYear >= 6) { if (finalYr == date + 1) { finayYearStudents++; } }
      });
      // numberOfStudentsPlaced(campusResponse, campusId, contactInfo, depRes,
      //                   universityResponse,
      //                   lookUpRes,
      //                   enrollRes,
      //                   function(err, placedRes) {
      //                     if (err) {
      //                       errorFunction('err', enrollcb);
      //                     } else {
      //                       var campEvent = server.models.CampusEvent;
      //                       campEvent.find({'where': {'campusId': campusId}},
      //                       function(err3, evntRes) {
      //                         if (err3) {
      //                           errorFunction('err', enrollcb);
      //                         } else {
      //                           evntRes.filter(function(data) {
      //                             compvisted++;
      //                           });
      var final = createObject(campusResponse, contactInfo, depRes, universityResponse, lookUpRes);
      enrollcb(null, final);
                        //       }
                        //     });
                        //   }
                        // });
    }
  });
}
function numberOfStudentsPlaced(campusResponse, campusId, contactInfo, depRes,
                                universityResponse,
                                lookUpRes,
                                enrollRes, placedCb) {
  var eventStudent = server.models.EventStudentList;
  eventStudent.find({'where': {'campusId': campusId}}, function(err, placedRes) {
    if (err) {
      errorFunction('err', placedCb);
    } else {
      offerd = [];
      var async = require('async');
      async.map(placedRes, getPlacedCount, function(err, res) {
        if (err) {
          errorFunction('err', placedCb);
        } else {
          placedCb(null, 'done');
        }
      });
    }
  });
}
function getPlacedCount(obj, cb) {
  var lookup = server.models.LookupType;
  var lookupvalue = server.models.LookupValue;
  lookup.findOne({'where': {'lookupCode': 'CANDIDATE_STATUS_TYPE'}}, function(err, res) {
    lookupvalue.findOne({'where': {'lookupValue': 'Offered', 'lookupTypeId': res.lookupTypeId}},
    function(err1, res1) {
      var async = require('async');
      // console.log(res1);
      //   console.log(obj.candidateStatusValueId);
      if (obj.candidateStatusValueId == res1.lookupValueId) {
        var comp = server.models.CompensationPackage;
        comp.findOne({'where': {'companyId': obj.companyId, 'compPackageId': obj.compPackageId}}, function(err, compRes) {
           // console.log(compRes);
          if (err) {
            errorFunction(err, placedCb);
          } else if (compRes == null) {
            cb(null, 'done');
          } else {
             // console.log('hear');
            offerd.push(compRes.totalCompPkgValue);
            cb(null, 'done');
          }
        });
        placed++;
      }
    });
  });
}
function createObject(campus, contact, dep, university, lookup) {
  var data = {};
  data.campusName = campus.name;
  data.brandingImage = campus.brandingImage;
  data.facebook = campus.facebook;
  data.twitter = campus.twitter;
  data.youtube = campus.youtube;
  data.linkedin = campus.linkedin;
  data.logo = campus.logo;
  data.description = campus.description;
  data.DepartmentList = dep;
  data.UniversityName = university.name;
  data.establishedDate = campus.establishedDate;
  data.indiaRank = campus.rank;
  data.Tier = university.tier;
  data.rating = campus.rating;
  data.missionStatement = campus.missionStatement;
  data.website = campus.website;
  data.contact = contact;
  data.totalStudentsEnrolled = enrolledStudents;
  // data.placedStudents = placed;
  // if (offerd.length > 0) {
  //   data.maxOffered = offerd.reduce(function(a, b) { return Math.max(a, b); });
  // } else {
  //   data.maxOffered = [];
  // }
  data.finalYearStudents = finayYearStudents;
  // data.totalCompaniesVisited = compvisted;
  return data;
};
function lookup(type, ob, lookupCallback) {
  var lookup = server.models.LookupType;
  var lookupvalue = server.models.LookupValue;
      //var type = 'SKILL_TYPE_CODE';
  lookup.find({
    'where': {
      lookupCode: type,
    },
  }, function(err, re) {
    if (err) {
      lookupCallback(err, null);
    } else {
      lookupvalue.findOne({
        'where': {
          lookupValueId: ob,
          lookupTypeId: re[0].lookupTypeId,
        },
      }, function(err, re1) {
        if (err) {
          lookupCallback('error in lookup', re1);
        } else {
          lookupCallback(null, re1);
        };
      });
    }
  });
}
function getDepartmentDetails(campusId, departmentId, depcb) {
  // console.log(campusId);
  // console.log(departmentId);
  var program = server.models.Program;
  program.find({'where': {'departmentId': departmentId, 'campusId': campusId}},
  function(err, res) {
    if (err) {
      errorFunction('error occured in dep', depcb);
    } else {
      var async = require('async');
      totalEnrollStudents = [];
      async.map(res, getStudents, function(err2, res1) {
        if (err2) {
          errorFunction('err', depcb);
        } else {
          // console.log('getting hear');
          // console.log(totalEnrollStudents);
          if (totalEnrollStudents == 0) {
            depcb(null, 'no one');
          } else {
            for (var i = 0; i < totalEnrollStudents.length; i++) {
              var async = require('async');
              fnlYr = [];
              async.map(totalEnrollStudents[i], getFinalYearStudents, function(err4, res3) {
                if (err4) {
                  errorFunction('err', depcb);
                } else {
                  //console.log('inhear');
                  if (i == totalEnrollStudents.length - 1) {
                    getStudentCompinesHiredDetails(function(err, hireRes) {
                      if (err) {
                        errorFunction('an err', depcb);
                      } else {
                        depcb(null, 'done');
                      }
                    });
                  }
                }
              });
            }
          }
        }
      });
    }
  });
}
function getStudents(ob, cb) {
  var enroll = server.models.Enrollment;
  enroll.find({'where': {'programId': ob.programId}}, function(err2, res2) {
    if (err2) {
      errorFunction('an error occured', cb);
    } else {
      if (res2 != 0) { totalEnrollStudents.push(res2); }
      cb(null, 'done');
    }
  });
}
var count = 0;

function getFinalYearStudents(ob1, cb1) {
  if (ob1 != 0) {
    var date = new Date().getFullYear();
    var date1 = JSON.stringify(new Date());
    console.log('date', date1);
    var compYear = date1.substr(6, 2);
    var finalYear = JSON.stringify(ob1.actualCompletionDate);
    var finalYr = finalYear.substr(1, 4);
    if (compYear < 6) { if (finalYr == date) { fnlYr.push(ob1); } }
    if (compYear >= 6) { if (finalYr == date + 1) { fnlYr.push(ob1); } }
    cb1(null, 'done');
  }
}
function getStudentCompinesHiredDetails(hireCb) {
  var async = require('async');
  async.map(fnlYr, getPlacedDetails, function(err5, res4) {
    if (err5) {
      errorFunction('err5', hireCb);
    } else {
      hireCb(null, res4);
    }
  });
}
function getPlacedDetails(ob2, hiredCalBc) {
  var eventStudent = server.models.EventStudentList;
  eventStudent.find({'where': {'studentId': ob2.studentId}},
                                      function(err6, res5) {
                                        if (err6) {
                                          errorFunction('err6', hiredCalBc);
                                        } else {
                                          if (res5 != 0) {
                                            var async = require('async');
                                            selectedPeople = [];
                                            totalOfferedJobs = 0;
                                            async.map(res5, getData, function(err7, res6) {
                                              if (err7) {
                                                errorFunction('err', hiredCalBc);
                                              } else {
                                                hiredCalBc(null, 'done');
                                              }
                                            });
                                          } else {
                                            hiredCalBc(null, 'done');
                                          }
                                        }
                                      });
}
function getData(data, callBc1) {
  getLookups('CANDIDATE_STATUS_TYPE', 'Offered',
  function(candidateStatus) {
    if (data.candidateStatusValueId == candidateStatus.lookupValueId) {
      totalOfferedJobs++;
      selectedPeople.push(data);
      callBc1(null, 'done');
    }
  });
}
var datasource = require('../server/datasources.json');
console.log(datasource.ScoraXChangeDB);
var CompanyDetails = [];
var total = [];
function getGraphAndTableData(campusId, date, cb) {
  console.log(date);
  var noOfOffersAndGraph = 'select sum(no_of_offers) numberOfStudentsPlaced , CAST(sum(total_offers)/sum(no_of_offers) as UNSIGNED) averageOffer ,CAST(max(maximum_offer) as UNSIGNED) maximumOffer FROM CAMPUS_PLACEMENT_AGGREGATES' +
  ' where academic_year=' + date + ' and campus_id=' + campusId + '';
  MySql(noOfOffersAndGraph, function(err, sqlRes) {
    if (err) {
      errorFunction('an err', cb);
    } else {
      var companiesVisited = 'select count(distinct(company_id)) totalCompanies from CAMPUS_PLACEMENT_AGGREGATES' +
     ' where academic_year=' + date + ' and campus_id=' + campusId + '';
      MySql(companiesVisited, function(err, sqlRes1) {
        if (err) {
          errorFunction('err', cb);
        } else {
          var graph = 'select company_id,sum(no_of_offers) totalStudents from CAMPUS_PLACEMENT_AGGREGATES' +
              ' where academic_year between ' + (date - 4) + ' and ' + (date) + ' and campus_id=' + campusId + ' group by company_id';
          MySql(graph, function(err, sqlRes3) {
            if (err) {
              errorFunction('err', cb);
            } else {
              var avg = 'select CAST(sum(total_offers)/sum(no_of_offers) as UNSIGNED) averageComp, academic_year from CAMPUS_PLACEMENT_AGGREGATES' +
                  ' where campus_id=' + campusId + ' and academic_year between ' + (date - 4) + ' and ' + (date) + ' group by academic_year;';
              MySql(avg, function(err, sqlRes4) {
                if (err) {
                  errorFunction('err', cb);
                } else {
                  var enrollment = 'select sum(no_of_students) totalStudentsgraduated from CAMPUS_OFFER_AGGREGATES' +
                  ' where academic_year=' + date + ' and campus_id=' + campusId + '';
                  MySql(enrollment, function(err, sqlRes5) {
                    if (err) {
                      errorFunction('err', cb);
                    } else {
                      createObjectForGraph(sqlRes, sqlRes1, sqlRes3, sqlRes4, sqlRes5, function(err, finalData) {
                        if (err) {
                          errorFunction('an err', cb);
                        } else {
                          cb(null, finalData);
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
function MySql(statment, sqlCb) {
  var app = require('../server/server.js');
  var connector = app.dataSources.ScoraXChangeDB.connector;
  connector.execute(statment, undefined, function(err, result) {
    if (err) {
      sqlCb(err, null);
    } else {
      sqlCb(null, result);
    }
  });
}

function createObjectForGraph(offerCount, visitedCompines, graphData, avg, graduated, createCb) {
  var async = require('async');
  CompanyDetails = [];
  async.map(graphData, getCompany, function(err, res) {
    if (err) {
      errorResponse('err', createCb);
    } else {
      var final = {};
      final.offer = offerCount[0];
      final.visited = visitedCompines[0];
      final.totalStudentsgraduated = graduated[0];
      final.companyPlacementData = CompanyDetails;
      final.avgOfferGraph = avg;
      createCb(null, final);
    }
  });
}
function getCompany(ob, cb5) {
  var company = server.models.Company;
  company.findOne({'where': {'companyId': ob.company_id}}, function(err, res) {
    if (err) {
      errorFunction('err', cb5);
    } else if (res == null) {
      var data = {};
      data.compName = null;
      data.noOfStudents = ob.totalStudents;
      CompanyDetails.push(data);
      total = [];
      cb5(null, 'done');
    } else {
      var data = {};
      data.compName = res.shortName;
      data.noOfStudents = ob.totalStudents;
      CompanyDetails.push(data);
      total = [];
      cb5(null, 'done');
    }
  });
}
function getDrivesAndData(campusId, cb) {
  var drives = [];
  var lookups = [];
  var inprogress = 0;
  var selected = [];
  var campus = server.models.CampusDrive;
  var eventstudent = server.models.EventStudentList;
  getLookups('CANDIDATE_STATUS_TYPE', 'Offered', function(res4) {
    getLookups('EDUCATION_EVENT_STATUS_CODE', 'In Progress', function(res5) {
      //console.log(res4, res5);
      lookups[0] = res4.lookupValueId;
      lookups[1] = res5.lookupValueId;
      eventstudent.find({'where': {'campusId': campusId}}, function(err6, res6) {
        if (err6) {

        } else {
        //  console.log(res6);
          var async = require('async');
          async.map(res6, getStudents, function(err7, res7) {
            if (err7) {
              errorFunction('an error', cb);
            } else {
              campus.find({'where': {'campusId': campusId}},
            function(err, res) {
              if (err) {
                cb(err, null);
              } else {
                //console.log('these are lookups', lookups);
                var async = require('async');
                async.map(res, getDrives, function(err1, res1) {
                  if (err1) {
                    errorFunction('errr', cb);
                  } else {
                    var details = {};
                    details.Drives = drives;
                    details.StudentsSelected = selected;
                    cb(null, details);
                    drives = [];
                    selected = [];
                  }
                });
              }
            });
            }
          });
        }
      });
    });
  });
  function getDrives(obj, callBc) {
    var campusEvent = server.models.CampusEvent;
    campusEvent.find({'where': {'campusDriveId': obj.campusDriveId}}, function(err2, res2) {
      if (err2) {
        errorFunction('errr', callBc);
      } else {
        var async = require('async');
        inprogress = 0;
        async.map(res2, getEvents, function(err3, res3) {
          if (err3) {
            errorFunction('errr', callBc);
          } else {
            var data = {};
            data.DriveName = obj.driveName;
            data.totalEvents = res2.length;
            data.inprogress = inprogress;
            drives.push(data);
            callBc(null, 'done');
          }
        });
      }
    });
    function getEvents(ob, callBack) {
      if (ob.eventStatusValueId == lookups[1]) {
        inprogress++;
        callBack(null, 'done');
      } else {
        callBack(null, 'done');
      }
    }
  }
  function getStudents(object, callBack1) {
    //console.log('hear');
    var enroll = server.models.Enrollment;
    var student = server.models.Student;
    if (object.candidateStatusValueId == lookup[0]) {
      student.findOne({'where': {'studentId': object.studentId}}, function(err10, res10) {
        if (err10) {

        } else {
          var company = server.models.Company;
          company.findOne({'where': {'companyId': object.companyId}}, function(err11, res11) {
            if (err11) {

            } else {
              enroll.findOne({'where': {'studentId': object.studentId}, 'include': 'enrollmentIbfk2rel'},
                          function(err8, res8) {
                            if (err8) {
                              errorFunction('err', callBack1);
                            } else {
                              if (res8 != 0) {
                                var program = server.models.Program;
                                program.findOne({'where': {'programId': res8.enrollmentIbfk2rel.programId},
            'include': 'departmentRel'},
            function(err9, res9) {
              if (err9) {

              } else {
                var data = {};
                data.StudentName = res10.firstName;
                data.Company = res11.shortName;
                data.departmentName = res8;
                data.ProgramName = res9;
                selected.push(data);
                callBack1(null, 'done');
              }
            });
                              }
                            }
                          });
            }
          });
        }
      });
    } else {
      callBack1(null, 'done');
    }
  }
}
exports.getDrivesAndData = getDrivesAndData;
exports.getCampusProfile = getCampusProfile;
exports.getGraphAndTableData = getGraphAndTableData;

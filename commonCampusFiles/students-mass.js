'use strict';
var cleanArray = require('../commonValidation/common-mass-upload').cleanArray;
var studentMassJson = require('../common/models/STUDENTS_MASS_UPLOAD_WORK.json');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateJsonInput;
var server = require('../server/server');
var async = require('async');
var key;
function createMassUpload(inputArray, stDataa, callBc) {
//   console.log('input array', inputArray);
  var studentsMassUploadWork = server.models.StudentsMassUploadWork;
  studentsMassUploadWork.create(inputArray, function(err, createdResponse) {
    if (err) {
      callBc(err, null);
    } else {
      // console.log('aaaaaaaaaaaaaaaaaa', createdResponse);
      var findRepeatedEmail = 'select row_number rowNo from STUDENTS_MASS_UPLOAD_WORK sw where email in ( ' +
        ' SELECT ' +
        ' email ' +
        ' FROM ' +
        '  STUDENTS_MASS_UPLOAD_WORK where campus_upload_log_id = ' + stDataa.campusUploadLogId + ' ' +
        ' GROUP BY email ' +
        ' HAVING COUNT(email) > 1) and sw.campus_upload_log_id = ' + stDataa.campusUploadLogId + '';
      MySql(findRepeatedEmail, function(err, repeatedEmails) {
        if (err) {
          callBc(err, null);
        } else {
          // console.log('rraaaaaaaaaar', repeatedEmails, err, findRepeatedEmail);
          getRepeatedArray(repeatedEmails, function(err, repeatedArray) {
            if (err) {
              callBc(err, null);
            } else {
              var tempEmailArray = (repeatedArray.length > 0) ? repeatedArray : 'null';
              var updateMessage = 'update STUDENTS_MASS_UPLOAD_WORK swrr set error = "Duplicate Email Id" ' +
              ' where row_number in ' + '(' + tempEmailArray + ')' + ' and campus_upload_log_id = ' +
               stDataa.campusUploadLogId + '';
              //  console.log('message', updateMessage);
              MySql(updateMessage, function(err, updatedResponse) {
                if (err) {
                  callBc(err, null);
                } else {
                  var findRepetedAddmission = 'select row_number rowNo from STUDENTS_MASS_UPLOAD_WORK sw where admission_no in ( ' +
                  ' SELECT ' +
                  ' admission_no ' +
                  ' FROM ' +
                  '  STUDENTS_MASS_UPLOAD_WORK where campus_upload_log_id = ' + stDataa.campusUploadLogId + ' ' +
                  ' GROUP BY admission_no ' +
                  ' HAVING COUNT(admission_no) > 1) and sw.campus_upload_log_id = ' + stDataa.campusUploadLogId + '';
                  MySql(findRepetedAddmission, function(err, admissionNoResponse) {
                    if (err) {
                      callBc(err, null);
                    } else {
                      getRepeatedArray(admissionNoResponse, function(err, repeatedAddRes) {
                        if (err) {
                          callBc(err, null);
                        } else {
                          var tempAddmissionArray = (repeatedAddRes.length > 0) ? repeatedAddRes : 'null';
                          var updateMessageOfAddmission = 'update STUDENTS_MASS_UPLOAD_WORK swrr set error = "Duplicate Admission Number" ' +
                          ' where row_number in (' + tempAddmissionArray + ') and campus_upload_log_id = ' +
                           stDataa.campusUploadLogId + '';
                          MySql(updateMessageOfAddmission, function(err, updateAddmissionRes) {
                            if (err) {
                              callBc(err, null);
                            } else {
                              var createStudent = require('./create-student-mass-upload').createStudent;
                              createStudent(createdResponse, stDataa, function(finalErr, finalOutput) {
                                if (finalErr) {
                                  throwError(finalErr, callBc);
                                } else {
                                  commonErrorRead(finalOutput, function(err, finalResponse) {
                                    if(err) {
                                      callBc(err, null);
                                    } else {
                                      callBc(null, finalResponse);
                                    }
                                  })
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
function getfailureMass(finalArray, cb) {
  var fs = require('fs');
  var csv = require('fast-csv');
  var name = stDataa.fileDetails.name;
  var container = stDataa.fileDetails.container;
  var pathForm = require('path');
  var json2csv = require('json2csv');
  var datasource = require('../server/datasources.json');
  var errorCsv = './attachments/' + container + '/' + name;
  errorCsv = pathForm.join(__dirname, '../../../', errorCsv);
  var campus = server.models.Campus;
  campus.findOne({ 'where': { 'campusId': stDataa.campusId } }, function (campusErr, campusOut) {
    var fs = require('fs');
    var csv = require('fast-csv');
    var fileName;
    fileName = campusOut.name;
    var randomNumber = Math.random().toString(20).slice(-10);
    var errorFile = campusOut.name + '_' + randomNumber + '.csv';
    fileName = './commonValidation/' + campusOut.name + '.csv';
    var fields = ['rowNumber', 'firstName', 'lastName', 'email', 'admissionNo', 'startDate', 'plannedCompletionDate', 'error'];
    var csv = json2csv({
      data: finalArray,
      fields: fields,
    });
    fs.writeFile(fileName, csv, function (err) {
      if (err) throw err;
    });
    var AWS = require('aws-sdk');
    var fs = require('fs');
    AWS.config.region = datasource.amazonupload.region;
    AWS.config.update({ accessKeyId: datasource.amazonupload.keyId, secretAccessKey: datasource.amazonupload.key });

    // Read in the file, convert it to base64, store to S3
    fs.readFile(fileName, function (err, data) {
      if (err) { throw err; }
      var base64data = new Buffer(data, 'binary');

      var s3 = new AWS.S3();
      var s3 = new AWS.S3({ params: { Bucket: container } });
      var params = {
        Key: errorFile, //file.name doesn't exist as a property
        Body: data,
      };
      s3.upload(params, function (err, data) {
        key = data.key;
        fs.unlink(fileName, (err) => {
          if (err) throw err;
          cb(null, finalArray);
        });
      });
    });
  });
}
function commonErrorRead(finalOutput, readCallBc) {
  var studentsMassUploadWork = server.models.StudentsMassUploadWork;
  studentsMassUploadWork.find({ 'where': { 'campusUploadLogId': stDataa.campusUploadLogId, 'error': {
    neq : null,
  } } }, function (workErr, workResponse) {
    if (workErr) {
    } else {
      var campusUploadLog = server.models.CampusUploadLog;
      var studentsMassUploadWork = server.models.StudentsMassUploadWork;
      var details = stDataa.fileDetails;
      var container = stDataa.fileDetails.container;
      getfailureMass(workResponse, function (err, failedArray) {
        var pathForm = require('path');
        var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
        var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + key;
        var logInput = {
          'errorFileLocation': errorFileLocation,
          'totalNoRecs': inputArray.length,
          'noFailRecs': workResponse.length,
          'noSuccessRecs': inputArray.length - workResponse.length,
          'errorFileLocation' : errorFileLocation,
          'csvFileLocation' : csvFileLocation
        };
        campusUploadLog.updateAll({ 'campusUploadLogId': stDataa.campusUploadLogId }, logInput, function (logErr, logOutput) {
          studentsMassUploadWork.destroyAll({
            'campusUploadLogId':
              stDataa.campusUploadLogId
          }, function (error, destroy) {
            readCallBc(null, logOutput);
          });
        });
      });
    }
  });
}
function getRepeatedArray(array, repeatCb) {
  var repeatedArray = [];
  if(array.length > 0) {
    for (var i = 0; i < array.length; i++) {
      repeatedArray.push(Number(array[i].rowNo));
      if (i === array.length - 1) {
        repeatCb(null, repeatedArray);
      }
    }
  } else {
    repeatCb(null, repeatedArray);
  }
}
function MySql(statment, sqlCb) {
  var app = require('../server/server.js');
  var connector = app.dataSources.ScoraXChangeDB.connector;
  connector.execute(statment, undefined, function(err, result) {
    if (err) {
      sqlCb(err, null);
    } else {
      sqlCb(null, result);
    }
  });
}
}
exports.createMassUpload = createMassUpload;

'use strict';
var server = require('../server/server');
var eventStudentList = server.models.EventStudentList;
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var logger = require('../server/boot/lib/logger');
/**
 *updateStudentEvents-function deals with updating the events
 *@constructor
 * @param {object} stData - contains all the data need to get updated
 * @param {any} cb - deals with the response
 */
function updateStudentEvents(stData, cb) {
  // console.log('objjjjjjjjjjjjjjjjjjjjjjj ', stData);
  eventStudentList.findOne({
    'where': {
      'and': [{
        'studentListId': stData.studentListId,
      },
      {
        'employerEventId': stData.employerEventId,
      },
      ],
    },
  }, function(err, events) {
    if (events) {
      // console.log('eventsssssssssssssssss ', stData);
      var lookup = require('../commonValidation/lookupMethods').lookupMethod;
      lookup('CANDIDATE_STATUS_TYPE', function(err, response) {
        var screeningId = response.find(findScreening);
        var sId = response.find(findShort);
        var rId = response.find(findReject);
        var oId = response.find(findHold);
        var iId = response.find(findInt);
        var offId = response.find(offFind);

        function findScreening(screeningVal) {
          return screeningVal.lookupValue === 'Screening';
        }

        function findShort(shortVal) {
          return shortVal.lookupValue === 'Shortlisted';
        }

        function findReject(rejectVal) {
          return rejectVal.lookupValue === 'Rejected';
        }

        function findHold(holdVal) {
          return holdVal.lookupValue === 'On Hold';
        }

        function findInt(intVal) {
          return intVal.lookupValue === 'Interview in progress';
        }

        function offFind(off) {
          return off.lookupValue === 'Offered';
        }
        var status = stData.status;
        if (offId.lookupValueId == status && stData.compPackageId) {
          // console.log('0000000000000000000000000000 ', offId.lookupValueId, status);
          var screeningObj = {};
          screeningObj = events;
          screeningObj.candidateStatusValueId = stData.status;
          screeningObj.compPackageId = stData.compPackageId;
          screeningObj.updateDatetime = new Date();
          // console.log('sreenigggggggggggggggggggggggggggggg ', screeningObj);
          events.updateAttributes(screeningObj, function(screeningErr, screeningResp) {
            if (events.campusEventId != null) {
              notificationPush(stData, 13, function(notificationErr, notificationOut) {
                screeningResp.notification = 'Notification Sent Successfully.';
                cb(null, screeningResp);
              });
            } else {
              cb(null, screeningResp);
            }
          });
        } else if (screeningId.lookupValueId == events.candidateStatusValueId && status == screeningId.lookupValueId || status == sId.lookupValueId || status == rId.lookupValueId || oId.lookupValueId == status) {
          var screeningObj = {};
          screeningObj.candidateStatusValueId = stData.status;
          screeningObj.updateDatetime = new Date();
          events.updateAttributes(screeningObj, function(screeningErr, screeningResp) {
            if (events.campusEventId != null) {
              notificationPush(stData, 12, function(notificationErr, notificationOut) {
                screeningResp.notification = 'Notification Sent Successfully.';
                cb(null, screeningResp);
              });
            } else {
              cb(null, screeningResp);
            }
          });
        } else if (sId.lookupValueId == events.candidateStatusValueId && status == rId.lookupValueId || status == iId.lookupValueId || status == offId.lookupValueId || oId.lookupValueId == status) {
          var screeningObj = {};
          screeningObj.candidateStatusValueId = stData.status;
          screeningObj.updateDatetime = new Date();
          events.updateAttributes(screeningObj, function(screeningErr, screeningResp) {
            if (events.campusEventId != null) {
              notificationPush(stData, 12, function(notificationErr, notificationOut) {
                screeningResp.notification = 'Notification Sent Successfully.';
                cb(null, screeningResp);
              });
            } else {
              cb(null, screeningResp);
            }
          });
        } else if (rId.lookupValueId == events.candidateStatusValueId && status == sId.lookupValueId || oId.lookupValueId == status) {
          var screeningObj = {};
          screeningObj.candidateStatusValueId = stData.status;
          screeningObj.updateDatetime = new Date();
          events.updateAttributes(screeningObj, function(screeningErr, screeningResp) {
            if (events.campusEventId != null) {
              notificationPush(stData, 12, function(notificationErr, notificationOut) {
                screeningResp.notification = 'Notification Sent Successfully.';
                cb(null, screeningResp);
              });
            } else {
              cb(null, screeningResp);
            }
          });
        } else if (oId.lookupValueId == events.candidateStatusValueId && status == sId.lookupValueId || iId.lookupValueId == status) {
          var screeningObj = {};
          screeningObj.candidateStatusValueId = stData.status;
          screeningObj.updateDatetime = new Date();
          events.updateAttributes(screeningObj, function(screeningErr, screeningResp) {
            if (events.campusEventId != null) {
              notificationPush(stData, 12, function(notificationErr, notificationOut) {
                screeningResp.notification = 'Notification Sent Successfully.';
                cb(null, screeningResp);
              });
            } else {
              cb(null, screeningResp);
            }
          });
        } else if (iId.lookupValueId == events.candidateStatusValueId && status == rId.lookupValueId || oId.lookupValueId == status || offId.lookupValueId == status) {
          var screeningObj = {};
          screeningObj.candidateStatusValueId = stData.status;
          screeningObj.updateDatetime = new Date();
          events.updateAttributes(screeningObj, function(screeningErr, screeningResp) {
            if (events.campusEventId != null) {
              notificationPush(stData, 12, function(notificationErr, notificationOut) {
                screeningResp.notification = 'Notification Sent Successfully.';
                cb(null, screeningResp);
              });
            } else {
              cb(null, screeningResp);
            }
          });
        } else {
          cb('Not a Proper Action to perform', null);
        }
      });
    } else {
      cb('Records not found', null);
    }
  });
}
exports.updateStudentEvents = updateStudentEvents;
/**
 *checkEventISOpen-function deals with checking the event was open or not
 *@constructor
 * @param {object} obj - contains all the data which required for searching an event
 * @param {any} callBc - deals with the response
 */
function checkEventISOpen(obj, callBc) {
  var openEventCheck = require('./get-student-events').getStudentEvents;
  openEventCheck(obj.studentId, function(err, resp) {
    var eventObj = resp.find(findId);

    function findId(statusVal) {
      return statusVal.campusEventId === obj.campusEventId;
    }
    callBc(null, eventObj);
  });
}
/**
 *updateOffer-function deals with updating the offer
 *@constructor
 * @param {object} response - contains offer details
 * @param {object} input - contains id to which it get updated
 * @param {any} cb - deals with the response
 */
function updateOffer(response, input, cb) {
  response.updateAttributes(input, function(err, info) {
    if (err) {
      throwError(err, cb);
    } else {
      logger.info('Student Event Offer record Updated Successfully');
      info['requestStatus'] = true;
      info['updateDatetime'] = new Date();
      cb(null, info);
    }
  });
}
/**
 *checkOfferValue-function deals with checking the offer
 *@constructor
 * @param {any} cb - deals with the response
 */
function checkOfferValue(cb) {
  var offerStatusValueId;
  var lookup = require('../commonValidation/lookupMethods').lookupMethod;
  lookup('OFFER_STATUS_CODE', function(err, response) {
    offerStatusValueId = response.find(findStatus);

    function findStatus(statusVal) {
      return statusVal.lookupValue === 'Offered';
    }
  });
}

function notificationPush(stData, notificationName, callBack) {
  var pushInput = {
    'userId': stData.userId,
    'role': 'RECDIR',
    'companyId': stData.companyId,
    'employerPersonId': stData.employerPersonId,
    // 'empEventId': stData.employerEventId,
    'campusEventId': stData.campusEventId,
    'notificationName': notificationName,
    'studentList': [{
      'studentId': stData.studentId,
    }],
  };
  // console.log('pushinputtttttttttttttttttt ', pushInput);
  var notificationTemplate = server.models.NotificationMessageTemplates;
  var pushNotification = notificationTemplate.pushNotification;
  pushNotification(pushInput, function(pushErr, pushOut) {
    // console.log(pushErr, pushOut);
    if (pushOut) {
      pushOut.Notification = 'Notificaton Sent';
      callBack(null, pushOut);
    } else {
      callBack(null, null);
    }
  });
}

  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var validation = require('../commonValidation/personValidation');
  var EducationPerson = server.models.EducationPerson;
  var lookupMethods = require('../commonValidation/lookupMethods');
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *educationPersonUpdate-function deals with updating the education person detalis
   *@constructor
   * @param {object} personData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function educationPersonUpdate(personData, cb) {
    if (personData) {
      if (personData.campusId && personData.educationPersonId) {
        //validating input json fields
        validation.validateContactJson(personData, function(err, validVal) {
          if (err) {
            cb(err, personData);
          } else if (validVal == true) {
            var prefixVal, genderVal, personTypeVal;
            var lookupValueObj = {};
            var lookupValueObj1 = {};
            var lookupValueObj2 = {};
            var prefixCode = 'PREFIX_CODE';
            var genderCode = 'GENDER_CODE';
            var personTypeCode = 'PERSON_TYPE_CODE';
            lookupValueObj['lookupValueId'] = personData.prefixValueId;
            lookupValueObj1['lookupValueId'] = personData.genderValueId;
            lookupValueObj2['lookupValueId'] = personData.personTypeValueId;
            // lookupMethods.typeCodeFunction(lookupValueObj, prefixCode, function(prefixVal) {
            //   prefixVal = prefixVal;

            //   //addressTypeCodeFunction to verify addressTypeValueId
            //   lookupMethods.typeCodeFunction(lookupValueObj1, genderCode, function(genderVal) {
            //     genderVal = genderVal;

            //     //addressFunction function verifies city id , countryCode and stateCode
            //     lookupMethods.typeCodeFunction(lookupValueObj2, personTypeCode, function(personTypeVal) {
            //       personTypeVal = personTypeVal;

            //       if (prefixVal == false || genderVal == false || personTypeVal == false) {
            //         throwError(' Invalid prefixVal or genderVal or personTypeVal', cb);
            //       } else {
            EducationPerson.findOne({
              'where': {
                'and': [{
                  'campusId': personData.campusId,
                }, {
                  'educationPersonId': personData.educationPersonId,
                }],
              },
            }, function(err, personList) {
              if (err) {
                        //throws error

                cb(err, personData);
              } else if (personList) {
                personList.updateAttributes(personData, function(err, info) {
                  if (err) {
                    cb(err, info);
                  } else {
                            //making requestStatus is true;
                    logger.info('educationperson record Updated Successfully');
                    info['requestStatus'] = true;
                    info['updateDatetime'] = new Date();
                    cb(null, info);
                  }
                });
              } else {
                        //throws error incase of invalid educationPersonId or campusId
                throwError('Invalid educationPersonId or campusId ', cb);
                logger.error('Invalid educationPersonId or campusId');
              }
            });
            //       }
            //     });
            //   });
            // });
          } else {
            logger.error('You cannot perform update operation');
            throwError('You cannot perform update operation: ', cb);
          }
        });
      } else {
        logger.error('campusId and  educationPersonId are required');
        throwError('campusId and  educationPersonId are required', cb);
      }
    } else {
      logger.error("Input can't be Null");
      throwError('Input is Null: ', cb);
    }
  }
  exports.educationPersonUpdate = educationPersonUpdate;

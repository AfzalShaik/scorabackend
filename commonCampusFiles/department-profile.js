'use strict';
var server = require('../server/server');
var errorFunction = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var getLookups = require('./get-campus-dashboard').getLookups;
var enrolledStudents = 0;
var finalYearStudents = 0;
function getDepartmentProfile(departmentData, depCb) {
  var program = server.models.Program;
  program.find({'where': {'departmentId': departmentData.departmentId}}, function(err1, programs) {
    if (err1) {
      errorFunction('an err', depCb);
    } else {
      getTotalStudentsEnrolled(departmentData, programs, function(err, enrollRes) {
        if (err) {
          errorFunction('an error', depCb);
        } else {
          var final = {};
          final.depDetails = departmentData;
          final.programs = programs;
          final.finlYearStudents = finalYearStudents;
          final.enrolledStudents = enrolledStudents;
          depCb(null, final);
        }
      });
    }
  });
}
function getTotalStudentsEnrolled(departmentData, programs, enrollcb) {
  var async = require('async');
  enrolledStudents = 0;
  finalYearStudents = 0;
  async.map(programs, getEnrolled, function(err, res) {
    if (err) {
      errorFunction('err', enrollcb);
    } else {
      enrollcb(null, res);
    }
  });
}
function getEnrolled(ob, cb) {
  var enroll = server.models.Enrollment;
  enroll.find({'where': {'programId': ob.programId}}, function(err, enrollRes) {
    if (err) {
      errorFunction('err', cb);
    } else {
      var async = require('async');
      enrollRes.filter(function(ob) {
        var date = new Date().getFullYear();
        var date1 = JSON.stringify(new Date());
        var compYear = date1.substr(6, 2);
        var final = ob.createDatetime;
        var parse = JSON.stringify(final);
        var p = parse.substr(1, 4);
        if (p == date) { enrolledStudents++; }
        var finalYear = JSON.stringify(ob.actualCompletionDate);
        var finalYr = finalYear.substr(1, 4);
        if (compYear < 6) { if (finalYr == date) { finalYearStudents++; } }
        if (compYear >= 6) { if (finalYr == date + 1) { finalYearStudents++; } }
      });
      cb(null, 'done');
    }
  });
}
var datasource = require('../server/datasources.json');
console.log(datasource.ScoraXChangeDB);
var CompanyDetails = [];
var total = [];
// TODO need to where condi tfor campusId to all sal get campusId from context object
function getGraphData(departmentDetails, cb) {
  var date = new Date().getFullYear();
  var past = date - 5;
  var companyPlacedCount = 'select company_id,sum(no_of_offers) totalOffers from CAMPUS_PLACEMENT_AGGREGATES' +
  ' where academic_year between ' + past + ' and ' + date + ' and department_id=' + departmentDetails.departmentId + ' group by company_id';
  MySql(companyPlacedCount, function(err, sqlRes1) {
    if (err) {
      errorFunction('an error occured', cb);
    } else {
      var programDetails = 'select program_id,sum(no_offers)/sum(no_of_students)*100 percentageOffers from CAMPUS_OFFER_AGGREGATES' +
      ' where academic_year between ' + past + ' and ' + date + ' and department_id=' + departmentDetails.departmentId + ' group by program_id';
      MySql(programDetails, function(err, sqlRes2) {
        if (err) {
          errorFunction('an error', cb);
        } else {
          var allCounts = 'select SUM(no_of_offers) totalOffers, SUM(total_offers)/SUM(no_of_offers) averageOffer, MAX(maximum_offer) maximumOffer, count(distinct(company_id)) totalCompines from CAMPUS_PLACEMENT_AGGREGATES' +
            ' where academic_year=' + date + ' and department_id=' + departmentDetails.departmentId + '';
          MySql(allCounts, function(err, sqlRes3) {
            if (err) {
              errorFunction('an error', cb);
            } else {
              var totalAndSalaryRange = 'select no_of_students,top_5,6_to_10,11_to_20,21_to_50,above_50 from CAMPUS_OFFER_AGGREGATES' +
                ' where department_id=' + departmentDetails.departmentId + ' and academic_year=' + date + '';
              MySql(totalAndSalaryRange, function(err, sqlRes4) {
                if (err) {
                  errorFunction('an error occured', cb);
                } else {
                  createObjectForGraph(sqlRes1, sqlRes2, sqlRes3, sqlRes4, function(err, finalData) {
                    if (err) {
                      errorFunction('an err', cb);
                    } else {
                      cb(null, finalData);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
function createObjectForGraph(companyPlacedCount, programDetails, allCounts, totalAndSalaryRange, createCb) {
  var async = require('async');
  CompanyDetails = [];
  async.map(companyPlacedCount, getCompany, function(err, res) {
    if (err) {
      errorFunction('err', createCb);
    } else {
      var final = {};
     // final.companyCount = companyPlacedCount;
      final.programDetails = programDetails;
      final.allCounts = allCounts;
      final.companyDetails = CompanyDetails;
      final.totalAndMaxSal = totalAndSalaryRange;
      createCb(null, final);
    }
  });
}
function getCompany(ob, cb5) {
  for (var key in ob) {
    if (key == 'sum(no_of_offers)') {
      total.push(ob[key]);
      passAnObj();
    }
  }
  function passAnObj() {
    var campus = server.models.Company;
    campus.findOne({'where': {'companyId': ob.company_id}}, function(err, res) {
      if (err) {
        errorFunction('err', cb5);
      } else if (res == null) {
        var data = {};
        data.campusName = null;
        data.TotalJobsForThatCampus = total[0];
        CompanyDetails.push(data);
        total = [];
        cb5(null, 'done');
      } else {
        var data = {};
        data.campusName = res.shortName;
        data.TotalJobsForThatCampus = total[0];
        CompanyDetails.push(data);
        total = [];
        cb5(null, 'done');
      }
    });
  }
}
function MySql(statment, sqlCb) {
  var app = require('../server/server.js');
  var connector = app.dataSources.ScoraXChangeDB.connector;
  connector.execute(statment, undefined, function(err, result) {
    if (err) {
      sqlCb(err, null);
    } else {
      sqlCb(null, result);
    }
  });
}
exports.getDepartmentProfile = getDepartmentProfile;
exports.getGraphData = getGraphData;

'use strict';
var server = require('../server/server');
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var throwError = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var errorArray = [];
var inputData, key;

function updateStudentUpload(input, stDataa, cb) {
  inputData = stDataa;
  async.map(input, getStudentId, function(studentErr, studentResp) {
    var out = [];
    out = cleanArray(studentResp);
    if (studentResp.length > 0) {
      async.map(out, validateStatus, function(statusErr, stuatusResp) {
        var statusResponse = [];
        statusResponse = cleanArray(stuatusResp);
        if (stuatusResp.length > 0) {
          var campusEvent = server.models.CampusEvent;
          campusEvent.findOne({
            'where': {
              'campusEventId': stDataa.campusEventId,
            },
          }, function(campusErr, campusOut) {
            var campusObj = {};
            campusObj = campusOut;
            campusObj.eventStatusValueId = 242;
            campusOut.updateAttributes(campusObj, function(eventErr, eventOut) {
              if (eventOut) {
                readErrors(statusResponse, input, stDataa, function(error, response) {
                  // console.log(response);
                  // Notification Will sent to Students from Screening to Shortlisted
                  var StudentArray = [];
                  for (var i = 0; i < statusResponse.length; i++) {
                    StudentArray.push(statusResponse[i].studentId);
                  }
                  var uniqueEmpResp = StudentArray.filter(function(elem, index, self) {
                    return index == self.indexOf(elem);
                  });
                  if (uniqueEmpResp.length > 0) {
                    async.map(uniqueEmpResp, sendStudent, function(campusErr, studentList) {
                      var pushInput = {
                        'userId': stDataa.userId,
                        'role': 'PLCDIR',
                        'campusId': stDataa.campusId,
                        'educationPersonId': stDataa.educationPersonId,
                        // 'empEventId': CampusEventData.empEventId,
                        'campusEventId': stDataa.campusEventId,
                        'notificationName': 11,
                        'studentList': studentList,
                      };
                      var notificationTemplate = server.models.NotificationMessageTemplates;
                      var pushNotification = notificationTemplate.pushNotification;
                      pushNotification(pushInput, function(pushErr, pushOut) {
                        if (pushErr) {
                          cb(pushErr, null);
                          StudentArray = [];
                        } else {
                          response.Notification = 'Notificaton Sent';
                          cb(null, response);
                          StudentArray = [];
                        }
                      });
                    });
                  } else {
                    cb(null, response);
                  }
                  // Notification Will sent to Students from Screening to Shortlisted
                });
              } else {
                readErrors(undefined, input, stDataa, function(error, response) {
                  // console.log(response);
                  cb(null, response);
                });
              }
            });
          });
        } else {
          // throwError('Status is not updated', cb);
          readErrors(undefined, input, stDataa, function(error, response) {
            // console.log(response);
            cb(null, response);
          });
        }
        // cb(null, out);
      });
    } else {
      readErrors(undefined, input, stDataa, function(error, response) {
        // console.log(response);
        cb(null, response);
      });
    }
  });
}

function getStudentId(obj, callBC) {
  var user = server.models.ScoraUser;
  user.findOne({
    'where': {
      'email': obj.Email,
    },
  }, function(userErr, userResp) {
    // console.log('objjjjjjjjjjjjjjjjjj ', userResp);
    if (userResp) {
      var student = server.models.Student;
      student.findOne({
        'where': {
          'id': userResp.id,
        },
      }, function(studentErr, studentResp) {
        if (studentResp) {
          obj.studentId = studentResp.studentId;
          callBC(null, obj);
        } else {
          obj.error = 'Invalid Email or Student';
          errorArray.push(obj);
          callBC(null, null);
        }
      });
    } else {
      obj.error = 'Invalid Email or Student';
      errorArray.push(obj);
      callBC(null, null);
    }
  });
}

function validateStatus(obj, cb) {
  // console.log(obj);
  if (obj.ScreeningStatus == 'Shortlisted' || obj.ScreeningStatus == 'shortlisted') {
    var shortlisted = 377;
    obj.status = shortlisted;
    updateCandidate(obj, function(err, out) {
      // console.log();
      if (out) {
        cb(null, out);
      } else {
        // obj.error = 'Invalid Input';
        // errorArray.push(obj);
        cb(null, null);
      }
    });
  } else if (obj.ScreeningStatus == 'Rejected' || obj.ScreeningStatus == 'rejected') {
    var rejected = 378;
    obj.status = rejected;
    updateCandidate(obj, function(err, out) {
      if (out) {
        cb(null, out);
      } else {
        // obj.error = 'Invalid Input';
        // errorArray.push(obj);
        cb(null, null);
      }
    });
  } else if (obj.ScreeningStatus == 'On Hold' || obj.ScreeningStatus == 'on Hold') {
    var onHold = 379;
    obj.status = onHold;
    updateCandidate(obj, function(err, out) {
      if (out) {
        cb(null, out);
      } else {
        // obj.error = 'Invalid Input';
        // errorArray.push(obj);
        cb(null, null);
      }
    });
  } else {
    obj.error = 'Invalid Status';
    errorArray.push(obj);
    cb(null, null);
  }
}

function updateCandidate(input, eventCB) {
  var eventStudentList = server.models.EventStudentList;
  eventStudentList.findOne({
    'where': {
      'and': [{
        'studentId': input.studentId,
      }, {
        'campusEventId': input.campusEventId,
      }],
    },
  }, function(eventErr, eventResp) {
    // console.log(input.studentId, input.campusEventId);
    if (eventResp) {
      var obj = {};
      obj = eventResp;
      obj.candidateStatusValueId = input.status;
      eventResp.updateAttributes(obj, function(updateErr, updateOut) {
        if (updateOut) {
          eventCB(null, updateOut);
        } else {
          input.error = 'Invalid Record Uploaded';
          errorArray.push(input);
          eventCB(null, null);
        }
      });
    } else {
      input.error = 'Invalid Record Uploaded';
      errorArray.push(input);
      eventCB(null, null);
    }
  });
}
function sendStudent(obj, callBc) {
  if (obj) {
    // campusArray.push({'campusId': obj.campusId});
    var objj = {};
    objj.studentId = obj;
    callBc(null, objj);
  } else {
    callBc('studentId is null', null);
  }
}

function readErrors(successArray, input, stDataa, clb) {
  var finalArray = [];
  for (var i = 0; i < errorArray.length; i++) {
    var obj = {
      'rowNumber': errorArray[i].rowNumber,
      'FirstName': errorArray[i].FirstName,
      'MiddleName': errorArray[i].MiddleName,
      'LastName': errorArray[i].LastName,
      'Email': errorArray[i].Email,
      'error': errorArray[i].error,
      'Skills': errorArray[i].Skills,
      'Interests': errorArray[i].Interests,
      'ScreeningStatus': errorArray[i].ScreeningStatus,
    };
    finalArray.push(obj);
  }
  var fs = require('fs');
  var csv = require('fast-csv');
  var name = stDataa.fileDetails.name;
  var container = stDataa.fileDetails.container;
  var pathForm = require('path');
  // var errorCsv = './attachments/' + container + '/' + name;
  // errorCsv = pathForm.join(__dirname, '../../../', errorCsv);
  // var ws = fs.createWriteStream(errorCsv);
  // csv
  //   .writeToPath(errorCsv, finalArray, {
  //     headers: true,
  //   })
  //   .on('finish', function() {
  //     // console.log('done!');
  //     errorArray = [];
  //   });
  var uploadCsv = require('./commonCSVUpload.js').uploadCSV;
  var campus = server.models.Campus;
  campus.findOne({'where': {'campusId': inputData.campusId}}, function(campusErr, campusOut) {
    var fs = require('fs');
    var csv = require('fast-csv');
    var json2csv = require('json2csv');
    var fileName;
    fileName = campusOut.name;
    var randomNumber = Math.random().toString(20).slice(-10);
    var errorFile = campusOut.name + '_' + randomNumber + '.csv';
    fileName = './commonValidation/' + campusOut.name + '.csv';
    var fields = ['rowNumber', 'FirstName', 'MiddleName', 'LastName', 'Email', 'Skills', 'Interests', 'ScreeningStatus', 'error'];
    var csv = json2csv({
      data: finalArray,
      fields: fields,
    });
    fs.writeFile(fileName, csv, function(err) {
      if (err) throw err;
      uploadCsv(fileName, container, errorFile, function(fileErr, fileOut) {
        key = fileOut;
        // console.log('keyyyyyy', key);
        var details = stDataa.fileDetails;
        var campusUploadLog = server.models.CampusUploadLog;
        var pathForm = require('path');
        var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
        // csvFileLocation = pathForm.join(__dirname, '../../../', csvFileLocation);
        var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + key;
        // errorFileLocation = pathForm.join(__dirname, '../../../', errorFileLocation);
        var logInput = {
          'campusUploadTypeValueId': stDataa.campusUploadTypeValueId,
          'campusId': stDataa.campusId,
          'inputParameters': 'string',
          'createUserId': 1,
          'createDatetime': new Date(),
          'csvFileLocation': csvFileLocation,
          'errorFileLocation': errorFileLocation,
          'totalNoRecs': input.length,
          'noFailRecs': cleanArray(finalArray).length,
          'noSuccessRecs': (successArray) ? cleanArray(successArray).length : 0,
        };
        campusUploadLog.create(logInput, function(logErr, logOutput) {
          // console.log(logOutput);
          finalArray = [];
          clb(null, logOutput);
        });
        errorArray = [];
        key = fileOut;
      });
    });
  });
}
exports.updateStudentUpload = updateStudentUpload;

'use strict';
var async = require('async');
var server = require('../server/server');
var lookup = require('../commonValidation/lookupMethods').lookupMethod;
var throwerror = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var postIntoUploadWork = require('./student-mass-upload').postIntoUploadWork;
var user = require('../server/config.json');
var dataSource = require('../server/datasources.json');
var host = user.host;
var port = user.port;
var globalPassword;
var nodemailer = require('nodemailer');
var path = require('path');
var signUpService = require('../commonValidation/signup-service.js').createSignUpService;
// console.log('hostttttttttttttttttttttttttttttttttt ', host, port);
function createStudent(finalArray, stDataa, cb) {
  var createTheSkipN = 'insert into ENROLLMENT(student_id, campus_id, program_id, planed_completion_date, ' +
  ' start_date, ' +
  ' admission_no, create_user_id, update_user_id, create_datetime, update_datetime, data_verified_ind) select smw.student_id , ' +
  ' smw.campus_id, smw.program_id, smw.planned_completion_date, smw.start_date, ' +
  ' smw.admission_no, 1, 1,sysdate(),sysdate(), "Y"  from STUDENTS_MASS_UPLOAD_WORK smw ' +
  ' where campus_upload_log_id = '+ stDataa.campusUploadLogId + ' ' +
  ' and smw.skip_user_ind = "Y" and smw.error is null';
          insertSkipUserId(createTheSkipN, function(err, skipCreateRes) {
            if (err) {
              cb(err, null);
            } else {
              createDataOfNewUsers(function(err, responseOfCreation) {
                if (err) {
                  cb(err, null);
                } else {
                  cb(null, responseOfCreation);
                }
              });
            }
          });
  // function createWorkData(array, cb) {
  //   console.log(array);
  //   var studentsMassUploadWork = server.models.StudentsMassUploadWork;
  //   studentsMassUploadWork.create(array, function(err, res) {
  //     console.log('e', err);
  //     cb(null, res);
  //   });
  // }
  function insertSkipUserId(createTheSkip, callb) {
      MySql(createTheSkip, function(err, skipYCreate){
        if(err) {
          callb(err, null);
        } else {
          callb(null, skipYCreate);
        }
      });
  }
  var loopCb = 0;
  function createDataOfNewUsers(callBack) {
    var createUserRecords = 'insert into SCORA_USER(create_datetime, update_datetime, ' +
      ' create_user_id, update_user_id , email , password ) ' +
      ' select sysdate(), sysdate(), create_user_id , update_user_id, email, ' +
      ' password from STUDENTS_MASS_UPLOAD_WORK ' +
      ' where campus_upload_log_id = ' + stDataa.campusUploadLogId + ' and skip_user_ind is null and error is null';
    MySql(createUserRecords, function(err, createdResponse) {
      // console.log('ee', err);
      if (err) {
        callBack(err, null);
      } else {
            var roleCreation = 'insert into SCORA_USER_ROLE(user_id, role_code, start_date, create_datetime, update_datetime, ' +
              ' create_user_id, update_user_id) select su.id, "STUDENT", sysdate() , sysdate(), '+
              ' sysdate(), su.create_user_id, su.update_user_id from ' +
              ' SCORA_USER su, STUDENTS_MASS_UPLOAD_WORK smw where ' +
              ' su.email = smw.email and ' +
              ' smw.campus_upload_log_id =' + stDataa.campusUploadLogId + ' and smw.skip_user_ind is null and smw.error is null'; 
              MySql(roleCreation, function(err, roleCreatedResponse) {
                if(err) {
                  callBack(err, null);
                } else {
                  var studentCreation = 'insert into STUDENT (user_id, student_status_value_id, ' +
                    ' first_name, last_name, create_datetime, update_datetime, ' +
                    ' create_user_id, update_user_id)  select su.id, 246, smw.first_name, ' +
                    ' smw.last_name, sysdate() , sysdate(), ' +
                    ' su.create_user_id, su.update_user_id from ' +
                    ' SCORA_USER su, STUDENTS_MASS_UPLOAD_WORK smw where ' +
                    ' su.email = smw.email and ' +
                    ' smw.campus_upload_log_id =' + stDataa.campusUploadLogId + ' and smw.skip_user_ind is null and smw.error is null';
                    MySql(studentCreation, function(err, studentCreatedResponse) {
                      if(err) {
                        callBack(err, null);
                      } else {
                        var createEnrollmentRecord = 'insert into ENROLLMENT (student_id, program_id, ' +
                          ' admission_no, start_date, planed_completion_date, data_verified_ind, ' +
                          ' create_datetime, update_datetime, create_user_id, update_user_id, ' +
                          ' campus_id)  select stu.student_id, smw.program_id, smw.admission_no, ' +
                          ' smw.start_date, smw.planned_completion_date , "Y", ' +
                          ' sysdate(), sysdate(), ' +
                          ' su.create_user_id, su.update_user_id, smw.campus_id from ' +
                          ' SCORA_USER su, STUDENTS_MASS_UPLOAD_WORK smw, ' +
                          ' STUDENT stu where ' +
                          ' su.email = smw.email and ' +
                          ' smw.campus_upload_log_id = ' + stDataa.campusUploadLogId + ' and ' +
                          ' smw.skip_user_ind is null and smw.error is null and su.id = stu.user_id';
                          MySql(createEnrollmentRecord, function(err, enrollCreateResponse) {
                            if(err) {
                              callBack(err, null);
                            } else {
                              var selectAllRecords = 'select su.id, su.email, su.password, suw.system_password sysPassword, ' +
                              ' suw.first_name firstName, suw.last_name lastName, suw.campus_id campusId, ' +
                              ' suw.program_id programId, suw.admission_no admissionNo, suw.start_date startDate, ' +
                              ' suw.planned_completion_date planedCompletionDate from ' +
                              ' SCORA_USER su , STUDENTS_MASS_UPLOAD_WORK suw ' +
                              ' where suw.campus_upload_log_id = ' + stDataa.campusUploadLogId + ' and suw.email = su.email and suw.error is null and suw.skip_user_ind is null';
                              MySql(selectAllRecords, function(err, createdUsersResponse) {
                                if (err) {
                                  callBack(err, null);
                                } else {
                                loopCb = 1;
                                // console.log('cc', createdUsersResponse.length);
                                sendMails(createdUsersResponse)
                        // if (err) {
                        // callBack(err, null);
                        // } else {
                        callBack(null, enrollCreateResponse);
                        // }
                        // });
                            }
                          });
                      }
                    });
                }
              })
          }
        });
      }
    });
    function sendMails(totalArray) {
      if(totalArray.length > 0) {
        totalArray.map((object) => {
          var crypto = require('crypto');
          generateToken(function(err, code) {
            if (err) {
    
            } else {
              // console.log('tt', totalArray);
              var scoraUser = server.models.ScoraUser;
              // var verification = 'ABCDEFGHIJKLM' + Math.random().toString(12) + 'abcdz0123456789';
              var updateObj = {
                'verificationToken': code,
              };
              scoraUser.updateAll({'email': object.email}, updateObj, function(err, updateRes) {
                if (err) {
                  callbcc(err, null);
                } else {
                  var config = require('../server/config.json');
                  var protocol = (config.userOptions.env === 'prod') ? config.userOptions.protocol : 'http';
                  var host = (config.userOptions.env === 'prod') ? config.userOptions.host : config.host;
                  var port = (config.userOptions.env === 'prod') ? undefined : config.port;
                  var hostAndPort = (config.userOptions.env === 'prod') ? '' : host + ':' + port;
                  var url = protocol + '://' + ((config.userOptions.env === 'prod') ? host : hostAndPort) + '/verfyAccount/' + object.id + '/' + code;
                  var html = 'Click  <a href="' + url + '">here</a> to verify your account. Use this password to login: ' + object.sysPassword;
                  scoraUser.app.models.Email.send({
                    to: object.email,
                    from: config.adminEmail.email,
                    subject: 'You have been registered to ScoraXchange',
                    html: html,
                  }, function(err, response) {
                    // console.log(err);
                    if (err) console.log('email count' ,err) ;
                    // console.log('email count' ,loopCb, object, response);
                    if(loopCb === totalArray.length) {
                      return loopCb; 
                    }
                    loopCb++;
                  });
                }
              });
            }
          });
    
            // var configuration = require('../server/config.json');
            // var protocol = (configuration.userOptions.env === 'prod') ? configuration.userOptions.protocol : 'http';
            // var host = (configuration.userOptions.env === 'prod') ? configuration.userOptions.host : configuration.host;
            // var port = (configuration.userOptions.env === 'prod') ? configuration.userOptions.port : configuration.port;
            // var url = protocol + '://' + host + ':' + port + '/api/ScoraUsers';
            // var options = {
            //   type: 'email',
            //   to: object.email,
            //   from: configuration.adminEmail.email,
            //   subject: 'Thanks for registering.',
            //   template: path.resolve(__dirname, '../server/views/verify.ejs'),
            //   redirect: '/verified',
            //   user: user,
            //   html: 'Use this Password to reset: ' + object.sysPassword,
            //   host: (configuration.userOptions.env === 'prod') ? configuration.userOptions.host : undefined,
            //   port: (configuration.userOptions.env === 'prod') ? configuration.userOptions.port : undefined,
            //   protocol: (configuration.userOptions.env === 'prod') ? configuration.userOptions.protocol : undefined,
            // };
            // var scoraUser = server.models.ScoraUser;
            // scoraUser.findOne({'where': {'email': object.email}}, function(err, responseData) {
            //   if (err) {
            //     callbcc(err,  null);
            //   } else {
            //     responseData.verify(options, function(err, response) {
            //       console.log('rr', response);
            //       if (err) {
            //         callbcc(err,  null);
            //       } else {
            //         if (loopCb === 1) {
            //           callbcc(null, 'done');
            //         }
            //         loopCb++;
            //         console.log('aa', loopCb);
            //       }
            //     });
            //   }
            // });
          });
      } else {
        return null;
      }
    }
    function generateToken(callBackk) {
      var crypto = require('crypto');
      crypto.randomBytes(64, function(err, buf) {
        callBackk(err, buf && buf.toString('hex'));
      });
    }
  }
  function studentInfoCreation(obj, callBc) {
    var data = [];
    var enrollment = server.models.Enrollment;
        // console.log('input::::::::::;; ', obj);
    var userRole = server.models.ScoraUserRole;
    var userRoleObj = {
      'id': obj.id,
      'roleCode': 'STUDENT',
      'startDate': new Date(),
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
    };
    userRole.create(userRoleObj, function(roleError, roleResponse) {
                  // console.log('////////////////', roleError);
      if (roleError) {
        callBc(null, null);
      } else {
        var student = server.models.Student;
        var studentObj = {
          'id': obj.id,
          'studentStatusValueId': 246,
          'firstName': obj.firstName,
          'lastName': obj.lastName,
          'createDatetime': new Date(),
          'updateDatetime': new Date(),
          'createUserId': 1,
          'updateUserId': 1,
        };
        student.create(studentObj, function(studentErr, studentResp) {
                      // console.log('studenterrrrrrrrrrrrrrrrrrrrrrrrrrrrrr ', studentErr);
          if (studentErr) {
            throwError(studentErr, callBc);
          } else {
            var campusId = (obj.campusId) ? obj.campusId : undefined;
            var enrollmentObj = {
              'studentId': studentResp.studentId,
              'programId': obj.programId,
              'admissionNo': obj.admissionNo,
              'startDate': obj.startDate,
              'planedCompletionDate': (obj.planedCompletionDate) ? obj.planedCompletionDate : obj.planedCompletionDate,
              'dataVerifiedInd': 'Y',
              'createDatetime': new Date(),
              'updateDatetime': new Date(),
              'createUserId': 1,
              'updateUserId': 1,
              'campusId': campusId,
            };

            enrollment.create(enrollmentObj, function(enrollmentError, enrollmentResponse) {
              if (enrollmentError) {
                throwError(enrollmentError, callBc);
              } else {
                callBc(null, enrollmentResponse);
              }
            });
          }
        });
      }
    });
  }
}
function MySql(statment, sqlCb) {
  var app = require('../server/server.js');
  var connector = app.dataSources.ScoraXChangeDB.connector;
  connector.execute(statment, undefined, function(err, result) {
    if (err) {
      sqlCb(err, null);
    } else {
      sqlCb(null, result);
    }
  });
}
exports.createStudent = createStudent;

function waitTillReqEnd(rs, callback) {
  setTimeout(function() { }, 10000);
}

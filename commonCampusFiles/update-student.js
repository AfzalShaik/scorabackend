'use strict';
var server = require('../server/server');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var studentJson = require('../common/models/STUDENT.json').properties;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var student = server.models.Student;
/**
 *updateStudentService-to update student service
 *@constructor
 * @param {object} input - contains all the data need to get updated
 * @param {any} cb - deals with the response
 */
function updateStudentService(input, cb) {
  if (input) {
    var inputData = input;
    // delete input.data.dateOfBirth;
    var arr = [];
    arr.push(input);
    // console.log('arrrrrrrrrrrr', arr);
    validateModel(arr, studentJson, function(error, response) {
      if (error) {
        throwError(JSON.stringify(error), cb);
      } else {
        var lookupdata = lookupDataValidation(input);
        lookupValidation(lookupdata, function(lookupValue) {
          if (lookupValue == true) {
            var student = server.models.Student;
            student.findOne({'where': {'studentId': inputData.studentId}}, function(studentErr, studentOut) {
              if (studentOut) {
                // inputData.dateOfBirth = '2018-02-09';
                // console.log(inputData);
                inputData.updateDatetime  = new Date();
                studentOut.updateAttributes(inputData, function(updateErr, updateOut) {
                  // console.log('updateeeeeeeeeeeeeeeeeee ', updateErr, updateOut);
                  if (updateOut) {
                    cb(null, updateOut);
                  } else {
                    cb(null, null);
                  }
                });
              } else {
                cb(null, null);
              }
            });
            // findAndUpdate(inputData, function(error, output) {
            //   if (error) {
            //     throwError(JSON.stringify(error), cb);
            //   } else {
            //     cb(null, output);
            //     arr = [];
            //   }
            // });
          } else {
            throwError('Invalid prefixValueId or genderValueId or maritalStatusValueId or studentStatusValueId', cb);
            arr = [];
          }
        });
      }
    });
  } else {
    throwError('Invalid Input', cb);
  }
};

function lookupDataValidation(lookupData) {
  var studentLookups = [{
    'lookupValueId': lookupData.prefixValueId,
    'lookupTypeId': 'PREFIX_CODE',
  }, {
    'lookupValueId': lookupData.genderValueId,
    'lookupTypeId': 'GENDER_CODE',
  }, {
    'lookupValueId': lookupData.maritalStatusValueId,
    'lookupTypeId': 'MARITAL_STATUS_CODE',
  }, {
    'lookupValueId': lookupData.studentStatusValueId,
    'lookupTypeId': 'STUDENT_STATUS_CODE',
  }];
  return studentLookups;
}
/**
 *findAndUpdate-function deals with finding and updating
 *@constructor
 * @param {object} input - values to find and update
 * @param {any} cb - deals with the response
 */
function findAndUpdate(input, cb) {
  var inputFilterObject = {};
  inputFilterObject['studentId'] = input.studentId;
  findEntity(inputFilterObject, student, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response) {
      response.data[0].updateAttributes(input, function(err, info) {
        if (err) {
          throwError(err, cb);
        } else {
          logger.info('Student record Updated Successfully');
          info['requestStatus'] = true;
          info['updateDatetime'] = new Date();
          cb(null, info);
        }
      });
    } else {
      throwError('Invalid StudentId', cb);
    }
  });
};

exports.updateStudentService = updateStudentService;

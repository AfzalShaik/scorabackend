'use strict';
var async = require('async');
var server = require('../server/server');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var deleteSingleEntityDetails = require('../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;
var throwError = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var errorArray = [];
var key;
function createCampusPlacementAggregate(fileInput, inputData, callBc) {
  var logInput = {
    'campusUploadTypeValueId': inputData.campusUploadTypeValueId,
    'campusId': inputData.campusId,
    'container': inputData.fileDetails.container,
    'name': inputData.fileDetails.name,
    'originalFileName': inputData.fileDetails.originalFileName,
    'totalNoRecs': fileInput.length,
  };
  var createCampusLog = require('./create-campus-log').createCampusLog;
  createCampusLog(logInput, function(logErr, logOutput) {
    if (logErr) {

    } else {
      inputData.campusUploadLogId = logOutput.campusUploadLogId;
      async.map(fileInput, validateInput, validateCallBack);

      function validateCallBack(validErr, validResponse) {
        if (validErr) {
          throwError(validErr, callBc);
        } else {
          var validArray = cleanArray(validResponse);
          if (validArray.length > 0) {
            async.map(validArray, verifyDepartment, departmentCB);
          } else {
            aggregateCB(undefined, []);
          }
        }
      }

      function verifyDepartment(obj, departmentCallBc) {
        var department = server.models.Department;
        department.findOne({
          'where': {
            'and': [{
              'name': obj.departmentName,
            }, {
              'campusId': inputData.campusId,
            }],
          },
        }, function(deptErr, deptOut) {
          var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
          if (deptErr) {
            obj.campusUploadLogId = inputData.campusUploadLogId;
            obj.error = 'Department Does Not Exist';
            postIntoUploadWork(obj, function(error, errorResponse) {
              departmentCallBc(null, null);
            });
          } else if (deptOut) {
            obj.departmentId = deptOut.departmentId;
            departmentCallBc(null, obj);
          } else {
            obj.campusUploadLogId = inputData.campusUploadLogId;
            obj.error = 'Department Does Not Exist';
            postIntoUploadWork(obj, function(error, errorResponse) {
              departmentCallBc(null, null);
            });
          }
        });
      }

      function verifyProgram(obj, programCallBc) {
        var program = server.models.Program;
        program.findOne({
          'where': {
            'and': [{
              'programName': obj.programName,
            }, {
              'campusId': inputData.campusId,
            }],
          },
        }, function(programErr, programOut) {
          var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
          if (programErr) {
            obj.campusUploadLogId = inputData.campusUploadLogId;
            obj.error = 'Program Does Not Exist';
            postIntoUploadWork(obj, function(error, errorResponse) {
              programCallBc(null, null);
            });
          } else if (programOut) {
            obj.programMajorValueId = programOut.programMajorValueId;
            obj.programId = programOut.programId;
            programCallBc(null, obj);
          } else {
            obj.campusUploadLogId = inputData.campusUploadLogId;
            obj.error = 'Program Does Not Exist';
            postIntoUploadWork(obj, function(error, errorResponse) {
              programCallBc(null, null);
            });
          }
        });
      }

      function postIntoUploadWork(object, cb) {
        // var count = 0;
        var input = {};
        input = {
          'rowNumber': object.rowNumber,
          'departmentName': object.departmentName,
          'programName': object.programName,
          'noOfOffers': object.noOfOffers,
          'academicYear': object.acedemicYear,
          'noOfStudents': object.noOfStudents,
          'totalOffers': object.totalOffers,
          'minimumOffer': object.minimumOffer,
          'maximumOffer': object.maximumOffer,
          'top5': object.top5,
          'sixToTen': object.sixToTen,
          'levenTo20': object.levenTo20,
          'twentyOneTo50': object.twentyOneTo50,
          'above50': object.above50,
          'error': object.error,
          'programId': object.programId,
          'departmentId': object.departmentId,
          'campusUploadLogId': inputData.campusUploadLogId,
        };
        var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
        campusPlacementAggregatesWork.create(input, function(err, uploadResponse) {
          if (err) {
            errorArray.push(input);
            cb(null, null);
          } else {
            cb(null, uploadResponse);
          }
        });
      }

      function programCB(programErr, programOutput) {
        var departmentResponse = cleanArray(programOutput);
        async.map(departmentResponse, verifyValidation, validationCB);
      }

      function departmentCB(deparmentErr, departmentOutput) {
        var departmentResponse = cleanArray(departmentOutput);
        async.map(departmentResponse, verifyProgram, programCB);
      }

      function validationCB(validationErr, validationOutput) {
        var validationResponse = cleanArray(validationOutput);
        var inputFilterObject = {};
        inputFilterObject['campusId'] = inputData.campusId;
        var campusPlacementAggregates = server.models.CampusPlacementAggregates;
        var campus = server.models.Campus;
        deleteSingleEntityDetails(inputFilterObject, campusPlacementAggregates, function(deletedErr, deletedRecords) {
          campus.findOne({
            'where': {
              'campusId': inputData.campusId,
            },
          }, function(campusErr, campusResponse) {
            var universityId = campusResponse.universityId;
            async.map(validationResponse, createAggregate, aggregateCB);

            function createAggregate(obj, aggregateCallBC) {
              var input = {
                'campusId': inputData.campusId,
                'universityId': universityId,
                'departmentId': obj.departmentId,
                'programId': obj.programId,
                'noOfStudents': obj.noOfStudents,
                'noOfOffers': obj.noOfOffers,
                'totalOffers': obj.totalOffers,
                'minimumOffer': obj.minimumOffer,
                'maximumOffer': obj.maximumOffer,
                'academicYear': obj.acedemicYear,
                // 'top5': obj.top5,
                // 'sixToTen': obj.sixToTen,
                // 'levenTo20': obj.levenTo20,
                // 'twentyOneTo50': obj.twentyOneTo50,
                // 'above50': obj.above50,
              };
              campusPlacementAggregates.create(input, function(createErr, createOut) {
                aggregateCallBC(null, createOut);
              });
            }
          });
        });
      }

      function aggregateCB(aggError, aggregateOutput) {
        var aggregateResponse = cleanArray(aggregateOutput);
        var createCampusLog = require('./create-campus-log').createCampusLog;
        var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
        // var readError = require('../commonValidation/read-error-into-file').readErrorRecords;
        campusPlacementAggregatesWork.find({}, function(findErr, failedOutput) {
          if (findErr) {
            campusPlacementAggregatesWork.destroyAll({
              'campusUploadLogId':
                inputData.campusUploadLogId,
            }, function(error, destroy) {
              callBc(null, failedOutput);
            });
          } else {
            // readError(failedOutput, inputData, function(readError, readResponse) {
            getfailureMass(failedOutput, function(err, failedArray) {
              var container = inputData.fileDetails.container;
              var logInput = {
                'totalNoRecs': fileInput.length,
                'noFailRecs': cleanArray(failedOutput).length,
                'noSuccessRecs': aggregateResponse.length,
                'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + key,
              };
              var campusUploadLog = server.models.CampusUploadLog;
              campusUploadLog.updateAll({
                'campusUploadLogId':
                  inputData.campusUploadLogId,
              }, logInput, function(err, response) {
                if (err) {
                  campusPlacementAggregatesWork.destroyAll({
                    'campusUploadLogId':
                      inputData.campusUploadLogId,
                  }, function(error, destroy) {
                    callBc(null, failedOutput);
                  });
                } else {
                  campusPlacementAggregatesWork.destroyAll({
                    'campusUploadLogId':
                      inputData.campusUploadLogId,
                  }, function(error, destroy) {
                    callBc(null, logOutput);
                  });
                }
              });
            });
          }
        });
      }

      function verifyValidation(object, verifyCallBack) {
        if (parseInt(object.noOfOffers) > 0 && parseInt(object.noOfStudents) > 0 && parseInt(object.totalOffers) > 0 && parseInt(object.minimumOffer) > 0 && parseInt(object.maximumOffer) > 0 && parseInt(object.top5) > 0 && parseInt(object.sixToTen) > 0 && parseInt(object.levenTo20) > 0 && parseInt(object.twentyOneTo50) > 0 && parseInt(object.above50) > 0 && (parseInt(object.maximumOffer) > parseInt(object.minimumOffer)) && (parseInt(object.noOfStudents) > parseInt(object.noOfOffers))) {
          checkSalary(object, function(salaryErr, salaryOut) {
            if (salaryOut) {
              verifyCallBack(null, object);
            } else {
              object.error = 'Validation Failed';
              postIntoUploadWork(object, function(error, errorResponse) {
                verifyCallBack(null, null);
              });
            }
          });
        } else {
          object.error = 'Validation Failed';
          postIntoUploadWork(object, function(error, errorResponse) {
            verifyCallBack(null, null);
          });
        }
      }

      function validateInput(validateObj, validateCB) {
        var validateUpload = require('../commonValidation/campus-placement-aggregate-validation').validateJson;
        validateUpload(validateObj, function(validationErr, validationResponse) {
          var lenghtVar = getlength(validateObj.acedemicYear);
          var numberVar = isNumeric(validateObj.acedemicYear);
          if (validationErr) {
            validateObj.error = validationErr.err;
            postIntoUploadWork(validateObj, function(postErr, postResp) {
              if (postErr) {
                errorArray.push(validateObj);
                validateCB(null, null);
              } else {
                validateCB(null, null);
              }
            });
          } else if (lenghtVar === 4 && numberVar == true) {
            validateObj.campusUploadLogId = inputData.campusUploadLogId;
            validateCB(null, validateObj);
          } else {
            validateObj.error = 'Invalid Acedemic Year';
            postIntoUploadWork(validateObj, function(postErr, postResp) {
              if (postErr) {
                errorArray.push(validateObj);
                validateCB(null, null);
              } else {
                validateCB(null, null);
              }
            });
          }
        });
      }

      function getfailureMass(iterateArray, cb) {
        var failedArray = [];
        var finalArray = [];
        failedArray = (iterateArray) ? iterateArray : [];
        if (errorArray.length > 0) {
          for (var j = 0; j < errorArray.length; j++) {
            failedArray.push(errorArray[j]);
          }
        } else {
          failedArray = iterateArray;
        }
        for (var i = 0; i < failedArray.length; i++) {
          var obj = {
            'rowNumber': failedArray[i].rowNumber,
            'departmentName': failedArray[i].departmentName,
            'programName': failedArray[i].programName,
            'noOfOffers': failedArray[i].noOfOffers,
            'academicYear': failedArray[i].acedemicYear,
            'error': failedArray[i].error,
            'noOfStudents': failedArray[i].noOfStudents,
            'totalOffers': failedArray[i].totalOffers,
            'minimumOffer': failedArray[i].minimumOffer,
            'maximumOffer': failedArray[i].maximumOffer,
            'top5': failedArray[i].top5,
            'sixToTen': failedArray[i].sixToTen,
            'levenTo20': failedArray[i].levenTo20,
            'twentyOneTo50': failedArray[i].twentyOneTo50,
            'above50': failedArray[i].above50,
          };
          finalArray.push(obj);
        }
        var fs = require('fs');
        var csv = require('fast-csv');
        var name = inputData.fileDetails.name;
        var container = inputData.fileDetails.container;
        var pathForm = require('path');
        // var errorCsv = './attachments/' + container + '/' + name;
        // errorCsv = pathForm.join(__dirname, '../../../', errorCsv);
        var uploadCsv = require('./commonCSVUpload.js').uploadCSV;
        var campus = server.models.Campus;
        campus.findOne({ 'where': { 'campusId': inputData.campusId } }, function(campusErr, campusOut) {
          var fs = require('fs');
          var csv = require('fast-csv');
          var json2csv = require('json2csv');
          var fileName;
          fileName = campusOut.name;
          var randomNumber = Math.random().toString(20).slice(-10);
          var errorFile = campusOut.name + '_' + randomNumber + '.csv';
          fileName = './commonValidation/' + campusOut.name + '.csv';
          var fields = ['rowNumber', 'departmentName', 'programName', 'noOfOffers', 'academicYear', 'noOfStudents', 'totalOffers', 'minimumOffer', 'maximumOffer', 'top5', 'sixToTen', 'levenTo20', 'twentyOneTo50', 'above50', 'error'];
          var csv = json2csv({
            data: finalArray,
            fields: fields,
          });
          fs.writeFile(fileName, csv, function(err) {
            if (err) throw err;
            uploadCsv(fileName, container, errorFile, function(fileErr, fileOut) {
              // console.log('----------------------- ', fileOut);
              // cb(null, 'success');
              // var container = inputData.fileDetails.container;
              var logInput = {
                'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + key,
              };
              var campusUploadLog = server.models.CampusUploadLog;
              var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
              campusUploadLog.updateAll({
                'campusUploadLogId':
                  inputData.campusUploadLogId,
              }, logInput, function(err, response) {
                if (err) {
                  campusPlacementAggregatesWork.destroyAll({
                    'campusUploadLogId':
                      inputData.campusUploadLogId,
                  }, function(error, destroy) {
                    cb(null, failedOutput);
                  });
                } else {
                  campusPlacementAggregatesWork.destroyAll({
                    'campusUploadLogId':
                      inputData.campusUploadLogId,
                  }, function(error, destroy) {
                    cb(null, 'success');
                  });
                }
              });
              errorArray = [];
              key = fileOut;
            });
          });
        });
       
        //   var ws = fs.createWriteStream(errorCsv);
        //   csv
        // .writeToPath(errorCsv, finalArray, {
        //   headers: true,
        // })
        // .on('finish', function() {
        // });
        //   cb(null, 'success');
        //   errorArray = [];
      }
      function getlength(number) {
        return number.length;
      }
      function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
      }
      function checkSalary(object, salaryCB) {
        if ((parseInt(object.top5) >= parseInt(object.sixToTen)) && (parseInt(object.sixToTen) >= parseInt(object.levenTo20)) && (parseInt(object.levenTo20) >= parseInt(object.twentyOneTo50)) && (parseInt(object.twentyOneTo50) >= parseInt(object.above50))) {
          salaryCB(null, object);
        } else {
          object.error = 'Salary Validation Failed';
          // postIntoUploadWork(object, function(error, errorResponse) {
          salaryCB(null, null);
          // });
        }
      }
    }
  });
}
exports.createCampusPlacementAggregate = createCampusPlacementAggregate;

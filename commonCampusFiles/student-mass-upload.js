'use strict';
var cleanArray = require('../commonValidation/common-mass-upload').cleanArray;
var studentMassJson = require('../common/models/STUDENTS_MASS_UPLOAD_WORK.json');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateJsonInput;
var server = require('../server/server');
var async = require('async');
var count = 0;
var errorArray = [];
var repeated = [];
var final = [];
var finalNonRepeat = [];
var key;
var sucess = 0;
function createMassUpload(inputArray, stDataa, callBc) {
  var campusUploadLog = server.models.CampusUploadLog;
  var studentsMassUploadWork = server.models.StudentsMassUploadWork;
  var details = stDataa.fileDetails;
  var container = stDataa.fileDetails.container;
  var pathForm = require('path');
  var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
  var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + key;
  var logInput = {
    'campusUploadTypeValueId': stDataa.campusUploadTypeValueId,
    'campusId': stDataa.campusId,
    'inputParameters': 'string',
    'createUserId': 1,
    'createDatetime': new Date(),
    'csvFileLocation': csvFileLocation,
    'errorFileLocation': errorFileLocation,
    'totalNoRecs': inputArray.length,
  };
  campusUploadLog.create(logInput, function (logErr, logOutput) {
    console.log('ss', logErr);
    studentsMassUploadWork.destroyAll({}, function (err, res) { });
    async.map(inputArray, validateInput, function (error, response) {
      if (error) {
        throwError('Invalid DataTypes', callBc);
      } else {
        console.log('response', response);
        finalNonRepeat = [];
        var validatedResponse = cleanArray(response);
        // // console.log('//////////////////////', validatedResponse);
        // getNonRepeatData(validatedResponse, function (err, res) {
        //   // console.log('hewarrrrrrrr');
        //   if (err) {
        //     throwError('an error', callBc);
        //   } else {
            async.map(validatedResponse, getFinalData, function (err1, res1) {
              // console.log('in hear');
              if (err1) {
                callBc(err1, null);
              } else {
                var studentsMassUploadWork = server.models.StudentsMassUploadWork;
                studentsMassUploadWork.destroyAll({
                  'where': {
                    'campusUploadLogId':
                      logOutput.campusUploadLogId
                  }
                }, function (derr, dres) {
                  if (derr) {
                    callBc(derr, null);
                  } else {
                    async.map(repeated, insertIntoTemp, function (err2, res2) {
                      if (finalNonRepeat.length > 0) {
                        // console.log('final not repppppppppp', finalNonRepeat);
                        async.map(finalNonRepeat, getEmailValid, function (uploadErr, workData) {
                          if (uploadErr) {
                            throwError(uploadErr, callBc);
                          } else {
                            stDataa.campusUploadLogId = logOutput.campusUploadLogId;
                            // console.log('workkkkkkkkk', workData);
                            var workDataArray = cleanArray(workData);
                            if (workDataArray.length > 0) {
                              async.map(workDataArray, getAdmissionNumberValid, function (adminErr, adminResponse) {
                                // console.log(adminResponse);
                                var createStudent = require('./create-student-mass-upload').createStudent;
                                var finalArray = cleanArray(adminResponse);
                                console.log('fffffffffffffffffff', finalArray);
                                // console.log('fffffffffffffffffff', finalArray.length);
                                createStudent(finalArray, stDataa, function (finalErr, finalOutput) {
                                  if (finalErr) {
                                    throwError(finalErr, callBc);
                                  } else {
                                    console.log('finall', finalOutput);
                                    sucess = finalArray.length;
                                    commonErrorRead(finalOutput, function (err, output) {
                                      callBc(null, output);
                                    });
                                  }
                                });
                              });
                            } else {
                              // console.log('...................................................');
                              commonErrorRead(undefined, function (err, output) {
                                callBc(null, output);
                              });
                            }
                          }
                        });
                      } else {
                        // var studentsMassUploadWork = server.models.StudentsMassUploadWork;
                        // studentsMassUploadWork.destroyAll({}, function(error, destroy) {
                        //   throwError('Invalid File', callBc);
                        // });
                        // console.log('in last else');
                        commonErrorRead(undefined, function (err, output) {
                          callBc(null, output);
                        });
                      }
                    });
                  }
                });
              }
            });
          // }

          function getFinalData(obj, cb) {
            var studentsMassUploadWork = server.models.StudentsMassUploadWork;
            studentsMassUploadWork.find({
              'where': {
                'or': [{
                  'email': obj.email,
                },
                {
                  'admissionNo': obj.admissionNo,
                },
                ],
              },
            }, function (err4, res4) {
              console.log('length', res4, err4);
              // console.log(res4.length === 1, res4.length > 1);
              if (res4.length === 1) {
                finalNonRepeat.push(obj);
                cb(null, 'done');
              } else if (res4.length > 1) {
                repeated.push(obj);
                cb(null, 'done');
              } else {
                finalNonRepeat.push(obj);
                cb(null, 'done');
              }
            });
          }
        // });

        function getNonRepeatData(array, cb) {
          var studentsMassUploadWork = server.models.StudentsMassUploadWork;
          studentsMassUploadWork.create(array, function (err, res) {
            cb(null, res);
          });
        }

        function seeForRep(array, data, callBack) {
          var count = 0;
          for (var i = 0; i < array.length; i++) {
            if (array[i].admissionNo == data.admissionNo || array[i].email == data.email) {
              count++;
              // console.log('this is data', data.admissionNo, array[i].admissionNo,
              // (data.admissionNo == array[i].admissionNo || data.email == array[i].email));
              // console.log('this is count', count, 'main array::::::: ', array[i], 'dataaaaaaaaaa', data);
              if (count > 1) {
                // console.log('repeatedddddddddd:::::;; ', array[i]);
                repeated.push(array[i]);
                // repeated.push(data);
                // postIntoUploadWork(array[i], function(err, rs) {
                //   //callBack(null, null);

                // })
              }
            }
            if (i == array.length - 1) {
              callBack(null, array);
            }
          }
        }

        function insertIntoTemp(obj, callbc) {
          console.log('non repeated', obj);
          obj.err = 'duplicate';
          errorArray.push(obj);
          // postIntoUploadWork(obj, function (err3, res3) {
            // console.log('......................... ', res3);
            callbc(null, 'done');
          // });
        }
      }
    });

    function postIntoUploadWork(object, cb) {
      // var count = 0;
      var input = {};
      input = {
        'rowNumber': object.rowNumber,
        'firstName': object.firstName,
        'lastName': object.lastName,
        'email': object.email,
        'admissionNo': object.admissionNo,
        'startDate': object.startDate,
        'planedCompletionDate': object.planedCompletionDate,
        // 'skipUserInd': object.skipUserInd,
        'programId': stDataa.programId,
        'campusId': stDataa.campusId,
        'departmentId': stDataa.departmentId,
        'error': object.error,
        'campusUploadLogId': logOutput.campusUploadLogId,
      };
      // console.log(input);
      var studentsMassUploadWork = server.models.StudentsMassUploadWork;
      studentsMassUploadWork.create(input, function (err, uploadResponse) {
        // console.log('....................... ', err, uploadResponse);
        if (err) {
          throwError(err, cb);
        } else {
          cb(null, uploadResponse);
        }
      });
    }
    exports.postIntoUploadWork = postIntoUploadWork;

    function getEmailValid(obj, callBack) {
      var scoraUser = server.models.ScoraUser;
      var student = server.models.Student;
      var enrollment = server.models.Enrollment;
      scoraUser.findOne({
        'where': {
          'email': obj.email,
        },
      }, function (err, userResult) {
        if (err) {
          obj.error = err;
          errorArray.push(obj);
          // postIntoUploadWork(obj, function (error, errorResponse) {
            callBack(null, null);
          // });
        } else if (userResult) {
          obj.error = 'Email Already Exist';
          // if (obj.skipUserInd == 'Y' || obj.skipUserInd == 'y') {
          student.findOne({
            'where': {
              'id': userResult.id,
            },
          }, function (studentErr, studentInfo) {
            if (studentErr) {
              obj.error = studentErr;
              errorArray.push(obj);
              // postIntoUploadWork(obj, function (error, errorResponse) {
                callBack(null, null);
              // });
            } else if (studentInfo) {
              enrollment.findOne({
                'where': {
                  'and': [{
                    'programId': obj.programId,
                  }, {
                    'studentId': studentInfo.studentId,
                  }],
                },
              }, function (enrollErr, enrollInfo) {
                if (enrollErr) {
                  obj.error = enrollErr;
                  errorArray.push(obj);
                  // postIntoUploadWork(obj, function (error, errorResponse) {
                    callBack(null, null);
                  // });
                } else if (enrollInfo) {
                  obj.error = 'Email Already Registered For Given Program';
                  errorArray.push(obj);
                  // postIntoUploadWork(obj, function (error, errorResponse) {
                    callBack(null, null);
                  // });
                } else {
                  obj.studentId = studentInfo.studentId;
                  obj.skipUserInd = 'Y';
                  validateDepartment(obj, function (deptErr, deptOut) {
                    if (deptOut) {
                      obj.error = 'Student Already Enrolled Under Given Department';
                      errorArray.push(obj);
                      // postIntoUploadWork(obj, function (error, errorResponse) {
                        callBack(null, null);
                      // });
                    } else {
                      callBack(null, obj);
                    }
                  });
                }
              });
            } else {
              obj.error = 'Email Already Exist';
              errorArray.push(obj);
              // postIntoUploadWork(obj, function (error, errorResponse) {
                callBack(null, null);
              // });
            }
          });
          // } else {
          //   obj.error = 'Email Already Exist';
          //   postIntoUploadWork(obj, function(error, errorResponse) {
          //     callBack(null, null);
          //   });
          // }
        } else {
          obj.error = 'no error';
          callBack(null, obj);
        }
      });
    }

    function getAdmissionNumberValid(obj, cBack) {
      var enrollment = server.models.Enrollment;
      enrollment.findOne({
        'where': {
          'and': [ {
            'campusId': obj.campusId,
          }, {
            'admissionNo': obj.admissionNo,
          }
          ],
        },
      }, function (err, adminResult) {
        if (err) {
          obj.error = err;
          // postIntoUploadWork(obj, function (error, errorResponse) {
            errorArray.push(obj);
            cBack(null, null);
          // });
        } else if (adminResult) {
          // if (obj.skipUserInd == 'Y' || obj.skipUserInd == 'y') {
          //   cBack(null, obj);
          // } else {
            if (adminResult.programId == obj.programId) {
              obj.error = 'Admission Number Already Registered for given Program';
              postIntoUploadWork(obj, function (error, errorResponse) {
                cBack(null, null);
              });
            } else {
              var student = server.models.Student;
              student.findOne({'where' : {'studentId' : adminResult.studentId}}, function(studentErr, studentOut) {
                var userModel = server.models.ScoraUser;
                userModel.findOne({'where' : {'id' : studentOut.id}}, function(userErr, userOut) {
                  if (userOut.email == obj.email) {
                    cBack(null, obj);
                  } else {
                    obj.error = 'Admission Number Already Exist With Other EmailId';
                    postIntoUploadWork(obj, function (error, errorResponse) {
                      cBack(null, null);
                    });
                  }
                });
              });
            }
          // }
        } else {
          obj.error = 'no error';
          cBack(null, obj);
        }
      });
    }

    function getfailureMass(iterateArray, cb) {
      var failedArray = [];
      var finalArray = [];
      // console.log('eeeeeeeeeeeeeeee', errorArray);
      // console.log(iterateArray);
      failedArray = iterateArray;
      if (errorArray.length > 0) {
        for (var j = 0; j < errorArray.length; j++) {
          // console.log();
          failedArray.push(errorArray[j]);
          // console.log(failedArray);
        }
      } else {
        failedArray = iterateArray;
      }
      for (var i = 0; i < failedArray.length; i++) {
        // console.log('starttttttttttttttttt ', failedArray[i].startDate, failedArray[i].plannedCompletionDate);
        var startDate = new Date(failedArray[i].startDate);
        var month = (startDate.getMonth() == 12) ? (startDate.getMonth()) : (startDate.getMonth() + 1);
        startDate = (startDate.getDate()) + '/' + month + '/' + startDate.getFullYear();
        var planedCompletionDate = (failedArray[i].planedCompletionDate) ? failedArray[i].planedCompletionDate : failedArray[i].planedCompletionDate;
        // console.log('pllllllllllllllllllllll ', plannedCompletionDate);
        planedCompletionDate = new Date(planedCompletionDate);
        var months = (planedCompletionDate.getMonth() == 12) ? (planedCompletionDate.getMonth()) : (planedCompletionDate.getMonth() + 1);
        planedCompletionDate = (planedCompletionDate.getDate()) + '/' + months + '/' + planedCompletionDate.getFullYear();
        var obj = {
          'rowNumber': failedArray[i].rowNumber,
          'firstName': failedArray[i].firstName,
          'lastName': failedArray[i].lastName,
          'email': failedArray[i].email,
          'admissionNo': failedArray[i].admissionNo,
          'error': (failedArray[i].error) ? failedArray[i].error : 'Duplicate Admission Number or EmailId',
          // 'skipUserInd': failedArray[i].skipUserInd,
          'startDate': (startDate == 'NaN/NaN/NaN') ? failedArray[i].startDate : startDate,
          'planedCompletionDate': (planedCompletionDate == 'NaN/NaN/NaN') ? failedArray[i].planedCompletionDate : planedCompletionDate,
        };
        finalArray.push(obj);
      }
      var fs = require('fs');
      var csv = require('fast-csv');
      var name = stDataa.fileDetails.name;
      var container = stDataa.fileDetails.container;
      var pathForm = require('path');
      var json2csv = require('json2csv');
      var datasource = require('../server/datasources.json');
      var errorCsv = './attachments/' + container + '/' + name;
      errorCsv = pathForm.join(__dirname, '../../../', errorCsv);
      var campus = server.models.Campus;
      campus.findOne({ 'where': { 'campusId': stDataa.campusId } }, function (campusErr, campusOut) {
        var fs = require('fs');
        var csv = require('fast-csv');
        var fileName;
        fileName = campusOut.name;
        var randomNumber = Math.random().toString(20).slice(-10);
        var errorFile = campusOut.name + '_' + randomNumber + '.csv';
        fileName = './commonValidation/' + campusOut.name + '.csv';
        var fields = ['rowNumber', 'firstName', 'lastName', 'email', 'admissionNo', 'startDate', 'planedCompletionDate', 'error'];
        var csv = json2csv({
          data: finalArray,
          fields: fields,
        });
        fs.writeFile(fileName, csv, function (err) {
          if (err) throw err;
        });
        var AWS = require('aws-sdk');
        var fs = require('fs');
        AWS.config.region = datasource.amazonupload.region;
        AWS.config.update({ accessKeyId: datasource.amazonupload.keyId, secretAccessKey: datasource.amazonupload.key });

        // Read in the file, convert it to base64, store to S3
        fs.readFile(fileName, function (err, data) {
          if (err) { throw err; }
          // console.log(data, '.........................', errorFile);
          var base64data = new Buffer(data, 'binary');

          var s3 = new AWS.S3();
          var s3 = new AWS.S3({ params: { Bucket: container } });
          var params = {
            Key: errorFile, //file.name doesn't exist as a property
            Body: data,
          };
          s3.upload(params, function (err, data) {
            // console.log('ddddddddddddddddddddddddddddd ', err, data.key);
            key = data.key;
            fs.unlink(fileName, (err) => {
              if (err) throw err;
              // console.log('successfully deleted');
              cb(null, finalArray);
              errorArray = [];
              repeated = [];
              finalNonRepeat = [];
            });
          });
        });
      });
    }

    function createEnrollment(obj, studentResp, enrollCallBck) {
      var enrollment = server.models.Enrollment;
      var enrollmentObj = {
        'studentId': studentResp.studentId,
        'programId': obj.programId,
        'admissionNo': obj.admissionNo,
        'startDate': obj.startDate,
        'planedCompletionDate': obj.planedCompletionDate,
        'dataVerifiedInd': 'Y',
        'createDatetime': new Date(),
        'updateDatetime': new Date(),
        'createUserId': 1,
        'updateUserId': 1,
        'campusId': obj.campusId,
      };
      enrollment.create(enrollmentObj, function (enrollError, enrollInfo) {
        enrollCallBck(null, enrollInfo);
      });
    }

    function validateInput(validateObj, validateCB) {
      console.log('oooo', validateObj);
      var bcrypt = require('bcrypt');
      var globalPassword = Math.random().toString(12).slice(-6);
      var encryptPass = bcrypt.hashSync(globalPassword, 10);
      validateObj.systemPassword = (globalPassword);
      validateObj.password = encryptPass;
      validateObj.createUserId = 1;
      validateObj.updateUserId = 1;
      validateObj.campusUploadLogId = logOutput.campusUploadLogId;
      var studentMassUpload = server.models.StudentsMassUploadWork;
      studentMassUpload.createData(validateObj, function(err, createdResponse) {
        if(err) {
          errorArray.push(err);
          validateCB(null, null);
        } else {
          validateCB(null, validateObj);
        }
      })
    }
      // var validateMassUpload = require('../commonValidation/student-mass-upload-validation').validateJson;
      // validateMassUpload(validateObj, function (validationErr, validationResponse) {
      //   if (validationErr) {
      //     if (validationErr.err == '"startDate" must be a number of milliseconds or valid date string') {
      //       // console.log('danceeeeeeeeeeeeeeeeeeeee ', validationErr);
      //       validateObj.error = 'Date Format Should be DD/MM/YYYY';
      //     } else if (validationErr.err == '"planedCompletionDate" must be a number of milliseconds or valid date string') {
      //       validateObj.error = 'Date Format Should be DD/MM/YYYY';
      //     } else {
      //       validateObj.error = validationErr.err;
      //     }
      //     errorArray.push(validateObj);
      //     // postIntoUploadWork(validateObj, function(postErr, postResp) {
      //     //    console.log('validation:::::::::::::::::; ', postErr, postResp);
      //     //   if (postErr) {
      //     //     errorArray.push(validateObj);
      //     //     // console.log('validation:::::::::::::::::; ', errorArray);
      //     //     validateCB(null, null);
      //     //   } else {
      //     //     // console.log('PPPPPPPPPPPPPPPPPPPPP', postResp);
      //     validateCB(null, null);
      //     //   }
      //     // });
      //   } else {
      //     validateCB(null, validateObj);
      //   }
      // });
    // }

    function commonErrorRead(finalOutput, readCallBc) {
      var studentsMassUploadWork = server.models.StudentsMassUploadWork;
      studentsMassUploadWork.find({ 'where': { 'campusUploadLogId': logOutput.campusUploadLogId } }, function (workErr, workResponse) {
        // console.log('work', workResponse);
        if (workErr) {
          // console.log('111111111111111111111111111111111111111');
          studentsMassUploadWork.destroyAll({ 'where': { 'campusUploadLogId': logOutput.campusUploadLogId } },
            function (error, destroy) {
              throwError(error, callBc);
            });
        } else {
          var campusUploadLog = server.models.CampusUploadLog;
          var studentsMassUploadWork = server.models.StudentsMassUploadWork;
          var details = stDataa.fileDetails;
          var container = stDataa.fileDetails.container;
          // console.log('........................ ', workResponse, '-------------');
          getfailureMass([], function (err, failedArray) {
            var pathForm = require('path');
            var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
            // csvFileLocation = pathForm.join(__dirname, '../../../', csvFileLocation);
            var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + key;
            // errorFileLocation = pathForm.join(__dirname, '../../../', errorFileLocation);
            // console.log(',,,,,,,,,,,,,,,,,,,,2 pathsssssssss,,,,,,,,,,,,,,,, ',  csvFileLocation);
            var logInput = {
              'errorFileLocation': errorFileLocation,
              'totalNoRecs': inputArray.length,
              'noFailRecs': cleanArray(failedArray).length,
              'noSuccessRecs': (sucess) ?(sucess) : 0,
              'errorFileLocation' : errorFileLocation,
              'csvFileLocation' : csvFileLocation
            };
            campusUploadLog.updateAll({ 'campusUploadLogId': logOutput.campusUploadLogId }, logInput, function (logErr, logOutput) {
              studentsMassUploadWork.destroyAll({
                'campusUploadLogId':
                  logOutput.campusUploadLogId
              }, function (error, destroy) {
                readCallBc(null, logOutput);
              });
            });
          });
        }
      });
    }
    function validateDepartment(obj, deptCB) {
      var program = server.models.Program;
      program.findOne({ 'where': { 'programId': obj.programId } }, function (programErr, programOut) {
        program.find({ 'where': { 'departmentId': programOut.departmentId } }, function (deptErr, deptOut) {
          // console.log('deptOutdeptOutdeptOutdeptOutdeptOut ', deptOut);
          if (deptOut.length == 1) {
            deptCB(null, null);
          } else {
            async.map(deptOut, validateProgram, function (enrollErr, enrollOut) {
              var out = [];
              out = cleanArray(enrollOut);
              // console.log('outtttttttt ', out);
              if (out.length > 0) {
                deptCB(null, obj);
              } else {
                deptCB(null, null);
              }
            });
          }
        });
      });
      function validateProgram(enrollObj, enrollCB) {
        var enrollment = server.models.Enrollment;
        enrollment.findOne({
          'where': {
            'and': [{
              'programId': enrollObj.programId,
            }, {
              'studentId': obj.studentId,
            }],
          },
        }, function (enrollErr, enrollOutt) {
          // console.log('enrollouttttttttttttttt ', enrollOutt);
          // console.log('enrollObj.programId ', enrollObj.programId);
          // console.log('obj.studentId ', obj.studentId);
          if (enrollOutt) {
            enrollCB(null, obj);
          } else {
            enrollCB(null, null);
          }
        });
      }
    }
  });
}
exports.createMassUpload = createMassUpload;

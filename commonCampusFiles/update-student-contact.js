'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var studentJson = require('../common/models/STUDENT_CONTACT.json').properties;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var findAndUpdate = require('../commonValidation/find-and-update-contact').findAndUpdate;
var studentContact = server.models.StudentContact;
var async = require('async');
  /**
   *updateStudentContactService-function deals with updating the student contact service
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateStudentContactService(input, cb) {
  if (input.length > 0) {
    validateModel(input, studentJson, function(error, response) {
      if (error) {
        throwError(JSON.stringify(error), cb);
      } else {
        async.map(input, getInfo, function(e, r) {
          if (e) {
            cb(e, null);
          } else {
            logger.info('Student Contact updated successfully');
            cb(null, r);
          }
        });
      }
    });
  } else {
    throwError('Invalid Input', cb);
  }
}

function getInfo(obj, callback) {
  if (obj.studentId && obj.contactId) {
    var findData = {};
    findData['studentId'] = obj.studentId;
    findData['contactId'] = obj.contactId;
    findAndUpdate(obj, findData, studentContact, function(err, output) {
      if (err) {
        callback(err, null);
      } else {
        callback(null, output);
      }
    });
  } else {
    callback('studentId and contactId are required', null);
  }
}
exports.updateStudentContactService = updateStudentContactService;

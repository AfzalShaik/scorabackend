'use strict';

function uploadCSV(fileName, container, errorFile, cb) {
  var fs = require('fs');
  var csv = require('fast-csv');
  var json2csv = require('json2csv');
  var datasource = require('../server/datasources.json');
  var AWS = require('aws-sdk');
  var fs = require('fs');
  AWS.config.region = datasource.amazonupload.region;
  AWS.config.update({accessKeyId: datasource.amazonupload.keyId, secretAccessKey: datasource.amazonupload.key});
  fs.readFile(fileName, function (err, data) {
    if (err) { throw err; }
    // console.log(data, '.........................', err);
    var base64data = new Buffer(data, 'binary');

    var s3 = new AWS.S3();
    var s3 = new AWS.S3({ params: { Bucket: container } });
    var params = {
      Key: errorFile, //file.name doesn't exist as a property
      Body: data,
    };
    s3.upload(params, function (err, data) {
      // console.log('ddddddddddddddddddddddddddddd ', err, data);
     var key = data.key;
      fs.unlink(fileName, (err) => {
        if (err) throw err;
        // console.log('successfully deleted', key, err);
        cb(null, key);
        // errorArray = [];
        // repeated = [];
        // finalNonRepeat = [];
      });
    });
  });
}
exports.uploadCSV = uploadCSV;

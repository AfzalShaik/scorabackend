'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//employer drive
describe(' employer drive Cases ;', function() {
  it('employer drive POST returns 200', function(done) {
    var input = {
      'companyId': 1007,
      'organizationId': 56788,
      'driveName': 'employer drive second',
      'description': 'first emp drive description',
      'driveTypeValueId': 299,
      'jobRoleId': 3334,
      'noOfPositions': 57,
      'budget': '7878.4',
      'startDate': '2017-08-21T00:00:00.000Z',
      'endDate': '2017-08-21T00:00:00.000Z',
      'driveStatusValueId': 306,
      'createDatetime': '2017-08-21T07:34:31.000Z',
      'createUserId': 1,
    };
    var url = 'http://localhost:3000/api/EmployerDrives';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //employer drive  put
  it('employer drive PUT returns 200', function(done) {
    var input = {
      'empDriveId': 65656,
      'companyId': 1007,
      'organizationId': 56788,
      'driveName': 'employer drive first',
      'description': 'first emp drive description',
      'driveTypeValueId': 301,
      'jobRoleId': 3334,
      'noOfPositions': 57,
      'budget': '7878.4',
      'startDate': '2017-08-21T00:00:00.000Z',
      'endDate': '2017-08-21T00:00:00.000Z',
      'driveStatusValueId': 306,
      'createDatetime': '2017-08-21T07:34:31.000Z',
      'updateDatetime': '2017-08-21T07:34:31.000Z',
      'createUserId': 1,
      'updateUserId': 1,
    };
    var url = 'http://localhost:3000/api/EmployerDrives/updateEmployerDrive';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //employer drive get
  it('employer drive GET returns 200', function(done) {
    var url = 'http://localhost:3000/api/EmployerDrives/getEmployerDriveDetails?empDriveId=65657';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('body', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});

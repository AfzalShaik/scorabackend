'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//Compensation Package item
describe(' Compensation Package Item Cases ;', function() {
  it('Compensation Package Item POST returns 200', function(done) {
    var input = {
      'compPackageId': 2222,
      'companyId': 1026,
      'compPackageItemName': 'item name',
      'compPackageTypeValueId': 337,
      'description': 'desc',
      'currencyCode': 'india',
      'amount': '1010101010',
      'createDatetime': '2017-08-16T12:44:12.676Z',
      'updateDatetime': '2017-08-16T12:44:12.676Z',
      'createUserId': 1,
      'updateUserId': 1,
    };
    var url = 'http://localhost:3000/api/CompensationPackageItems';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //compensation package item put
  it('Compensation Package Item PUT returns 200', function(done) {
    var input = {
      'compPackageId': 2222,
      'companyId': 1026,
      'compPackageItemId': 7766,
      'description': 'item description',
    };
    var url = 'http://localhost:3000/api/CompensationPackageItems/UpdateCompensationPackageItem';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //compensation package get
  it('Compensation Package GET returns 200', function(done) {
    var input = {
      'compPackageItemId': 7767,
      'compPackageId': 2222,
      'companyId': 1026,
    };

    var url = 'http://localhost:3000/api/CompensationPackageItems/getCompensationPkgItemDetails?compPackageId=2222&companyId=1026&compPackageItemId=7767';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('body', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});

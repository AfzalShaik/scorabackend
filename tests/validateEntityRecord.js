/**
 * Created by admin on 03-08-2017.
 */
'use strict';
 /**
   *checkEntitySingleRecordExistence-function deals with checking the pre existing details
   *@constructor
   * @param {object} entityName - contains all the data need to search
   * @param {object} inputFilterObject - contains all the data need to get search and compare
   * @param {function} callBack - deals with the response
   */
function checkEntitySingleRecordExistence(entityName,
                                          inputFilterObject,
                                          callBack) {
  var checkStatus = false;
  console.log('inputFilterObject: ' + JSON.stringify(inputFilterObject));
  entityName.findOne({'where': {'and': [inputFilterObject]}}, function(error, entityRes) {
    if (error) {
      callBack(checkStatus);
    } else if (entityRes == 0) {
      callBack(checkStatus);
    } else {
      checkStatus = true;
      callBack(checkStatus);
    }
  });
}

exports.checkEntitySingleRecordExistence = checkEntitySingleRecordExistence;

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);

//Employer person
describe(' Employer Person Cases ;', function () {
//Employer Person POST service
  it('Employer Person POST returns 200', function (done) {

      var input =
      {

    "companyId": 1012,
    "userId": 1,
    "firstName": "afzal",
    "middleName": "kb",
    "lastName": "shaik",
    "prefixValueId": 6,
    "genderValueId": 2,
    "personTypeValueId": 22,
    "designation": "programmer analyst",
    "createDatetime": "2017-08-08T09:18:48.568Z",
    "updateDatetime": "2017-08-08T09:18:48.568Z",
    "createUserId": 1,
    "updateUserId": 1
  }

    var url = "http://localhost:3000/api/EmployerPeople";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });
//Employer Person PUT service
  it('Employer Person PUT returns 200', function (done) {

      var input =

{"companyId":1020,
"employerPersonId":555,
"prefixValueId": 6,
	"genderValueId": 2,
	"personTypeValueId": 22,
"lastName":"khabul"
}


    var url = "http://localhost:3000/api/EmployerPeople/updateEmployeePerson";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "PUT",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //Employer Person GET service
    it('Employer Person GET returns 200', function (done) {

        var input =
        {
          "employerPersonId": 555,
       "companyId": 1020
    }

      var url = "http://localhost:3000/api/EmployerPeople/getEmployerPerson?employerPersonId=555&companyId=1020";
      var request = require('request');

      // console.log('Request:');
      // console.log(input);
      request({
        url: url,
        method: "GET",
        json: true

      }, function(err, response, body) {
        if(err) { return err; }

          expect(response.statusCode).to.equal(200);

        done();
      });
    });
});

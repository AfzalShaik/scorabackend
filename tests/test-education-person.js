var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);

//education person
describe(' education Person Cases ;', function () {
//education Person POST service
  it('education Person POST returns 200', function (done) {

      var input =
      {

  "campusId": 388589390,
  "userId": 1,
  "firstName": "chotu",
  "middleName": "kb",
  "lastName": "shaik",
  "prefixValueId": 6,
  "genderValueId": 2,
  "personTypeValueId": 22,
  "designation": "lead",
  "createDatetime": "2017-08-08T10:45:53.411Z",
  "updateDatetime": "2017-08-08T10:45:53.411Z",
  "createUserId": 1,
  "updateUserId": 1
}

    var url = "http://localhost:3000/api/EducationPeople";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });
//education Person PUT service
  it('education Person PUT returns 200', function (done) {

      var input =
      {"campusId":388589390,
      "educationPersonId":555,
      "prefixValueId": 6,
      	"genderValueId": 2,
      	"personTypeValueId": 22,
      "lastName":"khabul"}



    var url = "http://localhost:3000/api/EducationPeople/updateEducationPerson";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "PUT",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //education Person GET service
    it('education Person GET returns 200', function (done) {

        var input =
        {
          "educationPersonId": 556,
      "campusId": 388589390
    }

      var url = "http://localhost:3000/api/EducationPeople/getEducationPerson?educationPersonId=556&campusId=388589390";
      var request = require('request');

      // console.log('Request:');
      // console.log(input);
      request({
        url: url,
        method: "GET",
        json: true

      }, function(err, response, body) {
        if(err) { return err; }

          expect(response.statusCode).to.equal(200);

        done();
      });
    });
});

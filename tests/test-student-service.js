'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
//Student
describe(' Student Cases ;', function() {
  //student post method
  it('Student PUT returns 200', function(done) {
    var input = [{
      'studentId': 9,
      'userId': 1,
      'prefixValueId': 6,
      'firstName': 'cxv',
      'middleName': 'hghjgjh',
      'lastName': 'strxcvxcing',
      'genderValueId': 2,
      'countryOfBirth': 'IN',
      'nationality': 'IN',
      'maritalStatusValueId': 10,
      'guardianName': 'stringsdfvsd',
      'studentStatusValueId': 246,
      'disabilityInd': 'h',
      'allergies': 'string',
      'aboutMe': 'string',
      'highlights': 'string',
      'hobbies': 'string',
      'profBodyMembership': 'memb',
      'dreamCompany': 'string',
      'createUserId': 1,
      'updateUserId': 1,
      'offCampusInd': 'string',
    }];
    var url = 'http://localhost:3000/api/Students/updateStudent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
});

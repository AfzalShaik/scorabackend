var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);


//Organization contact
describe(' Organization contact Cases ;', function () {
//Organization contact post method
  it('Organization Contact POST returns 200', function (done) {
    var input = {
"organizationId":56767,
"companyId" :1007,
"contactTypeValueId": 14,
  "contactInfo": "contact information",
  "primaryInd": "N",
  "createDatetime": "2017-08-08T05:41:54.877Z",
  "updateDatetime": "2017-08-08T05:41:54.877Z",
  "createUserId": 1,
  "updateUserId": 1
}
    var url = "http://localhost:3000/api/OrganizationContacts/createOrganizationContact";
    var request = require('request');

    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }
        expect(response.statusCode).to.equal(200);
      done();
    });
  })

  //Organization contact put method
    it('Organization Contact PUT returns 200', function (done) {
      var input =
      {

      "organizationId":56767,
      "contactId":549,
      "companyId" :1000,
      "contactTypeValueId": 14,
      "contactInfo":"contact information for testing"

      }

      var url = "http://localhost:3000/api/OrganizationContacts/UpdateOrganizationContact";
      var request = require('request');

      request({
        url: url,
        method: "PUT",
        json: true,
        body: input

      }, function(err, response, body) {
        if(err) { return err; }
          expect(response.statusCode).to.equal(200);
        done();
      });
    })
    //Organization contact get method
      it('Organization Contact GET returns 200', function (done) {
        var input =
  {

  "organizationId":56767,
  "contactId":549,
  "companyId" :1000,

  }

        var url = "http://localhost:3000/api/OrganizationContacts/getOrganizationContact?organizationId=56767&companyId=1000&contactId=549";
        var request = require('request');

        request({
          url: url,
          method: "GET",
          json: true


        }, function(err, response, body) {
          if(err) { return err; }
            expect(response.statusCode).to.equal(200);
          done();
        });
      })
})

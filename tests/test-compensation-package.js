'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//Compensation Package
describe(' Compensation Package Cases ;', function() {
  it('Compensation Package POST returns 200', function(done) {
    var input = [{
      'companyId': 1026,
      'compPackageName': 'package Name',
      'compPackageTypeValueId': 337,
      'description': 'compensation package creation',
      'compApprovalStatusValueId': 334,
      'createUserId': 1,
      'totalCompPkgValue': '5',
    }, ];
    var url = 'http://localhost:3000/api/CompensationPackages';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //compensation package put
  it('Compensation Package PUT returns 200', function(done) {
    var input = {
      'compPackageId': 2222,
      'companyId': 1026,
      'description': 'compensation package updation',

    };

    var url = 'http://localhost:3000/api/CompensationPackages/UpdateCompensationPackage';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //compensation package get
  it('Compensation Package GET returns 200', function(done) {
    var input = {
      'compPackageId': 2222,
    };

    var url = 'http://localhost:3000/api/CompensationPackages/getCompensationPkgDetails?compPackageId=2222';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
});

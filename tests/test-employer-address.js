var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);

//Employer person address
describe(' Employer Person Address Cases ;', function () {
//Employer Person POST service
  it('Employer Person Address POST returns 200', function (done) {

      var input =
      {
"employerPersonId": 555,
"companyId":1020,
  "addressTypeValueId": 20,
  "addressLine1": "string",
  "addressLine2": "string",
  "addressLine3": "string",
  "postalCode": "380060",
  "cityId": 4,
  "stateCode": "GJ",
  "countryCode": "IN",
  "primaryInd": "N",
  "createDatetime": "2017-08-08T09:18:46.343Z",
  "updateDatetime": "2017-08-08T09:18:46.343Z",
  "createUserId": 1,
  "updateUserId": 1
}

    var url = "http://localhost:3000/api/EmployerPersonAddresses/createEmployerPersonAddress";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //Employer Person put service
    it('Employer Person Address PUT returns 200', function (done) {

        var input =
        {
  "companyId":1009,
  "employerPersonId":555,
  "addressId":40133,
  "addressLine1":"testing on sunday"
   }


      var url = "http://localhost:3000/api/EmployerPersonAddresses/updateEmployeePersonAddress";
      var request = require('request');

      // console.log('Request:');
      // console.log(input);
      request({
        url: url,
        method: "PUT",
        json: true,
        body: input

      }, function(err, response, body) {
        if(err) { return err; }

          expect(response.statusCode).to.equal(200);

        done();
      });
    });

    //Employer Person get service
      it('Employer Person Address GET returns 200', function (done) {

          var input =
          {"employerPersonId": 555,
      "companyId": 1009,
      "addressId": 40133}


        var url = "http://localhost:3000/api/EmployerPersonAddresses/getEmployerPersonAddress?employerPersonId=555&companyId=1009&addressId=40133";
        var request = require('request');

        // console.log('Request:');
        // console.log(input);
        request({
          url: url,
          method: "GET",
          json: true

        }, function(err, response, body) {
          if(err) { return err; }

            expect(response.statusCode).to.equal(200);

          done();
        });
      });
  });

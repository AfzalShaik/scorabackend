'use strict';
module.exports = function(CAMPUS_UPLOAD_LOG) {
  CAMPUS_UPLOAD_LOG.getAllUploads = function(campusId, cb) {
    if (campusId) {
      CAMPUS_UPLOAD_LOG.find({'where': {'campusId': campusId}},
              function(err, response) {
                if (err) {
                  cb(err, null);
                } else {
                  cb(null, response);
                }
              });
    } else {
      errorResponse('results not found', cb);
    }
  };
          //accpect method to update campus drive
  CAMPUS_UPLOAD_LOG.remoteMethod('getAllUploads', {
    description: 'Event details ',
    returns: {
      arg: 'array',
      root: true,
    },
    accepts: [
      {
        arg: 'campusId',
        type: 'number',
        require: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/getAllUploads',
      verb: 'GET',
    },
  });
};

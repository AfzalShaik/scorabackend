'use strict';
module.exports = function(SCORA_ROLE_ACCESS) {
  SCORA_ROLE_ACCESS.getUserDetails = function(input, cb) {
    console.log('.................', input);
    cb(null, input);
  };
  SCORA_ROLE_ACCESS.remoteMethod('getUserDetails', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getUserDetails',
      verb: 'POST',
    },
  });
};

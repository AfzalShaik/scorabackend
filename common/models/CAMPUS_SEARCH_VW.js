'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var campusDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
var async = require('async');
var campusSearchList = [];
var dispayFlag;
module.exports = function(CAMPUS_SEARCH_VW) {
  // remote method definition to get campusDetails item details
  /**
   *searchCampus- To search campus based on given parameters
   *@constructor
   * @param {number} campusId - unique id of campus
   * @param {number} universityId - unique id of university
   * @param {number} regionFlag - contains region flag
   * @param {number} stateCode - contains state cose
   * @param {number} cityId - contains state id or a pin code
   * @param {function} callBc - deals with response
   */
  CAMPUS_SEARCH_VW.searchCampus = function(campusId, searchName, universityId, regionFlag, stateCode, cityId, tierValueId, dispayFlag, programTypeValueId, programMajorValueId, minSalary, maxSalary, skills, interests, callBc) {
    // if (campusId || universityId || regionFlag || stateCode || cityId || tierValueId  || searchName || dispayFlag) {
    dispayFlag = dispayFlag;
    var query = 'select * from CAMPUS_SEARCH_VW vw ';
    var count = 0;
    if (campusId) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' campus_id = ' + campusId;
    }
    if (universityId) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' university_id = ' + universityId;
    }
    if (cityId) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' city_id = ' + cityId;
    }
    if (tierValueId) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' tier_value_id = ' + tierValueId;
    }
    if (programTypeValueId || programMajorValueId) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' exists ( select 1 from PROGRAM p where p.campus_id = vw.campus_id ';
      if (programTypeValueId) {
        query = query + ' and p.program_type_value_id = ' + programTypeValueId;
      }
      if (programMajorValueId) {
        query = query + ' and p.program_major_value_id = ' + programMajorValueId;
      }
      query = query + ' ) ';
    }
    if (minSalary !== 0 && maxSalary !== 0) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' average_salary between ' + minSalary + ' and ' + maxSalary + '';
    }
    if (skills || interests) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      if (interests && !skills) {
        query = query + ' exists (select 1 from STUDENT_INTERESTS S join ENROLLMENT E on (S.student_id=E.student_id) where  S.interest_type_value_id in ' + '(' + interests + ')' + ' and E.campus_id=vw.campus_id)';
      }
      if (skills && !interests) {
        query = query + ' exists (select 1 from STUDENT_SKILLS S join ENROLLMENT E on (S.student_id=E.student_id) where  S.skill_type_value_id in ' + '(' + skills + ')' + ' and E.campus_id=vw.campus_id)';
      }
      if (skills && interests) {
        query = query + ' exists (select 1 from STUDENT_INTERESTS S join ENROLLMENT E on (S.student_id=E.student_id) where  S.interest_type_value_id in ' + '(' + interests + ')' + ' and E.campus_id=vw.campus_id) and exists (select 1 from STUDENT_SKILLS S join ENROLLMENT E on (S.student_id=E.student_id) where  S.skill_type_value_id in ' + '(' + skills + ')' + ' and E.campus_id=vw.campus_id)';
      }
      // query = query + ' ) ';
    }
    if (searchName) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' search_name like \'' + searchName + '%\'';
    }
    if (regionFlag) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' region_flag = \'' + regionFlag + '\'';
    }
    if (stateCode) {
      if (count == 0) {
        query = query + ' where ';
        count = count = 1;
      } else {
        query = query + ' and ';
      }
      query = query + ' state_code = \'' + stateCode + '\'';
    }
    executeQuery(query, function(eror, response) {
      // console.log('-------------- ', response);
      // if (response && (dispayFlag == 'Y' || dispayFlag == 'y')) {
      //   async.map(response, arrageSkills, function(finalErr, finalResponse) {
      //     callBc(null, finalResponse);
      //   });
      // } else {
      async.map(response, getResponse, function(err, resp) {
        callBc(null, resp);
      });
      // }
    });
  };

  // remote method declaration to get campusDetails details
  CAMPUS_SEARCH_VW.remoteMethod('searchCampus', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'searchName',
      type: 'string',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'universityId',
      type: 'number',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'regionFlag',
      type: 'string',
      //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'stateCode',
      type: 'string',
      //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'cityId',
      type: 'number',
      //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'tierValueId',
      type: 'number',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'dispayFlag',
      type: 'string',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programTypeValueId',
      type: 'number',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programMajorValueId',
      type: 'number',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'minSalary',
      type: 'number',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'maxSalary',
      type: 'number',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'skills',
      type: 'array',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'interests',
      type: 'array',
      //   required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/searchCampus',
      verb: 'get',
    },
  });

  function executeQuery(statment, cb) {
    var app = require('../../server/server.js');
    var connector = app.dataSources.ScoraXChangeDB.connector;
    connector.execute(statment, undefined, function(err, result) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, result);
      }
    });
  }

  function arrageSkills(obj, respCB) {
    // console.log('objjjjjjjjj ', obj);
    var skillsStatement = '(select distinct(skill_type_value_id) id from STUDENT_SKILLS skills where exists(select 1 from ENROLLMENT enr where skills.student_id = enr.student_id and enr.campus_id' + '=' + obj.campus_id + '))';
    var interestStatement = 'select distinct(interest_type_value_id) id from STUDENT_INTERESTS interests where exists(select 1 from ENROLLMENT enr where interests.student_id = enr.student_id and enr.campus_id' + '=' + obj.campus_id + ')';
    executeQuery(skillsStatement, function(err, resp) {
      executeQuery(interestStatement, function(error, interestResp) {
        var programObj = {
          'skills': resp,
          'interests': interestResp,
        };
        // console.log('0000000000000', obj);
        var campusObj = {};
        campusObj.universityName = obj.University_Name;
        campusObj.campusName = obj.Campus_Name;
        campusObj.cityId = obj.city_id;
        campusObj.stateCode = obj.state_code;
        campusObj.regionFlag = obj.region_flag;
        campusObj.tierValueId = obj.tier_value_id;
        // campusObj.averageSalary = obj.average_salary;
        campusObj.campusId = obj.campus_id;
        campusObj.universityId = obj.university_id;
        campusObj.rank = obj.rank;
        campusObj.cityName = obj.city_name;
        campusObj.tier = obj.tier;
        // campusObj.state = obj.state_code;
        campusObj.establishedDate = obj.established_date;
        campusObj.searchName = obj.search_name;
        campusObj.searchShortName = obj.search_short_name;
        campusObj.campusStatus = obj.campus_status;
        campusObj.campusStatusValueId = obj.campus_status_value_id;
        campusObj.skills = (resp) ? resp : [];
        campusObj.interests = (interestResp) ? interestResp : [];
        // campusObj.searchedArray = [];
        // campusObj.searchedArray = finalResponse;
        // console.log('finalllllllllllllll ', programObj);
        // skills = resp;
        // interests = interestResp;
        respCB(null, campusObj);
      });
    });
  }

  function getResponse(obj, cb) {
    // console.log('....................-------------- ', obj);
    // console.log('....................-------------- ', obj['RowDataPacket']);
    var campusObj = {};
    campusObj.universityName = obj.University_Name;
    campusObj.campusName = obj.Campus_Name;
    campusObj.cityId = obj.city_id;
    campusObj.stateCode = obj.state_code;
    campusObj.regionFlag = obj.region_flag;
    campusObj.tierValueId = obj.tier_value_id;
    campusObj.averageSalary = obj.average_salary;
    campusObj.campusId = obj.campus_id;
    campusObj.universityId = obj.university_id;
    campusObj.rank = obj.rank;
    campusObj.cityName = obj.city_name;
    campusObj.tier = obj.tier;
    // campusObj.state = obj.state_code;
    campusObj.establishedDate = obj.established_date;
    campusObj.searchName = obj.search_name;
    campusObj.searchShortName = obj.search_short_name;
    campusObj.campusStatus = obj.campus_status;
    campusObj.campusStatusValueId = obj.campus_status_value_id;
    // campusObj.skills = resp;
    // campusObj.interests = interestResp;
    cb(null, campusObj);
  }
};

'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(ORGANIZATION_ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;

  // remote method definition to create address for organization
   /**
 *createOrganizationAddress- To create organization Details
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  ORGANIZATION_ADDRESS.createOrganizationAddress = function(data, callBc) {
    if (data.organizationId && data.companyId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = data.companyId;
      inputFilterObject['organizationId'] = data.organizationId;
      var addressModel = server.models.Address;
      var organizationModel = server.models.Organization;
      checkEntityExistence(organizationModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          addressModel.create(data, function(error, addressRes) {
            if (error) {
              logger.error('error in addressModel.create()');
              callBc(error, null);
            } else if (addressRes) {
              ORGANIZATION_ADDRESS.create({
                'organizationId': Number(data.organizationId),
                'companyId': Number(data.companyId),
                'addressId': Number(addressRes.addressId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  logger.error('error while creating organization address');
                  // console.log('0000000000000000000000', err);
                  errorResponse(callBc);
                } else {
                  logger.info('organization address created successfully.');
                  // addressRes.requestStatus = true;
                  callBc(null, addressRes);
                }
              });
            } else {
              logger.error('error in creating addressModel');
              errorResponse(callBc);
            }
          });
        } else {
          logger.error('record is not present');
          errorResponse(callBc);
        }
      });
    } else {
      logger.error('organizationId and companyId are required');
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    ORGANIZATION_ADDRESS.find({'where': {'companyId': data.companyId,
    'organizationId': data.organizationId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        ORGANIZATION_ADDRESS.updateAll({'companyId': data.companyId,
        'organizationId': data.organizationId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create address for organization
  ORGANIZATION_ADDRESS.remoteMethod('createOrganizationAddress', {
    description: 'Useful to create address for organization',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createOrganizationAddress',
      verb: 'post',
    },
  });

  // remote method definition to get  organization address details
    /**
 *getOrganizationAddress- To get organization address details by taking required fields
 *@constructor
 * @param {object} organizationId - Unique id for ecah and every organization
 * @param {number} companyId - unique id for each and every company
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  ORGANIZATION_ADDRESS.getOrganizationAddress = function(organizationId, companyId, addressId, callBc) {
    var inputObj = {};
    var organizationAddressModel = server.models.OrganizationAddress;
    var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
    var campusDeptAddress = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (organizationId && companyId && addressId) {
      inputObj['organizationId'] = organizationId;
      inputObj['companyId'] = companyId;
      inputObj['addressId'] = addressId;
      logger.info('fetching data using organizationId,companyId and addressId');
      // below function will give address details for an entity based on loopback include filter
      campusDeptAddress(inputObj, organizationAddressModel, 'organizationAddress', callBc);
    } else if (organizationId && companyId) {
      inputObj['organizationId'] = organizationId;
      inputObj['companyId'] = companyId;
      logger.info('fetching data using organizationId and companyId');
      // below function will give address details for an entity based on loopback include filter
      campusDeptAddress(inputObj, organizationAddressModel, 'organizationAddress', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus department address details
  ORGANIZATION_ADDRESS.remoteMethod('getOrganizationAddress', {
    description: 'To get organization address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'organizationId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getOrganizationAddress',
      verb: 'get',
    },
  });
  // remote method definition to delete organization address details
    /**
 *deleteOrganizationAddress- To delete organization address details by taking required fields
 *@constructor
 * @param {object} organizationId - Unique id for ecah and every organization
 * @param {number} companyId - unique id for each and every company
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  ORGANIZATION_ADDRESS.deleteOrganizationAddress = function(organizationId, companyId, addressId, callBc) {
    if (organizationId && companyId && addressId) {
      var organizationAddressModel = server.models.OrganizationAddress;
      var addressModel = server.models.Address;
      var delCampusDeptAddress = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['addressId'] = addressId;
      var inputFilterObject = {};
      inputFilterObject['organizationId'] = organizationId;
      inputFilterObject['companyId'] = companyId;
      inputFilterObject['addressId'] = addressId;
      logger.info('deleting organization address record');
      delCampusDeptAddress(primaryCheckInput, inputFilterObject, addressModel, organizationAddressModel, callBc);
    } else {
      logger.error('organizationId,companyId and addressId are required fields');
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus department address details
  ORGANIZATION_ADDRESS.remoteMethod('deleteOrganizationAddress', {
    description: 'To delete organization address details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'organizationId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteOrganizationAddress',
      verb: 'delete',
    },
  });
  //  UpdateOrganizationAddress remoteMethod
   /**
 *updateOrganizationAddress- To update Organization address Details
 *@constructor
 * @param {object} orgData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  ORGANIZATION_ADDRESS.updateOrganizationAddress = function(orgData, cb) {
    var orgUpdate = require('../../commonCompanyFiles/update-organization-address.js');
    if (orgData.primaryInd == 'Y') {
      searchForInd(orgData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    logger.info('updating organization address');
    orgUpdate.organizationAddressUpdate(orgData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = orgData.primaryInd;
        ORGANIZATION_ADDRESS.updateAll({'companyId': orgData.companyId,
        'addressId': orgData.addressId, 'organizationId': orgData.organizationId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //update Organization address remoteMethod
  ORGANIZATION_ADDRESS.remoteMethod('updateOrganizationAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateOrganizationAddress',
      verb: 'PUT',
    },
  });
};

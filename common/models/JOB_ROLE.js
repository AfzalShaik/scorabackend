'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(JOB_ROLE) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  JOB_ROLE.observe('before save', function jobRoleBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.jobRoleId == undefined) {
      var jobRoleTypeCode = 'JOB_ROLE_TYPE_CODE';
      var jobRoleTypeObj = {};
      jobRoleTypeObj['lookupValueId'] = ctx.instance.jobRoleTypeValueId;
      lookupMethods.typeCodeFunction(jobRoleTypeObj, jobRoleTypeCode, function(jobRoleTypeCodeStatus) {
        if (jobRoleTypeCodeStatus) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('JOB_ROLE Creation Initiated');
          next();
        } else {
          errorFunction('jobRoleTypeCodeStatus validation error', next);
        }
      });
    } else if (ctx.isNewInstance) {
      errorFunction('jobRoleId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get job role details
    /**
 *getJobRoleDetails- To get job role details by taking required fields
 *@constructor
 * @param {number} companyId - Unique id for ecah and every company
 * @param {number} jobeRoleId - unique id for each and every job
 * @param {function} callBc - deals with response
 */
  JOB_ROLE.getJobRoleDetails = function(companyId, jobRoleId, callBc) {
    var inputObj = {};
    var JobRole = server.models.JobRole;
    var jobRoleDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (companyId && jobRoleId) {
      inputObj['companyId'] = companyId;
      inputObj['jobRoleId'] = jobRoleId;
      var includeModels = ['jobRoleLookupValue'];
      // below function will give details for an entity based on loopback include filter
      jobRoleDetails(inputObj, JobRole, includeModels, callBc);
    } else if (companyId) {
      inputObj['companyId'] = companyId;
      inputObj['jobRoleId'] = jobRoleId;
      var includeModels = ['jobRoleLookupValue'];
      // below function will give details for an entity based on loopback include filter
      jobRoleDetails(inputObj, JobRole, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get job role details
  JOB_ROLE.remoteMethod('getJobRoleDetails', {
    description: 'To get job role details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'jobRoleId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getJobRoleDetails',
      verb: 'GET',
    },
  });
  // remote method declaration to delete job role details if it’s not associated with Employee Campus List HDR
    /**
 *deleteJobRoleDetails- To delete job role details by taking required fields
 *@constructor
 * @param {number} jobRoleId - Unique id for ecah and every role
 * @param {function} callBc - deals with response
 */
  JOB_ROLE.deleteJobRoleDetails = function(jobRoleId, callBc) {
    if (jobRoleId) {
      var getEntityDetailsCount = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').getEntityDetailsCount;
      var deleteSingleEntityDetails = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;
      var inputFilterObject = {};
      inputFilterObject['jobRoleId'] = jobRoleId;
      var employerCampusListHdrModel = server.models.EmployerCampusListHdr;
      var jobRoleModel = server.models.JobRole;
      getEntityDetailsCount(inputFilterObject, employerCampusListHdrModel, function(err, entityRecordsCount) {
        if (err) {
          errorFunction(err, callBc);
        } else if (entityRecordsCount != undefined &&  entityRecordsCount == 0) {
          deleteSingleEntityDetails(inputFilterObject, jobRoleModel, callBc);
        } else {
          errorFunction('job role is used in employer campus lists', callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete job role details if it’s not associated with Employee Campus List HDR
  JOB_ROLE.remoteMethod('deleteJobRoleDetails', {
    description: 'To delete job role details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'jobRoleId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteJobRoleDetails',
      verb: 'delete',
    },
  });
  //UpdateJobRole remote method starts here
   /**
 *updateJobRole- To update job role Details
 *@constructor
 * @param {object} roleData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  JOB_ROLE.updateJobRole = function(roleData, cb) {
    var roleUpdate = require('../../commonCompanyFiles/update-job-role');
    roleUpdate.updateJobRoleService(roleData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateJobRole method creation
  JOB_ROLE.remoteMethod('updateJobRole', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateJobRole',
      verb: 'PUT',
    },
  });
  var studentIds = [];
  var Students = [];
  JOB_ROLE.getTotalJobsBasedOnRole = function(companyId, cb) {
    var jobRoleDetails = require('../../ServicesImpl/CampusImpl/get-jobRole-Details.js').getAllJobRoleDetials;
    jobRoleDetails(companyId, function(err, res) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, res);
      }
    });
  };
 
  function getData(array, cb1) {
    var statment = 'SELECT first_name,name,campus_name,program_name FROM CAMPUS_EVENT_STUDENT_SEARCH_VW WHERE student_id IN (' + array + ')';
    var app = require('../../server/server.js');
    var connector = app.dataSources.ScoraXChangeDB.connector;
    connector.execute(statment, undefined, function(err, result) {
      if (err) {
        cb1(null, null);
      } else {
        cb1(null, result);
      }
    });
  }

  function getNames(obj, cb) {
    console.log('objjjjjjjjjj', obj);
    studentIds.push(obj.studentId);
    var detailsOf = server.models.CampusEventStudentSearchVw;
    detailsOf.findOne({'where': {'studentId': obj.studentId}}, function(err, res) {
      if (err) {
        cb(err, null);
      } else {
        var data = {};
        data.StudentName = res.firstName;
        data.CampusName = res.campusName;
        data.Department = res.name;
        data.Program = res.programName;
        data.Date = obj.updateDatetime;
        Students.push(data);
        cb(null, 'done');
      }
    });
  }
  JOB_ROLE.remoteMethod('getTotalJobsBasedOnRole', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'CompanyDetails',
      type: 'array',
    },
    accepts: {
      arg: 'companyId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    http: {
      path: '/getTotalJobsBasedOnRole',
      verb: 'GET',
    },
  });
};

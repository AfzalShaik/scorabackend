'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var lookupValidation = require('../../commonValidation/lookupMethods.js').validateLookups;
var lookupMethods = require('../../commonValidation/lookupMethods');
// exporting function to use it in another modules if requires

/**
 * @param {function} StudentMissingWork
 */
module.exports = function(STUDENT_MISSING_PROGRAM_WORK) {
  //var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
  //var Joi = require('joi');
 /**
 *Creating a new user with StudentMissingWork
 *@constructor
 * @param {obj} ctx-which was the input from api
 * @param {method} next-to skip the function if the exe was already done
 */
  STUDENT_MISSING_PROGRAM_WORK.createStudentMissingProgramWork = function(data, cb) {
    var StudentMissingProgramWork = server.models.StudentMissingProgramWork;
    findCampus(data, function(campusErr, campusResp) {
      if (campusErr) {
        errorFunction('invalid campus id or university id', cb);
      } else if (campusResp == 'true') {
        findDepartment(data, function(departerr, departResp) {
          if (departerr) {
            errorFunction('invalid department id', cb);
          } else if (departResp == 'true') {
            // console.log('in correct place only');
            var programTypeCode = 'PROGRAM_TYPE_CODE';
            var programClassCode = 'PROGRAM_CLASSIFICATION_CODE';
            var programCatCode = 'PROGRAM_CATEGORY_CODE';
            var programMajorCode = 'PROGRAM_MAJOR';
            var programCatVal, programTypeVal, programMajorVal, programClassVal;
            var lookupValueObj = {};
            var lookupValueObj1 = {};
            var lookupValueObj2 = {};
            var lookupValueObj3 = {};
            lookupValueObj.lookupValueId = data.programTypeValueId;
            lookupValueObj1.lookupValueId = data.programClassValueId;
            lookupValueObj2.lookupValueId = data.programCatValueId;
            lookupValueObj3.lookupValueId = data.programMajorValueId;
            lookupMethods.typeCodeFunction(lookupValueObj, programTypeCode, function(programTypeVal) {
              programTypeVal = programTypeVal;
              lookupMethods.typeCodeFunction(lookupValueObj1, programClassCode, function(programClassVal) {
                programClassVal = programClassVal;
                lookupMethods.typeCodeFunction(lookupValueObj2, programCatCode, function(programCatVal) {
                  programCatVal = programCatVal;
                  lookupMethods.typeCodeFunction(lookupValueObj3, programMajorCode, function(programMajorVal) {
                    programMajorVal = programMajorVal;
                    // console.log('this is val', programTypeVal, 'cat', programCatVal, 'major', programMajorVal, 'this is class value', programClassVal);
                    if (programTypeVal == false || programClassVal == false || programCatVal == false || programMajorVal == false) {
                      errorFunction(' Invalid programTypeVal or programClassVal or programCatVal or programMajorVal', cb);
                      logger.error('Invalid programTypeVal or programClassVal or programCatVal or programMajorVal');
                    } else {
                      StudentMissingProgramWork.create(data, function(err, createResponse) {
                        cb(null, data);
                      });
                    }
                  });
                });
              });
            });
          } else {
            cb(null, data);
          }
        });
      } else {
        errorFunction('invalid Input Params', cb);
      }
    });
  };
  var findCampus = function(data, campusCallback) {
    var campus = server.models.Campus;
    var campusObject = {};
    campusObject.campusId = (data.campusId) ? data.campusId : undefined;
    campusObject.universityId = (data.universityId) ? data.universityId : undefined;
    findEntity(campusObject, campus, function(err, campusResponse) {
      if (err) {
        campusCallback('invalid campus id', campusCallback);
      }  else if (campusResponse == null) {
        campusCallback(null, 'false');
      } else {
        campusCallback(null, 'true');
      }
    });
  };
  exports.findCampus = findCampus;
  var findDepartment = function(data, departmentCallback) {
    var departmentObject = {};
    departmentObject.campusId = (data.campusId) ? data.campusId : undefined;
    departmentObject.departmentId = (data.departmentId) ? data.departmentId : undefined;
    var department = server.models.Department;
    // console.log('=============', departmentObject);
    // departmentObject.departmentId = data.departmentId;
    findEntity(departmentObject, department, function(err, departmentResponse) {
      // console.log('this is dep res', departmentResponse, 'err', err);
      if (departmentResponse == null) {
        errorFunction('invalid department id', departmentCallback);
      } else if (err) {
        departmentCallback(null, 'false');
      } else {
        departmentCallback(null, 'true');
      }
    });
  };
  exports.findDepartment = findDepartment;
  STUDENT_MISSING_PROGRAM_WORK.remoteMethod('createStudentMissingProgramWork', {
    description: 'Useful to create missing program',
    returns: {

      type: 'array',
      root: true,

    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createStudentMissingProgramWork',
      verb: 'post',
    },
  });
 //getting data from database
/**
 * the functions which deals with getting data from the database
 * @constructor
 * @param {number} studentId- which deals as a primiry key to retrive the data from the database
 * @param {function} callBc-which take cares of the response
 */
  STUDENT_MISSING_PROGRAM_WORK.getMissingWorkDetails = function(studentId, callBc) {
    var inputObj = {};
    var StudentMissingProgramWork = server.models.StudentMissingProgramWork;
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (studentId) {
      inputObj['studentId'] = studentId;
      findEntity(inputObj, StudentMissingProgramWork, function(err, res) {
        if (err) {
          errorResponse(err, callBc);
        } else if (res == 0) {
          errorResponse('result not found Invalid Id', callBc);
        } else {
          callBc(null, res);
        }
      });
    } else {
      errorResponse('result not found', callBc);
    } };
/**
 * remote method---
 * exe when ever getMissingWorkDetails was called
 * accpects-studentid
 * gets-array
 */
  STUDENT_MISSING_PROGRAM_WORK.remoteMethod('getMissingWorkDetails', { // TODO: CODEREVIEW - VARA – 09SEP17 -- Don’t abbreviate, give full names to method names, descriptions, comments
 
    //.............................
    description: 'To get student missing work details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'studentId', type: 'number', required: true, http: {source: 'query'}},
    ],
    http: {
      path: '/getMissingWorkDetails',
      verb: 'get',
    },
  });
  STUDENT_MISSING_PROGRAM_WORK.deleteMissingProgramWork = function(enrollmentId, callBc) {
    if (enrollmentId) {
      var StudentMissingProgramWork = server.models.StudentMissingProgramWork;
      let filter = {"where":{
        "enrollmentId": enrollmentId
      }}
      STUDENT_MISSING_PROGRAM_WORK.destroyAll(filter.where, function(err, responseDelete) {
        if (err) {
          callBc(err, null);
        } else if (responseDelete.count == 0) {
          errorFunction('no student id with that id missed a program', callBc);
        } else {
          callBc(null, responseDelete);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus missing program details
  STUDENT_MISSING_PROGRAM_WORK.remoteMethod('deleteMissingProgramWork', {
    description: 'To delete campus missing program',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: {
      arg: 'enrollmentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    http: {
      path: '/deleteMissingProgramWork',
      verb: 'delete',
    },
  });
  /**
  *updateStudentMissingWork which deals with the updating required data in the database
  * @constructor
  * @param {function} updateStudentMissingWork
  * @param {object} missingProgramdata-which has all the data user want to update
  * @param {callback} cb-acts as a call back function holds the response and status
  */
  STUDENT_MISSING_PROGRAM_WORK.updateStudentMissingWork = function(missingProgramdata, cb) {
    var missingWorkUpdate = require('../../update/missing-work-update.js');
    missingWorkUpdate.updateMissingWork(missingProgramdata, function(err, resp) {
      if (err) {
         // response.statusCode = 404;
        cb(err, resp);
      } else if (resp == 0) {
        errorResponse('Invalid student Id', cb);
      } else {
        cb(null, resp);
      }
    });
  };
 /**
 * remote method for update missing program work update
 */
  STUDENT_MISSING_PROGRAM_WORK.remoteMethod('updateStudentMissingWork', {// TODO: CODEREVIEW - VARA – 09SEP17 -- Don’t abbreviate, give full names to method names, descriptions, comments
    description: 'update student missing program details',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateStudentMissingWork',
      verb: 'PUT',
    },
  });
};

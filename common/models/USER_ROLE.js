'use strict';
var server = require('../../server/server');
var dataSource = require('../../server/config.json');
module.exports = function(USER_ROLE) {
  USER_ROLE.sendEmail = function(data, callBc) {
    var email = server.models.Email;
    var subject = (data.flag === 'demo') ? 'Requesting For Demo' : 'Subscription For News Letters';
    var demoText = 'Name : ' + data.name + '\n' + 'Email : ' + data.emailId + '\n' + 'Phone Number : ' + data.phone + '\n';
    var newsText = 'Email : ' + data.emailId + '\n';
    var text = (data.flag === 'demo') ?  demoText : newsText;
    email.send({
      to: dataSource.adminEmail.email,
      from: 'admin@scoraxchange.com',
      subject: subject,
      text: text,
      // html: data.message,
    }, function(err, mail) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, data);
      }
    });
  };
  USER_ROLE.remoteMethod('sendEmail', {
    description: 'Send Email',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/sendEmail',
      verb: 'post',
    },
  });
}
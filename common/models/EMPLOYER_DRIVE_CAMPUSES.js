'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(EMPLOYER_DRIVE_CAMPUSES) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_DRIVE_CAMPUSES.observe('before save', function employerDriveCampusesBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.empDriveCampusId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('EMPLOYER_DRIVE_CAMPUSES Creation Initiated');
      next();
    } else if (ctx.isNewInstance) {
      errorFunction('compPackageId is system generated value', next);
    } else {
      next();
    }
  });
  EMPLOYER_DRIVE_CAMPUSES.getEmployerDriveCampus = function(empEventId, empDriveId, campusId, callBc) {
    EMPLOYER_DRIVE_CAMPUSES.find({'where': {'empEventId': empEventId,
    'empDriveId': empDriveId,
    'campusId': campusId}}, function(err, responseOfCampus) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, responseOfCampus);
      }
    });
  };
  EMPLOYER_DRIVE_CAMPUSES.remoteMethod('getEmployerDriveCampus', {
    description: 'To get education person details ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'empEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'empDriveId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEmployerDriveCampus',
      verb: 'get',
    },
  });
  // emp drive campus new api for adding campuses
  EMPLOYER_DRIVE_CAMPUSES.createEmpDriveCampuses = function(input, cb) {
    EMPLOYER_DRIVE_CAMPUSES.create(input.data, function(err, resp) {
      if (resp) {
        cb(null, resp);
      } else {
        cb(null, null);
      }
    });
  };
  EMPLOYER_DRIVE_CAMPUSES.remoteMethod('createEmpDriveCampuses', {
    description: 'Send Valid Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createEmpDriveCampuses',
      verb: 'POST',
    },
  });
};

'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(EDUCATION_PERSON_ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // remote method definition to create address for education person
   /**
 *createEducationPersonAddress- To crete eduction person address
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  EDUCATION_PERSON_ADDRESS.createEducationPersonAddress = function(data, callBc) {
    if (data.educationPersonId && data.campusId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = data.campusId;
      inputFilterObject['educationPersonId'] = data.educationPersonId;
      var addressModel = server.models.Address;
      var educationPersonModel = server.models.EducationPerson;
      checkEntityExistence(educationPersonModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          addressModel.create(data, function(error, addressRes) {
            if (error) {
              logger.error('error while creating education person address model');
              callBc(error, null);
            } else if (addressRes) {
              // console.log(data.primaryInd);
              EDUCATION_PERSON_ADDRESS.create({
                'educationPersonId': Number(data.educationPersonId),
                'campusId': Number(data.campusId),
                'addressId': Number(addressRes.addressId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  errorResponse(callBc);
                } else {
                  logger.info('education person address service created successfully');
                  // addressRes.requestStatus = true;
                  callBc(null, addressRes);
                }
              });
            } else {
              logger.error('empty response while creating education person address model');
              errorResponse(callBc);
            }
          });
        } else {
          logger.error('error while validating education person before creating education person address');
          errorResponse(callBc);
        }
      });
    } else {
      logger.error('error while validating education person address create method input params');
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    EDUCATION_PERSON_ADDRESS.find({'where': {'educationPersonId': data.educationPersonId,
    'campusId': data.campusId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        // console.log('in error zone');
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
        priCallback(null, res);
        // console.log('in else if zone');
      } else {
        var update = {};
        update.primaryInd = 'N';
        EDUCATION_PERSON_ADDRESS.updateAll({'educationPersonId': data.educationPersonId,
        'campusId': data.campusId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            // console.log('in error zoneeeeeee');
            errorResponse('there was an error');
          } else {
            // console.log('in elseeeeeee zone');
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create address for education person
  EDUCATION_PERSON_ADDRESS.remoteMethod('createEducationPersonAddress', {
    description: 'Useful to create address for education person',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createEducationPersonAddress',
      verb: 'post',
    },
  });
  // remote method definition to get  education person address details
   /**
 *getEducationPersonAddress- To get education person address
 *@constructor
 * @param {object} educationPersonId - unique id for education person
 * @param {number} campusId- unique id for every campus
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  EDUCATION_PERSON_ADDRESS.getEducationPersonAddress = function(educationPersonId, campusId, addressId, callBc) {
    var inputObj = {};
    var employerPersonAddressModel = server.models.EducationPersonAddress;
    var educationPersonAddressDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (educationPersonId && campusId && addressId) {
      inputObj['educationPersonId'] = educationPersonId;
      inputObj['campusId'] = campusId;
      inputObj['addressId'] = addressId;
      // below function will give address details for an entity based on loopback include filter
      logger.info('retrieved education-person-address details successfully based on educationPersonId,campusId,addressId');
      educationPersonAddressDet(inputObj, employerPersonAddressModel, 'educationPersonAddress', callBc);
    } else if (educationPersonId && campusId) {
      inputObj['educationPersonId'] = educationPersonId;
      inputObj['campusId'] = campusId;
      // below function will give address details for an entity based on loopback include filter
      logger.info('retrieved education-person-address details successfully based on educationPersonId and campusId');
      educationPersonAddressDet(inputObj, employerPersonAddressModel, 'educationPersonAddress', callBc);
    } else {
      logger.error('error while retrieving education-person-address details');
      errorResponse(callBc);
    }
  };
  // remote method declaration to get education person address details
  EDUCATION_PERSON_ADDRESS.remoteMethod('getEducationPersonAddress', {
    description: 'To get education person address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'educationPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEducationPersonAddress',
      verb: 'get',
    },
  });
  //  EDUCATION_PERSON remoteMethod
   /**
 *updateEducationPersonAddress- To update Education Person address
 *@constructor
 * @param {object} personData - contains all the data need to get updated
  * @param {function} cb - deals with response
 */
  EDUCATION_PERSON_ADDRESS.updateEducationPersonAddress = function(personData, cb) {
    var personUpdate = require('../../commonCampusFiles/update-education-person-address.js');
    if (personData.primaryInd == 'Y') {
      searchForInd(personData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    personUpdate.educationPersonAddressUpdate(personData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updateData = {};
        updateData.primaryInd = personData.primaryInd;
        EDUCATION_PERSON_ADDRESS.updateAll({'campusId': personData.campusId,
        'addressId': personData.addressId, 'educationPersonId': personData.educationPersonId},
        updateData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //update EDUCATION_PERSON remoteMethod
  EDUCATION_PERSON_ADDRESS.remoteMethod('updateEducationPersonAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEducationPersonAddress',
      verb: 'PUT',
    },
  });

  // remote method definition to deleteEducationPersonAddress
  EDUCATION_PERSON_ADDRESS.deleteEducationPersonAddress = function(campusId, addressId, educationPersonId, callBc) {
    if (campusId && addressId && educationPersonId) {
      var educationPersonAddress = server.models.EducationPersonAddress;
      var addressModel = server.models.Address;
      var deleteEducationPersonAddressDet = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['addressId'] = addressId;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      inputFilterObject['addressId'] = addressId;
      inputFilterObject['educationPersonId'] = educationPersonId;
      deleteEducationPersonAddressDet(primaryCheckInput, inputFilterObject, addressModel, educationPersonAddress, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to deleteEducationPersonAddress
  EDUCATION_PERSON_ADDRESS.remoteMethod('deleteEducationPersonAddress', {
    description: 'To delete education person address',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'educationPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteEducationPersonAddress',
      verb: 'delete',
    },
  });
};

'use strict';
var server = require('../../server/server');

module.exports = function(STUDENT_ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwerror = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // remote method definition to create address for particular student
   /**
 *createStudentAddress- To Create Student Address Details
 *@constructor
 * @param {object} data - contains all the data need to get Created
 * @param {function} callBc - deals with response
 */
  STUDENT_ADDRESS.createStudentAddress = function(data, callBc) {
    if (data.studentId && data.addresses.length > 0) {
      var myArray = data.addresses;
      var iterateobj = {};
      var count = 0;
      for (var j = 0; j < myArray.length; j++) {
        iterateobj = myArray[j];
        if (myArray[j].primaryInd == 'Y') {
          iterateobj = myArray[j];
          count++;
        }
      }
      // console.log('this is count', count);
      if (count <= 1) {
        if (count == 1) {
          searchForInd(data, function(err, res) {
            if (err) {
              errorResponse('there was an error', callBc);
            } else {
              // console.log('sucess');
            }
          });
        }
        var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
        var inputFilterObject = {};
        inputFilterObject['studentId'] = data.studentId;
        var addressModel = server.models.Address;
        var studentModel = server.models.Student;

        checkEntityExistence(studentModel, inputFilterObject,
        function(checkStatus) {
          if (checkStatus) {
            addressModel.create(data.addresses, function(error, addressRes) {
              if (error) {
                callBc(error, null);
              } else if (addressRes) {
                var persistAddressDataInForeignEntity = require('../../ServicesImpl/AddressImpl/persistAddressRelationData.js').persistAddressDataInForeignEntity;
                var studentAddressModel = server.models.StudentAddress;
                var inputObj = {};
                inputObj['studentId'] = Number(data.studentId);
                // inputObjToPersist['addressId']=Number(addressRes.addressId);
                persistAddressDataInForeignEntity(addressRes.length, addressRes, studentAddressModel, inputObj, callBc);
              } else {
                errorResponse(callBc);
              }
            });
          } else {
            errorResponse(callBc);
          }
        });
      } else {
        throwerror('cant have tow or more primary ind as Y', callBc);
      }
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    STUDENT_ADDRESS.find({'where': {'studentId': data.studentId, 'primaryInd': 'Y'}},
    function(err, res) {
      if (err) {
        errorResponse('campusId was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
        priCallback(null, res);
      } else {
        var update = {};
        update.primaryInd = 'N';
        STUDENT_ADDRESS.updateAll({'studentId': data.studentId, 'primaryInd': 'Y'},
        update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create address for particular student
  STUDENT_ADDRESS.remoteMethod('createStudentAddress', {
    description: 'Useful to create address for particular student',
    returns: {
      type: 'array',
      root: true,

    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createStudentAddress',
      verb: 'POST',
    },
  });

  // remote method definition to get student address details
    /**
 *getStudentAddress- To get Student Address details by taking required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every student
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  STUDENT_ADDRESS.getStudentAddress = function(studentId, addressId, callBc) {
    var inputObj = {};
    var studentAddress = server.models.StudentAddress;

    var studentAddressDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId && addressId) {
      inputObj['studentId'] = studentId;
      inputObj['addressId'] = addressId;
      // below function will give address details for an entity based on loopback include filter
      studentAddressDet(inputObj, studentAddress, 'studentAddressIbfk2rel', callBc);
    } else if (studentId) {
      inputObj['studentId'] = studentId;
      // below function will give student address details for an entity based on loopback include filter
      studentAddressDet(inputObj, studentAddress, 'studentAddressIbfk2rel', callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to get student address details
  STUDENT_ADDRESS.remoteMethod('getStudentAddress', {
    description: 'To get student address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getStudentAddress',
      verb: 'GET',
    },
  });

  // remote method definition to delete student address details
    /**
 *deleteStudentAddress- To delete student address details by taking required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every student
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  STUDENT_ADDRESS.deleteStudentAddress = function(studentId, addressId, callBc) {
    if (studentId && addressId) {
      var studentAddressModel = server.models.StudentAddress;
      var addressModel = server.models.Address;
      var deleteStudentAddress = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['addressId'] = addressId;
      var inputFilterObject = {};
      inputFilterObject['studentId'] = studentId;
      inputFilterObject['addressId'] = addressId;
      deleteStudentAddress(primaryCheckInput, inputFilterObject, addressModel, studentAddressModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to delete student address details
  STUDENT_ADDRESS.remoteMethod('deleteStudentAddress', {
    description: 'To delete student address',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteStudentAddress',
      verb: 'DELETE',
    },
  });

  //updateStudentAddress remote method starts here
   /**
 *updateStudentAddress- To update Student Address Details
 *@constructor
 * @param {object} addData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  STUDENT_ADDRESS.updateStudentAddress = function(addData, cb) {
    var studentAddUpdate = require('../../commonCampusFiles/update-student-address');
    if (addData.primaryInd == 'Y') {
      searchForInd(addData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    studentAddUpdate.updateStudentAddressService(addData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = addData.primaryInd;
        STUDENT_ADDRESS.updateAll({'studentId': addData.studentId, 'addressId': addData.addressId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //updateStudentAddress method creation
  STUDENT_ADDRESS.remoteMethod('updateStudentAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateStudentAddress',
      verb: 'PUT',
    },
  });
};

//requiring server path so that we can use exported functions or methods when ever we need
'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

module.exports = function(ENROLLMENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to enroll model then we execute logic or change request object before saving into database
  ENROLLMENT.observe('before save', function StudentEnrollBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });

  // remote method definition to get enrollment details
    /**
 *getEnrollmentDetails- To get Enrollment details by taking required fields
 *@constructor
 * @param {number} enrollmentId - Unique id for ecah and every enrollment
 * @param {function} callBc - deals with response
 */
  ENROLLMENT.getEnrollmentDetails = function(studentId, callBc) {
    var inputObj = {};
    var enrollment = server.models.Enrollment;
    var enrollmentDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (studentId) {
      ENROLLMENT.find({'where': {'studentId': studentId}}, function(err, res) {
        if (err) {

        } else {
          var async = require('async');
          async.map(res, getDetails,
            function(err, studentResponse) {
              callBc(null, studentResponse);
            });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  function getDetails(obj, callBack) {
    if (obj.programId != null) {
      var program = server.models.Program;
      program.findOne({'where': {'programId': obj.programId},
      'include': ['programTypeValueLookupValue', 'departmentDetails']}, function(err, programResponse) {
        if (err) {
          callBack(err, null);
        } else {
          var campus = server.models.Campus;
          campus.findOne({'where': {'campusId': programResponse.campusId},
          'fields': {
            'shortName': true,
            'name': true,
            'universityId': true,
          }},
          function(err, campusRespone) {
            var university = server.models.University;
            university.findOne({'where': {'universityId': campusRespone.universityId},
            'fields':
            {
              'name': true,
              'shortName': true,
            }},
            function(err, universityResponse) {
              if (err) {
                callBack(err, null);
              } else {
                var studentDetails = {};
                studentDetails.Details = obj;
                studentDetails.ProgramDetails = programResponse;
                studentDetails.UniversityDetails = universityResponse;
                studentDetails.CampusDetails = campusRespone;
                callBack(null, studentDetails);
              }
            });
          });
        }
      });
    } else {
      var studentMissingProgramWork = server.models.StudentMissingProgramWork;
      studentMissingProgramWork.findOne({'where': {'enrollmentId': obj.enrollmentId}},
            function(err, missingProgramWorkRes) {
              if (err) {

              } else {
                var campus = server.models.Campus;
                campus.findOne({'where': {'campusId': missingProgramWorkRes.campusId},
                    'fields': {
                      'shortName': true,
                      'name': true,
                      'universityId': true,
                    }},
                    function(err, campusRespone) {
                      var university = server.models.University;
                      university.findOne({'where': {'universityId': missingProgramWorkRes.universityId},
                      'fields':
                      {
                        'name': true,
                        'shortName': true,
                      }},
                      function(err, universityResponse) {
                        if (err) {
                          callBack(err, null);
                        } else {
                          var depart = server.models.Department;
                          depart.findOne({'where': {'departmentId': missingProgramWorkRes.departmentId}},
                          function(err, depRes) {
                            if (err) {
                              callBack(err, null);
                            } else {
                              var studentDetails = {};
                              var depFromMissing = {};
                              depFromMissing.name = (missingProgramWorkRes.departmentName) ? missingProgramWorkRes.departmentName : null;
                              var data = {};
                              data.programName = missingProgramWorkRes.programName;
                              data.departmentDetails = (depRes) ? depRes : depFromMissing;
                              var universityData = {};
                              universityData.name = (studentMissingProgramWork.universityName) ? studentMissingProgramWork.universityName : null;
                              var campusData = {};
                              campusData.name = missingProgramWorkRes.campusName;
                              studentDetails.Details = obj;
                              studentDetails.ProgramDetails = data;
                              studentDetails.UniversityDetails = (universityResponse !== null) ? universityResponse :
                              universityData;
                              studentDetails.CampusDetails = (campusRespone) ? campusRespone :
                              campusData;
                              callBack(null, studentDetails);
                            }
                          });
                        }
                      });
                    });
              }
            });
    }
  }
  // remote method declaration to get campus address details // TODO: CODEREVIEW - VARA – 09SEP17 -- Should be enrollment details not campus details
  ENROLLMENT.remoteMethod('getEnrollmentDetails', {
    description: 'To get Enrollment details based on enrollment id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEnrollmentDetails',
      verb: 'GET',
    },
  });
  //updateEnrollment custom method starts here
   /**
 *updateEnrollment- To update enrollment Details
 *@constructor
 * @param {object} enrollment - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  ENROLLMENT.updateEnrollment = function(enrollment, cb) {
    var updateEnrollment = require('../../commonValidation/update-enrollment');
    updateEnrollment.updateEnrollmentService(enrollment, function(err, resp) {
      logger.info('upating Enrollment ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //remoteMethod for enrollment update
  ENROLLMENT.remoteMethod('updateEnrollment', {
    description: 'Send Valid Data ', //TODO: CODEREVIEW - VARA – 09SEP17 -- Need better descriptions similar to getEnrollmentDetails
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEnrollment',
      verb: 'PUT',
    },
  });
  //enrollent delete api
  ENROLLMENT.deleteEnrollment = function(enrollmentId, callBc) {
    ENROLLMENT.destroyById(enrollmentId, function(err, responseDelete) {
      if (err) {
        callBc(err, null);
      } else if (responseDelete.count == 0) {
        errorFunction('enrollmentId is not found', callBc);
      } else {
        callBc(null, responseDelete);
      }
    });
  };
  // remote method declaration to delete campus missing program details
  ENROLLMENT.remoteMethod('deleteEnrollment', {
    description: 'To delete unregisterd student details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: {
      arg: 'enrollmentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    http: {
      path: '/deleteEnrollment',
      verb: 'delete',
    },
  });
};

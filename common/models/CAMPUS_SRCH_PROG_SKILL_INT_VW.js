'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var campusDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var async = require('async');
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
module.exports = function(CAMPUS_SRCH_PROG_SKILL_INT_VW) {
  // remote method definition to get campusDetails item details
  /**
   *searchSkillsAndProgram- To search campus based on given parameters
   *@constructor
   * @param {number} interestTypeValueId - unique id of campus
   * @param {number} programId - unique id of university
   * @param {number} programTypeValueId - contains region flag
   * @param {number} programClassValueId - contains state cose
   * @param {number} programCatValueId - contains state id or a pin code
   * @param {function} callBc - deals with response
   */
  CAMPUS_SRCH_PROG_SKILL_INT_VW.searchSkillsAndProgram = function(programTypeValueId, programClassValueId, programCatValueId, programMajorValueId, skillTypeValueId, interestTypeValueId, callBc) {
    if (programTypeValueId || programClassValueId || programCatValueId || programMajorValueId || skillTypeValueId || interestTypeValueId) {
      var campusObj = {};
      campusObj['interestTypeValueId'] = (interestTypeValueId) ? interestTypeValueId : undefined;
      campusObj['programCatValueId'] = (programCatValueId) ? programCatValueId : undefined;
      campusObj['programClassValueId'] = (programClassValueId) ? programClassValueId : undefined;
      campusObj['programTypeValueId'] = (programTypeValueId) ? programTypeValueId : undefined;
      campusObj['programMajorValueId'] = (programMajorValueId) ? programMajorValueId : undefined;
      campusObj['skillTypeValueId'] = (skillTypeValueId) ? skillTypeValueId : undefined;
      campusDetails(campusObj, CAMPUS_SRCH_PROG_SKILL_INT_VW, function(err, campusSearch) {
        if (err) {
          errorFunction(err, callBc);
        } else if (campusSearch.data) {
          var campusSearchResult = campusSearch.data;
          async.map(campusSearchResult, returnCampusList, function(error, finalList) {
            callBc(null, finalList);
          });
        } else {
          errorFunction('No records Found', callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campusDetails details
  CAMPUS_SRCH_PROG_SKILL_INT_VW.remoteMethod('searchSkillsAndProgram', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'programTypeValueId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programClassValueId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programCatValueId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programMajorValueId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'skillTypeValueId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'interestTypeValueId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/searchSkillsAndProgram',
      verb: 'get',
    },
  });

  function returnCampusList(obj, cb) {
    var campusSearchVw = server.models.CampusSearchVw;
    var searchCampus = campusSearchVw.searchCampus;
    searchCampus(obj.campusId, undefined, undefined, undefined, undefined, undefined, undefined, 'N', undefined, undefined, 0, 0, undefined, undefined, function(err, searchResponse) {
      cb(null, searchResponse);
    });
  }
//--------------------------------------------------------------
  CAMPUS_SRCH_PROG_SKILL_INT_VW.skillsInterestsFilter = function(object, cb) {
    if (object) {
      var campuses = object.campusList;
      async.map(campuses, getCampusData, function(campusErr, campusInfo) {
        if (campusErr) {
          errorFunction(campusErr, cb);
        } else {
          var campusInformation = cleanArray(campusInfo);
          getProram(object, function(programErr, programInfo) {
            if (programInfo) {
              var programResponse = programInfo;
              filterCampuses(campusInformation, programResponse, function(error, programCampuses) {
                var output = [];
                output = cleanArray(programCampuses);
                if (object.skills.length > 0 && !object.interests.length > 0) {
                  var skills = object.skills;
                  skillsFilter(skills, output, function(skillsErr, skillsResponse) {
                    getFinalCampusList(skillsResponse, function(skillErr, skillOutput) {
                      if (skillErr) {
                        errorFunction(skillErr, cb);
                      } else {
                        cb(null, skillOutput);
                      }
                    });
                  });
                } else if (object.interests.length > 0 && !object.skills.length > 0) {
                  var interests = object.interests;
                  interestsFilter(interests, output, function(interestErr, interestResponse) {
                    getFinalCampusList(interestResponse, function(interestErr, interestOutput) {
                      if (interestErr) {
                        errorFunction(interestErr, cb);
                      } else {
                        cb(null, interestOutput);
                      }
                    });
                  });
                } else if (object.interests.length > 0 && object.skills.length > 0) {
                  var skills = object.skills;
                  var interests = object.interests;
                  skillsFilter(skills, output, function(err, response) {
                    if (err) {
                      cb(err, cb);
                    } else {
                      var skillsOutput = [];
                      skillsOutput = cleanArray(response);
                      interestsFilter(interests, skillsOutput, function(interestErr, interestResponse) {
                        getFinalCampusList(interestResponse, function(interestErr, interestOutput) {
                          if (interestErr) {
                            errorFunction(interestErr, cb);
                          } else {
                            cb(null, interestOutput);
                          }
                        });
                      });
                    }
                  });
                } else {
                  getFinalCampusList(output, function(proErr, programOutput) {
                    if (proErr) {
                      errorFunction(proErr, cb);
                    } else {
                      cb(null, programOutput);
                    }
                  });
                }
              });
            } else {
              errorFunction('Invalid Input', cb);
            }
          });
        }
      });
    } else {
      errorFunction('Invalid Input', cb);
    }
  };
  function filterCampuses(campusInformation, secondArray, cb) {
    async.map(secondArray, getList, function(err, list) {
      if (err) {
        errorFunction(err, cb);
      } else {
        cb(null, list);
      }
    });
    function getList(obj, callBack) {
      var campusId = campusInformation.find(findCampus);
      function findCampus(value) {
        return value.campusId === obj.campusId;
      }
      callBack(null, campusId);
    }
  }
  function getSkills(obj, callBc) {
    var skillsObj = {};
    skillsObj['skillTypeValueId'] = (obj.skillTypeValueId) ? obj.skillTypeValueId : undefined;
    getResponse(skillsObj, function(skillsErr, skillsInfo) {
      if (skillsErr) {
        errorFunction(skillsErr, callBc);
      } else {
        callBc(null, skillsInfo);
      }
    });
  }
  function getInterests(obj, callBc) {
    var interestObj = {};
    interestObj['interestTypeValueId'] = (obj.interestTypeValueId) ? obj.interestTypeValueId : undefined;
    getResponse(interestObj, function(interestErr, interestInfo) {
      if (interestErr) {
        errorFunction(interestErr, callBc);
      } else {
        callBc(null, interestInfo);
      }
    });
  }
  function getProram(object, callBack) {
    var campusObj = {};
    campusObj['programCatValueId'] = (object.programCatValueId) ? object.programCatValueId : undefined;
    campusObj['programClassValueId'] = (object.programClassValueId) ? object.programClassValueId : undefined;
    campusObj['programTypeValueId'] = (object.programTypeValueId) ? object.programTypeValueId : undefined;
    campusObj['programMajorValueId'] = (object.programMajorValueId) ? object.programMajorValueId : undefined;
    getResponse(campusObj, function(programErr, programInfo) {
      if (programErr) {
        errorFunction(programErr, callBack);
      } else {
        callBack(null, programInfo);
      }
    });
  }

  function getCampusData(obj, cb) {
    var campusObj = {};
    campusObj['campusId'] = (obj.campusId) ? obj.campusId : undefined;
   
    // getResponse(campusObj, function(campusErr, campusInfo) {
    //   if (campusErr) {
    //     errorFunction(campusErr, cb);
    //   } else {
    //     console.log('//////////', campusInfo);
    //     cb(null, campusInfo[0]);
    //   }
    // });
    var campusSearch = server.models.CampusSearchVw;
    campusSearch.findOne({'where': {'campusId': obj.campusId}}, function(campusErr, campusInfo) {
      cb(null, campusInfo);
    });
  }

  function getResponse(inputObj, cb) {
    campusDetails(inputObj, CAMPUS_SRCH_PROG_SKILL_INT_VW, function(error, response) {
      var finalResponse = (response) ? response.data : null;
      if (error) {
        errorFunction(error, cb);
      } else {
        cb(null, finalResponse);
      }
    });
  }

  function skillsFilter(skills, output, cb) {
    async.map(skills, getSkills, function(skillsError, skillsList) {
      if (skillsError) {
        errorFunction(skillsError, cb);
      } else {
        var listOfSkills = skillsList[0];
        filterCampuses(output, listOfSkills, function(skillsErr, skillCampuses) {
          cb(null, skillCampuses);
        });
      }
    });
  }

  function interestsFilter(interests, output, cb) {
    async.map(interests, getInterests, function(interestError, interestList) {
      if (interestError) {
        errorFunction(interestError, cb);
      } else {
        var listOfInterests = interestList[0];
        filterCampuses(output, listOfInterests, function(interestErr, interestCampuses) {
          cb(null, interestCampuses);
        });
      }
    });
  }
  function getFinalCampusList(campusSearchResult, callBc) {
    async.map(campusSearchResult, returnCampusList, function(error, finalList) {
      callBc(null, finalList);
    });
    function returnCampusList(obj, cb) {
      var campusSearchVw = server.models.CampusSearchVw;
      var searchCampus = campusSearchVw.searchCampus;
      searchCampus(obj.campusId, undefined, undefined, undefined, undefined, undefined, undefined, 'N', undefined, undefined, 0, 0, undefined, undefined, function(err, searchResponse) {
        cb(null, searchResponse[0]);
      });
    }
  }
  
   // remote method declaration to get campusDetails details
  CAMPUS_SRCH_PROG_SKILL_INT_VW.remoteMethod('skillsInterestsFilter', {
    description: 'Send Valid Details ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/skillsInterestsFilter',
      verb: 'POST',
    },
  });
};

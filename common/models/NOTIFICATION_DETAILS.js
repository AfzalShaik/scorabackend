'use strict';
var server = require('../../server/server');
module.exports = function(NOTIFICATION_DETAILS) {
  NOTIFICATION_DETAILS.pullNotification = function(input, callBack) {
    var createPull = require('../../commonValidation/create-pull-notification.js').createPullNotification;
    createPull(input, function(error, output) {
      if (error) {
        callBack(error, null);
      } else {
        callBack(null, output);
      }
    });
  };
  NOTIFICATION_DETAILS.remoteMethod('pullNotification', {
    description: 'Send Valid Input ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/pullNotification',
      verb: 'POST',
    },
  });

  // update campus event Id service
  NOTIFICATION_DETAILS.updtaeNotificationEvent = function(input, eventCB) {
    var notificationEvent = server.models.NotificationEvents;
    notificationEvent.findOne({
      'where': {
        'notificationEventId': input.notificationEventId,
      },
    }, function(err, resp) {
      if (err) {
        eventCB(err, null);
      } else {
        resp.transactionId = input.campusEventId;
        resp.updateAttributes(resp, function(updateErr, updateResp) {
          if (updateErr) {
            eventCB(updateErr, null);
          } else {
            eventCB(null, updateResp);
          }
        });
      }
    });
  };
  NOTIFICATION_DETAILS.remoteMethod('updtaeNotificationEvent', {
    description: 'Send Valid Input ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updtaeNotificationEvent',
      verb: 'PUT',
    },
  });
};

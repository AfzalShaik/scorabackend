'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(PROGRAM_CONTACT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;

  // remote method definition to create contact for particular program
   /**
 *createProgramContact- To Create Program Conatct Details
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  PROGRAM_CONTACT.createProgramContact = function(data, callBc) {
    if (data.programId && data.campusId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = data.campusId;
      inputFilterObject['programId'] = data.programId;
      var contactModel = server.models.Contact;
      var programModel = server.models.Program;
      checkEntityExistence(programModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          contactModel.create(data, function(error, contactRes) {
            if (error) {
              callBc(error, null);
            } else if (contactRes) {
              PROGRAM_CONTACT.create({
                'programId': Number(data.programId),
                'campusId': Number(data.campusId),
                'contactId': Number(contactRes.contactId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  errorResponse(callBc);
                } else {
                  // contactRes.requestStatus = true;
                  callBc(null, contactRes);
                }
              });
            } else {
              errorResponse(callBc);
            }
          });
        } else {
          errorResponse(callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    PROGRAM_CONTACT.find({'where': {'programId': data.programId,
    'campusId': data.campusId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        PROGRAM_CONTACT.updateAll({'programId': data.programId,
        'campusId': data.campusId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create contact for particular program
  PROGRAM_CONTACT.remoteMethod('createProgramContact', {
    description: 'Useful to create contact for particular program',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createProgramContact',
      verb: 'post',
    },
  });
  // remote method definition to get  campus program contact details
    /**
 *getProgramContact- To get program contact details by taking required fields
 *@constructor
 * @param {object} programId - Unique id for ecah and every program
 * @param {number} contactId - unique id for each and every contact
 * @param {function} callBc - deals with response
 */
  PROGRAM_CONTACT.getProgramContact = function(programId, contactId, callBc) {
    var inputObj = {};
    var programContactModel = server.models.ProgramContact;
    var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
    var programContact = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (programId && contactId) {
      inputObj['programId'] = programId;
      inputObj['contactId'] = contactId;
      // below function will give contact details for an entity based on loopback include filter
      programContact(inputObj, programContactModel, 'programContact', callBc);
    } else if (programId) {
      inputObj['programId'] = programId;
      // below function will give contact details for an entity based on loopback include filter
      programContact(inputObj, programContactModel, 'programContact', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get  campus program contact details
  PROGRAM_CONTACT.remoteMethod('getProgramContact', {
    description: 'To get get  campus program contact details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'programId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getProgramContact',
      verb: 'get',
    },
  });
/*  // remote method definition to delete campus program contact details
  PROGRAM_CONTACT.deleteProgramContact = function(programId, contactId, callBc) {
    if (programId && contactId) {
      var programContactModel = server.models.ProgramContact;
      var contactModel = server.models.Contact;
      var delprogramContact = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['programId'] = programId;
      // inputFilterObject['campusId']=campusId;
      inputFilterObject['contactId'] = contactId;
      delprogramContact(primaryCheckInput, inputFilterObject, contactModel, programContactModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus program contact details
  PROGRAM_CONTACT.remoteMethod('deleteProgramContact', {
    description: 'To delete campus program contact details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'programId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
      {
        arg: 'contactId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/deleteProgramContact',
      verb: 'delete',
    },
  });*/
  //  PROGRAM remoteMethod
   /**
 *updateProgramContact- To update program contact Details
 *@constructor
 * @param {object} programData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  PROGRAM_CONTACT.updateProgramContact = function(programData, cb) {
    var programUpdate = require('../../commonCampusFiles/update-program-contact.js');
    if (programData.primaryInd == 'Y') {
      searchForInd(programData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    programUpdate.programContactUpdate(programData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = programData.primaryInd;
        PROGRAM_CONTACT.updateAll({'campusId': programData.campusId,
        'contactId': programData.contactId, 'programId': programData.programId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };
  //update PROGRAM remoteMethod
  PROGRAM_CONTACT.remoteMethod('updateProgramContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateProgramContact',
      verb: 'PUT',
    },
  });

  // remote method definition to deleteProgramContact
    /**
 *deleteProgramContact- To delete program contact details by taking required fields
 *@constructor
 * @param {object} campusId - Unique id for ecah and every campu
 * @param {number} contactId - unique id for each and every contact
 * @param {number} programId - unique id for every program
 * @param {function} callBc - deals with response
 */
  PROGRAM_CONTACT.deleteProgramContact = function(campusId, contactId, programId, callBc) {
    if (campusId && contactId && programId) {
      var programContact = server.models.ProgramContact;
      var contactModel = server.models.Contact;
      var deleteProgramContactDet = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      inputFilterObject['contactId'] = contactId;
      inputFilterObject['programId'] = programId;
      deleteProgramContactDet(primaryCheckInput, inputFilterObject, contactModel, programContact, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to deleteProgramContact
  PROGRAM_CONTACT.remoteMethod('deleteProgramContact', {
    description: 'To delete program contact',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteProgramContact',
      verb: 'delete',
    },
  });
};

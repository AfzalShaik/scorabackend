'use strict';
module.exports = function(COMPANY_UPLOAD_LOG) {
  COMPANY_UPLOAD_LOG.getAllUploads = function(companyId, cb) {
    if (companyId) {
      COMPANY_UPLOAD_LOG.find({'where': {'companyId': companyId}},
                  function(err, response) {
                    if (err) {
                      cb(err, null);
                    } else {
                      cb(null, response);
                    }
                  });
    } else {
      errorResponse('results not found', cb);
    }
  };
              //accpect method to update campus drive
  COMPANY_UPLOAD_LOG.remoteMethod('getAllUploads', {
    description: 'Event details ',
    returns: {
      arg: 'array',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        require: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/getAllUploads',
      verb: 'GET',
    },
  });
};

'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(EDUCATION_PERSON_CONTACT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // remote method definition to create contact for education person contact
   /**
 *createEducationContact- To create education Contact
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  EDUCATION_PERSON_CONTACT.createEducationContact = function(data, callBc) {
    if (data.educationPersonId && data.campusId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = data.campusId;
      inputFilterObject['educationPersonId'] = data.educationPersonId;
      var contactModel = server.models.Contact;
      var educationPersonModel = server.models.EducationPerson;
      checkEntityExistence(educationPersonModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          contactModel.create(data, function(error, contactRes) {
            if (error) {
              logger.error('error while creating education person contact model');
              callBc(error, null);
            } else if (contactRes) {
              EDUCATION_PERSON_CONTACT.create({
                'educationPersonId': Number(data.educationPersonId),
                'campusId': Number(data.campusId),
                'contactId': Number(contactRes.contactId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  errorResponse(callBc);
                } else {
                  logger.info('education person contact service created successfully');
                  // contactRes.requestStatus = true;
                  callBc(null, contactRes);
                }
              });
            } else {
              logger.error('empty response while creating education person contact model');
              errorResponse(callBc);
            }
          });
        } else {
          logger.error('error while validating education person before creating education person contact');
          errorResponse(callBc);
        }
      });
    } else {
      logger.error('error while validating education person contact create method input params');
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    EDUCATION_PERSON_CONTACT.find({'where': {'educationPersonId': data.educationPersonId,
    'campusId': data.campusId, 'primaryInd': 'Y'}}, function(err, res) {
      // console.log(res);
      if (err) {
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        EDUCATION_PERSON_CONTACT.updateAll({'educationPersonId': data.educationPersonId,
        'campusId': data.campusId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create contact for education person contact
  EDUCATION_PERSON_CONTACT.remoteMethod('createEducationContact', {
    description: 'Useful to create contact for education person contact',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createEducationContact',
      verb: 'post',
    },
  });
  // remote method definition to get education person contact details
    /**
 *getEducationPersonContact- To get Education person contact details by taking required fields
 *@constructor
 * @param {object} educationPersonId - Unique id for ecah and every education person
 * @param {number} campusId - unique id for each and every campus
 * @param {number} contactId - unique id for every contact
 * @param {function} callBc - deals with response
 */
  EDUCATION_PERSON_CONTACT.getEducationPersonContact = function(educationPersonId, campusId, contactId, callBc) {
    var inputObj = {};
    var educationPersonContactModel = server.models.EducationPersonContact;
    var educationPersonContactDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (educationPersonId && campusId && contactId) {
      inputObj['educationPersonId'] = educationPersonId;
      inputObj['campusId'] = campusId;
      inputObj['contactId'] = contactId;
      // below function will give contact details for an entity based on loopback include filter
      logger.info('retrieved education-person-contact details successfully based on educationPersonId,campusId,contactId');
      educationPersonContactDet(inputObj, educationPersonContactModel, 'educationPersonContact', callBc);
    } else if (educationPersonId && campusId) {
      inputObj['educationPersonId'] = educationPersonId;
      inputObj['campusId'] = campusId;
      // below function will give contact details for an entity based on loopback include filter
      logger.info('retrieved education-person-contact details successfully based on educationPersonId and campusId');
      educationPersonContactDet(inputObj, educationPersonContactModel, 'educationPersonContact', callBc);
    } else {
      logger.error('error while retrieving education-person-contact details');
      errorResponse(callBc);
    }
  };
  // remote method declaration to get education person contact details
  EDUCATION_PERSON_CONTACT.remoteMethod('getEducationPersonContact', {
    description: 'To get education person contact details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'educationPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEducationPersonContact',
      verb: 'get',
    },
  });
  //  EDUCATION_PERSON remoteMethod
   /**
 *updateEducationPersonContact- To update Education person contact
 *@constructor
 * @param {object} personData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EDUCATION_PERSON_CONTACT.updateEducationPersonContact = function(personData, cb) {
    var personUpdate = require('../../commonCampusFiles/update-education-person-contact.js');
    if (personData.primaryInd == 'Y')  {
      searchForInd(personData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    personUpdate.updateEducationPersonContact(personData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = personData.primaryInd;
        EDUCATION_PERSON_CONTACT.updateAll({'campusId': personData.campusId,
        'contactId': personData.contactId, 'educationPersonId': personData.educationPersonId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //update EDUCATION_PERSON remoteMethod
  EDUCATION_PERSON_CONTACT.remoteMethod('updateEducationPersonContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEducationPersonContact',
      verb: 'PUT',
    },
  });

  // remote method definition to deleteEducationPersonContact
    /**
 *deleteEducationPersonContact- To delete education person contact by taking required fields
 *@constructor
 * @param {object} campusId - Unique id for ecah and every campus
 * @param {number} contactId - unique id for each and every contact
 * @param {number} educationPersonId - unique id for every education person
 * @param {function} callBc - deals with response
 */
  EDUCATION_PERSON_CONTACT.deleteEducationPersonContact = function(campusId, contactId, educationPersonId, callBc) {
    if (campusId && contactId && educationPersonId) {
      var EducationPersonContact = server.models.EducationPersonContact;
      var contactModel = server.models.Contact;
      var deleteEducationPersonContact = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      inputFilterObject['contactId'] = contactId;
      inputFilterObject['educationPersonId'] = educationPersonId;
      deleteEducationPersonContact(primaryCheckInput, inputFilterObject, contactModel, EducationPersonContact, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to deleteEducationPersonContact
  EDUCATION_PERSON_CONTACT.remoteMethod('deleteEducationPersonContact', {
    description: 'To delete education person contact',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'educationPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteEducationPersonContact',
      verb: 'delete',
    },
  });
};

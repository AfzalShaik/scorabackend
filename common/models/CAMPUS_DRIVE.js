'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(CAMPUS_DRIVE) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  CAMPUS_DRIVE.observe('before save', function campusDriveBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.campusDriveId == undefined) {
      var campusDriveTypeCode = 'EDUCATION_DRIVE_TYPE_CODE';
      var campusDriveTypeObj = {};
      campusDriveTypeObj['lookupValueId'] = ctx.instance.driveTypeValueId;
      lookupMethods.typeCodeFunction(campusDriveTypeObj, campusDriveTypeCode, function(campusDriveTypeCodeCheck) {
        if (campusDriveTypeCodeCheck) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('Campus Drive Creation Initiated');
          next();
        } else {
          errorFunction('campusDriveTypeCode validation error', next);
        }
      });
    } else if (ctx.isNewInstance) {
      errorFunction('campusDriveId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get campus drive details
  /**
 *getCampusDriveDetails- To get drive details using campus drive id
 *@constructor
 * @param {number} campusDriveId - unique id for campus drive
 * @param {function} callBc - deals with response
 */
  CAMPUS_DRIVE.getCampusDriveDetails = function(campusId, campusDriveId, callBc) {
    var inputObj = {};
    var campusDriveModel = server.models.CampusDrive;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (campusId || campusDriveId) {
      inputObj['campusId'] = campusId;
      inputObj['campusDriveId'] = (campusDriveId) ? campusDriveId : undefined;
      var includeModels = ['campusDriveTypeLookupValue', 'campusDriveStatusLookupValue'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, campusDriveModel, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get campus drive details
  CAMPUS_DRIVE.remoteMethod('getCampusDriveDetails', {
    description: 'To get campus drive details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusDriveId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getCampusDriveDetails',
      verb: 'GET',
    },
  });
  //updateCampusDrive remote method starts here
  /**
 *updateCampusDrive - To update campuys drive data
 *@constructor
 * @param {object} campusDriveData - Contains all the data need to get updated
 * @param {function} callBc - deals with response
 */
  CAMPUS_DRIVE.updateCampusDrive = function(campusDriveData, cb) {
    var campusDriveUpdate = require('../../commonCampusFiles/update-campus-drive.js');
    campusDriveUpdate.updateCampusDriveService(campusDriveData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //updateCampusDrive method to update campus drive
  CAMPUS_DRIVE.remoteMethod('updateCampusDrive', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCampusDrive',
      verb: 'PUT',
    },
  });
  var driveData = [];
  var inProgress = 0;
  var totalEvents = 0;
  var sheduled = 0;
  CAMPUS_DRIVE.getAllDriveDetails = function(campusId, cb) {
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    console.log(currentYear + 1);
    if (month >= 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month < 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    var eventList = server.models.EventStudentList;
    CAMPUS_DRIVE.find({'where': {'campusId': campusId, 'driveStatusValueId': {
      'neq': 234,
    }, 'createDatetime': {
      between: [startDate, endDate],
    }}}, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        console.log(startDate, endDate);
        var async = require('async');
        async.map(response, getAllDrivesAndStatusCounts,
          function(err, drivesResponse) {
            if (err) {
              cb(err, null);
            } else {
              // var drive = {};
              // drive.DriveInfo = driveData;
              cb(null, driveData);
              driveData = [];
            }
          });
      }
    });
  };
  function getAllDrivesAndStatusCounts(object, callBack) {
    var campusEvent = server.models.CampusEvent;
    campusEvent.find({'where': {'campusDriveId': object.campusDriveId}},
    function(err, eventDetails) {
      if (err) {
        callBack(err, null);
      } else {
        var async = require('async');
        async.map(eventDetails, getEventDetails, function(err, countResponse) {
          if (err) {
            callBack(err, null);
          } else {
            var driveDetails = {};
            driveDetails.name = object.driveName;
            driveDetails.totalEvents = totalEvents;
            driveDetails.inProgress = inProgress;
            // driveDetails.sheduled = sheduled;
            driveData.push(driveDetails);
            callBack(null, countResponse);
            totalEvents = 0;
            inProgress = 0;
          }
        });
      }
    });
  }
  function getEventDetails(obj, cb) {
    if (obj.eventStatusValueId === 239) {
      inProgress++;
      totalEvents++;
      cb(null, 'done');
    } else if (obj.eventStatusValueId === 243) {
      sheduled++;
      totalEvents++;
      cb(null, 'done');
    } else {
      totalEvents++;
      cb(null, 'done');
    }
  }
  //updateCampusDrive method to update campus drive
  CAMPUS_DRIVE.remoteMethod('getAllDriveDetails', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      require: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getAllDriveDetails',
      verb: 'GET',
    },
  });
  var driveDataOfCampus = [];
  var inProgressCampusEvents = 0;
  var totalCampusEvents = 0;
  var totalCampusEventsSheduled = 0;
  CAMPUS_DRIVE.getAllDriveDetailsCampusDashbord = function(campusId, cb) {
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    console.log(currentYear + 1);
    if (month >= 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month < 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    var eventList = server.models.EventStudentList;
    CAMPUS_DRIVE.find({'where': {'campusId': campusId, 'driveStatusValueId': {
      'neq': 234,
    }, 'createDatetime': {
      between: [startDate, endDate],
    }}}, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        console.log(startDate, endDate);
        var async = require('async');
        async.map(response, getAllDrivesAndStatusDetails,
          function(err, drivesResponse) {
            if (err) {
              cb(err, null);
            } else {
              // var drive = {};
              // drive.DriveInfo = driveData;
              cb(null, driveDataOfCampus);
              driveDataOfCampus = [];
            }
          });
      }
    });
  };
  function getAllDrivesAndStatusDetails(object, callBack) {
    var campusEvent = server.models.CampusEvent;
    campusEvent.find({'where': {'campusDriveId': object.campusDriveId}},
    function(err, eventDetails) {
      if (err) {
        callBack(err, null);
      } else {
        var async = require('async');
        async.map(eventDetails, getEventCounts, function(err, countResponse) {
          if (err) {
            callBack(err, null);
          } else {
            var driveDetails = {};
            driveDetails['Drive Name'] = object.driveName;
            driveDetails['No. of Events'] = totalCampusEvents;
            driveDetails['Events in Progress'] = inProgressCampusEvents;
            driveDetails['Events Scheduled'] = totalCampusEventsSheduled;
            driveDataOfCampus.push(driveDetails);
            callBack(null, countResponse);
            totalCampusEvents = 0;
            totalCampusEventsSheduled = 0;
            inProgressCampusEvents = 0;
          }
        });
      }
    });
  }
  function getEventCounts(obj, cb) {
    if (obj.eventStatusValueId === 239) {
      inProgressCampusEvents++;
      totalCampusEvents++;
      cb(null, 'done');
    } else if (obj.eventStatusValueId === 243) {
      totalCampusEventsSheduled++;
      totalCampusEvents++;
      cb(null, 'done');
    } else {
      totalCampusEvents++;
      cb(null, 'done');
    }
  }
  //updateCampusDrive method to update campus drive
  CAMPUS_DRIVE.remoteMethod('getAllDriveDetailsCampusDashbord', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      require: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getAllDriveDetailsCampusDashbord',
      verb: 'GET',
    },
  });
};

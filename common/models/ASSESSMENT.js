//requiring server path so that we can use exported functions or methods when ever we need
'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

// exporting function to use it in another modules if requires

/**
 * @param {function} ASSESSMENT
 */
module.exports = function(ASSESSMENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var errorWithId = require('../../ErrorHandling/customizeErrorTypes.js').errorWithId;
  //var Joi = require('joi');
 /**
 *Creating a new user with assessmentBeforeSave
 *@constructor
 * @param {obj} ctx-which was the input from api
 * @param {method} next-to skip the function if the exe was already done
 */
  ASSESSMENT.observe('before save', function assessmentBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
 //getting data from database
/**
 * the functions which deals with getting data from the database
 * @constructor
 * @param {number} assessmentId- which deals as a primiry key to retrive the data from the database
 * @param {function} callBc-which take cares of the response
 */
  ASSESSMENT.getAssessmentDet = function(assessmentId, callBc) {
    var inputObj = {};
    var assessment = server.models.Assessment;
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (assessmentId) {
      inputObj['assessmentId'] = assessmentId;
      findEntity(inputObj, assessment, function(err, res) {
        if (err) {
          var object = {};
          object.messageCategeory = 1;
          object.messageId = 3;
          errorWithId(object, function(response) {
            console.log(response);
            var error = {};
            error.message = response.messageText;
            error.messageCategeory = response.messageCategeory;
            error.messageId = response.messageId;
            callBc(error, null);
          });
          //errorResponse(err, callBc);
        } else if (res == 0) {
          var object = {};
          object.messageCategeory = 1;
          object.messageId = 3;
          errorWithId(object, function(err, res) {
            if (err) {
              callBc(err, null)
            } else {
              callBc(res, null);
            }
          });
         // errorResponse('result not found Invalid Id', callBc);
        } else {
          var result = require('../../server/boot/caching/labelCaching.js');
          var lookupValue = server.models.LookupValue;
          callBc(null, res);
        }
      });
    } else {
      errorResponse('result not found', callBc);
    } };
/**
 * remote method---
 * exe when ever getAssessmentDet was called
 * accpects-assessmentid
 * gets-array
 */
  ASSESSMENT.remoteMethod('getAssessmentDet', { // TODO: CODEREVIEW - VARA – 09SEP17 -- Don’t abbreviate, give full names to method names, descriptions, comments
    description: 'To get student assessment details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {arg: 'assessmentId ',
             type: 'number',
             required: true,
             http: {source: 'query'}},
    http: {
      path: '/getAssessmentDet',
      verb: 'get',
    },
  });
/**
 *updateAssessmentDet which deals with the updating required data in the database
 * @constructor
 * @param {function} updateAssessmentDet
 * @param {object} assessmentdata-which has all the data user want to update
 * @param {callback} cb-acts as a call back function holds the response and status
 */
  ASSESSMENT.updateAssessmentDet = function(assessmentData, cb) {
    var assessmentUpdate = require('../../update/assessment-update.js');
    assessmentUpdate.updateAssessment(assessmentData, function(err, resp) {
      if (err) {
        // response.statusCode = 404;
        cb(err, resp);
      } else if (resp == 0) {
        errorResponse('result not found Invalid Id', cb);
      } else {
        cb(null, resp);
      }
    });
  };
/**
* remote method for update assessment
*/
  ASSESSMENT.remoteMethod('updateAssessmentDet', {// TODO: CODEREVIEW - VARA – 09SEP17 -- Don’t abbreviate, give full names to method names, descriptions, comments
    description: 'update assessment details',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateAssessmentDet',
      verb: 'PUT',
    },
  });
};

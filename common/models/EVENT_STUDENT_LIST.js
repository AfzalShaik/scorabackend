'use strict';
var server = require('../../server/server');
var getSkills = require('../models/CAMPUS_EVENT_STUDENT_SEARCH_VW.js').getStudentIntrestsAndSkills;
module.exports = function(EVENT_STUDENT_LIST) {
  EVENT_STUDENT_LIST.observe('before save', function eventStudentListBeforeSave(ctx, next) {
    // console.log('ctx.isNewInstance: ' + ctx.isNewInstance);
    if (ctx.isNewInstance) {
      console.log('student id', ctx.instance.studentId);
      EVENT_STUDENT_LIST.findOne({
        'where': {
          'studentId': ctx.instance.studentId,
          'campusEventId': ctx.instance.campusEventId
        }, 'include': 'eventStudentListIbfk1rel'
      }, function(err, response) {
        console.log(response);
        if (err) {
          next(err, null);
        } else if (response == null) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          next();
        } else {
          if (response.registrationInd == 'N') {
            EVENT_STUDENT_LIST.findOne({
              'where': {
                'studentId': ctx.instance.studentId,
                'employerEventId': ctx.instance.employerEventId, 'registrationInd': 'N'
              }
            },
              function(err, responseData) {
                if (err) {
                  next(err, null);
                } else if (responseData == 0) {
                  ctx.instance.updateUserId = ctx.instance.createUserId;
                  ctx.instance.createDatetime = new Date();
                  ctx.instance.updateDatetime = new Date();
                } else {
                  next(err, null);
                }
              });
          } else {
            var studentData = response.toJSON();
            var studentInformation = (studentData.eventStudentListIbfk1rel.firstName) + ' ' +
              (studentData.eventStudentListIbfk1rel.lastName) + ' already part of Event';
            next(studentInformation, null);
          }
        }
      });
    } else {
      next();
    }
  });
  //studentEvents remote method starts here
  /**
   *studentEvents- To get student event details by taking required fields
   *@constructor
   * @param {object} stData - contains the id for required data
   * @param {function} cb - deals with response
   */
  EVENT_STUDENT_LIST.studentEvents = function(stData, cb) {
    var getEvents = require('../../commonCampusFiles/get-student-events');
    getEvents.getStudentEvents(stData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //studentEvents method creation

  EVENT_STUDENT_LIST.remoteMethod('studentEvents', {
    description: 'To get student event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/studentEvents',
      verb: 'get',
    },
  });
  EVENT_STUDENT_LIST.createStudentUnreg = function(stData, cb) {
    EVENT_STUDENT_LIST.create(stData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //studentEvents method creation

  EVENT_STUDENT_LIST.remoteMethod('createStudentUnreg', {
    description: 'To get student event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'obj',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createStudentUnreg',
      verb: 'post',
    },
  });
  //studentEventActions remote method starts here
  /**
   *studentEventActions- Deals with the actions of student event
   *@constructor
   * @param {object} stData - contains all the data need to get updated
   * @param {function} cb - deals with response
   */
  EVENT_STUDENT_LIST.studentEventActions = function(stData, cb) {
    var getEvents = require('../../commonCampusFiles/update-student-events');
    stData.updateDatetime = new Date();
    console.log('ssssssss', stData);
    getEvents.updateStudentEvents(stData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //studentEventActions method creation
  EVENT_STUDENT_LIST.remoteMethod('studentEventActions', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/studentEventActions',
      verb: 'PUT',
    },
  });
  var studentDetails = [];
  EVENT_STUDENT_LIST.getStudentsAssociatedWithCampusEvent = function(campusEventId, empEventId, cb) {
    EVENT_STUDENT_LIST.find({
      'where': {
        'and': [{
          'campusEventId': campusEventId,
        },
        {
          'employerEventId': empEventId,
        },
        ],
      },
    }, function(err, res) {
      // console.log('.................................... ', res);
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        studentDetails = [];
        async.map(res, getStudents, function(err1, res1) {
          if (err) {
            cb(err, null);
          } else {
            cb(null, studentDetails);
            studentDetails = [];
          }
        });
      }
    });
    var totalSkills = [];
    var totalIntrests = [];

    function getStudents(obj, cb1) {
      // var skills = server.models.StudentSkills;
      // skills.find({'where': {'studentId': obj.studentId}}, function(err, res) {
      //   if (err) {
      //     cb1(err, null);
      //   } else {
      // var intrests = server.models.StudentInterests;
      // intrests.find({'where': {'studentId': obj.studentId}}, function(err1, res1) {
      //   if (err1) {

      //   } else {
      // var async = require('async');
      // async.map(res1, getIntrestName, function(err17, res17) {
      //   if (err17) {

      //   } else {
      var skills = server.models.CampusEventStudentSearchVw;
      skills.getSkillsAndInt(obj, function(err18, res18) {
        // console.log('...................', res18);
        if (err18) {
          cb1(err18, null);
        } else {
          var eventstudent = server.models.EventStudentList;
          eventstudent.find({
            'where': {
              'studentId': obj.studentId,
            },
          }, function(err5, res5) {
            if (err5) {
              cb1(err5, null);
            } else {
              var enrolled = res5.length;
              getIds(function(err6, res6) {
                if (err6) {
                  cb1(err6, null);
                } else {
                  getAllCounts(res6, obj, function(err9, res9) {
                    if (err9) {
                      cb1(err9, null);
                    } else {
                      var student = server.models.CampusEventStudentSearchVw;
                      student.findOne({
                        'where': {
                          'studentId': obj.studentId,
                        },
                      }, function(err2, res2) {
                        if (err2) {
                          cb1(err2, null);
                        } else {
                          // totalSkills = [];
                          // totalIntrests = [];
                          // var async = require('async');
                          // async.map(res, getSkillName, function(err16, res16) {
                          //   if (err16) {
                          //     cb1(err16, null);
                          //   } else {
                          var data = {};

                          // data.Skills = totalSkills;
                          // data.Intrests = totalIntrests;
                          var eventStudentList = server.models.EventStudentList;
                          eventStudentList.findOne({
                            'where': {
                              'and': [{
                                'studentId': res18.studentInfo.studentId,
                              }, {
                                'employerEventId': obj.employerEventId,
                              }, {
                                'campusEventId': campusEventId,
                              }],
                            },
                            'include': 'eventStudentListIbfk7rel'
                          }, function(statusErr, status) {
                            // console.log('=================== ',  status['eventStudentListIbfk7rel']);
                            // data.StudentInfo.studentStatus = {};
                            // data.studentInfo.studentStatus.eventStudentListIbfk7rel = {};
                            var studentStatus = status.toJSON();
                            // console.log('statssssssss', stat.eventStudentListIbfk7rel);
                            if (status.compPackageId == null) {
                              studentStatus['totalCompPkgValue'] = null;
                              //status['packageName'] = null;
                            } else {
                              studentStatus['totalCompPkgValue'] = studentStatus.eventStudentListIbfk7rel.totalCompPkgValue;
                              delete studentStatus.eventStudentListIbfk7rel;
                              // console.log('statusssss', status);
                              //status['packageName'] = stat.eventStudentListIbfk7rel.compPackageName;
                            }
                            data.StudentInfo = res18;
                            data.StudentInfo.student = res2;
                            data.StudentInfo.studentStatus = studentStatus;
                            data.totalOffers = res9.totalOffers;
                            data.totalShortlisted = res9.totalShortlisted;
                            data.enrolled = res5.length;
                            studentDetails.push(data);
                            totalSkills = [];
                            totalIntrests = [];
                            cb1(null, 'done');
                          });

                          //   }
                          // });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
      //   }
      // });
      //   }
      // });
      //   }
      // });
    }
  };

  function getDetails(skill, intrest, cb6) {
    totalIntrests = [];
    totalSkills = [];
    console.log(skill);
  }

  function getSkillName(ob, cb5) {
    lookupValueName('SKILL_TYPE_CODE', ob.skillTypeValueId, function(err, res) {
      if (err) {
        cb5(err, null);
      } else {
        totalSkills.push(res[0].lookupValue);
        cb5(null, 'done');
      }
    });
  }

  function getIntrestName(ob, cb5) {
    lookupValueName('INTEREST_TYPE_CODE', ob.interestTypeValueId, function(err, res) {
      if (err) {
        cb5(err, null);
      } else {
        totalIntrests.push(res[0].lookupValue);
        cb5(null, 'done');
      }
    });
  }

  function getAllCounts(lookup, studentDetails, cb3) {
    var eventstudent = server.models.EventStudentList;
    eventstudent.find({
      'where': {
        and: [{
          'studentId': studentDetails.studentId,
        },
        {
          'candidateStatusValueId': lookup.Offered,
        },
        ],
      },
    }, function(err10, res10) {
      if (err10) {
        cb3(err10, null);
      } else {
        eventstudent.find({
          'where': {
            and: [{
              'studentId': studentDetails.studentId,
            },
            {
              'candidateStatusValueId': lookup.Shortlisted,
            },
            ],
          },
        }, function(err11, res11) {
          if (err10) {
            cb3(err11, null);
          } else {
            var data = {};
            data.totalOffers = res10.length;
            data.totalShortlisted = res11.length;
            cb3(null, data);
          }
        });
      }
    });
  }

  function getIds(cb2) {
    lookup('CANDIDATE_STATUS_TYPE', 'Offered', function(err7, res7) {
      if (err7) {
        cb2(err7, null);
      } else {
        lookup('CANDIDATE_STATUS_TYPE', 'Shortlisted', function(err8, res8) {
          if (err8) {
            cb2(err8, null);
          } else {
            var lookupData = {};
            lookupData.Offered = res7[0].lookupValueId;
            lookupData.Shortlisted = res8[0].lookupValueId;
            cb2(null, lookupData);
          }
        });
      }
    });
  }

  function lookup(type, ob, lookupCallback) {
    var lookup = server.models.LookupType;
    var lookupvalue = server.models.LookupValue;
    lookup.find({
      'where': {
        lookupCode: type,
      },
    }, function(err, re) {
      if (err) {
        lookupCallback(err, null);
      } else {
        lookupvalue.find({
          'where': {
            lookupValue: ob,
            lookupTypeId: re[0].lookupTypeId,
          },
        }, function(err, re1) {
          if (err) {
            lookupCallback('error', null);
          } else {
            lookupCallback(null, re1);
          };
        });
      }
    });
  }

  function lookupValueName(type, ob, lookupCallback) {
    var lookup = server.models.LookupType;
    var lookupvalue = server.models.LookupValue;
    //var type = 'SKILL_TYPE_CODE';
    lookup.find({
      'where': {
        lookupCode: type,
      },
    }, function(err, re) {
      if (err) {
        lookupCallback(err, null);
      } else {
        lookupvalue.find({
          'where': {
            lookupValueId: ob,
            lookupTypeId: re[0].lookupTypeId,
          },
        }, function(err, re1) {
          if (err) {
            lookupCallback('error', null);
          } else {
            lookupCallback(null, re1);
          };
        });
      }
    });
  }
  EVENT_STUDENT_LIST.remoteMethod('getStudentsAssociatedWithCampusEvent', {
    description: 'To get student event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'empEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getStudentsAssociatedWithCampusEvent',
      verb: 'get',
    },
  });

  // Remove Student's from event remote method
  EVENT_STUDENT_LIST.removeStudent = function(input, callBC) {
    var inputObj = {};
    inputObj['studentListId'] = input.studentListId;
    inputObj['campusId'] = input.campusId;
    EVENT_STUDENT_LIST.findOne({
      'where': {
        'and': [inputObj],
      },
    }, function(error, response) {
      // console.log(error, response);
      if (response) {
        var updateObj = {};
        updateObj = response;
        updateObj.campusPublishInd = 'N';
        response.updateAttributes(updateObj, function(err, resp) {
          // console.log(err, resp);
          if (resp) {
            var pushInput = {
              'userId': input.userId,
              'role': 'PLCDIR',
              'campusId': input.campusId,
              'educationPersonId': input.educationPersonId,
              // 'empEventId': CampusEventData.empEventId,
              'campusEventId': input.campusEventId,
              'notificationName': 11,
              'studentList': [{
                'studentId': input.studentId,
              }],
            };
            var notificationTemplate = server.models.NotificationMessageTemplates;
            var pushNotification = notificationTemplate.pushNotification;
            pushNotification(pushInput, function(pushErr, pushOut) {
              // console.log(pushErr, pushOut);
              if (pushOut) {
                resp.Notification = 'Notificaton Sent';
                callBC(null, resp);
              } else {
                callBC(null, resp);
              }
            });
            // callBC(null, resp);
          } else {
            callBC(err, null);
          }
        });
      } else {
        callBC(error, null);
      }
    });
  };

  //studentEventActions method creation
  EVENT_STUDENT_LIST.remoteMethod('removeStudent', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/removeStudent',
      verb: 'PUT',
    },
  });

  // event student list update with upload.

  EVENT_STUDENT_LIST.updateCandidateUpload = function(stDataa, cb) {
    // var inputFile = './commonValidation/exported1.csv';
    var config = require('../../server/config.json');
    var name = stDataa.fileDetails.name;
    var container = stDataa.fileDetails.container;
    // var pathForm = require('path');
    // var inputFile = './attachments/' + container + '/' + name;
    // inputFile = pathForm.join(__dirname, '../../../../', inputFile);
    // var protocol = (config.userOptions.env == 'prod') ? 'https://' : 'http://';
    // var host = (config.userOptions.env == 'prod') ? config.userOptions.host : config.host;
    // var port = (config.userOptions.env == 'prod') ? undefined : config.port;
    // var devUrl = protocol + host + ':' + port + '/api/Attachments/' + container + '/download/' + name;
    // var prodUrl = protocol + host + '/api/Attachments/' + container + '/download/' + name;
    // var inputFile = (config.userOptions.env == 'prod') ? prodUrl : devUrl;
    var fileObj = {
      'container': container,
      'name': name,
    };
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readCSVFiles;
    var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
    var studentMassJson = require('./STUDENTS_MASS_UPLOAD_WORK.json');
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var logger = require('../../server/boot/lib/logger');
    var output = [];
    readCsvFile(fileObj, function(err, fileResponses) {
      // console.log(err, fileResponse);
      if (err) {
        cb(err, null);
      } else {
        var fileResponse = cleanArray(fileResponses);
        // console.log('fileresponseeeeeeeeeeeeeeeeeeee ', fileResponse);
        if (fileResponse.length > 0) {
          var firstObj = fileResponse[0];
          // console.log(firstObj.firstName, firstObj.firstName == '');
          if ((firstObj.CampusName || firstObj.CampusName == '') && (firstObj.FirstName || firstObj.FirstName == '') && (firstObj.MiddleName || firstObj.MiddleName == '') && (firstObj.LastName || firstObj.LastName == '') && (firstObj.Email || firstObj.Email == '') && (firstObj.Skills || firstObj.Skills == '') && (firstObj.Interests || firstObj.Interests == '') && (firstObj.ScreeningStatus || firstObj.ScreeningStatus == '')) {
            var updateStudentUpload = require('../../commonCampusFiles/student-list-upload.js').updateStudentUpload;
            for (var i = 0; i < fileResponse.length; i++) {
              var obj = {};
              obj = fileResponse[i];
              obj.rowNumber = i + 1;
              // obj.campusId = stDataa.campusId;
              obj.campusEventId = stDataa.campusEventId;
              output.push(obj);
            }
            updateStudentUpload(output, stDataa, function(createErr, createResponse) {
              if (createErr) {
                throwError(createErr, cb);
              } else {
                output = [];
                cb(null, createResponse);
              }
            });
          } else {
            throwError('Invalid Csv File Uploaded ', cb);
          }
        } else {
          throwError('Empty File Cannot be Uploaded. ', cb);
        }
      }
    });
  };

  EVENT_STUDENT_LIST.remoteMethod('updateCandidateUpload', {
    description: 'Send Valid Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCandidateUpload',
      verb: 'POST',
    },
  });

  // unregistered api to update candidate status and compensation package in event student list
  EVENT_STUDENT_LIST.unregStudentEventActions = function(stData, cb) {
    var updateEvent = require('../../commonCampusFiles/update-unregistered-student-events').updateUnRegStudentEvents;
    updateEvent(stData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //unregStudentEventActions method creation
  EVENT_STUDENT_LIST.remoteMethod('unregStudentEventActions', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/unregStudentEventActions',
      verb: 'PUT',
    },
  });
  EVENT_STUDENT_LIST.getStudentsByEventId = function(campusEventId, empEventId, cb) {
    EVENT_STUDENT_LIST.find({
      'where': {
        and: [{ 'employerEventId': empEventId },
        { 'campusEventId': campusEventId }]
      }, 'include': 'eventStudentListIbfk7rel'
    }, function(err, studentsInvolved) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(studentsInvolved, getStudentDetails, function(err, studentDetails) {
          if (err) {
            cb(err, null);
          } else {
            cb(null, studentDetails);
          }
        });
      }
    });
    function getStudentDetails(object, callBack) {
      var campusEvent = server.models.CampusEvent;
      if (object.campusEventId === null && object.registrationInd === 'N') {
        var empDriveCampus = server.models.EmployerDriveCampuses;
        empDriveCampus.findOne({ 'where': { 'empEventId': object.employerEventId } },
          function(err, listResponse) {
            if (err) {
              callBack(err, null);
            } else {
              var campusData = server.models.Campus;
              campusData.findOne({ 'where': { 'campusId': listResponse.campusId } }, function(err, campusResponse) {
                if (err) {
                  callBack(err, null);
                } else {
                  var unregStudent = server.models.UnregisterCampusStudent;
                  unregStudent.findOne({ 'where': { 'unregStudentId': object.studentId } }, function(err, studentResponse) {
                    if (err) {
                      callBack(err, null);
                    } else {
                      var studentStatus = object.toJSON();
                      if (object.compPackageId == null) {
                        studentStatus['totalCompPkgValue'] = null;
                      } else {
                        studentStatus['totalCompPkgValue'] = studentStatus.eventStudentListIbfk7rel.totalCompPkgValue;
                        delete studentStatus.eventStudentListIbfk7rel;
                      }
                      studentResponse.campusName = campusResponse.name;
                      var data = {};
                      data.studentInfo = studentResponse;
                      data.skills = studentResponse.skills;
                      data.intrests = studentResponse.interests;
                      data.status = studentStatus;
                      callBack(null, data);
                    }
                  });
                }
              });
            }
          });
      } else {
        // campusEvent.findOne({'where': {'campusEventId': object.campusEventId}},
        // function(err, eventResponse) {
        //   if (err) {
        //     callBack(err, null);
        //   } else {
        //     var campusDrive = server.models.CampusDrive;
        //     campusDrive.findOne({'where': {'campusDriveId': eventResponse.campusDriveId}},
        //   function(err, driveResponse) {
        //     if (err) {
        //       callBack(err, null);
        //     } else {
        var campusStudentVw = server.models.CampusEventStudentSearchVw;
        campusStudentVw.findOne({
          'where': {
            'departmentId': object.departmentId,
            'studentId': object.studentId,
          }
        }, function(err, studentDetailsResponse) {
          if (err) {
            callBack(err, null);
          } else {
            var studentSkills = server.models.StudentSkills;
            studentSkills.find({ 'where': { 'studentId': object.studentId } },
              function(err, skillsData) {
                if (err) {
                  callBack(err, null);
                } else {
                  var studentIntrests = server.models.StudentInterests;
                  studentIntrests.find({ 'where': { 'studentId': object.studentId } },
                    function(err, intrestData) {
                      if (err) {
                        callBack(err, null);
                      } else {
                        var studentStatus = object.toJSON();
                        if (object.compPackageId == null) {
                          studentStatus['totalCompPkgValue'] = null;
                        } else {
                          studentStatus['totalCompPkgValue'] = studentStatus.eventStudentListIbfk7rel.totalCompPkgValue;
                          delete studentStatus.eventStudentListIbfk7rel;
                        }
                        var data = {};
                        data.studentInfo = studentDetailsResponse;
                        data.skills = skillsData;
                        data.intrests = intrestData;
                        data.status = studentStatus;
                        callBack(null, data);
                      }
                    });
                }
              });
          }
        });
        //     }
        //   });
        //   }
        // });
      }
    }
  };
  EVENT_STUDENT_LIST.remoteMethod('getStudentsByEventId', {
    description: 'To get student event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'empEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getStudentsByEventId',
      verb: 'get',
    },
  });
  EVENT_STUDENT_LIST.deleteSetOfStudents = function(stData, cb) {
    var async = require('async');
    // console.log('sss', stData);
    // if (stData) {
    //   console.log('sss', stData);
    // }
    if (stData.data.length > 0) {
      var inputArray = [];
      inputArray = stData.data;
      async.map(inputArray, deleteStudents, function(err, response) {
        if (err) {
          cb(err, null);
        } else {
          cb(null, response);
        }
      });
      function deleteStudents(obj, callBack) {
        EVENT_STUDENT_LIST.destroyById(Number(obj.studentListId), function(err, deleteResponse) {
          if (err) {
            callBack(err, null);
          } else {
            callBack(null, deleteResponse);
          }
        });
      }
    } else {
      cb(null, null);
    }
  };
  //unregStudentEventActions method creation
  EVENT_STUDENT_LIST.remoteMethod('deleteSetOfStudents', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/deleteSetOfStudents',
      verb: 'PUT',
    },
  });
  // in campus event campus can add multiple students to an event
  EVENT_STUDENT_LIST.createEventStudentList = function(input, cb) {
    EVENT_STUDENT_LIST.create(input.data, function(err, resp) {
      if (resp) {
        cb(null, resp);
      } else {
        cb(null, null);
      }
    });
  };
  EVENT_STUDENT_LIST.remoteMethod('createEventStudentList', {
    description: 'Send Valid Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createEventStudentList',
      verb: 'POST',
    },
  });
};

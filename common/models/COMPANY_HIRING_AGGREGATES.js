'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var async = require('async');
var output = [];
var throwError = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var config = require('../../server/config.json');
module.exports = function(COMPANY_HIRING_AGGREGATES) {
  //create company hiring mass upload data
  COMPANY_HIRING_AGGREGATES.companyHiringMassUpload = function(companyHiringData, cb) {
    var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
    companyHiringAggregatesWork.destroyAll({}, function(destroyError, destroyOutput) {
      // var inputFile = './commonValidation/hiring.csv';
      var name = companyHiringData.fileDetails.name;
      var container = companyHiringData.fileDetails.container;
      // var pathForm = require('path');
      // var inputFile = './attachments/' + container + '/' + name;
      // var fullPathForm = pathForm.join(__dirname, '../../../../', inputFile);
      // var protocol = (config.userOptions.env == 'prod') ? 'https://' : 'http://';
      // var host = (config.userOptions.env == 'prod') ? config.userOptions.host : config.host;
      // var port = (config.userOptions.env == 'prod') ? undefined : config.port;
      // var devUrl = protocol + host + ':' + port + '/api/Attachments/' + container + '/download/' + name;
      // var prodUrl = protocol + host + '/api/Attachments/' + container + '/download/' + name;
      // var fullPathForm = (config.userOptions.env == 'prod') ? prodUrl : devUrl;
      var fileObj = {
        'container': container,
        'name': name,
      };
      var readCsvFile = require('../../commonValidation/common-mass-upload.js').readCSVFiles;
      readCsvFile(fileObj, function(err, fileResponse) {
        if (err) {
          cb(err, null);
        } else {
          if (fileResponse.length > 0) {
            var firstObj = fileResponse[0];
            if ((firstObj.organizationName || firstObj.organizationName == '') && (firstObj.jobRoleName || firstObj.jobRoleName == '') && (firstObj.campusName || firstObj.campusName == '') && (firstObj.noOfOffers || firstObj.noOfOffers == '') && (firstObj.sumOfOffers || firstObj.sumOfOffers == '') && (firstObj.minimumOffer || firstObj.minimumOffer == '') && (firstObj.maximumOffer || firstObj.maximumOffer == '') && (firstObj.calendarYear || firstObj.calendarYear == '')) {
              var createMass = require('../../commonCompanyFiles/company-hiring-aggregate.js').createCompanyHiringAggregate;
              for (var i = 0; i < fileResponse.length; i++) {
                var obj = {};
                obj = fileResponse[i];
                obj.rowNumber = i + 1;
                output.push(obj);
              }
              createMass(output, companyHiringData, function(createErr, createResponse) {
                if (createErr) {
                  throwError(createErr, cb);
                } else {
                  output = [];
                  cb(null, createResponse);
                }
              });
            } else {
              throwError('Invalid Csv File Uploaded ', cb);
            }
          } else {
            throwError('Empty File Cannot be Uploaded. ', cb);
          }
        };
      });
    });
  };

  //comapny hiring mass upload method creation
  COMPANY_HIRING_AGGREGATES.remoteMethod('companyHiringMassUpload', {
    description: 'Send Valid assessment Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/companyHiringMassUpload',
      verb: 'POST',
    },
  });

  //To load mass upload data
  function loadMassUploadHiringAggregatesData(inputFile, cb) {
    var fs = require('fs');
    var parse = require('csv-parse');
    // var async = require('async');
    var parser = parse({
      delimiter: ',',
      columns: true,
    }, function(err, data) {
      //   async.eachSeries(data, function(line, callBc) {  });
      if (err) {
        cb(err, null);
      } else {
        cb(null, data);
      }
    });

    fs.createReadStream(inputFile).pipe(parser);
  }
};

'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var config = require('../../server/config.json');
process.setMaxListeners(0);
module.exports = function(CAMPUS_PLACEMENT_AGGREGATES_WORK) {
  //mass upload remote method starts here
  var async = require('async');
  CAMPUS_PLACEMENT_AGGREGATES_WORK.campusPlacementUpload = function(inputData, cb) {
    // var inputFile = './commonValidation/campus_placement_aggregates.csv';
    var name = inputData.fileDetails.name;
    var container = inputData.fileDetails.container;
    // var pathForm = require('path');
    // var inputFile = './attachments/' + container + '/' + name;
    // var commonPathFile = require('../../commonValidation/common-mass-upload.js').commonPathFile;
    // commonPathFile(config, container, name, function(commonErr, fileUrl) {
    // console.log('fileurllllllllllllllll ', fileUrl);
    // var fullPathForm = fileUrl;
    var fileObj = {
      'container': container,
      'name': name,
    };
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readCSVFiles;
    var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
    var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
    var campusAggregateJson = require('./CAMPUS_PLACEMENT_AGGREGATES_WORK.json');
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var logger = require('../../server/boot/lib/logger');
    var output = [];
    var campusPlacementAggregates = server.models.CampusPlacementAggregates;
    var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
    CAMPUS_PLACEMENT_AGGREGATES_WORK.destroyAll({}, function(error, destroy) {
      readCsvFile(fileObj, function(err, fileResponse) {
        // console.log('responseeeeeeeeeeeeeeeeeeeeeeeee ', fileResponse);
        if (err) {
          cb(err, null);
        } else {
          if (fileResponse.length > 0) {
            var firstObj = fileResponse[0];
            if ((firstObj.departmentName || firstObj.departmentName == '') && (firstObj.programName || firstObj.programName == '') && (firstObj.noOfOffers || firstObj.noOfOffers == '') && (firstObj.acedemicYear || firstObj.acedemicYear == '') && (firstObj.noOfStudents || firstObj.noOfStudents == '') && (firstObj.totalOffers || firstObj.totalOffers == '') && (firstObj.minimumOffer || firstObj.minimumOffer == '') && (firstObj.maximumOffer || firstObj.maximumOffer == '') && (firstObj.top5 || firstObj.top5 == '') && (firstObj.sixToTen || firstObj.sixToTen == '') && (firstObj.levenTo20 || firstObj.levenTo20 == '') && (firstObj.twentyOneTo50 || firstObj.twentyOneTo50 == '') && (firstObj.above50 || firstObj.above50 == '')) {
              var createMass = require('../../commonCampusFiles/campus-placement-aggregate.js').createCampusPlacementAggregate;
              for (var i = 0; i < fileResponse.length; i++) {
                var obj = {};
                obj = fileResponse[i];
                obj.rowNumber = i + 1;
                output.push(obj);
              }
              createMass(output, inputData, function(createErr, createResponse) {
                if (createErr) {
                  throwError(createErr, cb);
                } else {
                  output = [];
                  cb(null, createResponse);
                }
              });
            } else {
              throwError('Invalid Csv File Uploaded ', cb);
            }
          } else {
            throwError('Empty File Cannot be Uploaded. ', cb);
          }
        }
      });
    });
    // });
    // var protocol = (config.userOptions.env == 'prod') ? 'https://' : 'http://';
    // var host = (config.userOptions.env == 'prod') ? config.userOptions.host : config.host;
    // var port = (config.userOptions.env == 'prod') ? undefined : config.port;
    // var devUrl = protocol + host + ':' + port + '/api/Attachments/' + container + '/download/' + name;
    // var prodUrl = protocol + host + '/api/Attachments/' + container + '/download/' + name;
    // var fullPathForm = (config.userOptions.env == 'prod') ? prodUrl : devUrl;
    // var fullPathForm = pathForm.join(__dirname, '../../../../', inputFile);
  };
  //event test upload method creation
  CAMPUS_PLACEMENT_AGGREGATES_WORK.remoteMethod('campusPlacementUpload', {
    description: 'Send Valid Data ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/campusPlacementUpload',
      verb: 'POST',
    },
  });
};

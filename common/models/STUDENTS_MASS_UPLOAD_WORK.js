'use strict';
module.exports = function(STUDENTS_MASS_UPLOAD_WORK) {
  //mass upload remote method starts here
  var server = require('../../server/server');
  var async = require('async');
  var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
  var config = require('../../server/config.json');
  STUDENTS_MASS_UPLOAD_WORK.observe('before save', function massUploadBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      var Joi = require('joi');
      var schema = Joi.object().keys({
        firstName: Joi.string().max(50).required(),
        lastName: Joi.string().max(50).required(),
        email: Joi.string().email().max(254).required(),
        admissionNo: Joi.string().max(50).required(),
        startDate: Joi.date().required(),
        plannedCompletionDate: Joi.date().required().min(new Date()),
        error: [Joi.string().max(500), Joi.allow(null)],
        rowNumber: Joi.number().integer().max(999999999999999),
        campusId: Joi.number().integer().max(999999999999999),
        programId: Joi.number().integer().max(999999999999999),
        systemPassword: Joi.string().max(100),
        password: Joi.string().max(999999999999999),
        createUserId: Joi.number().max(10),
        campusUploadLogId: Joi.number().max(999999999999999),
        updateUserId: Joi.number().max(10),
      });
      var sampleObject = {
        'firstName': ctx.instance.firstName,
        'lastName': ctx.instance.lastName,
        'email': ctx.instance.email,
        'admissionNo': ctx.instance.admissionNo,
        'startDate': ctx.instance.startDate,
        'plannedCompletionDate': ctx.instance.plannedCompletionDate,
        'rowNumber': ctx.instance.rowNumber,
        'campusId': ctx.instance.campusId,
        'programId': ctx.instance.programId,
        'systemPassword': ctx.instance.systemPassword,
        'password': ctx.instance.password,
        'createUserId': ctx.instance.createUserId,
        'updateUserId': ctx.instance.updateUserId,
        'campusUploadLogId': ctx.instance.campusUploadLogId};
      Joi.validate(sampleObject, schema, function(err, value) {
        console.log(ctx.instance);
        if (err) {
          console.log('ee', err);
          ctx.instance.error = err.details[0].message;
          next(null, ctx.instance);
        } else {
          ctx.instance.plannedCompletionDate = (ctx.instance.samplePlanedDate);
          ctx.instance.startDate = (ctx.instance.sampleStartDate);
          getEmailValid(sampleObject, function(error, validateMailResponse) {
            if (error) {
              // console.log('vv', validateMailResponse);
              ctx.instance.error = error.error;
              sampleObject.error = error.error;
              next(null, ctx.instance);
            } else {
              getAdmissionNumberValid(sampleObject, function(errorAdmission, admissionNoValidate) {
                if (errorAdmission) {
                  ctx.instance.error = errorAdmission.error;
                  sampleObject.error = errorAdmission.error;
                  next(null, ctx.instance);
                } else {
                  // console.log('aa', sampleObject);
                  next(null, ctx.instance);
                }
              });
            }
          });
        }
      });
    } else {
      next();
    }
    function getEmailValid(obj, callBack) {
      var scoraUser = server.models.ScoraUser;
      var student = server.models.Student;
      var enrollment = server.models.Enrollment;
      scoraUser.findOne({
        'where': {
          'email': obj.email,
        },
      }, function(err, userResult) {
        if (err) {
          obj.error = err;
          callBack(obj, null);
        } else if (userResult) {
          // obj.error = 'Email Already Exist';
          student.findOne({
            'where': {
              'id': userResult.id,
            },
          }, function(studentErr, studentInfo) {
            if (studentErr) {
              callBack(null, obj);
            } else if (studentInfo) {
              enrollment.findOne({
                'where': {
                  'and': [{
                    'programId': obj.programId,
                  }, {
                    'studentId': studentInfo.studentId,
                  }],
                },
              }, function(enrollErr, enrollInfo) {
                if (enrollErr) {
                  obj.error = enrollErr;
                  callBack(obj, null);
                } else if (enrollInfo) {
                  obj.error = 'Email Already Registered For Given Program';
                  callBack(obj, null);
                } else {
                  ctx.instance.studentId = studentInfo.studentId;
                  ctx.instance.skipUserInd = 'Y';
                  var flag = 1;
                  validateDepartment(obj, function(deptErr, deptOut) {
                    if (deptOut) {
                      obj.error = 'Student Programs Cannot be Overlaped.';
                      callBack(obj, null);
                    } else {
                      callBack(null, obj);
                    }
                  });
                }
              });
            } else {
              obj.error = 'Email Already Exist';
              callBack(obj, null);
            }
          });
        } else {
          callBack(null, obj);
        }
      });
    }
    function validateDepartment(obj, deptCB) {
      var program = server.models.Program;
      program.findOne({'where': {'programId': obj.programId}}, function(programErr, programOut) {
        program.find({'where': {'departmentId': programOut.departmentId}}, function(deptErr, deptOut) {
          if (deptOut.length == 1) {
            deptCB(null, null);
          } else {
            async.map(deptOut, validateProgram, function(enrollErr, enrollOut) {
              var out = [];
              out = cleanArray(enrollOut);
              if (out.length > 0) {
                deptCB(null, obj);
              } else {
                deptCB(null, null);
              }
            });
          }
        });
      });
      function validateProgram(enrollObj, enrollCB) {
        var enrollment = server.models.Enrollment;
        enrollment.findOne({
          'where': {
            'and': [{
              'programId': enrollObj.programId,
            }, {
              'studentId': obj.studentId,
            }, {'dataVerifiedInd': 'Y'}],
          },
        }, function(enrollErr, enrollOutt) {
          if (enrollOutt) {
            // console.log('--------------------------------- ', enrollOutt, obj);
            var currentDate;
            if (enrollOutt.actualCompletionDate) {
              currentDate = new Date(enrollOutt.actualCompletionDate).getTime();
            } else {
              currentDate = new Date(enrollOutt.planedCompletionDate).getTime();
            }
            var plcdExist = new Date(obj.plannedCompletionDate).getTime();
            var startExist = new Date(obj.startDate).getTime();
            // console.log(startExist, '00000000000000000000000000 ', currentDate, startExist >= currentDate);
            if (startExist >= currentDate && plcdExist > currentDate) {
              enrollCB(null, null);
            } else {
              enrollCB(null, obj);
            }
          } else {
            enrollCB(null, null);
          }
        });
      }
    }
    function getAdmissionNumberValid(obj, cBack) {
      var enrollment = server.models.Enrollment;
      enrollment.findOne({
        'where': {
          'and': [{
            'campusId': obj.campusId,
          }, {
            'admissionNo': obj.admissionNo,
          },
          ],
        },
      }, function(err, adminResult) {
        if (err) {
          obj.error = err;
          cBack(obj, null);
        } else if (adminResult) {
          if (adminResult.programId == obj.programId) {
            obj.error = 'Admission Number Already Registered for given Program';
            cBack(obj, null);
          } else {
            var student = server.models.Student;
            student.findOne({'where': {'studentId': adminResult.studentId}}, function(studentErr, studentOut) {
              var userModel = server.models.ScoraUser;
              userModel.findOne({'where': {'id': studentOut.id}}, function(userErr, userOut) {
                if (userOut.email == obj.email) {
                  cBack(null, obj);
                } else {
                  obj.error = 'Admission Number Already Exist With Other EmailId';
                  cBack(obj, null);
                }
              });
            });
          }
        } else {
          cBack(null, obj);
        }
      });
    }
  });
  STUDENTS_MASS_UPLOAD_WORK.studentMassUpload = function(stDataa, cb) {
    var name = stDataa.fileDetails.name;
    var container = stDataa.fileDetails.container;
    // var pathForm = require('path');
    // var protocol = (config.userOptions.env == 'prod') ? 'https://' : 'http://';
    // var host = (config.userOptions.env == 'prod') ? config.userOptions.host : config.host;
    // var port = (config.userOptions.env == 'prod') ? undefined : config.port;
    // var devUrl = protocol + host + ':' + port + '/api/Attachments/' + container + '/download/' + name;
    // var prodUrl = protocol + host + '/api/Attachments/' + container + '/download/' + name;
    // var fullPathForm = (config.userOptions.env == 'prod') ? prodUrl : devUrl;
    // var inputFile = './attachments/' + container + '/' + name;
    //------------------------0000000000000000000000---------------------------
    // var attachment = server.models.Attachment;
    // var reader;
    // var content = '';
    // reader = attachment.downloadStream('scora-campus-upload', 'mass.csv', {}, cb);
    // reader
    //   .on('readable', function() {
    //     var chunk;
    //     while (null !== (chunk = reader.read())) {
    //       content = content + chunk;
    //       console.log('contenttttttttttt ', content);
    //     }
    //   })
    //   .on('end', function() {
    var fileObj = {
      'container': container,
      'name': name,
    };
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readCSVFiles;
    // var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
    var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
    var studentMassJson = require('./STUDENTS_MASS_UPLOAD_WORK.json');
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var logger = require('../../server/boot/lib/logger');
    var output = [];
    var i = 0;
    readCsvFile(fileObj, function(err, fileResponses) {
      // console.log('fileresppppppppppppppppp ', fileResponses);
      if (err) {
        cb(err, null);
      } else {
            // var fileResponse = cleanArray(fileResponses);
            // console.log('fileresponseeeeeeeeeeeeeeeeeeeee ', fileResponse);
        if (fileResponses.length > 0 && fileResponses.length <= 400) {
          var firstObj = fileResponses[0];
              // console.log(firstObj.startDate);
          if ((firstObj.firstName || firstObj.firstName == '') && (firstObj.lastName || firstObj.lastName == '') && (firstObj.email || firstObj.email == '') && (firstObj.admissionNo || firstObj.admissionNo == '') && (firstObj.startDate || firstObj.startDate == '') && (firstObj.plannedCompletionDate || firstObj.plannedCompletionDate == '')) {
            var createMass = require('../../commonCampusFiles/students-mass.js').createMassUpload;
            var details = stDataa.fileDetails;
            var container = stDataa.fileDetails.container;
            var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.name;
            var logInput = {
              'campusUploadTypeValueId': stDataa.campusUploadTypeValueId,
              'campusId': stDataa.campusId,
              'inputParameters': 'string',
              'createUserId': 1,
              'createDatetime': new Date(),
              'csvFileLocation': csvFileLocation,
            };
            var campusUploadLog = server.models.CampusUploadLog;
            campusUploadLog.create(logInput, function(err, logResponse) {
              if (err) {

              } else {
                async.map(fileResponses, formateObject, function(err, responseOfData) {
                  if (err) {
                    cb(err, null);
                  } else {
                    stDataa.campusUploadLogId = logResponse.campusUploadLogId;
                    createMass(responseOfData, stDataa, function(createErr, createResponse) {
                      if (createErr) {
                        throwError(createErr, cb);
                      } else {
                        output = [];
                        cb(null, createResponse);
                      }
                    });
                  }
                });
                function formateObject(obj, callback) {
                  var globalPassword = Math.random().toString(12).slice(-6);
                  obj.rowNumber = i++;
                  obj.campusId = stDataa.campusId;
                  obj.programId = stDataa.programId;
                  obj.systemPassword = (globalPassword);
                  obj.password = globalPassword;
                  obj.createUserId = 1;
                  obj.updateUserId = 1;
                  obj.campusUploadLogId = logResponse.campusUploadLogId;
                  if (obj.startDate) {
                    var start = obj.startDate.split('/');
                    if (start[0] && start[1] && start[2]) {
                      obj.sampleStartDate = start[2] + '-' + start[1] + '-' + start[0];
                      start = new Date(start[1] + '-' + start[0] + '-' + start[2]);
                      obj.startDate = start;
                    } else {
                      obj.error = 'startDate should be MM/DD/YYY';
                      obj.startDate = obj.startDate;
                    }
                  }
                  if (obj.plannedCompletionDate) {
                    var plannedCompletionDate = obj.plannedCompletionDate.split('/');
                    if (plannedCompletionDate[1] && plannedCompletionDate[0] && plannedCompletionDate[2]) {
                      obj.samplePlanedDate = plannedCompletionDate[2] + '-' + plannedCompletionDate[1] + '-' + plannedCompletionDate[0];
                      plannedCompletionDate = new Date(plannedCompletionDate[1] + '-' + plannedCompletionDate[0] + '-' + plannedCompletionDate[2]);
                      obj.plannedCompletionDate = plannedCompletionDate;
                    } else {
                      obj.error = 'plannedCompletionDate should be MM/DD/YYY';
                      obj.plannedCompletionDate = obj.plannedCompletionDate;
                    }
                  }
                  callback(null, obj);
                }
              }
            });
          } else {
            throwError('Invalid Csv File Uploaded ', cb);
          }
        } else {
          if (fileResponses.length > 400) {
            throwError('Please upload a file with 400 records. ', cb);
          } else {
            throwError('Empty File Cannot be Uploaded. ', cb);
          }
        }
      }
    });
    // });

    // });
  };

  //mass upload method creation
  STUDENTS_MASS_UPLOAD_WORK.remoteMethod('studentMassUpload', {
    description: 'Send Valid Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/studentMassUpload',
      verb: 'POST',
    },
  });

  //mass upload method creation
  STUDENTS_MASS_UPLOAD_WORK.testapi = function(input, req, res, cb) {
    var attachment = server.models.Attachment;
    // var s3 = new AWS.S3({params: {Bucket: container}});
    // attachment.download('amazon', req, res, 'scora-campus-upload', 'mass.csv', function(err, out) {
    // attachment.download('scora-campus-upload', 'mass.csv', req, res, function(err, out) {
    // attachment.downloadStream('scora-campus-upload', 'mass.csv')
    var reader;
    var content = '';
    // var fs = require('fs');
    // var liner = require('liner');
    // var Liner = require('liner');
    // var liner = new Liner('./story.txt');
    reader = attachment.downloadStream('scora-campus-upload', 'mass.csv', {}, cb);
    reader
      .on('readable', function() {
        var chunk;
        while (null !== (chunk = reader.read())) {
          content = content + chunk;
          // console.log('contenttttttttttt ', content);
        }
      })
      .on('end', function() {
        // console.log(hash.digest('hex'));
        // console.log('contenttt0000000000000000000000000000000tttttttt ', content);
      });
  };
  STUDENTS_MASS_UPLOAD_WORK.remoteMethod('testapi', {
    description: 'Send Valid Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }, {arg: 'req', type: 'object', 'http': {source: 'req'}},
    {arg: 'res', type: 'object', 'http': {source: 'res'}}],
    http: {
      path: '/testapi',
      verb: 'POST',
    },
  });
  STUDENTS_MASS_UPLOAD_WORK.createData = function(stData, cb) {
    var Joi = require('joi');
    var schema = Joi.object().keys({
      firstName: Joi.string().max(50).required(),
      lastName: Joi.string().max(50).required(),
      email: Joi.string().email().max(254).required(),
      admissionNo: Joi.string().max(50).required(),
      startDate: Joi.date().required(),
      plannedCompletionDate: Joi.date().required(),
      error: [Joi.string().max(500), Joi.allow(null)],
      rowNumber: Joi.number().integer().max(999999999999999),
      campusId: Joi.number().integer().max(999999999999999),
      programId: Joi.number().integer().max(999999999999999),
      systemPassword: Joi.string().max(100),
      password: Joi.string().max(999999999999999),
      createUserId: Joi.number().max(10),
      campusUploadLogId: Joi.number().max(999999999999999),
      updateUserId: Joi.number().max(10),
    });
    Joi.validate(stData, schema, function(err, value) {
      if (err) {
        stData.error = err.details[0].message;
        // console.log('hheeaarr', err.details[0].message);
        cb(stData, null);
      } else {
        STUDENTS_MASS_UPLOAD_WORK.create(stData, function(err, response) {
          if (err) {
            stData.error = err.message;
            cb(err, null);
          } else {
            cb(null, stData);
          }
        });
      }
    });
  };

  //updateStudent method creation
  STUDENTS_MASS_UPLOAD_WORK.remoteMethod('createData', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'array',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createData',
      verb: 'PUT',
    },
  });
};

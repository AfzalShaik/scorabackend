'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
// var final = [];
// var repeated = [];
var finalNonRepeat = [];
var notInsertedIntoDataBase = [];
var inserteddata1 = [];
var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
var config = require('../../server/config.json');
module.exports = function(UNREGISTER_CAMPUS_STUDENT_WORK) {
  notInsertedIntoDataBase = [];
  var validateModel = require('../../commonValidation/unregistered-studentvalidation.js').validateJson;
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var async = require('async');
  var unsucess = 0;
  var sucess = 0;
  var empEvent = {};
  var key;
  UNREGISTER_CAMPUS_STUDENT_WORK.unregisteredCampusStudentUpload = function(stdata, cb) {
    var name = stdata.fileDetails.name;
    var container = stdata.fileDetails.container;
    empEvent.empEventId = stdata.empEventId;
    // var pathForm = require('path');
    // var inputFile = './attachments/' + container + '/' + name;
    // var fullPathForm = pathForm.join(__dirname, '../../../../', inputFile);
    // var protocol = (config.userOptions.env == 'prod') ? 'https://' : 'http://';
    // var host = (config.userOptions.env == 'prod') ? config.userOptions.host : config.host;
    // var port = (config.userOptions.env == 'prod') ? undefined : config.port;
    // var devUrl = protocol + host + ':' + port + '/api/Attachments/' + container + '/download/' + name;
    // var prodUrl = protocol + host + '/api/Attachments/' + container + '/download/' + name;
    // var fullPathForm = (config.userOptions.env == 'prod') ? prodUrl : devUrl;
    // var errorFileLocation = './attachments/' + container + '/download/' + stdata.fileDetails.name;
    // var inputFile = './commonValidation/unregister-student-details.csv';
    // console.log('----------------------------------- ', fullPathForm);
    // console.log('=================================== ', inputFile);
    var fileObj = {
      'container': container,
      'name': name,
    };
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readCSVFiles;
    var employerevent = server.models.EmployerEvent;
    employerevent.find({
      'where': {
        'empEventId': stdata.empEventId,
        'companyId': stdata.companyId,
      },
    }, function(err, res) {
      if (err) {
        errorResponse('An error occured', cb);
      } else if (res == 0) {
        errorResponse('Employer event id is invalid', cb);
      } else {
        // var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
        var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
        // var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
        // var unregisteredCampusStudentJson = require('./UNREGISTER_CAMPUS_STUDENT.json');
        var logger = require('../../server/boot/lib/logger');
        readCsvFile(fileObj, function(err, fileResponse) {
          // console.log('responseeeeeeeeeeeeeeeeeeeeeeeeeee ', fileResponse);
          if (err) {
            cb(err, null);
          } else {
            var firstObj = fileResponse[0];
            if ((firstObj.firstName || firstObj.firstName == '') &&
              (firstObj.lastName || firstObj.lastName == '') && (firstObj.middleName || firstObj.middleName == '') &&
              (firstObj.gender || firstObj.gender == '') && (firstObj.dateOfBirth || firstObj.dateOfBirth == '') &&
              (firstObj.highlights || firstObj.highlights == '') && (firstObj.phoneNo || firstObj.phoneNo == '') &&
              (firstObj.emailId || firstObj.emailId == '') ||
              (firstObj.score || firstObj.score == '') ||
              (firstObj.programName || firstObj.programName == '') ||
              (firstObj.interests || firstObj.interests == '') ||
              (firstObj.skills || firstObj.skills == '')) {
              async.map(fileResponse, validateTheInput, function(error, response) {
                if (error) {
                  errorResponse(JSON.stringify(error), cb);
                } else {
                  var upload = {};
                  var companyFileUpload = server.models.CompanyUploadLog;
                  upload.compUploadTypeValueId = stdata.compUploadTypeValueId;
                  upload.companyId = stdata.companyId;
                  upload.createUserId = stdata.createUserId;
                  upload.createDatetime = new Date();
                  companyFileUpload.create(upload, function(err5, res5) {
                    if (err5) {
                      errorResponse(err5, cb);
                    } else {
                      // console.log(res5);
                      var final = cleanArray(response);
                      async.map(final, getNonRepeatData, function(err, res) {
                        if (err) {
                          throwError('an error', cb);
                        } else {
                          var driveCampus = server.models.EmployerDriveCampuses;
                          driveCampus.findOne({'where': {'empEventId': stdata.empEventId}},
                            function(err, driveResponse) {
                              if (err) {
                                cb(err, null);
                              } else {
                                stdata.campusId = driveResponse.campusId;
                                async.map(final, getFinalData, function(err3, res3) {
                                  if (err3) {
                                    cb(err3, null);
                                  } else {
                                    var unregisterWork = server.models.UnregisterCampusStudentWork;
                                    unregisterWork.destroyAll({
                                      'companyUploadLogId': res5.companyUploadLogId,
                                    },
                                      function(err11, res11) {
                                        if (err11) {
                                          cb(err5, null);
                                        } else {
                                          async.map(finalNonRepeat, createDetails, function(err, res) {
                                            if (err) {
                                              errorResponse(err, cb);
                                            } else {
                                              var async = require('async');
                                              // console.log('insssereted', inserteddata1);

                                              async.map(inserteddata1, createEvent,
                                                function(err, response1) {
                                                  // if (err) {
                                                  //   cb(err, null);
                                                  // } else {
                                                  var companyFileUpload = server.models.CompanyUploadLog;
                                                  var upload = {};
                                                  upload.companyId = stdata.companyId;
                                                  upload.inputParameters = stdata.inputParameters;
                                                  upload.createUserId = stdata.createUserId;
                                                  upload.createDatetime = new Date();
                                                  var pathForm = require('path');
                                                  upload.csvFileLocation = stdata.fileDetails.name;
                                                  // upload.errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + stdata.fileDetails.name;
                                                  // upload.csvFileLocation = pathForm.join(__dirname, '../../../../', csvFileLocation1);
                                                  // upload.errorFileLocation = pathForm.join(__dirname, '../../../../', errorFileLocation1);
                                                  // console.log('fileeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', errorFileLocation1, csvFileLocation1);
                                                  upload.totalNoRecs = fileResponse.length;
                                                  upload.noSuccessRecs = sucess;
                                                  upload.noFailRecs = fileResponse.length - sucess;
                                                  // var companyFileUpload = server.models.CompanyUploadLog;
                                                  companyFileUpload.updateAll({
                                                    'companyUploadLogId': res5.companyUploadLogId,
                                                  }, upload, function(err, uploadRes) {
                                                    if (err) {
                                                      errorResponse('cant create an event', cb);
                                                    } else {
                                                      // console.log('not isnserted in database', notInsertedIntoDataBase);
                                                      failed(notInsertedIntoDataBase, stdata, res5, function(err1, res1) {
                                                        if (err1) {
                                                          errorResponse('err', cb);
                                                        } else {
                                                          notInsertedIntoDataBase = [];
                                                          // console.log('noooooooooooottttttttttttttttttt', notInsertedIntoDataBase);
                                                          companyFileUpload.findOne({
                                                            'where': {
                                                              'companyUploadLogId': res5.companyUploadLogId,
                                                            },
                                                          }, function(err10, res10) {
                                                            sucess = 0;
                                                            finalNonRepeat = [];
                                                            inserteddata1 = [];
                                                            notInsertedIntoDataBase = [];
                                                            cb(null, res10);
                                                          });
                                                          sucess = 0;
                                                          unsucess = 0;
                                                          inserteddata1 = [];
                                                        }
                                                      });
                                                    }
                                                  });
                                                  // }
                                                });
                                            }
                                          });
                                        }
                                      });
                                  }
                                });
                              }
                            });
                        }
                      });

                      function getNonRepeatData(obj, cb) {
                        // console.log(obj);
                        obj.companyUploadLogId = res5.companyUploadLogId;
                        var unregisterWork = server.models.UnregisterCampusStudentWork;
                        unregisterWork.create(obj, function(err, res) {
                          if (err) {
                            // console.log(err);
                            var data1 = {};
                            data1.firstName = obj.firstName;
                            data1.lastName = obj.lastName;
                            data1.middleName = obj.middleName;
                            data1.gender = obj.gender;
                            data1.dateOfBirth = obj.dateOfBirth;
                            data1.highlights = obj.highlights;
                            data1.phoneNo = obj.phoneNo;
                            data1.emailId = obj.emailId;
                            data1.error = err.message;
                            notInsertedIntoDataBase.push(data1);
                            cb(null, null);
                          } else {
                            cb(null, res);
                          }
                        });
                      }
                    }
                  });
                }
              });

              function getFinalData(obj, cb) {
                var data = {};
                data['firstName'] = (obj.firstName);
                data['middleName'] = (obj.middleName) ? (obj.middleName) : undefined;
                data['lastName'] = (obj.lastName);
                data['phoneNo'] = (obj.phoneNo) ? (obj.phoneNo) : undefined;
                // data['dateOfBirth'] = (obj.dateOfBirth) ? (obj.dateOfBirth) : undefined;
                data['emailId'] = (obj.emailId) ? (obj.emailId) : undefined;
                // console.log(data);
                var unregisterWork = server.models.UnregisterCampusStudentWork;
                unregisterWork.find({
                  'where': {
                    // 'or': [{
                    //   'and': [{
                    //     'firstName': obj.firstName,
                    //   }, {
                    //     'lastName': obj.lastName,
                    //   }],
                    // },
                    // {
                    'or': [{
                      'phoneNo': data.phoneNo,
                    }, {
                      'emailId': data.emailId,
                    }],
                    // },
                    // ],
                  },
                }, function(err4, res4) {
                  // console.log('there are responses', res4);
                  if (res4.length === 1) {
                    finalNonRepeat.push(obj);
                    cb(null, 'done');
                  } else if (res4.length > 1) {
                    obj.error = 'repeated entry';
                    notInsertedIntoDataBase.push(obj);
                    cb(null, 'done');
                  } else {
                    cb(null, 'done');
                  }
                });
              }

              function validateTheInput(object, validCb) {
                var validateModel = require('../../commonValidation/unregistered-studentvalidation.js').validateJson;
                var lenghtVar = getlength(object.phoneNo);
                var numberVar = isNumeric(object.phoneNo);
                validateModel(object, function(validationErr, validationResponse) {
                  // console.log('.......========............', validationErr);
                  // if (lenghtVar == 10 && numberVar == true) {
                  if (validationErr) {
                    // console.log('.......========............', validationErr);
                    // var err = {};
                    // err.error = validationErr.error;
                    // err.data = validationErr.data;
                    // console.log('an err', err);
                    // console.log('in validation error', validationErr);
                    if (validationErr.error == '"phoneNo" must be less than or equal to 9999999999') {
                      validationErr.error = 'Invalid Phone Numner';
                      notInsertedIntoDataBase.push(validationErr);
                      validCb(null, 0);
                    } else {
                      notInsertedIntoDataBase.push(validationErr);
                      validCb(null, 0);
                    }
                  } else {
                    validCb(null, validationResponse);
                  }
                  // } else {
                  //   var validationErr = {};
                  //   validationErr.message = 'Invalid Phone Number Entered';
                  //   notInsertedIntoDataBase.push(validationErr);
                  //   validCb(null, 0);
                  // }
                });
              }
            } else {
              errorResponse('Invalid file Headers', cb);
            }
          }
        });
      }
    });

    function createEvent(eventData, eventCallback) {
      // console.log('cccccccccccccccccccccccc', eventData);
      var eventStudent = server.models.EventStudentList;
      var date = new Date();
      var input = {

        'studentId': eventData.unregStudentId,
        'companyId': stdata.companyId,
        'employerEventId': stdata.empEventId,
        'campusId': stdata.campusId,
        'campusEventId': null,
        'totalCompValue': null,
        'candidateStatusValueId': 376,
        'campusComments': 'string',
        'employerComments': 'string',
        'createDatetime': date,
        'updateDatetime': date,
        'createUserId': stdata.createUserId,
        'updateUserId': stdata.createUserId,
        'eligibilityInd': 'Y',
        'studentSubscribeInd': 'Y',
        'campusPublishInd': 'Y',
        'offerStatusValueId': null,
        'compPackageId': null,
        'registrationInd': 'N',
      };
      eventStudent.findOne({
        'where': {
          and: [{
            'studentId': eventData.unregStudentId,
          },
          {
            'employerEventId': stdata.empEventId,
          },
          {
            'registrationInd': 'N',
          },
          ],
        },
      }, function(err, res) {
        // console.log(res);
        if (err) {

        } else if (res == null) {
          var eventStudentList = server.models.EventStudentList;
          eventStudentList.create(input, function(Eventerr, eventRes) {
            // console.log('ressssssssssssssssssssssssssssssssssssssssssssssssssssss', Eventerr);
            // console.log('innnnnnnnnnnnnnnnnnnnnnnnnnn ', input);
            if (Eventerr) {
              errorResponse(Eventerr, eventCallback);
            } else {
              var employerEvent = server.models.EmployerEvent;
              employerEvent.findOne({
                'where': {
                  'empEventId': stdata.empEventId,
                },
              }, function(errorEmp, successEmp) {
                var empObj = {};
                empObj = successEmp;
                empObj.eventStatusValueId = 317;
                successEmp.updateAttributes(empObj, function(empErr, empOut) {
                  sucess++;
                  eventCallback(null, eventRes);
                });
              });
            }
          });
        } else {
          console.log('getting hear');
          var data1 = {};
          data1.firstName = eventData.firstName;
          data1.lastName = eventData.lastName;
          data1.middleName = eventData.middleName;
          data1.gender = eventData.gender;
          data1.dateOfBirth = eventData.dateOfBirth;
          data1.highlights = eventData.highlights;
          data1.phoneNo = eventData.phoneNo;
          data1.emailId = eventData.emailId;
          data1.error = 'already part of the event';
          notInsertedIntoDataBase.push(data1);
          eventCallback(null, 0);
        }
      });
    }

    function createDetails(data, callb) {
      var skippedDetails = [];
      var inserteddata = [];
      var count = 0;
      var indicator = 0;
      var first = data.firstName.toUpperCase();
      var last = data.lastName.toUpperCase();
      data.searchName = first + ' ' + last;
      data.createDatetime = new Date();
      data.updateDatetime = new Date();
      data.createUserId = stdata.createUserId;
      data.updateUserId = stdata.updateUserId;
      var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
      data.empEventId = stdata.empEventId;
      // data.phoneNo = parseInt(data.phoneNo);
      // delete data.dateOfBirth;
      // console.log('this is data', data);
      UnregisterCampusStudent.find({'where': {'emailId': data.emailId}}, function(emailErr, emailOut) {
        if (emailOut) {
          async.map(emailOut, getStudentInfo, function(studentErr, studentOut) {
            var studentOutput = [];
            studentOutput = cleanArray(studentOut);
            // console.log('studentoutputtttttttttttttttttttt ', studentOutput);
            if (studentOutput.length > 0) {
              var data1 = {};
              data1.firstName = data.firstName;
              data1.lastName = data.lastName;
              data1.middleName = data.middleName;
              data1.gender = data.gender;
              data1.dateOfBirth = data.dateOfBirth;
              data1.highlights = data.highlights;
              data1.phoneNo = data.phoneNo;
              data1.emailId = data.emailId;
              data1.error = 'already part of the event';
              notInsertedIntoDataBase.push(data1);
              callb(null, 0);
            } else {
              UnregisterCampusStudent.create(data, function(err, response) {
                // console.log('eeeeeeeeeeeeeeeeeeeeeeeeeee', err, response);
                if (err) {
                  errorResponse(err, callb);
                } else {
                  // console.log(response);
                  inserteddata[indicator] = response;
                  indicator = indicator + 1;
                  inserteddata1.push(response);
                  callb(null, inserteddata);
                }
              });
            }
          });
        } else {
          // console.log('ppppppppppppppppppppppp');
          UnregisterCampusStudent.create(data, function(err, response) {
            // console.log('eeeeeeeeeeeeeeeeeeeeeeeeeee', err, response);
            if (err) {
              errorResponse(err, callb);
            } else {
              // console.log(response);
              inserteddata[indicator] = response;
              indicator = indicator + 1;
              inserteddata1.push(response);
              callb(null, inserteddata);
            }
          });
        }
      });

      function getStudentInfo(studentObj, studentCB) {
        var eventStudentList = server.models.EventStudentList;
        eventStudentList.findOne({
          'where': {
            and: [{
              'studentId': studentObj.unregStudentId,
            },
            {
              'employerEventId': data.empEventId,
            },
            {
              'registrationInd': 'N',
            },
            ],
          },
        }, function(eventErr, eventOut) {
          // console.log('eventputttttttttttttttttttttttt ', eventOut);
          if (eventOut) {
            // var data1 = {};
            // data1.firstName = studentObj.firstName;
            // data1.lastName = studentObj.lastName;
            // data1.middleName = studentObj.middleName;
            // data1.gender = studentObj.gender;
            // data1.dateOfBirth = studentObj.dateOfBirth;
            // data1.highlights = studentObj.highlights;
            // data1.phoneNo = studentObj.phoneNo;
            // data1.emailId = studentObj.emailId;
            // data1.error = 'already part of the event';
            // notInsertedIntoDataBase.push(data1);
            studentCB(null, studentObj);
          } else {
            studentCB(null, 0);
          }
        });
      }
    }
    // }

    function failed(data, stdata, res5, cb) {
      // console.log(data);
      var container = stdata.fileDetails.container;
      unsucess++;
      var fs = require('fs');
      var csv = require('fast-csv');
      // var ws = fs.createWriteStream(fullPathForm);
      // csv
      //   .writeToPath(fullPathForm, data, {
      //     headers: true,
      //   })
      //   .on('finish', function() { });
      // cb(null, 'done');
      var uploadCsv = require('../../commonCampusFiles/commonCSVUpload.js').uploadCSV;
      var company = server.models.Company;
      company.findOne({'where': {'campusId': stdata.companyId}}, function(companyErr, companyOut) {
        var fs = require('fs');
        var csv = require('fast-csv');
        var json2csv = require('json2csv');
        var fileName;
        fileName = companyOut.name;
        var randomNumber = Math.random().toString(20).slice(-10);
        var errorFile = companyOut.name + '_' + randomNumber + '.csv';
        fileName = './commonValidation/' + companyOut.name + '.csv';
        var fields = ['rowNumber', 'firstName', 'middleName', 'lastName', 'gender', 'dateOfBirth', 'highlights', 'phoneNo', 'emailId', 'interests', 'skills', 'score', 'programName', 'error'];
        var csv = json2csv({
          data: data,
          fields: fields,
        });
        fs.writeFile(fileName, csv, function(err) {
          if (err) throw err;
          uploadCsv(fileName, container, errorFile, function(fileErr, fileOut) {
            var upload = {
              'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + fileOut,
            };
            var companyFileUpload = server.models.CompanyUploadLog;
            companyFileUpload.updateAll({
              'companyUploadLogId': res5.companyUploadLogId,
            }, upload, function(err, uploadRes) {
              cb(null, 'done');
            });
          });
        });
      });
    }
  };
  UNREGISTER_CAMPUS_STUDENT_WORK.remoteMethod('unregisteredCampusStudentUpload', {
    description: 'Send Valid Data ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/unregisteredCampusStudentUpload',
      verb: 'POST',
    },
  });

  function checkexistense(data, existCallBack) {
    // console.log('this is data', data);
    var obj = {};
    obj.firstName = data.firstName;
    obj.lastName = data.lastName;
    obj.searchName = data.searchName;
    obj.middleName = (data.middleName) ? (data.middleName) : undefined;
    obj.emailId = (data.emailId) ? (data.emailId) : undefined;
    obj.phoneNo = (data.phoneNo) ? (data.phoneNo) : undefined;
    // console.log(obj);
    var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
    UnregisterCampusStudent.findOne({
      'where': {
        'or': [{
          'phoneNo': data.phoneNo,
        }, {
          'emailId': data.emailId,
        }],
      },
    }, function(err, checkResponse) {
      console.log('this is check response', checkResponse);
      if (err) {
        var status = {};
        status.data = 'error';
        existCallBack(null, status);
      } else if (checkResponse == null) {
        var status = {};
        status.data = 'true';
        existCallBack(null, status);
      } else {
        var status = {};
        // console.log('check response', checkResponse);
        status.data = 'false';
        status.res = checkResponse;
        existCallBack(null, status);
      }
    });
  }

  // to get unregistered student details
  UNREGISTER_CAMPUS_STUDENT_WORK.getUnregisteredStudents = function(employerEventId, companyId, cb) {
    var eventstudentList = server.models.EventStudentList;
    eventstudentList.find({
      'where': {
        'and': [{
          employerEventId: employerEventId,
        }, {
          'companyId': companyId,
        },
        {
          'registrationInd': 'N',
        },
        ],
      },
    }, function(err, resp) {
      if (resp) {
        async.map(resp, getSkills, function(skillsErr, skillsOut) {
          cb(null, skillsOut);
        });
      } else {
        cb(null, []);
      }
    });
  };

  function getSkills(obj, callBc) {
    var unregisterCampusStudent = server.models.UnregisterCampusStudent;
    unregisterCampusStudent.findOne({
      'where': {
        'unregStudentId': obj.studentId,
      },
    }, function(studentErr, studentOut) {
      if (studentOut) {
        callBc(null, studentOut);
      } else {
        callBc(null, studentOut);
      }
    });
  }
  function getlength(number) {
    return number.length;
  }
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  UNREGISTER_CAMPUS_STUDENT_WORK.remoteMethod('getUnregisteredStudents', {
    description: 'To get Unregistered Students details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'employerEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getUnregisteredStudents',
      verb: 'get',
    },
  });
};

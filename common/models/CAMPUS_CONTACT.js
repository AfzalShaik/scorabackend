'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(CAMPUS_CONTACT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwerror = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // remote method definition to create address for particular campus
  /**
 *createCampusContact- To Create campus contact
 *@constructor
 * @param {object} data - contains all the data need to be created
 * @param {function} callBc - deals with response
 */
  CAMPUS_CONTACT.createCampusContact = function(data, callBc) {
    if (data.campusId && data.contacts.length > 0) {
      var myArray = data.contacts;
      var iterateobj = {};
      var count = 0;
      for (var j = 0; j < myArray.length; j++) {
        iterateobj = myArray[j];
        if (myArray[j].primaryInd == 'Y') {
          iterateobj = myArray[j];
          count++;
        }
      }
      // console.log('this is count', count);
      if (count <= 1) {
        if (count == 1) {
          searchForInd(data, iterateobj, function(err, res) {
            if (err) {
              errorResponse('there was an error', callBc);
            } else {
              // console.log('sucess');
            }
          });
        }
        var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
        var inputFilterObject = {};
        inputFilterObject['campusId'] = data.campusId;
        var contactModel = server.models.Contact;
        var campusModel = server.models.Campus;

      // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- New line when a new parameter starts.
      // WIll be easier to read and will also allow to add comments per parameter (if required)
        checkEntityExistence(campusModel, inputFilterObject, function(checkStatus) {
          if (checkStatus) {
            contactModel.create(data.contacts, function(error, contactRes) {
              if (error) {
                callBc(error, null);
              } else if (contactRes) {
                var persistContactDataInForeignEntity = require('../../ServicesImpl/ContactImpl/persistContactRelationData.js').persistContactDataInForeignEntity;
                var campusContactModel = server.models.CampusContact;
                var inputObj = {};
                inputObj['campusId'] = Number(data.campusId);
                persistContactDataInForeignEntity(contactRes.length, contactRes, campusContactModel, inputObj, callBc);
              } else {
                errorResponse(callBc);
              }
            });
          } else {
            errorResponse(callBc);
          }
        });
      } else {
        throwerror('cant have two contacts with address idicator Y', callBc);
      }
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    // console.log('1');
    CAMPUS_CONTACT.find({'where': {'campusId': data.campusId, 'primaryInd': 'Y'}},
    function(err, res) {
      if (err) {
        errorResponse('campusId was invalid', priCallback);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
        priCallback(null, res);
      } else {
        var update = {};
        update.primaryInd = 'N';
        CAMPUS_CONTACT.updateAll({'campusId': data.campusId, 'primaryInd': 'Y'},
        update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create contact for particular campus
  CAMPUS_CONTACT.remoteMethod('createCampusContact', {
    description: 'Useful to create contact for particular campus',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{arg: 'data', type: 'object', required: true, http: {source: 'body'}},
    ],
    http: {
      path: '/createCampusContact',
      verb: 'post', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Standardize - POST
    },
  }); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Add a space after methods.
  // remote method definition to get campus contact details
  /**
 *getCampusContact- To get campus contact details by passing required paranaters
 *@constructor
 * @param {number} campusId - unique id of campus
 * @param {number} contactId - unique id of campus contact
 * @param {function} callBc - deals with response
 */
  CAMPUS_CONTACT.getCampusContact = function(campusId, contactId, callBc) {
    var inputObj = {};
    var campusContactModel = server.models.CampusContact;
    var campusContactDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (campusId && contactId) {
      inputObj['campusId'] = campusId;
      inputObj['contactId'] = contactId;
      // below function will give address details for an entity based on loopback include filter
      campusContactDet(inputObj, campusContactModel, 'campusContact', callBc);
    } else if (campusId) {
      inputObj['campusId'] = campusId;
      // below function will give campus address details for an entity based on loopback include filter
      campusContactDet(inputObj, campusContactModel, 'campusContact', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus contact details
  CAMPUS_CONTACT.remoteMethod('getCampusContact', {
    description: 'To get campus address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{arg: 'campusId', type: 'number', required: true, http: {source: 'query'}},
      {arg: 'contactId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/getCampusContact',
      verb: 'get', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Standardize .. use GET
    },
  });
  // remote method definition to delete campus contact details
  /**
 *deleteCampusContact- To deleate campus contact details
 *@constructor
 * @param {number} campusId - unique id of campus
 * @param {number} contactId - unique id of contact
 * @param {function} callBc - deals with response
 */
  CAMPUS_CONTACT.deleteCampusContact = function(campusId, contactId, callBc) {
    if (campusId && contactId) {
      var campusContactModel = server.models.CampusContact;
      var contactModel = server.models.Contact;

      // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Construct this path or get this path via a utility.
      // That way if this file changes or is renamed - a lot of files dont have to be changed.
      var deleteCampusContact = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      inputFilterObject['contactId'] = contactId;
      deleteCampusContact(primaryCheckInput, inputFilterObject, contactModel, campusContactModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus contact details
  CAMPUS_CONTACT.remoteMethod('deleteCampusContact', {
    description: 'To delete campus contact',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{arg: 'campusId', type: 'number', required: true, http: {source: 'query'}},
      {arg: 'contactId', type: 'number', required: true, http: {source: 'query'}},
    ],
    http: {
      path: '/deleteCampusContact',
      verb: 'delete',
    },
  });

  // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Camelcasing
 //updateCampusContact remote method starts here
 /**
 *updateCampusContact- To update campus contact details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} callBc -  deals with response
 */
  CAMPUS_CONTACT.updateCampusContact = function(contactData, cb) {
    var campusUpdate = require('../../commonCampusFiles/update-campus-contact.js');
    if (contactData.primaryInd == 'Y') {
      searchForInd(contactData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    campusUpdate.updateCampusContactService(contactData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = contactData.primaryInd;
        CAMPUS_CONTACT.updateAll({'campusId': contactData.campusId, 'contactId': contactData.contactId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //updateCampusContact method to update both company and campus contacts
  CAMPUS_CONTACT.remoteMethod('updateCampusContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCampusContact',
      verb: 'PUT',
    },
  });
};

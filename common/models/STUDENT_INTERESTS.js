'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

module.exports = function(STUDENT_INTERESTS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
    // when request comes to student-interest model then we execute logic or change request object before saving into database
  STUDENT_INTERESTS.observe('before save', function studentInterestBeforeSave(ctx, next) {
    // console.log('ctx.isNewInstance: ' + ctx.isNewInstance);
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });

    // remote method definition to get student address details
      /**
 *getStudentInterest- To get Student Intrest details by taking required fields
 *@constructor
 * @param {number} studentId - Unique id for ecah and every student
 * @param {function} callBc - deals with response
 */
  STUDENT_INTERESTS.getStudentInterest = function(studentId, callBc) {
    var inputObj = {};
    var studentInterest = server.models.StudentInterests;

    var studentInterestDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId) {
      inputObj['studentId'] = studentId;
          // below function will give student skill details for an entity based on loopback include filter
      studentInterestDet(inputObj, studentInterest, 'studentInterest', callBc);
    } else {
      errorResponse(callBc);
    }
  };

    // remote method declaration to get student skill details
  STUDENT_INTERESTS.remoteMethod('getStudentInterest', {
    description: 'To get student interest details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
        ],
    http: {
      path: '/getStudentInterest',
      verb: 'GET',
    },
  });

  // remote method definition to delete student interest details
   /**
 *deleteStudentInterest- To Delete Student Intrest Details
 *@constructor
 * @param {number} studentId - unique id for each and every student
 * @param {number} intrestTypeValeId - unique id for each and every Intrest
 * @param {function} callBc - deals with response
 */
  STUDENT_INTERESTS.deleteStudentInterest = function(studentId, interestTypeValueId, callBc) {
    if (studentId && interestTypeValueId) {
      var studentInterestModel = server.models.StudentInterests;
      var deleteStudentSkill = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;
      var inputFilterObject = {};
      inputFilterObject['studentId'] = studentId;
      inputFilterObject['interestTypeValueId'] = interestTypeValueId;
      deleteStudentSkill(inputFilterObject, studentInterestModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to delete student interest details
  STUDENT_INTERESTS.remoteMethod('deleteStudentInterest', {
    description: 'To delete student interest',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'interestTypeValueId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteStudentInterest',
      verb: 'DELETE',
    },
  });

};

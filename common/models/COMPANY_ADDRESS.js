'use strict';
var server = require('../../server/server');
module.exports = function(COMPANY_ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwerror = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // remote method definition to create address for particular company
  /**
 *createCompanyAddress- To create company details
 *@constructor
 * @param {number} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  COMPANY_ADDRESS.createCompanyAddress = function(data, callBc) {
    if (data.companyId && data.addresses.length > 0) {
      var myArray = data.addresses;
      var iterateobj = {};
      var count = 0;
      for (var j = 0; j < myArray.length; j++) {
        iterateobj = myArray[j];
        if (myArray[j].primaryInd == 'Y') {
          iterateobj = myArray[j];
          count++;
        }
      }
      // console.log('this is count', count);
      if (count <= 1) {
        if (count == 1) {
          searchForInd(data, iterateobj, function(err, res) {
            if (err) {
              errorResponse('there was an error', callBc);
            } else {
              // console.log('sucess');
            }
          });
        }
        var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
        var inputFilterObject = {};
        inputFilterObject['companyId'] = data.companyId;
        var addressModel = server.models.Address;
        var companyModel = server.models.Company;
      /**
 *checkEntityExistence - To check was there any entity with the provided details
 *@constructor
 * @param {Model} companyModel - The model in which we need to check the data exist
 * @param {object} inputFilterObject - the input object contains all the data which need to be created
 * @param {function} callBc - deals with response
 */
        checkEntityExistence(companyModel, inputFilterObject,
        function(checkStatus) {
          if (checkStatus) {
            addressModel.create(data.addresses, function(error, addressRes) {
              if (error) {
                callBc(error, null);
              } else if (addressRes) {
                var persistAddressDataInForeignEntity = require('../../ServicesImpl/AddressImpl/persistAddressRelationData.js').persistAddressDataInForeignEntity;
                var companyAddressModel = server.models.CompanyAddress;
                var inputObj = {};
                inputObj['companyId'] = Number(data.companyId);
                persistAddressDataInForeignEntity(addressRes.length, addressRes, companyAddressModel, inputObj, callBc);
              } else {
                errorResponse(callBc);
              }
            });
          } else {
            errorResponse(callBc);
          }
        });
      } else {
        throwerror('cant have two or more addresses as a primary Y', callBc);
      }
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    COMPANY_ADDRESS.find({'where': {'companyId': data.companyId, 'primaryInd': 'Y'}},
    function(err, res) {
      if (err) {
        errorResponse('campusId was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        COMPANY_ADDRESS.updateAll({'companyId': data.companyId, 'primaryInd': 'Y'},
        update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create address for particular company
  COMPANY_ADDRESS.remoteMethod('createCompanyAddress', {
    description: 'Useful to create address for particular company',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCompanyAddress',
      verb: 'post',
    },
  });
/*  // remote method definition to create address for particular company
  COMPANY_ADDRESS.createCompanyAddress = function(data, callBc) {
    if (data.companyId) {
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = data.companyId;
      var addressModel = server.models.Address;
      var companyModel = server.models.Company;
      checkEntityExistence(companyModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          addressModel.create(data, function(error, addressRes) {
            if (error) {
              callBc(error, null);
            } else if (addressRes) {
              COMPANY_ADDRESS.create({
                'companyId': Number(data.companyId),
                'addressId': Number(addressRes.addressId),
              }, function(err, resp) {
                console.log(' Company resp: ' + resp);
                if (err) {
                  errorResponse(callBc);
                } else {
                  // addressRes.requestStatus = true;
                  callBc(null, addressRes);
                }
              });
            } else {
              errorResponse(callBc);
            }
          });
        } else {
          errorResponse(callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to create address for particular company
  COMPANY_ADDRESS.remoteMethod('createCompanyAddress', {
    description: 'Useful to create address for particular company',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCompanyAddress',
      verb: 'post',
    },
  });*/
  //update company address remoteMethod
  /**
 *updateCompanyAddress- To update company address details
 *@constructor
 * @param {object} companyData - data need to get updated
 * @param {function} cb - deals with response
 */
  COMPANY_ADDRESS.updateCompanyAddress = function(companyData, cb) {
    var companyUpdate = require('../../commonCompanyFiles/update-company-address.js');
    if (companyData.primaryInd == 'Y') {
      searchForInd(companyData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    companyUpdate.updateCompany(companyData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = companyData.primaryInd;
        COMPANY_ADDRESS.updateAll({'companyId': companyData.companyId, 'addressId': companyData.addressId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //update company address remoteMethod
  COMPANY_ADDRESS.remoteMethod('updateCompanyAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCompanyAddress',
      verb: 'PUT',
    },
  });
};

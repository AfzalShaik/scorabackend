'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var campusDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
var async = require('async');
var empId = [];
var skills = [];
var interests = [];
module.exports = function(CAMPUS_EVENT_STUDENT_SEARCH_VW) {
  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  // remote method definition to get campusDetails item details
  /**
   *searchCampus- To search campus based on given parameters
   *@constructor
   * @param {number} studentId - unique id of campus
   * @param {number} obj.programId - unique id of university
   * @param {number} regionFlag - contains region flag
   * @param {number} stateCode - contains state cose
   * @param {number} cityId - contains state id or a pin code
   * @param {function} callBc - deals with response
   */
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getOnCampusSearch = function(obj, callBc) {
    if (obj) {
      var campusObj = {};
      campusObj['studentId'] = (obj.studentId) ? obj.studentId : undefined;
      campusObj['programCatValueId'] = (obj.programCatValueId) ? obj.programCatValueId : undefined;
      campusObj['programClassValueId'] = (obj.programClassValueId) ? obj.programClassValueId : undefined;
      campusObj['programTypeValueId'] = (obj.programTypeValueId) ? obj.programTypeValueId : undefined;
      campusObj['programMajorValueId'] = (obj.programMajorValueId) ? obj.programMajorValueId : undefined;
      campusObj['highlights'] = (obj.highlights) ? obj.highlights : undefined;
      campusObj['cgpaScore'] = (obj.cgpaScore) ? obj.cgpaScore : undefined;
      var campusEventStudentSearchVw = server.models.CampusEventStudentSearchVw;
      campusEventStudentSearchVw.find({
        'where': {
          'and': [campusObj],
        },
      }, function(err, campusSearch) {
        if (err) {
          errorFunction(err, callBc);
        } else if (campusSearch.length > 0) {
          async.map(campusSearch, oncampusData, function(error, placementInfo) {
            // console.log(placementInfo);

            callBc(null, placementInfo);
          });
        } else {
          errorFunction([], callBc);
        }
      });
    } else {
      errorFunction([], callBc);
    }
  };
  // remote method declaration to get campusDetails details
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getOnCampusSearch', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getOnCampusSearch',
      verb: 'POST',
    },
  });
  CAMPUS_EVENT_STUDENT_SEARCH_VW.searchData = function(obj, cb) {
    if (obj) {
      var campusObj = {};
      campusObj['studentId'] = (obj.studentId) ? obj.studentId : undefined;
      campusObj['departmentId'] = (obj.departmentId) ? obj.departmentId : undefined;
      campusObj['programId'] = (obj.programId) ? obj.programId : undefined;
      var campusEventStudentSearchVw = server.models.CampusEventStudentSearchVw;
      campusEventStudentSearchVw.find({
        'where': {
          'and': [campusObj],
        },
      }, function(err, campusSearch) {
        if (err) {
          errorFunction(err, cb);
        } else if (campusSearch.length > 0) {
          var students = [];
          console.log(campusSearch);
          async.map(campusSearch, getStudentIntrestsAndSkills, function(error, placementInfo) {
            if (error) {
              errorFunction('an error occured', cb);
            } else {
              if (obj.skillTypeValueId != null || obj.interestTypeValueId != null) {
                for (var i in placementInfo) {
                  for (var j in placementInfo[i].skills) {
                    if (placementInfo[i].skills[j].lookupValueId == obj.skillTypeValueId ||
                      placementInfo[i].skills[j].lookupValueId == obj.interestTypeValueId) {
                      students.push(placementInfo[i]);
                    }
                  }
                  if (i == placementInfo.length - 1) {
                    cb(null, students);
                  }
                }
              } else {
                cb(null, placementInfo);
              }
            }
          });
        } else {
          errorFunction('Records not found', cb);
        }
      });
    } else {
      errorResponse(cb);
    }
  };
  // remote method declaration to get campusDetails details
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('searchData', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/searchData',
      verb: 'POST',
    },
  });

  CAMPUS_EVENT_STUDENT_SEARCH_VW.getDetailsBasedOnSkillId = function(object, cb1) {
    var studentSkills = server.models.StudentSkills;
    var studentInterests = server.models.StudentInterests;
    studentSkills.find({
      'where': {
        and: [object],
      },
    }, function(err, res) {
      if (err) {
        errorFunction('an error occured', cb1);
      } else {
        console.log(res);
        var async = require('async');
        async.map(res, getStudentIntrestsAndSkills, function(err, resFinal) {
          if (err) {
            errorFunction('error occured', cb1);
          } else {
            cb1(null, resFinal);
          }
        });
      }
    });
  };
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getDetailsBasedOnSkillId', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getDetailsBasedOnSkillId',
      verb: 'POST',
    },
  });
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getDetailsBasedOnIntrestId = function(object, cb1) {
    var studentSkills = server.models.StudentSkills;
    var studentInterests = server.models.StudentInterests;
    studentInterests.find({
      'where': {
        and: [object],
      },
    }, function(err, res) {
      if (err) {
        errorFunction('an error occured', cb1);
      } else {
        console.log(res);
        var async = require('async');
        async.map(res, getStudentIntrestsAndSkills, function(err, resFinal) {
          if (err) {
            errorFunction('error occured', cb1);
          } else {
            cb1(null, resFinal);
          }
        });
      }
    });
  };
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getDetailsBasedOnIntrestId', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getDetailsBasedOnIntrestId',
      verb: 'POST',
    },
  });
  // function oncampusData(obj, cb) {
  //   var studentSkills = server.models.StudentSkills;
  //   var studentInterests = server.models.StudentInterests;
  //   var studentDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
  //   var studentObj = {};
  //   studentObj['studentId'] = obj.studentId;
  //   console.log('studentObjstudentObjstudentObjstudentObj ', studentObj);
  //   studentDetails(studentObj, studentSkills, function(error, skillsOut) {
  //     if (error) {
  //       cb(error, cb);
  //     } else {
  //       //obj.skills = (skillsOut) ? skillsOut.data : [];
  //      // console.log(obj);
  //       studentDetails(studentObj, studentInterests, function(error, interestsOut) {
  //         if (error) {
  //           cb(error, null);
  //         } else {
  //           var studentDetails = {};
  //           studentDetails.skills = skillsOut;
  //           studentDetails.intrests = interestsOut;
  //           cb(null, studentDetails);
  //         }
  //         //obj.interests = (interestsOut) ? interestsOut.data : null;
  //       });
  //     }
  //   });
  // }
  function oncampusData(obj, cb) {
    console.log(obj);
    var studentSkills = server.models.StudentSkills;
    var studentInterests = server.models.StudentInterests;
    var studentObj = {};
    //studentObj['studentId'] = obj.studentId;
    studentSkills.find({
      'where': {
        'studentId': obj.studentId,
      },
    }, function(err, skills) {
      if (err) {
        errorFunction('error in finding skills', cb);
      } else {
        var async = require('async');
        async.map(skills, getSkillValue, function(err, skillsOfStudent) {
          if (err) {

          } else {
            studentInterests.find({
              'where': {
                'studentId': obj.studentId,
              },
            }, function(error, intrests) {
              if (err) {

              } else {
                async.map(intrests, getIntrestsValue, function(err, intrestOfStudents) {
                  if (err) {

                  } else {
                    var final = skillsOfStudent.concat(intrestOfStudents);
                    cb(null, final);
                  }
                });
              }
            });
          }
        });
      }
    });
  }
  // studentDetails(studentObj, studentSkills, function(error, skillsOut) {
  //   if (error) {
  //     cb(error, cb);
  //   } else {
  //     //obj.skills = (skillsOut) ? skillsOut.data : [];
  //    // console.log(obj);
  //     studentDetails(studentObj, studentInterests, function(error, interestsOut) {
  //       if (error) {
  //         cb(error, null);
  //       } else {
  //         var studentDetails = {};
  //         studentDetails.skills = skillsOut;
  //         studentDetails.intrests = interestsOut;
  //         cb(null, studentDetails);
  //       }
  //       //obj.interests = (interestsOut) ? interestsOut.data : null;
  //     });
  //   }
  // });

  // function oncampusData(obj, cb) {
  //   var studentSkills = server.models.StudentSkills;
  //   var studentInterests = server.models.StudentInterests;
  //   var studentDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
  //   var studentObj = {};
  //   studentObj['studentId'] = obj.studentId;
  //   console.log('studentObjstudentObjstudentObjstudentObj ', studentObj);
  //   studentDetails(studentObj, studentSkills, function(error, skillsOut) {
  //     if (error) {
  //       cb(error, cb);
  //     } else {
  //       // obj.skills = (skillsOut) ? skillsOut.data : [];
  //       // console.log(obj);
  //       // studentDetails(studentObj, studentInterests, function(error, interestsOut) {
  //       //   obj.interests = (interestsOut) ? interestsOut.data : null;
  //       cb(null, skillsOut);
  //       // });
  //     }
  //   });
  // }

  function getDrives(campusDrives, obj, callBc) {
    var campus = server.models.Campus;
    var enrollmentModel = server.models.Enrollment;
    var campusObj = {};
    campusObj['obj.studentId'] = obj.studentId;
    campus.findOne({
      'where': {
        'and': [campusObj],
      },
    }, function(err, campusDetails) {
      enrollmentModel.find({
        'where': {
          'and': [campusObj],
        },
      }, function(errEnroll, enrollmentDetails) {
        var start = enrollmentDetails[0];
        start = (enrollmentDetails.length > 0) ? enrollmentDetails[0].startDate : new Date();
        var actual = (enrollmentDetails.length > 0) ? enrollmentDetails[0].planedCompletionDate : new Date();
        var currentDate = new Date();
        var enrollmentArray = [];
        enrollmentArray = (start.getTime() < currentDate.getTime() && actual.getTime() > currentDate.getTime()) ? enrollmentDetails.length : 0;
        obj.activeoncampusData = 'Active';
        obj.numberOfStudents = campusDetails.numberOfStudents;
        obj.studentsAvailable = enrollmentArray;
        callBc(null, obj);
      });
    });
  }
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getAllStudents = function(empEventId, cb) {
    var event = server.models.EventStudentList;
    event.find({
      'where': {
        'employerEventId': empEventId,
      },
    }, function(err, studentsRes) {
      if (err) {

      } else {
        //console.log(studentsRes);
        empId[0] = empEventId;
        var async = require('async');
        async.map(studentsRes, getStudentDetails, function(err1, res) {
          if (err1) {

          } else {
            cb(null, res);
          }
        });
      }
    });
  };

  function getStudentDetails(ob, cb1) {
    getStudentIntrestsAndSkills(ob, function(error, finalResp) {
      if (error) {
        errorFunction('error occured', cb);
      } else {
        var data = {};
        data.studentDetails = ob;
        data.studentFullDetails = finalResp;
        cb1(null, data);
      }
    });
  }
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getSkills = function(studentId, cb) {
    var obj = {};
    obj.studentId = studentId;
    getStudentIntrestsAndSkills(obj, function(err, res) {
      if (err) {
        errorFunction('err', cb);
      } else {
        cb(null, res);
      }
    });
  };
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getSkills', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getSkills',
      verb: 'GET',
    },
  });

  function getStudentIntrestsAndSkills(obj, callBack) {
    var studentSkills = server.models.StudentSkills;
    var studentInterests = server.models.StudentInterests;
    //var student = server.models.Student;
    CAMPUS_EVENT_STUDENT_SEARCH_VW.findOne({
      'where': {
        'studentId': obj.studentId,
      },
    }, function(err, studentRes) {
      if (err) {
        errorFunction('err occured', callBack);
      } else {
        studentSkills.find({
          'where': {
            'studentId': obj.studentId,
          },
        }, function(er, skillResponse) {
          if (er) {
            errorFunction('an error', callBack);
          } else {
            var async = require('async');
            async.map(skillResponse, getSkillValue, function(err, skillsOfStudent) {
              if (err) {

              } else {
                studentInterests.find({
                  'where': {
                    'studentId': obj.studentId,
                  },
                }, function(error, intrests) {
                  if (err) {

                  } else {
                    async.map(intrests, getIntrestsValue, function(err, intrestOfStudents) {
                      if (err) {

                      } else {
                        var skillsAndIntrests = {};
                        skillsAndIntrests.studentInfo = studentRes;
                        // skillsAndIntrests.studentInfo.student = studentRes;
                        getObject(skillsOfStudent, function(err, res) {
                          if (err) {

                          } else {
                            getObject1(intrestOfStudents, function(err, res1) {
                              skillsAndIntrests.skills = skills;
                              skillsAndIntrests.intrests = interests;
                              //var final = skillsOfStudent.concat(intrestOfStudents);
                              callBack(null, skillsAndIntrests);
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getSkillsAndInt = function(obj, cb) {
    getStudentIntrestsAndSkills(obj, function(err, res) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, res);
      }
    });
  };
  exports.getStudentIntrestsAndSkills = getStudentIntrestsAndSkills;
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getAllStudents', {
    description: 'Provide Student Id ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'empEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getAllStudents',
      verb: 'GET',
    },
  });

  function getObject(array, skillintcb) {
    skills = [];
    for (var i = 0; i < array.length; i++) {
      for (var j = 0; j < array[i].length; j++) {
        skills.push(array[i][j]);
      }
    }
    skillintcb(null, 'done');
  }

  function getObject1(array, Intcb) {
    interests = [];
    for (var i = 0; i < array.length; i++) {
      for (var j = 0; j < array[i].length; j++) {
        interests.push(array[i][j]);
      }
    }
    Intcb(null, 'done');
  }

  function getSkillValue(obj, callBc) {
    lookup('SKILL_TYPE_CODE', obj.skillTypeValueId, function(err, skillResponse) {
      if (err) {
        errorFunction('err', callBc);
      } else {
        callBc(null, skillResponse);
      }
    });
  }

  function getIntrestsValue(obj1, callBack) {
    lookup('INTEREST_TYPE_CODE', obj1.interestTypeValueId, function(err, intrestResponse) {
      if (err) {
        errorFunction('err', callBack);
      } else {
        callBack(null, intrestResponse);
      }
    });
  }

  function lookup(type, ob, lookupCallback) {
    var lookup = server.models.LookupType;
    var lookupvalue = server.models.LookupValue;
    //var type = 'SKILL_TYPE_CODE';
    lookup.find({
      'where': {
        lookupCode: type,
      },
    }, function(err, re) {
      if (err) {
        lookupCallback(err, null);
      } else {
        lookupvalue.find({
          'where': {
            lookupValueId: ob,
            lookupTypeId: re[0].lookupTypeId,
          },
        }, function(err, re1) {
          if (err) {
            lookupCallback('error', re1);
          } else {
            lookupCallback(null, re1);
          };
        });
      }
    });
  }
  var totalShortlisted = 0;
  var totalOffers = 0;
  CAMPUS_EVENT_STUDENT_SEARCH_VW.campusStudentSearchVw = function(campusId, firstName, departmentId, lastName, programId, cgpaScore, flag, cb) {
    var campusObj = {};
    campusObj['campusId'] = campusId;
    campusObj['departmentId'] = (departmentId) ? departmentId : undefined;
    campusObj['programId'] = (programId) ? programId : undefined;
    var cgpa = (cgpaScore) ? (cgpaScore) : 0;
    var sampleCgpa = (cgpaScore) ? (cgpaScore) : null;
    var symbol = '%';
    var firstNameSearch = (firstName) ? (firstName) : '';
    var lastNameSearch = (lastName) ? (lastName) : '';
    if (campusId) {
      CAMPUS_EVENT_STUDENT_SEARCH_VW.find({
        'where': {
          'campusId': campusId,
        },
      }, function(err, res) {
        if (err) {
          cb(err, null);
        } else if (res == 0) {
          cb(null, 'No Data Found');
        } else {
          var endDate = new Date();
          var startDate = new Date();
          var currentYear = new Date().getFullYear();
          var month = new Date().getMonth() + 1;
            // console.log(currentYear + 1);
          if (month >= 6) {
            startDate.setFullYear((currentYear), 5, 1);
            endDate.setFullYear((currentYear + 1), 4, 31);
          } else if (month < 6) {
            startDate.setFullYear((currentYear - 1), 5, 1);
            endDate.setFullYear((currentYear), 4, 31);
          }
          CAMPUS_EVENT_STUDENT_SEARCH_VW.find({
            'where': {
              and: [{
                or: [{
                  'cgpaScore': {
                    gt: cgpa,
                  },
                }, {
                  'cgpaScore': sampleCgpa,
                }],
              },
              {
                and: [{
                  'campusId': campusId,
                }, {
                  'departmentId': departmentId,
                }, {
                  'programId': programId,
                },
                {
                  or: [{
                    'offCampusIndicator': 'N',
                  }, {
                    'offCampusIndicator': null,
                  }],
                },
                      ],
              }, {
                'firstName': {
                  like: firstNameSearch + symbol,
                },
              },
              {
                'lastName': {
                  like: lastNameSearch + symbol,
                },
              },
              {
                'planedCompletionDate': {
                  between: [startDate, endDate],
                },
              },
                  ],
            },
          },
              function(err, res) {
                // console.log(campusObj);
                if (err) {
                  cb(err, null);
                } else {
                  var async = require('async');
                  async.map(res, getDetails, function(err13, res13) {
                    if (err13) {
                      cb(null, err13);
                    } else {
                      cb(null, res13);
                    }
                  });
                }
              });
        }
      });
    } else {
      errorFunction('campusId cant be null', cb);
    }
    function getDetails(obj, cb4) {
      var skills = [];
      var interests = [];
      var skill = server.models.StudentSkills;
      skill.getStudentSkill(obj.studentId, function(err2, res2) {
        var intrests = server.models.StudentInterests;
        var async = require('async');
        async.map(res2.data, getSkillObj, function(err, res) {
          intrests.getStudentInterest(obj.studentId, function(err3, res3) {
            async.map(res3.data, getIntrestObj, function(err, res) {
              if (err) {

              } else {
                if (flag == 'Y' || flag == 'y') {
                  getAllCounts(obj, function(err1, res1) {
                    var details = {};
                    details.studentDetails = obj;
                    details.skills = skills;
                    details.intrests = interests;

                    details.eventDetails = res1;
                    cb4(null, details);
                    totalOffers = 0;
                    totalShortlisted = 0;
                  });
                } else {
                  var details = {};
                  details.studentDetails = obj;
                  details.skills = skills;
                  details.intrests = interests;
                  cb4(null, details);
                }
              }
              // });
            });
          });

          function getIntrestObj(object2, cb6) {
            var data = object2.toJSON();
            // console.log(object2);
            // interests['value'] = data.studentInterest;
            var intrests = {};
            intrests['intrestsData'] = data.studentInterest;
            interests.push(intrests);
            cb6(null, interests);
          }
        });

        function getSkillObj(object1, cb5) {
          var data = object1.toJSON();
          var skillData = {};
          skillData['skillData'] = data.studentSkill;
          skills.push(skillData);
          cb5(null, skills);
        }
      });
    }

    // function getSearchName(searchName, myArray, callBc) {
    //   async.map(myArray, searchWithName, function(error, response) {
    //     var output = [];
    //     output = cleanArray(response);
    //     callBc(null, output);
    //   });

    //   function searchWithName(object, cb) {
    //     searchName = searchName.toUpperCase();
    //     var searchFirst = ((object.firstName).toUpperCase()) ? ((object.firstName).toUpperCase()).indexOf(searchName) >= 0 : false;
    //     var searchLast = ((object.lastName).toUpperCase()) ? ((object.lastName).toUpperCase()).indexOf(searchName) >= 0 : false;
    //     // console.log('sssssssssssssssssss ', searchShort, search);
    //     if (searchFirst || searchLast) {
    //       cb(null, object);
    //     } else {
    //       cb(null, null);
    //     }
    //   }
    // }

    function getAllCounts(studentDetails, cb3) {
      var eventstudent = server.models.EventStudentList;
      eventstudent.find({
        'where': {
          'studentId': studentDetails.studentId,
          'campusId': campusId,
        },
        'include': ['eventStudentListIbfk3rel', 'eventStudentListIbfk6rel'],
      }, function(err12, res12) {
        if (err12) {

        } else {
          var async = require('async');
          totalOffers = 0;
          totalShortlisted = 0;
          async.map(res12, getTotalCount, function(error, respons) {
            if (error) {

            } else {
              if (res12.length === respons.length) {
                console.log('obj', respons);
                var data = {};
                data.totalOffers = totalOffers;
                data.totalShortlisted = totalShortlisted;
                data.totalEnrolled = res12.length;
                async.map(res12, getStatus, function(err2, res3) {
                  if (res12.length === res3.length) {
                    data.events = res3;
                    cb3(null, data);
                  }
                });
              }
            }
          });
        }
      });
    }
    function getTotalCount(obj, cb) {
      console.log(obj.candidateStatusValueId === 381);
      if (obj.candidateStatusValueId == 381) {
        // console.log('total offeres', totalOffers);
        totalOffers++;
        cb(null, 'done');
      } else if (obj.candidateStatusValueId == 377) {
        totalShortlisted++;
        cb(null, 'done');
      } else {
        cb(null, 'done');
      }
    }
    function getStatus(obj, cb) {
      // console.log(obj.employerEventId);
      if (obj.campusEventId != null) {
        var campusEvent = server.models.CampusEvent;
        campusEvent.findOne({
          'where': {
            'campusEventId': obj.campusEventId,
          },
          'include': ['campusEventIbfk6rel', 'campusEventIbfk5rel'],
        }, function(err11, res11) {
          if (err11) {

          } else {
            // console.log('resssssssss 11', res11);
            var data = {};
            data = obj;
            data.details = res11;
            cb(null, data);
          }
        });
      } else {
        cb(null, obj);
      }
    }
    function getIds(cb2) {
      lookup('CANDIDATE_STATUS_TYPE', 'Offered', function(err7, res7) {
        if (err7) {
          cb2(err7, null);
        } else {
          lookup('CANDIDATE_STATUS_TYPE', 'Shortlisted', function(err8, res8) {
            if (err8) {
              cb2(err8, null);
            } else {
              var lookupData = {};
              lookupData.Offered = res7[0].lookupValueId;
              lookupData.Shortlisted = res8[0].lookupValueId;
              cb2(null, lookupData);
            }
          });
        }
      });
    }

    function lookup(type, ob, lookupCallback) {
      var lookup = server.models.LookupType;
      var lookupvalue = server.models.LookupValue;
      lookup.find({
        'where': {
          lookupCode: type,
        },
      }, function(err, re) {
        if (err) {
          lookupCallback(err, null);
        } else {
          lookupvalue.find({
            'where': {
              lookupValue: ob,
              lookupTypeId: re[0].lookupTypeId,
            },
          }, function(err, re1) {
            if (err) {
              lookupCallback('error', null);
            } else {
              lookupCallback(null, re1);
            };
          });
        }
      });
    }

    function lookupForId(type, id, lookupCallback) {
      var lookup = server.models.LookupType;
      var lookupvalue = server.models.LookupValue;
      lookup.find({
        'where': {
          lookupCode: type,
        },
      }, function(err, re) {
        if (err) {
          lookupCallback(err, null);
        } else {
          lookupvalue.find({
            'where': {
              lookupValueId: id,
              lookupTypeId: re[0].lookupTypeId,
            },
          }, function(err, re1) {
            if (err) {
              lookupCallback('error', null);
            } else {
              lookupCallback(null, re1);
            };
          });
        }
      });
    }
  };
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('campusStudentSearchVw', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      require: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'firstName',
      type: 'string',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'departmentId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'lastName',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'programId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'cgpaScore',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'flag',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/campusStudentSearchVw',
      verb: 'get',
    },
  });
};

'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(COMPENSATION_PACKAGE_ITEM) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  COMPENSATION_PACKAGE_ITEM.observe('before save', function compensationPkgItemBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.compPackageItemId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('COMPENSATION_PACKAGE_ITEM Creation Initiated');
      next();
    } else if (ctx.isNewInstance) {
      errorFunction('compPackageItemId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get compensation package item details
  /**
 *getCompensationPkgItemDetails- To get pakage by taking required inputs
 *@constructor
 * @param {number} compPackageId  - unique id of pakage
 * @param {number} companyId - unique id of company
 * @param {number} compPackageItemId - unique id of company pakage
 * @param {function} callBc - deals with response
 */
  COMPENSATION_PACKAGE_ITEM.getCompensationPkgItemDetails = function(compPackageId, companyId, compPackageItemId, callBc) {
    var inputObj = {};
    var CompensationPackageItem = server.models.CompensationPackageItem;
    var compensationPackageItemDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (compPackageId && companyId && compPackageItemId) {
      inputObj['compPackageId'] = compPackageId;
      inputObj['companyId'] = companyId;
      inputObj['compPackageItemId'] = compPackageItemId;
      var includeModels = ['compensationPackage'];
      // below function will give details for an entity based on loopback include filter
      compensationPackageItemDet(inputObj, CompensationPackageItem, includeModels, callBc);
    } else if (compPackageId && companyId) {
      inputObj['compPackageId'] = compPackageId;
      inputObj['companyId'] = companyId;
      var includeModels = ['compensationPackage'];
      // below function will give details for an entity based on loopback include filter
      compensationPackageItemDet(inputObj, CompensationPackageItem, includeModels, callBc);
    } else if (companyId) {
      inputObj['companyId'] = companyId;
      var includeModels = ['compensationPackage'];
      // below function will give details for an entity based on loopback include filter
      compensationPackageItemDet(inputObj, CompensationPackageItem, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get compensation package details
  COMPENSATION_PACKAGE_ITEM.remoteMethod('getCompensationPkgItemDetails', {
    description: 'To get compensation package details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'compPackageId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'compPackageItemId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getCompensationPkgItemDetails',
      verb: 'get',
    },
  });
  //UpdateCompensationPackageItem remote method starts here
  /**
 *updateCompensationPackageItem - To update compensation pakage details
 *@constructor
 * @param {object} packageData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  COMPENSATION_PACKAGE_ITEM.updateCompensationPackageItem = function(packageData, cb) {
    var packageItemUpdate = require('../../commonCompanyFiles/update-compensation-package-item');
    packageItemUpdate.updatePackageItemService(packageData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateCompensationPackageItem method creation
  COMPENSATION_PACKAGE_ITEM.remoteMethod('updateCompensationPackageItem', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCompensationPackageItem',
      verb: 'PUT',
    },
  });
};

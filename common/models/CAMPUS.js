'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
// exporting function to use it in another modules if requires
module.exports = function(CAMPUS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to campus model then we execute logic or change request object before saving into database
  CAMPUS.createCampus = function(data, cb) {
    if (data.tireValueId) {
      // console.log('going in');
      // console.log(data.tireValueId);
      var lookupValue = server.models.LookupValue;
      var lookupType = server.models.LookupType;
      var status = [];
      lookupType.findOne({'where': {lookupCode: 'INSTITUTE_TIER'}}, function(err, lookupResponse) {
        if (err) {
          cb(err, null);
        } else {
          lookupValue.find({'where': {'lookupTypeId': lookupResponse.lookupTypeId}}, function(err, res) {
            if (err) {
              cb(err, null);
            } else {
              res.map(function(campusDet) {
                if (campusDet.lookupValueId == data.tireValueId) {
                  status.push('true');
                }
              });
              if (status[0] == 'true') {
                data.searchName = (data.name) ? data.name.toUpperCase() : null;
                data.searchShortName = (data.shortName) ? data.shortName.toUpperCase() : null;
                data.updateUserId = data.createUserId;
                data.createDatetime = new Date();
                data.updateDatetime = new Date();
                CAMPUS.create(data, function(err, res) {
                  if (err) {
                    cb(err, null);
                  } else {
                    cb(null, res);
                  }
                });
              } else {
                // console.log('the id was invalid');
                errorFunction('Invalid TierValueId', cb);
              }
            }
          });
        }
      });
    } else {
      data.searchName = (data.name) ? data.name.toUpperCase() : null;
      data.searchShortName = (data.shortName) ? data.shortName.toUpperCase() : null;
      data.updateUserId = data.createUserId;
      data.createDatetime = new Date();
      data.updateDatetime = new Date();
      CAMPUS.create(data, function(err, res) {
        if (err) {
          cb(err, null);
        } else {
          cb(null, res);
        }
      });
    }
  };
  CAMPUS.remoteMethod('createCampus', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: {
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    },
    http: {
      path: '/createCampus',
      verb: 'POST',
    },
  });
  // remote method definition to get campus details based on campus id
  /**
 *getCampus - To get campus details
 *@constructor
 * @param {number} campusId - contains unique campus id
 * @param {function} callBc - deals with response
 */
  CAMPUS.getCampus = function(campusId, callBc) {
    if (campusId) {
      var campusDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      var campus = server.models.Campus;
      campusDetById(inputFilterObject, campus, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus details based on campus id
  CAMPUS.remoteMethod('getCampus', {
    description: 'To get campus details based on campus id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampus',
      verb: 'get',
    },
  });
  /**
 *updateCampus- To update campus details
 *@constructor
 * @param {number} campusData - contains object need to get updated
 * @param {function} callBc - deals with response
 *///updateCampus custom method starts here
  CAMPUS.updateCampus = function(campusData, cb) {
    var updateCampusDet = require('../../commonCampusFiles/update-campus-profile');
    updateCampusDet.updateCampusProfile(campusData, function(err, resp) {
      logger.info('upating campus profile ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //remoteMethod for campus update
  CAMPUS.remoteMethod('updateCampus', {
    description: 'Send Valid campusId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCampus',
      verb: 'PUT',
    },
  });
  //remoteMethod for campus Profile
  CAMPUS.getCampusDashboard = function(inputObj, callBc) {
    var getCampusDashboardDetails = require('../../commonCampusFiles/get-campus-dashboard.js').getCampus;
    getCampusDashboardDetails(inputObj, function(err, response) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, response);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusDashboard', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCampusDashboard',
      verb: 'POST',
    },
  });
  //remote method to get graphs for each department

  CAMPUS.getDepartmentData = function(inputObj, callBc) {
    var getGraphsInfo = require('../../commonCampusFiles/get-campus-graphs').getDepartmentGraphs;
    getGraphsInfo(inputObj, function(err, response) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, response);
      }
    });
  };
  CAMPUS.remoteMethod('getDepartmentData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getDepartmentData',
      verb: 'POST',
    },
  });
  CAMPUS.getCampusProfile = function(campusId, callBc) {
    var getProfile = require('../../commonCampusFiles/campus-profile').getCampusProfile;
    getProfile(campusId, function(err, profileRes) {
      if (err) {
        errorFunction('error', callBc);
      } else {
        callBc(null, profileRes);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusProfile', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusProfile',
      verb: 'GET',
    },
  });
  CAMPUS.getGraphAndTableData = function(campusId, callBc) {
    var getProfile = require('../../commonCampusFiles/campus-profile').getGraphAndTableData;
    var date = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    if (month < 6) {
      date = date - 1;
    }
    getProfile(campusId, date, function(err, profileRes) {
      if (err) {
        errorFunction('error', callBc);
      } else {
        callBc(null, profileRes);
      }
    });
  };
  CAMPUS.remoteMethod('getGraphAndTableData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getGraphAndTableData',
      verb: 'GET',
    },
  });
  CAMPUS.getCampusGraphDataForDashboard = function(campusId, callBc) {
    var graphDataOfCampus = require('../../commonCampusFiles/campus-dashboard').campusDashboard;
    graphDataOfCampus(campusId, function(err, campusGraphDataResponse) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(err, campusGraphDataResponse);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusGraphDataForDashboard', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusGraphDataForDashboard',
      verb: 'GET',
    },
  });
  CAMPUS.getRecentPlacements = function(campusId, cb) {
    var endDate = new Date();
    var currentYear = new Date().getFullYear();
    var startDate = new Date();
    var month = new Date().getMonth();
    var date = new Date().getUTCDate();
    startDate.setFullYear(currentYear, (month - 1), date);
    var eventList = server.models.EventStudentList;
    eventList.find({
      'where': {
        'campusId': campusId, 'candidateStatusValueId': 381, 'updateDatetime': {
          between: [startDate, endDate],
        }, order: 'updateDatetime DESC'
      }
    }, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(response, getNameAndDetails,
          function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              var finalArray = resp.reverse();
              cb(null, finalArray);
            }
          });
      }
    });
  };
  function getNameAndDetails(eventStudentObject, callBack) {
    var campusEvent = server.models.CampusEvent;
    campusEvent.findOne({
      'where': {'campusEventId': eventStudentObject.campusEventId},
      'include': ['campusEventIbfk1rel', 'campusEventIbfk4rel']
    },
      function(err, eventResponse) {
        if (err) {
          callBack(err, null);
        } else {
          var campusEventData = eventResponse.toJSON();
          var campusEventSearchVw = server.models.CampusEventStudentSearchVw;
          campusEventSearchVw.findOne({
            'where': {
              'studentId': eventStudentObject.studentId,
              'departmentId': eventStudentObject.departmentId
            }
          }, function(err, studentResposne) {
            if (err) {
              callBack(err, null);
            } else {
              // console.log(studentResposne, eventResponse, eventStudentObject);
              var data = {};
              data['Student Name'] = (studentResposne !== null) ? studentResposne.firstName +
                ' ' + studentResposne.lastName : null;
              data['Placed in Company'] = campusEventData.campusEventIbfk4rel.name;
              data['Department'] = (studentResposne !== null) ? studentResposne.depName : null;
              // data.updateDatetime = eventStudentObject.updateDatetime;
              data['Program'] = (studentResposne !== null) ? studentResposne.programName : null;
              callBack(null, data);
            }
          });
        }
      });
    // var student = server.models.Student;
    // student.findOne({'where': {'studentId': eventStudentObject.studentId}},
    // function(err, studentDetails) {
    //   if (err) {
    //     callBack(err, null);
    //   } else {
    //     var company = server.models.Company;
    //     company.findOne({'where': {'companyId': eventStudentObject.companyId}},
    //     function(err, companyResponse) {
    //       if (err) {
    //         callBack(err, null);
    //       } else {
    //         var enrollment = server.models.Enrollment;
    //         enrollment.findOne({'where': {'studentId': eventStudentObject.studentId}},
    //         function(err, enrollObject) {
    //           if (err) {
    //             callBack(err, null);
    //           } else {
    //             var program = server.models.Program;
    //             program.findOne({'where': {'programId': enrollObject.programId}},
    //             function(err, programResponse) {
    //               if (err) {
    //                 callBack(err, null);
    //               } else {
    //                 var department = server.models.Department;
    //                 department.findOne({'where': {'departmentId': programResponse.departmentId}},
    //                 function(err, departmentResponse) {
    //                   if (err) {
    //                     callBack(err, null);
    //                   } else {
    //                     var data = {};
    //                     data['Student Name'] = studentDetails.firstName + ' ' + studentDetails.lastName;
    //                     data['Placed in Company'] = companyResponse.name;
    //                     data['Department'] = departmentResponse.shortName;
    //                     // data.updateDatetime = eventStudentObject.updateDatetime;
    //                     data['Program'] = programResponse.programName;
    //                     callBack(null, data);
    //                   }
    //                 });
    //               }
    //             });
    //           }
    //         });
    //       }
    //     });
    //   }
    // });
  }
  CAMPUS.remoteMethod('getRecentPlacements', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getRecentPlacements',
      verb: 'GET',
    },
  });
  CAMPUS.upCommingEvents = function(campusId, cb) {
    var campusEvents = server.models.CampusEvent;
    campusEvents.find({
      'where': {
        'campusId': campusId, 'scheduledDate': {
          gte: new Date(),
        }
      }, 'include': 'campusEventIbfk5rel'
    },
      function(err, resp) {
        if (err) {
          cb(err, null);
        } else {
          var async = require('async');
          async.map(resp, getThisEventCount,
            function(err, response) {
              if (err) {
                cb(err, null);
              } else {
                cb(null, response);
              }
            });
        }
      });
    var shortlisted = 0;
    function getThisEventCount(obj, cb) {
      var eventsData = obj.toJSON();
      var eventList = server.models.EventStudentList;
      eventList.find({'where': {'campusEventId': obj.campusEventId}},
        function(err, response) {
          if (err) {
            cb(err, null);
          } else {
            var date = (new Date(obj.scheduledDate).getMonth() + 1) + '/' +
              new Date(obj.scheduledDate).getUTCDate() + '/' +
              new Date(obj.scheduledDate).getFullYear();
            var data = {};
            data['Event Name'] = obj.eventName;
            data['No of Students'] = response.length;
            data['Date'] = date;
            data['Event Type'] = obj.eventTypeValueId;
            cb(null, data);
            shortlisted = 0;
          }
        });
    }
  };
  CAMPUS.remoteMethod('upCommingEvents', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/upCommingEvents',
      verb: 'GET',
    },
  });
  var uniqueCompanyIds = [];
  var companyData = {};
  var details = [];
  var orgDetails = [];
  var campusDetails = [];
  var companySelectedCount = [];
  var campusNameAndSelected = [];
  var selected = 0;
  var companyNameDetails = {};
  var companyIds = {};
  var companyDataAndNames = {};
  CAMPUS.getCampusHiringData = function(campusId, departmentId, cb) {
    companyNameDetails = {};
    companyDataAndNames = {};
    companyData = {};
    companyIds = {};
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    // console.log(currentYear + 1);
    if (month >= 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month < 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    if (campusId && departmentId) {
      var statment = 'select count(student_id) totalOffers, evst.company_id companyId, comp.name companyName from ' +
        ' EVENT_STUDENT_LIST evst ' +
        ' join COMPANY comp on ' +
        ' evst.company_id = comp.company_id where candidate_status_value_id = 381 ' +
        ' and campus_id =' + campusId + ' and evst.department_id = ' + departmentId + ' and evst.update_datetime BETWEEN ' + JSON.stringify(startDate) + '' +
        ' and ' + JSON.stringify(endDate) + ' group by evst.company_id';
      MySql(statment, function(err, response) {
        if (err) {
          cb(err, null);
        } else {
          cb(null, response);
        }
      });
      //   var campusDrive = server.models.CampusDrive;
      //   campusDrive.find({'where': {'campusId': campusId, 'departmentId': departmentId,
      //     'createDateTime':
      //     {
      //       between: [startDate, endDate],
      //     },
      // }},
      //   function(err, res) {
      //     if (err) {
      //       cb(err, null);
      //     } else {
      //       var async = require('async');
      //       async.map(res, getDepBasedEventDetails, function(err, orgResp) {
      //         if (err) {
      //           cb(err, null);
      //         } else {
      //           var async = require('async');
      //           // async.map(uniqueCompanyIds, getCompanySelectedCount, function(err, res) {
      //           //   if (err) {
      //           //     cb(err, null);
      //           //   } else {
      //           async.map(uniqueCompanyIds, getCompanyAndNameDetails,
      //                 function(err, companyDataAndNames) {
      //                   if (err) {
      //                     cb(err, null);
      //                   } else {
      //                     cb(null, companyDataAndNames);
      //                     selected = 0;
      //                     companyNameDetails = {};
      //                     companyIds = {};
      //                     companyData = {};
      //                     uniqueCompanyIds = [];
      //                     details = [];
      //                     orgDetails = [];
      //                     campusDetails = [];
      //                     companySelectedCount = [];
      //                     campusNameAndSelected = [];
      //                   }
      //                 });
      //           //   }
      //           // });
      //         }
      //       });
      //     }
      //   });
    } else {
      var statment = 'select count(student_id) totalOffers, evst.company_id companyId, comp.name companyName from ' +
        ' EVENT_STUDENT_LIST evst ' +
        ' join COMPANY comp on ' +
        ' evst.company_id = comp.company_id where candidate_status_value_id = 381 ' +
        ' and campus_id =' + campusId + ' and evst.update_datetime BETWEEN ' + JSON.stringify(startDate) + '' +
        ' and ' + JSON.stringify(endDate) + ' group by evst.company_id';
      MySql(statment, function(err, response) {
        if (err) {
          cb(err, null);
        } else {
          // var companyData = {};
          // companyData.companyName = response[0].companyName;
          // companyData.companyId = response[0].companyId;
          // companyData.totalOffers = response[0].totalOffers;
          cb(null, response);
          // var async = require('async');
          // async.map(response, getCompanyName, function(err, resp) {
          //   if (err) {
          //     cb(err, null);
          //   } else {
          //     cb(null, resp);
          //   }
          // });
        }
      });
    }
  };
  function getDepBasedEventDetails(obje, cb) {
    var campEvent = server.models.CampusEvent;
    campEvent.find({'where': {'campusDriveId': obje.campusDriveId}},
      function(err, resp) {
        if (err) {
          cb(err, null);
        } else if (resp == null) {
          cb(null, 'done');
        } else {
          var async = require('async');
          async.map(resp, getEventName, function(err, res) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, res);
            }
          });
        }
      });
  }
  function getEventName(obj, cb) {
    var eventStudent = server.models.EventStudentList;
    eventStudent.find({
      'where': {'campusEventId': obj.campusEventId},
      'include': 'eventStudentListIbfk3rel'
    },
      function(er, response) {
        if (er) {
          cb(er, null);
        } else {
          var async = require('async');
          async.map(response, getCountsOfStudents,
            function(err, resp) {
              if (err) {
                cb(err, null);
              } else {
                cb(null, 'done');
              }
            });
        }
      });
  }
  function getCountsOfStudents(obj, cb) {
    console.log('oooooooooooooo', obj);
    if (obj.candidateStatusValueId === 381) {
      if (obj.companyId in companyData) {
        var eventStudentInfo = obj.toJSON();
        var temp = companyData[obj.companyId];
        companyData[obj.companyId] = temp + 1;
        companyNameDetails[obj.companyId] = eventStudentInfo.eventStudentListIbfk3rel.name;
        companyIds[obj.companyId] = obj.companyId;
        cb(null, 'done');
      } else {
        uniqueCompanyIds.push(parseInt(obj.companyId));
        var eventStudentInfo = obj.toJSON();
        companyData[obj.companyId] = 1;
        companyNameDetails[obj.companyId] = eventStudentInfo.eventStudentListIbfk3rel.name;
        companyIds[obj.companyId] = obj.companyId;
        cb(null, 'done');
      }
    } else {
      cb(null, 'done');
    }
    // if (obj.candidateStatusValueId == 381) {
    //   details.push(obj);
    //   if (uniqueCompanyIds.includes(parseInt(obj.companyId))) {
    //     cb(null, 'done');
    //   } else {
    //     uniqueCompanyIds.push(parseInt(obj.companyId));
    //     cb(null, 'done');
    //   }
    // } else {
    //   cb(null, 'done');
    // }
  }
  // function getCompanySelectedCount(companyId, cb) {
  //   selected = 0;
  //   for (var i = 0; i < details.length; i++) {
  //     if (details[i].companyId == companyId) {
  //       selected++;
  //     } if (i == details.length - 1) {
  //       var data = {};
  //       data.companyId = companyId;
  //       data.selected = selected;
  //       companySelectedCount.push(data);
  //       cb(null, 'done');
  //       selected = 0;
  //     }
  //   }
  // }
  function getCompanyAndNameDetails(obj, cb) {
    var data = {};
    data.companyName = companyNameDetails[obj];
    data.companyId = companyIds[obj];
    data.totalOffers = companyData[obj];
    cb(null, data);
  }
  // function getCompanyName(obj, callBc) {
  //   var company = server.models.Company;
  //   company.findOne({'where': {'companyId': obj.companyId},
  //     'fields': {
  //       'name': true,
  //     }}, function(err, resp) {
  //     if (err) {
  //       callBc(err, null);
  //     } else {
  //       var companyData = {};
  //       companyData.companyName = resp.name;
  //       companyData.companyId = obj.companyId;
  //       companyData.totalOffers = obj.totalOffers;
  //       callBc(null, companyData);
  //     }
  //   });
  // }
  CAMPUS.remoteMethod('getCampusHiringData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'departmentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusHiringData',
      verb: 'GET',
    },
  });
  CAMPUS.dashBordSummaryData = function(campusId, callBc) {
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    if (month >= 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month < 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    if (campusId) {
      var totalComp = 0;
      var maxComp = 0;
      var evenStudentList = server.models.EventStudentList;
      evenStudentList.find({
        'where': {
          'campusId': campusId, 'candidateStatusValueId': 381,
          'updateDatetime': {
            between: [startDate, endDate],
          }
        }, 'include': 'eventStudentListIbfk7rel'
      }, function(err, eventStudentResponse) {
        if (err) {
          callBc(err, null);
        } else {
          var async = require('async');
          async.map(eventStudentResponse, getAverageAndMaxComp, function(error, averageMaxResponse) {
            if (error) {

            } else {
              var totalVisited = 'select count(distinct company_id) visited from CAMPUS_EVENT where scheduled_date < ' + JSON.stringify(new Date()) + ' and ' +
                ' event_status_value_id = 244 or event_status_value_id = 239 ' +
                ' and update_datetime between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + '';
              MySql(totalVisited, function(er, countResponse) {
                console.log('total', totalVisited);
                if (er) {
                  callBc(er, null);
                } else {
                  var data = {};
                  data.averageComp = totalComp / (eventStudentResponse.length);
                  data.maxComp = maxComp;
                  data.visited = countResponse[0].visited;
                  totalComp = 0;
                  maxComp = 0;
                  callBc(null, data);
                }
              });
            }
          });
          function getAverageAndMaxComp(obj, cb) {
            var eventStudentData = obj.toJSON();
            if (obj.compPackageId !== null) {
              totalComp = Number((obj.eventStudentListIbfk7rel.totalCompPkgValue) ?
                obj.eventStudentListIbfk7rel.totalCompPkgValue : 0
              ) + totalComp;
              maxComp = Math.max(maxComp, Number((obj.eventStudentListIbfk7rel.totalCompPkgValue)
                ? obj.eventStudentListIbfk7rel.totalCompPkgValue : 0));
              cb(null, 'done');
            } else {
              totalComp = Number((obj.totalCompValue) ? obj.totalCompValue : 0) + totalComp;
              maxComp = Math.max(maxComp, Number((obj.totalCompValue) ?
                obj.totalCompValue : 0));
              cb(null, 'done');
            }
          }
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus details based on campus id
  CAMPUS.remoteMethod('dashBordSummaryData', {
    description: 'To get campus details based on campus id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/dashBordSummaryData',
      verb: 'get',
    },
  });
  function MySql(statment, sqlCb) {
    var app = require('../../server/server.js');
    var connector = app.dataSources.ScoraXChangeDB.connector;
    connector.execute(statment, undefined, function(err, result) {
      if (err) {
        sqlCb(null, null);
      } else {
        sqlCb(null, result);
      }
    });
  }
  // get campus info using universityId and studentId for mobile app
  CAMPUS.getCampusInfo = function(universityId, studentId, callBC) {
    var campus = server.models.Campus;
    campus.find({'where': {'and': [{'universityId': universityId}]}}, function(campusErr, campusOut) {
      if (campusOut) {
        callBC(null, campusOut);
      } else {
        callBC(null, null);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusInfo', {
    description: 'To get campus details based on campus id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'universityId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusInfo',
      verb: 'get',
    },
  });
};

'use strict';
var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var employerCampusListHDRJson = require('./EMPLOYER_CAMPUS_LIST_HDR.json').properties;
var employerCampusListDTLJson = require('./EMPLOYER_CAMPUS_LIST_DTL.json').properties;
delete employerCampusListDTLJson['listId'];
delete employerCampusListDTLJson['updateUserId'];
delete employerCampusListHDRJson['updateUserId'];
var employerCampusListCompPkgJson = require('./EMPLOYER_CAMPUS_LIST_COMP_PKG.json').properties;
delete employerCampusListCompPkgJson['updateUserId'];
delete employerCampusListCompPkgJson['listId'];
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(EMPLOYER_CAMPUS_LIST_HDR) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_CAMPUS_LIST_HDR.observe('before save', function employerCampusListBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.listId == undefined) {
      var compApprovalCode = 'COMP_APPROVAL_STATUS_CODE';
      var compApprovalObj = {};
      compApprovalObj['lookupValueId'] = ctx.instance.compApprovalStatusValueId;
      lookupMethods.typeCodeFunction(compApprovalObj, compApprovalCode, function(compApprovalStatus) {
        if (compApprovalStatus) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('EmployerCampusListHDR Creation Initiated');
          next();
        } else {
          errorFunction('compApprovalStatus validation error', next);
        }
      });
    } else if (ctx.isNewInstance) {
      errorFunction('EmployerCampusListHDR listId is system generated value', next);
    } else {
      next();
    }
  });

  // remote method definition to get employer campus list hdr details
  /**
   *getEmployerCampusListHdrDetails- To get details by taking required fields
   *@constructor
   * @param {number} listId - Unique id for ecah and every list
   * @param {function} callBc - deals with response
   */
  EMPLOYER_CAMPUS_LIST_HDR.getEmployerCampusListHdrDetails = function(listId, callBc) {
    var inputObj = {};
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    var employerCampusListHdrDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (listId) {
      inputObj['listId'] = listId;
      var includeModels = ['employerCampusListsLookupValue', 'employerCampusListsCompany', 'jobRoleDetails'];
      // below function will give details for an entity based on loopback include filter
      employerCampusListHdrDetails(inputObj, employerCampusListHdr, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer campus list hdr details
  EMPLOYER_CAMPUS_LIST_HDR.remoteMethod('getEmployerCampusListHdrDetails', {
    description: 'To get get employer campus list hdr details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'listId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEmployerCampusListHdrDetails',
      verb: 'GET',
    },
  });
  // Get List and Jobrole Details
  // remote method definition to get employer campus list hdr details
  EMPLOYER_CAMPUS_LIST_HDR.getCampusListWithCompany = function(companyId, callBC) {
    var inputObj = {};
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    var employerCampusList = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (companyId) {
      inputObj['companyId'] = companyId;
      var includeModels = ['employerCampusListsLookupValue', 'employerCampusListsCompany', 'jobRoleDetails'];
      // below function will give details for an entity based on loopback include filter
      employerCampusList(inputObj, employerCampusListHdr, includeModels, function(err, response) {
        var resp = response.data;
        var async = require('async');
        async.map(resp, getList, function(error, finalResp) {
          var finalObj = {};
          finalObj.data = finalResp;
          callBC(null, finalObj);
        });
        function getList(obj, cb) {
          // console.log('objjj');
          var employerCampusListDtl = server.models.EmployerCampusListDtl;
          employerCampusListDtl.find({ 'where': { 'listId': obj.listId } }, function(listErr, listOut) {
            var object = {};
            var campusArray = [];
            if (listOut) {
              for (var i = 0; i < listOut.length; i++) {
                // console.log('listoutttttttttttttt ', listOut.length, obj.listId);
                campusArray.push(listOut[i].campusId);
              }
              var uniqueEmpResp = campusArray.filter(function(elem, index, self) {
                return index == self.indexOf(elem);
              });
              // console.log('uniqueEmpRespuniqueEmpResp', uniqueEmpResp);
              object = obj;
              object.campusListLength = uniqueEmpResp.length;
              cb(null, object);
              campusArray = [];
            } else {
              object = obj;
              object.campusListLength = 0;
              cb(null, object);
            }
          });
        }
      });
    } else {
      errorResponse(callBC);
    }
  };
  EMPLOYER_CAMPUS_LIST_HDR.remoteMethod('getCampusListWithCompany', {
    description: 'To get get employer campus list hdr details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusListWithCompany',
      verb: 'GET',
    },
  });
  //UpdateEmployerCampusListHdr remote method starts here
  /**
   *updateEmployerCampusListHdr- To update Campus list Details
   *@constructor
   * @param {object} empCampusData - contains all the data need to get updated
   * @param {function} cb - deals with response
   */
  EMPLOYER_CAMPUS_LIST_HDR.updateEmployerCampusListHdr = function(empCampusData, cb) {
    var empCampusUpdate = require('../../commonCompanyFiles/update-emp-campus-list-hdr-service');
    empCampusUpdate.updateEmployerCampusListService(empCampusData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateEmployerCampusListHdr method creation
  EMPLOYER_CAMPUS_LIST_HDR.remoteMethod('updateEmployerCampusListHdr', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerCampusListHdr',
      verb: 'PUT',
    },
  });

  var employerCampusListHdrCreation = function(empHdr) {
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    return new Promise(function(resolve, reject) {
      employerCampusListHdr.create(empHdr, function(err, empHdrResp) {
        if (err) {
          reject(err);
        } else if (empHdrResp) {
          resolve(empHdrResp);
        }
      });
    });
  };
  var employerCampusListDtlCreation = function(empHdr, empDtl) {
    return new Promise(function(resolve, reject) {
      var async = require('async');

      var employerCampusListId = empHdr[0].listId;
      // console.log("employerCampusListId: "+employerCampusListId);
      async.map(empDtl, createIndividualCampusListDTL.bind({
        listId: employerCampusListId,
      }), function(error, empDtlCreationResp) {
        if (error) {
          reject(error);
        } else if (empDtlCreationResp) {
          resolve(empDtlCreationResp);
        }
      });
    });
  };

  function createIndividualCampusListDTL(empDtl, callback) {
    console.log('listId: ' + this.listId);
    var employerCampusListDtl = server.models.EmployerCampusListDtl;
    empDtl.listId = this.listId;
    employerCampusListDtl.create(empDtl, function(err, empDtlResp) {
      if (err)
        console.log('errorempDtlResp: ' + err);
      console.log('empDtlResp: ' + JSON.stringify(empDtlResp));
      callback(null, empDtlResp);
    });
  }

  var employerCampusListCompPkgCreation = function(empHdrResolveResp, empDtlResolveRep, empListCompPkg) {
    console.log('employerCampusListCompPkgCreation..............1111');
    return new Promise(function(resolve, reject) {
      var async = require('async');
      var employerCampusListId = empHdrResolveResp[0].listId;
      console.log('employerCampusListId: ' + employerCampusListId);
      console.log('employerCampusListCompPkgCreation..............2222');
      async.map(empListCompPkg, createIndividualEmpListCompPkg.bind({
        listId: employerCampusListId,
      }), function(error, empListCompPkgResp) {
        if (error) {
          reject(error);
        } else if (empListCompPkgResp) {
          // empDtlResolveRep.empListCompPkg=empListCompPkgResp;
          var resp = {};
          resp.empHdr = empHdrResolveResp;
          resp.empDtl = empDtlResolveRep;
          resp.empListCompPkg = empListCompPkgResp;
          resolve(resp);
        }
      });
    });
  };

  function createIndividualEmpListCompPkg(empListCompPkgData, callback) {
    console.log('employerCampusListCompPkgCreation..............individual');
    console.log('listId: ' + this.listId);
    var employerCampusListCompPkg = server.models.EmployerCampusListCompPkg;
    empListCompPkgData.listId = this.listId;
    employerCampusListCompPkg.create(empListCompPkgData, function(err, empListCompPkgResp) {
      console.log('error empListCompPkgResp: ' + JSON.stringify(err));
      console.log('empListCompPkgResp: ' + JSON.stringify(empListCompPkgResp));
      callback(null, empListCompPkgResp);
    });
  }
  /**
   *createCompleteEmployerCampusList- To create complete employer campus list
   *@constructor
   * @param {object} inputData - contains all the data need to get created
   * @param {function} cb - deals with response
   */
  var throwError = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  EMPLOYER_CAMPUS_LIST_HDR.createCompleteEmployerCampusList = function(inputData, cb) {
    if (inputData.empHdr.length == 1 && inputData.empDtl.length > 0 && inputData.empListCompPkg.length > 0) {
      validateModel(inputData.empHdr, employerCampusListHDRJson, function(err, employerCampusListHDRValidationResp) {
        validateModel(inputData.empDtl, employerCampusListDTLJson, function(error, employerCampusListDTLValidationResp) {
          validateModel(inputData.empListCompPkg, employerCampusListCompPkgJson, function(error, employerCampusListCompPkgValidationResp) {
            console.log(error);
            if (employerCampusListHDRValidationResp.valid == true && employerCampusListDTLValidationResp.valid == true &&
              employerCampusListCompPkgValidationResp.valid == true) {
              employerCampusListHdrCreation(inputData.empHdr).then(function(empHdrResolveResp) {
                employerCampusListDtlCreation(empHdrResolveResp, inputData.empDtl).then(function(empDtlResolveRep) {
                  employerCampusListCompPkgCreation(empHdrResolveResp, empDtlResolveRep, inputData.empListCompPkg).then(
                    function(employerListCompPkgResolveResp) {
                      cb(null, employerListCompPkgResolveResp);
                    },
                    function(employerListCompPkgRejectResp) {
                      cb(null, employerListCompPkgRejectResp);
                    }
                  );
                  // cb(null,empDtlResolveRep);
                }, function(empDtlRejectRep) {
                  cb(empDtlRejectRep, null);
                });
              }, function(empHdrRejectResp) {
                cb(empHdrRejectResp, null);
              });
            } else {
              /*cb(resp.errors, null);*/
              errorResponse(cb);
            }
            // add here })
          });
        });
      });
    } else {
      throwError('Compensation Package and List of Campuses has to be Mandotory', cb);
    }
  };
  //Create EMPLOYER_CAMPUS_LIST_HDR
  EMPLOYER_CAMPUS_LIST_HDR.remoteMethod('createCompleteEmployerCampusList', {
    description: 'send EmployerCampusList HDR and DTL along with employer campus list compensation package details ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCompleteEmployerCampusList',
      verb: 'POST',
    },
  });
};

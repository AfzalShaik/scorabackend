'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var Contact = server.models.Contact;
/**
   *updateContact-function deals with updating the contact Data
   *@constructor
   * @param {object} contactData - contains the id need to get updated and the contactid
   * @param {function} cb - deals with the response
   */
function updateContact(contactData, cb) {
  Contact.findOne({
    'where': {
      'contactId': contactData.contactId,
    },
  }, function(err, contactResp) {
    console.log('this is contact dataaaaaaaaaaaaaaaaaaaaaaaa', contactResp);
    //if record found it will the  updateAttributes
    contactResp.updateAttributes(contactData, function(err, info) {
      if (err) {
        cb(err, info);
      } else {
        //making requestStatus is true;
        logger.info('Campus Contact Updated Successfully');
        info['requestStatus'] = true;
        info['updateDatetime'] = new Date(); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use Date Util to fetch Base standard date time object. 
        cb(null, info);
      }
    });
  });
}
exports.updateContact = updateContact;

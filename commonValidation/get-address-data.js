'use strict';
var server = require('../server/server');
var async = require('async');
var cleanArray = require('./common-mass-upload').cleanArray;
var campusDetails = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
/**
   *getRegione-function deals with getting the region
   *@constructor
   * @param {number} ststeCode - unique id for each and every state
   * @param {function} callBc - deals with the response
   */
function getRegion(stateCode, callBc) {
  var stateModal = server.models.State;
  var addressModel = server.models.Address;
  var inputObj = {};
  if (stateCode) {
    inputObj['stateCode'] = stateCode;
    var includeModels = ['stateDetails'];
    // below function will give details for an entity based on loopback include filter
    campusDetails(inputObj, addressModel, includeModels, function(err, resp) {
      getCampusList(resp.data, function(error, stateResp) {
        if (error) {
          callBc(error, null);
        } else {
          callBc(null, stateResp);
        }
      });
    });
  };
};

function getCityCampuses(cityId, callBc) {
  var inputObj = {};
  var addressModel = server.models.Address;
  inputObj['cityId'] = cityId;
  var includeModels = ['cityDetails'];
   // below function will give details for an entity based on loopback include filter
  campusDetails(inputObj, addressModel, includeModels, function(error, cityResp) {
    getCampusList(cityResp.data, function(error, stateResp) {
      if (error) {
        callBc(error, null);
      } else {
        callBc(null, stateResp);
      }
    });
  });
}
/**
   *getCampusList-used to get the campus list
   *@constructor
   * @param {array} input - array of objects which details we want to get
   * @param {function} callBc - deals with the response
   */
exports.getRegion = getRegion;
exports.getCityCampuses = getCityCampuses;
function getCampusList(input, callBc) {
  async.map(input, getCampusDetails, function(e, r) {
    if (e) {
      callBc(e, null);
    } else {
      var output = [];
      output = cleanArray(r);
      callBc(null, output);
    }
  });

  function getCampusDetails(obj, callBack) {
    var campusAddress = server.models.CampusAddress;
    var campusModel = server.models.Campus;
    var inputObj = {};
    campusAddress.find({
      'where': {
        'addressId': obj.addressId,
      },
    }, function(err, campusAdd) {
      if (err) {
        callBack(err, null);
      } else if (campusAdd.length > 0) {
        inputObj['campusId'] = campusAdd[0].campusId;
        inputObj['addressId'] = campusAdd[0].addressId;
        var includeModels = ['campusDetails', 'campusAddress'];
        // below function will give details for an entity based on loopback include filter
        campusDetails(inputObj, campusAddress, includeModels, function(err, response) {
          if (err) {
            callBack(err, null);
          } else {
            console.log(response);
            callBack(null, response.data[0]);
          }
        });
      } else {
        callBack(null, null);
      }
    });
  }
}

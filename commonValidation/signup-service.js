'use strict';
var server = require('../server/server');
var lookup = require('./lookupMethods').lookupMethod;
var campusSignup = require('./campus-signup-service').campusSignupService;
var companySignup = require('./company-signup-service').companySignupService;
var studentSignup = require('./student-signup-service').studentSignupService;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;

function createSignUpService(userInstance, callBc) {
  // console.log('------------------------------------------------------------------');
  // To Get Campus Role_Type_Value_Id
  lookup('ROLE_TYPE_CODE', function(err, response) {
    var campusIndicator = response.find(findIndicatior);
    var CompanyIndicator = response.find(findCompany);
    var studentIndicator = response.find(findStudent);
    // console.log('indicator, ', studentIndicator.lookupValueId == userInstance.roleTypeValueId);
    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Campus';
    }
    //To Get Company Role_Type_Value_Id

    function findCompany(companyVal) {
      return companyVal.lookupValue === 'Employer';
    }
    // To Get Student Role_Type_Value_Id
    function findStudent(studentVal) {
      return studentVal.lookupValue === 'Student';
    }
    if (campusIndicator.lookupValueId == userInstance.roleTypeValueId) {
      campusSignup(userInstance, function(campusErr, campusResponse) {
        if (campusErr) {
          throwError(campusErr, callBc);
        } else {
          callBc(null, campusResponse);
        }
      });
    } else if (CompanyIndicator.lookupValueId == userInstance.roleTypeValueId) {
      companySignup(userInstance, function(companyErr, companyResponse) {
        if (companyErr) {
          throwError(companyErr, callBc);
        } else {
          callBc(null, companyResponse);
        }
      });
    } else if (studentIndicator.lookupValueId == userInstance.roleTypeValueId) {
      // console.log('signupppppppppppppppppppppp ', userInstance.id);
      studentSignup(userInstance, function(studentErr, studentResponse) {
        if (studentErr) {
          throwError(studentErr, callBc);
        } else {
          callBc(null, studentResponse);
        }
      });
    } else {
      throwError('Indicator is required', callBc);
    }
  });
}
exports.createSignUpService = createSignUpService;

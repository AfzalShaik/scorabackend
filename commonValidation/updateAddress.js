'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var Address = server.models.Address;
/**
   *updateAddress-function deals with updating the address
   *@constructor
   * @param {object} addressData - contains the id need to get updated and address id
   * @param {function} cb - deals with the response
   */
function updateAddress(addressData, cb) {
  Address.findOne({
    'where': {
      'addressId': addressData.addressId,
    },
  }, function(err, AddressResp) {
    //if record found it will the  updateAttributes
    AddressResp.updateAttributes(addressData, function(err, info) {
      if (err) {
        cb(err, info);
      } else {
        //making requestStatus is true;
        logger.info('Campus Address Updated Successfully'); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Isn't this generic? Also use debug() methods for logging. 
        info['requestStatus'] = true;
        info['updateDatetime'] = new Date(); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use date util and not direct Date API.. Sometimes we might want to fetch from the DB time as the servers could be hosted on different timezones and we dont want local times (from each server's timezone) to be saved. Base standard datetime should be stored so that its same for all the instances. 
        cb(null, info);
      }
    });
  });
}
exports.updateAddress = updateAddress;

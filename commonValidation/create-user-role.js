'use strict';
var server = require('../server/server');
var async = require('async');
/**
   *createRoleService-function deals with creating roles
   *@constructor
   * @param {object} roleData - contains user id for creating an role
   * @param {function} cb - deals with the response
   */
function createRoleService(roleData, cb) {
  async.map(roleData, createRoleMethod, function(e, r) {
    if (e) {
      cb(e.errors, null);
    } else {
      cb(null, r);
    }
  });

  function createRoleMethod(obj, callback) {
    var userRole = server.models.ScoraUserRole;
    var role = roleInput(obj);
    userRole.create(role, function(err, roleResp) {
      if (err) {
        callback(err, null);
      } else {
        callback(null, roleResp);
      }
    });
  }
}

function roleInput(obj) {
  var studentRoleCode = 'STUDENT';
  var role = {
    'userId': obj.id,
    'roleCode': studentRoleCode,
    'startDate': new Date(),
    'endDate': new Date(),
    'createDatetime': new Date(),
    'updateDatetime': new Date(),
    'createUserId': obj.createUserId,
    'updateUserId': obj.updateUserId,
  };
  return role;
}
exports.createRoleService = createRoleService;

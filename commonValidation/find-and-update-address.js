'use strict';
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var updateAddService = require('../commonValidation/update-service-address').updateAddresService;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
/**
   *findAndUpdate-function deals with finding and updating the data
   *@constructor
   * @param {object} input - contains the id need to get updated
   * @param {object} findData - contains the data that need to get find
   * @param {string} modelname - contains the model name where to find the data
   * @param {function} cb - deals with the response
   */
function  findAndUpdate(input, findData, modelname, cb) {
  findEntity(findData, modelname, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response) {
      updateAddService(input.addresses[0], function(err, info) {
        if (err) {
          cb(err, null);
        } else {
          cb(null, info);
        }
      });
    } else {
      throwError('Invalid StudentId or addressId', cb);
    }
  });
}
exports.findAndUpdate = findAndUpdate;

'use strict';
var Joi = require('joi');
var validVal;
  /**
   *validateJson-function deals with validating the correct json format
   *@constructor
   * @param {object} massuploadData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function validateJson(massuploadData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    testName: Joi.string().max(50).required(),
    testVendorName: Joi.string().max(50).required(),
    description: Joi.string().max(100).required(),
    error: [Joi.string().max(100), Joi.allow(null)],
    // studentEmail: Joi.string().max(254).required(),
    studentEmail: Joi.string().email().max(254).required(),
    score: Joi.number().integer().max(999).required(),
    maxScore: Joi.number().integer().max(999).required(),
    rowNumber: Joi.number().integer().max(999999999999999),
    empEventId: Joi.number().integer().max(999999999999999),
    companyId: Joi.number().integer().max(999999999999999),
    employerEventId: Joi.number().integer().max(999999999999999),
  });

  Joi.validate(massuploadData, schema, function(err, value) {
    if (err) {
      var error = new Error('');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err.details[0].message;
      validVal = false;

      cb(error, null);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateJson = validateJson;

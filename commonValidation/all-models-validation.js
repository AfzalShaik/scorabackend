'use strict';
var Joi = require('joi');
var validVal;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var lookupMethods = require('./lookupMethods');
var logger = require('../server/boot/lib/logger');
var employerCampusListJson = require('./all-models-json').employerCampusListJson;
  /**
   *validateModelsJson-function deals with validating the json
   *@constructor
   * @param {object} modelsData - contains model data
   * @param {object} modelsData - contains all the data need to get valiadated
   * @param {any} cb - deals with the response
   */
function validateModelsJson(modelsData, compareddata, cb) {
  //validating input json fields
  var schema = Joi.object().keys(compareddata);

  Joi.validate(modelsData, schema, function(err, value) {
    if (err) {
      throwError('Validation Error', cb); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Can we take the error object too and decide or wrap before throwing a custom error from the generic method? That is how exceptions should be wrapped so that the original error is visible when the user is investigating. 
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateModelsJson = validateModelsJson;

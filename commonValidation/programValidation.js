'use strict';
var Joi = require('joi');
var validVal;
/**
   *validateJson-function deals with validating the json format
   *@constructor
   * @param {object} programData - data that need to get validated
   * @param {function} cb - deals with the response
   */
function validateJson(programData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    programId: Joi.number().max(999999999999999).required(),
    campusId: Joi.number().max(999999999999999).required(),
    departmentId: Joi.number().max(999999999999999).required(),
    programTypeValueId: Joi.number().max(999999999999999),
    programClassValueId: Joi.number().max(999999999999999),
    programCatValueId: Joi.number().max(999999999999999),
    numberOfStudents: Joi.number().max(999999999999999),
    programMajorValueId: Joi.number().max(999999999999999),
    offCampusInd: Joi.string().max(1),
    programName: Joi.string().max(50),
    description: [Joi.string().max(2000), Joi.allow(null)],
    startDate: Joi.date().iso(),
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
  });

  Joi.validate(programData, schema, function(err, value) {
    if (err) {
      var error = new Error('Validation Error');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err;
      validVal = false;

      cb(error, validVal);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateJson = validateJson;

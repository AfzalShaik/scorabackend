'use strict';
var Joi = require('joi');
var validVal;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var lookupMethods = require('../commonValidation/lookupMethods');
var logger = require('../server/boot/lib/logger');
/**
   *validateJobRoleJson-function deals with validating the json format
   *@constructor
   * @param {object} roleData - contains the json data need to get validated
   * @param {function} cb - deals with the response
   */
function validateJobRoleJson(roleData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    jobRoleId: Joi.number().max(999999999999999).required(),
    companyId: Joi.number().max(999999999999999).required(),
    jobRoleTypeValueId: Joi.number().max(999999999999999),
    jobRoleName: Joi.string().max(50),
    skillSet: Joi.string().max(2000),
    summary: Joi.string().max(2000),
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
  });

  Joi.validate(roleData, schema, function(err, value) {
    if (err) {
      throwError('Validation Error', cb);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
/**
   *validateJobRoleLookup-function deals validating the lookup value of job rple
   *@constructor
   * @param {object} roleData - contains jobRoleTypeValueId for validating the job role
   * @param {function} cb - deals with the response
   */
function validateJobRoleLookup(roleData, cb) {
  var jobRoleTypeVal, jobRoleVal;
  var jobRoleTypeObj = {};
  jobRoleTypeObj['lookupValueId'] = roleData.jobRoleTypeValueId;
  var jobRoleTypeCode = 'JOB_ROLE_TYPE_CODE';
  lookupMethods.typeCodeFunction(jobRoleTypeObj, jobRoleTypeCode, function(jobRoleTypeVal) {
    jobRoleTypeVal = jobRoleTypeVal;
    if (jobRoleTypeVal == true) {
      jobRoleVal = true;
      cb(null, jobRoleVal);
    } else {
      jobRoleVal = false;
      throwError('Invalid jobRoleTypeValueId', cb);
      logger.error('Invalid jobRoleTypeValueId');
    }
  });
}
exports.validateJobRoleJson = validateJobRoleJson;
exports.validateJobRoleLookup = validateJobRoleLookup;

'use strict';
var Joi = require('joi');
var validVal;
function validateJson(massuploadData, cb) {
    //validating input json fields
  var schema = Joi.object().keys({
    firstName: Joi.string().max(50).required(),
    lastName: Joi.string().max(500).required(),
    middleName: [Joi.string().max(50), Joi.allow(null)],
    gender: Joi.string().max(50).required(),
    dateOfBirth: [Joi.date().min('1-1-2000').max('now'), Joi.allow(null)],
    highlights: [Joi.string().max(500), Joi.allow(null)],
    // emailId: Joi.string().max(50).required(),
    emailId: Joi.string().email().max(512).required(),
    phoneNo: [Joi.number().max(9999999999), Joi.allow(null)],
    // phoneNo: Joi.string().phoneNumber(),
    skills: [Joi.string().max(200), Joi.allow(null)],
    interests: [Joi.string().max(200), Joi.allow(null)],
    score: [Joi.number().max(3), Joi.allow(null)],
    programName: [Joi.string().max(50), Joi.allow(null)],
  });

  Joi.validate(massuploadData, schema, function(err, value) {
    var lenghtVar = getlength(massuploadData.phoneNo);
    var numberVar = isNumeric(massuploadData.phoneNo);
    // console.log(err, value);
    if (err) {
      var error = {};
      // console.log('massss', err.details[0]);
      error.firstName = massuploadData.firstName;
      error.lastName = massuploadData.lastName;
      error.middleName = massuploadData.middelName;
      error.gender = massuploadData.gender;
      error.dateOfBirth = massuploadData.dateOfBirth;
      error.highlights = massuploadData.highlights;
      error.phoneNo = massuploadData.phoneNo;
      error.emailId = massuploadData.emailId;
      error.skills = massuploadData.skills;
      error.interests = massuploadData.interests;
      error.score = massuploadData.score;
      error.programName = massuploadData.programName;
      error.error = (lenghtVar == 10 && numberVar == true) ? err.details[0].message : 'Invalid Phone Number Entered';
      cb(error, null);
    } else if (lenghtVar == 10 && numberVar == true) {
      cb(null, massuploadData);
    } else {
      var error = {};
      error.firstName = massuploadData.firstName;
      error.lastName = massuploadData.lastName;
      error.middleName = massuploadData.middelName;
      error.gender = massuploadData.gender;
      error.dateOfBirth = massuploadData.dateOfBirth;
      error.highlights = massuploadData.highlights;
      error.phoneNo = massuploadData.phoneNo;
      error.emailId = massuploadData.emailId;
      error.skills = massuploadData.skills;
      error.interests = massuploadData.interests;
      error.score = massuploadData.score;
      error.programName = massuploadData.programName;
      error.error = (lenghtVar == 10 && numberVar == true) ? err.details[0].message : 'Invalid Phone Number Entered';
      cb(error, null);
    }
  });
  function getlength(number) {
    return number.length;
  }
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
}
exports.validateJson = validateJson;


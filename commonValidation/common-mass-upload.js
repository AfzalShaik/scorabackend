'use strict';
//function to read the csv file
function readFile(inputFile, cb) {
  var fs = require('fs');
  var parse = require('csv-parse');
  var request = require('request');
  // request.get(inputFile, function(error, response, body) {
  //   if (!error && response.statusCode == 200) {
  //     var csv = body;
  // console.log(body);
  parse(inputFile, {delimiter: ',', columns: true}, function(err, output) {
    console.log(err);
    // console.log('outttttttttttt ', output);
    if (err) {
      cb(err, null);
    } else {
      cb(null, output);
    }
  });
  // } else {
  //   console.log(error);
  //   cb(error, null);
  // }
  // });
}
function cleanArray(actual) {
  var newArray = new Array();
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}
function commonPathFile(config, container, name, cb) {
  var protocol = (config.userOptions.env == 'prod') ? 'https://' : 'http://';
  var host = (config.userOptions.env == 'prod') ? config.userOptions.host : config.host;
  var port = (config.userOptions.env == 'prod') ? undefined : config.port;
  var devUrl = protocol + host + ':' + port + '/api/Attachments/' + container + '/download/' + name;
  var prodUrl = protocol + host + '/api/Attachments/' + container + '/download/' + name;
  var fullPathForm = (config.userOptions.env == 'prod') ? prodUrl : devUrl;
  // console.log('fileurllllllllllllllll ', fullPathForm);
  cb(null, fullPathForm);
}
function readCSVFiles(input, cb) {
  var server = require('../server/server');
  var attachment = server.models.Attachment;
  var parse = require('csv-parse');
  var reader;
  var content = '';
  reader = attachment.downloadStream(input.container, input.name, {}, cb);
  reader
    .on('readable', function() {
      var chunk;
      while (null !== (chunk = reader.read())) {
        content = content + chunk;
        // console.log('contenttttttttttt ', content);
      }
    })
    .on('end', function() {
      parse(content, {delimiter: ',', columns: true}, function(err, output) {
        // console.log(err);
        // console.log('outttttttttttt ', output);
        if (err) {
          cb(err, null);
        } else {
          cb(null, output);
        }
      });
    });
}
exports.readFile = readFile;
exports.cleanArray = cleanArray;
exports.commonPathFile = commonPathFile;
exports.readCSVFiles = readCSVFiles;

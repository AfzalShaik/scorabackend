'use strict';
var server = require('../server/server');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var lookup = require('./lookupMethods').lookupMethod;

function campusSignupService(userInstance, callBack) {
  var campus = server.models.Campus;
  getUniversity(userInstance, function(campusObj) {
    campus.create(campusObj, function(campusErr, campusResp) {
      if (campusErr) {
        throwError(campusErr, callBack);
      }
      var userRole = server.models.ScoraUserRole;
      var educationPerson = server.models.EducationPerson;
      if (campusResp) {
        var userRoleObj = {
          'id': userInstance.id,
          'roleCode': 'PLCDIR',
          'startDate': new Date(),
          'createDatetime': new Date(),
          'updateDatetime': new Date(),
          'createUserId': 1,
          'updateUserId': 1,
        };
        userRole.create(userRoleObj, function(roleError, roleResponse) {
          if (roleError) {
            throwError(roleError, callBack);
          } else {
            var educationPersonObj = {
              'campusId': campusResp.campusId,
              'id': userInstance.id,
              'firstName': userInstance.firstName,
              'lastName': userInstance.lastName,
              'createDatetime': new Date(),
              'updateDatetime': new Date(),
              'createUserId': 1,
              'updateUserId': 1,
            };
            educationPerson.create(educationPersonObj, function(eduError, eduResponse) {
              if (eduError) {
                throwError(eduError, callBack);
              } else {
                callBack(null, eduResponse);
              }
            });
          }
        });
      }
    });
  });
  // });
}

function getUniversity(userInstance, callBc) {
  // To Get Campus status_Type_Value_Id
  lookup('CAMPUS_STATUS_CODE', function(err, response) {
    var campusIndicator = response.find(findIndicatior);

    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Registered';
    }
    // var universityName = (userInstance.universityName) ? userInstance.universityName : 'OTHER';
    // var university = server.models.University;
    // university.findOne({
    //   'where': {
    //     'name': universityName,
    //   },
    // }, function(uniErr, uniResp) {
    var campusObj = {
      'universityId': userInstance.universityId,
      'name': userInstance.entityName,
      'searchName': userInstance.entityName.toUpperCase(),
      'campusStatusValueId': campusIndicator.lookupValueId,
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
    };
    callBc(campusObj);
    // });
  });
}
exports.campusSignupService = campusSignupService;

'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var lookup = require('../../commonValidation/lookupMethods').lookupMethod;
var async = require('async');
var cleanArray = require('../../commonValidation/common-mass-upload').cleanArray;
var inputData;
/**
 * onCampusActions-used for event actions
 * @constructor
 * @param {object} eventResponseData -get event response data from employer event
 * @param {object} inputData -details entered by the user
 * @param {function} cb-callbcak
 */
function onCampusActions(eventResponseData, inputData, cb) {
  inputData = inputData;
  if (inputData.action == 'Event Request to Campus') {
    //send event request to campus
    eventRquestToCampus(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb(null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Share Shortlisted Students') {
    //employer event action share shortlisted students
    eventActionShortListedStudents(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb(null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Offer') {
    //employer event offer action
    eventActionOfferStudent(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb(null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'In Progress') {
    // console.log('cominggggggggggggg');
    //employer event offer action
    eventActionInProgressStudent(eventResponseData, inputData, function(err, respData) {
      if (respData) {
        cb(null, respData);
      } else {
        cb(err, null);
      }
    });
  } else if (inputData.action == 'Reject') {
    //employer event reject action
    eventActionRejectStudent(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb(null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Close') {
    //employer event close action
    eventActionClose(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb(null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Scheduled') {
    //employer event action share shortlisted students
    eventActionScheduled(eventResponseData, inputData, function(err, respData) {
      if (inputData.scheduledStartTime && inputData.scheduledDate) {
        if (err) {
          cb(err, null);
        } else if (respData) {
          cb(null, respData);
        } else {
          cb(null, null);
        }
      } else {
        var errResp = {};
        errResp.message = 'Scheduled StartTime && Scheduled Date are required';
        errResp.status = 'failure';
        cb(errResp, null);
      }
    });
  } else {
    var errResp = {};
    errResp.message = 'Action Not Found';
    errResp.status = 'failure';
    cb(errResp, null);
  }
};

//Employer event request to campus
function eventRquestToCampus(eventResponseData, inputData, cb) {
  lookup('EMPLOYER_EVENT_STATUS_CODE', function(err, response) {
    var eventStatusValueInitial = response.find(findIndicatior);
    var eventStatusValuePublished = response.find(findPublishIndicatior);

    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Initial';
    }

    function findPublishIndicatior(publish) {
      return publish.lookupValue === 'Published';
    }
    if (eventResponseData.eventStatusValueId == eventStatusValueInitial.lookupValueId) {
      var eventApprovedObj = {};
      eventApprovedObj = eventResponseData;
      eventApprovedObj.eventStatusValueId = eventStatusValuePublished.lookupValueId;
      eventResponseData.updateAttributes(eventApprovedObj, function(err, info) {
        if (err) {
          cb(err, null);
        } else {
          updateEmpDrive(eventResponseData, function(driveErr, driveOut) {
            var resp = {};
            resp.message = 'event published';
            var employerDriveCampus = server.models.EmployerDriveCampuses;
            employerDriveCampus.find({
              'where': {
                'empEventId': inputData.employerEventId,
              },
            }, function(empErr, empResponse) {
              if (empErr) {
                cb(empErr, null);
              } else {
                async.map(empResponse, driveResponse, function(driveErr, driveResp) {
                  if (driveErr) {
                    cb(driveErr, null);
                  } else {
                    var notificationTemplate = server.models.NotificationMessageTemplates;
                    var pushNotification = notificationTemplate.pushNotification;
                    var employerDriveCampuses = server.models.EmployerDriveCampuses;
                    employerDriveCampuses.find({
                      'where': {
                        'and': [{
                          'companyId': info.companyId,
                        }, {
                          'empEventId': info.empEventId,
                        }],
                      },
                    }, function(empErr, empResp) {
                      if (empErr) {
                        cb(empErr, null);
                      } else {
                        // resp.campusList = empResp;
                        if (empResp.length > 0) {
                          checkUnregistered(empResp, function(unregErr, unregResp) {
                            // console.log('1111111111111111111111111111111111 ', unregResp.length);
                            if (unregResp.length > 0 && unregResp[0] != null) {
                              async.map(empResp, getCampus, function(campusErr, campusResp) {
                                var pushInput = {
                                  'userId': inputData.userId,
                                  'role': 'RECDIR',
                                  'companyId': inputData.companyId,
                                  'employerPersonId': inputData.employerPersonId,
                                  'empEventId': info.empEventId,
                                  'notificationName': 1,
                                  'campusList': campusResp,
                                };
                                pushNotification(pushInput, function(pushErr, pushOut) {
                                  if (pushErr) {
                                    cb(pushErr, null);
                                  } else {
                                    resp.Notification = 'Notificaton Sent';
                                    cb(null, resp);
                                  }
                                });
                              });
                            } else {
                              cb(null, resp);
                            }
                          });
                        } else {
                          cb('No Records Found for given EmpEventId', null);
                        }
                      }
                    });
                  }
                });
              }

              function driveResponse(obj, driveCB) {
                obj.empDriveStatusValueId = 314;
                obj.updateAttributes(obj, function(updateErr, updateResp) {
                  if (updateErr) {
                    driveCB(updateErr, null);
                  } else {
                    driveCB(null, updateResp);
                  }
                });
              }
            });
          });

        }
      });
    } else {
      var error = new Error();
      error.message = 'Cant send request to campus';
      error.status = 'failure';
      throwError(error, cb);
    }
  });
}
//getCampus
var campusArray = [];

function checkUnregistered(empResp, unregCB) {
  async.map(empResp, getUnreg, function(unregErr, unregResp) {
    var out = [];
    out = cleanArray(unregResp);
    console.log('------------ ', out.length);
    unregCB(null, unregResp);
  });
}

function getUnreg(object, unregCb) {
  var campus = server.models.Campus;
  // console.log(object);
  campus.findOne({
    'where': {
      'campusId': object.campusId,
    },
  }, function(campusErr, campusOut) {
    // console.log('resppppppp ', campusOut);
    if (campusOut.campusStatusValueId == 369) {
      unregCb(null, null);
    } else {
      unregCb(null, object);
    }
  });
}

function getCampus(obj, callBc) {
  if (obj.campusId) {
    // campusArray.push({'campusId': obj.campusId});
    var objj = {};
    objj.campusId = obj.campusId;
    callBc(null, objj);
  } else {
    callBc('CampusId is null', null);
  }
}
// get company
function getCompany(obj, callBc) {
  if (obj.companyId) {
    // campusArray.push({'campusId': obj.campusId});
    var objj = {};
    objj.companyId = obj.companyId;
    callBc(null, objj);
  } else {
    callBc('CompanyId is null', null);
  }
}

//Employer event short listed students
function eventActionShortListedStudents(eventResponseData, inputData, cb) {
  // console.log(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ', eventResponseData);
  lookup('EMPLOYER_EVENT_STATUS_CODE', function(err, response) {
    var eventStatusValueScreening = response.find(findIndicatior);
    var eventStatusValueShort = response.find(findPublishIndicatior);

    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Screening';
    }

    function findPublishIndicatior(publish) {
      return publish.lookupValue === 'Students shortlisted';
    }
    //................................to get campusEventId............................
    var campusEvent = server.models.CampusEvent;
    campusEvent.findOne({
      'where': {
        'and': [{
          'companyId': eventResponseData.companyId,
        }, {
          'employerEventId': eventResponseData.empEventId,
        }],
      },
    }, function(empErr, campusEventResp) {
      // });
      // console.log('============================================ ', campusEventResp);
      if (campusEventResp) {
        var inputshareObject = {};
        inputshareObject['campusEventId'] = campusEventResp.campusEventId;
        if (eventResponseData.eventStatusValueId == eventStatusValueScreening.lookupValueId) {
          updateStudentInfo(eventResponseData.empEventId, eventResponseData.companyId, function(studentErr, studentOut) {
            var eventShortListedObj = {};
            eventShortListedObj = eventResponseData;
            eventShortListedObj.eventStatusValueId = eventStatusValueShort.lookupValueId;
            delete eventResponseData.scheduledStartTime;
            eventResponseData.updateAttributes(eventShortListedObj, function(err, info) {
              if (err) {
                cb(err, null);
              } else {
                // console.log('infoooooooooooooooooooooooooooooooooooooo ', info);
                //find employer event in campus event
                var campusEventmodel = server.models.CampusEvent;
                campusEventmodel.findOne({
                  'where': {
                    'and': [inputshareObject],
                  },
                }, function(err, campusResp) {
                  // console.log('..............campus respp...pppp............. ', campusResp);
                  if (err) {
                    cb(err, null);
                  } else {
                    lookup('EDUCATION_EVENT_STATUS_CODE', function(err, response) {
                      // console.log('..............campus respppppp............. ', err, response);
                      var eventCampusValueShort = response.find(findCampusIndicatior);

                      function findCampusIndicatior(campusVal) {
                        return campusVal.lookupValue === 'Students shortlisted';
                      }
                      // console.log(':::::::::::::::::::::::::::::::; ', eventCampusValueShort);
                      var capusEventObj = {};
                      capusEventObj = campusResp;
                      capusEventObj.eventStatusValueId = eventCampusValueShort.lookupValueId;

                      campusResp.updateAttributes(capusEventObj, function(err, campusInfo) {
                        // console.log('..............campus event............. ', campusInfo);
                        if (err) {
                          cb(err, null);
                        } else {
                          var resp = {};
                          resp.message = 'students short listed';
                          // push notification
                          var notificationTemplate = server.models.NotificationMessageTemplates;
                          var pushNotification = notificationTemplate.pushNotification;
                          var employerDriveCampuses = server.models.EmployerDriveCampuses;
                          var driveObj = {};
                          driveObj['companyId'] = eventResponseData.companyId;
                          driveObj['empEventId'] = eventResponseData.empEventId;
                          updateEmployerDriveCampuses(driveObj, 'Students shortlisted', function(driveErr, driveOut) {
                            campusEventmodel.find({
                              'where': {
                                'and': [{
                                  'companyId': eventResponseData.companyId,
                                }, {
                                  'employerEventId': eventResponseData.empEventId,
                                }],
                              },
                            }, function(empErr, empResp) {
                              console.log(empErr, empResp);
                              if (empErr) {
                                cb(empErr, null);
                              } else {
                                // resp.campusList = empResp;
                                // console.log('empresppppppppppppppppppppppp ', empResp);
                                if (empResp.length > 0) {
                                  async.map(empResp, getCampus, function(campusErr, campusList) {
                                    var pushInput = {
                                      'userId': inputData.userId,
                                      'role': 'RECDIR',
                                      'companyId': eventResponseData.companyId,
                                      'employerPersonId': inputData.employerPersonId,
                                      'empEventId': eventResponseData.empEventId,
                                      'campusEventId': campusResp.campusEventId,
                                      'notificationName': 6,
                                      'campusList': campusList,
                                    };
                                    // console.log('.................................... ', pushInput);
                                    pushNotification(pushInput, function(pushErr, pushOut) {
                                      // console.log('=========================== ', pushOut);
                                      if (pushErr) {
                                        cb(pushErr, null);
                                      } else {
                                        resp.Notification = 'Notificaton Sent';
                                        cb(null, resp);
                                      }
                                    });
                                  });
                                } else {
                                  cb('No Records Found for given EmpEventId', null);
                                }
                              }
                            });
                          });
                        }
                      });
                      // }
                    });
                  }
                });
              }
            });
          });
        } else {
          var error = new Error();
          error.message = 'Cant Shortlist the student';
          error.status = 'failure';
          throwError(error, cb);
        }
      } else {
        var eventShortListedObj = {};
        eventShortListedObj = eventResponseData;
        eventShortListedObj.eventStatusValueId = 318;
        delete eventResponseData.scheduledStartTime;
        eventResponseData.updateAttributes(eventShortListedObj, function(err, info) {
          if (err) {
            cb(err, null);
          } else {
            var resp = {};
            resp = info;
            resp.message = 'Employer Event Updated Successfully';
            var driveObj = {};
            driveObj['companyId'] = eventResponseData.companyId;
            driveObj['empEventId'] = eventResponseData.empEventId;
            updateEmployerDriveCampuses(driveObj, 'Students shortlisted', function(driveErr, driveOut) {
              cb(null, resp);
            });
          }
        });
      }
    });
  });
}

//Employer event Offer action
function eventActionOfferStudent(eventResponseData, inputData, cb) {
  lookup('EMPLOYER_EVENT_STATUS_CODE', function(err, response) {
    var eventStatusValueSchedule = response.find(findIndicatior);
    var eventStatusValueProgress = response.find(findPublishIndicatior);

    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Scheduled';
    }

    function findPublishIndicatior(publish) {
      return publish.lookupValue === 'In Progress';
    }
    if (eventResponseData.eventStatusValueId == eventStatusValueSchedule.lookupValueId) {
      var eventStudentListModel = server.models.EventStudentList;
      var inputOfferStudentObject = {};
      inputOfferStudentObject['studentId'] = inputData.studentId;
      //find student in event student list
      eventStudentListModel.findOne({
        'where': {
          'and': [inputOfferStudentObject],
        },
      }, function(err, eventStudentListResp) {
        //
        if (err) {
          cb(err, null);
        } else {
          var eventStudentListObj = {};
          eventStudentListObj = eventStudentListResp;
          eventStudentListObj.offerStatusValueId = eventStatusValueProgress.lookupValueId;
          eventStudentListResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
            if (err) {
              cb(err, null);
            } else {
              var response = {};
              response.message = 'student offered';
              cb(null, response);
            }
          });
        }
      });
      //end update student list
    } else {
      var error = new Error();
      error.message = 'Cant Offer the students';
      error.status = 'failure';
      throwError(error, cb);
    }
  });
}

//Employer eventActionInProgressStudent
function eventActionInProgressStudent(eventResponseData, inputData, cb) {
  // console.log('..................... ', eventResponseData);
  lookup('EMPLOYER_EVENT_STATUS_CODE', function(err, response) {
    var eventStatusValueSchedule = response.find(findIndicatior);
    var eventStatusValueProgress = response.find(findPublishIndicatior);

    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Scheduled';
    }

    function findPublishIndicatior(publish) {
      return publish.lookupValue === 'In Progress';
    }
    if (eventResponseData.eventStatusValueId == eventStatusValueSchedule.lookupValueId) {
      var employerEvent = server.models.EmployerEvent;
      var inputObject = {};
      inputObject['empEventId'] = inputData.employerEventId;
      // console.log('[[[[[[[[[[[[[[[[ ', inputObject);
      //find student in event student list
      employerEvent.findOne({
        'where': {
          'and': [inputObject],
        },
      }, function(err, eventResp) {
        // console.log(';;;;;;;;;;;;;;;;;;;;;;; ', eventResp);
        //
        if (err) {
          cb(err, null);
        } else {
          var eventStudentListObj = {};
          eventStudentListObj = eventResp;
          eventStudentListObj.eventStatusValueId = eventStatusValueProgress.lookupValueId;
          eventResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
            // console.log('resooooooooooooooo ', err, studentListUpdateInfo);
            if (err) {
              cb(err, null);
            } else {
              var response = {};
              response.message = 'Event Status Updates as In Progress';
              // to make campus event status as in progress
              var campusEvent = server.models.CampusEvent;
              var objEvent = {};
              objEvent['employerEventId'] = inputData.employerEventId;
              campusEvent.find({
                'where': {
                  'and': [objEvent],
                },
              }, function(err, eventStudentArray) {
                if (err) {
                  cb(err, null);
                } else if (eventStudentArray) {
                  var campusArrayArray = [];
                  for (var i = 0; i < eventStudentArray.length; i++) {
                    campusArrayArray.push(eventStudentArray[i].campusEventId);
                  }
                  var uniqueEduResp = campusArrayArray.filter(function(elem, index, self) {
                    return index == self.indexOf(elem);
                  });
                  async.map(uniqueEduResp, updateCampusEvent, function(campusErr, eventResp) {
                    var empObject = {};
                    empObject['employerEventId'] = inputData.employerEventId;
                    changeCandidateStatus(empObject, 'Shortlisted', 'Interview in progress', function(candiErr, candidate) {
                      if (candidate) {
                        response.studentStatus = 'Shortlisted Students status updated as Interview in progress';
                        var driveObj = {};
                        driveObj['companyId'] = eventResponseData.companyId;
                        driveObj['empEventId'] = eventResponseData.empEventId;
                        updateEmployerDriveCampuses(driveObj, 'In Progress', function(driveErr, driveOut) {
                          // -----------------------//
                          // Students Status shortlisted to interview inprogress
                          var eventStudentList = server.models.EventStudentList;
                          eventStudentList.find({
                            'where': {
                              'and': [{
                                  'companyId': eventResponseData.companyId,
                                }, {
                                  'employerEventId': inputData.employerEventId,
                                },
                                {
                                  'candidateStatusValueId': 380,
                                },
                              ],
                            },
                          }, function(empErr, empResp) {
                            // console.log('emprespppppppppppppppp ', empResp);
                            if (empErr) {
                              cb(empErr, null);
                            } else if (empResp.length > 0) {
                              //----remove duplicates
                              var studentArray = [];
                              for (var i = 0; i < empResp.length; i++) {
                                studentArray.push(empResp[i].studentId);
                              }
                              var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                                return index == self.indexOf(elem);
                              });
                              async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                                var pushInput = {
                                  'userId': inputData.userId,
                                  'role': 'RECDIR',
                                  'companyId': inputData.companyId,
                                  'employerPersonId': inputData.employerPersonId,
                                  // 'empEventId': CampusEventData.empEventId,
                                  'campusEventId': empResp[0].campusEventId,
                                  'notificationName': 12,
                                  'studentList': studentList,
                                };
                                var notificationTemplate = server.models.NotificationMessageTemplates;
                                var pushNotification = notificationTemplate.pushNotification;
                                pushNotification(pushInput, function(pushErr, pushOut) {
                                  if (pushErr) {
                                    cb(pushErr, null);
                                    studentArray = [];
                                  } else {
                                    response.Notification = 'Notificaton Sent';
                                    cb(null, response);
                                    studentArray = [];
                                  }
                                });
                              });
                            } else {
                              cb(null, response);
                            }
                          });
                          //----------------------------//
                          // cb(null, response);
                        });
                      } else {
                        cb(candiErr, null);
                      }
                    });
                  });
                } else {
                  var empObject = {};
                  empObject['employerEventId'] = inputData.employerEventId;
                  changeCandidateStatus(empObject, 'Shortlisted', 'Interview in progress', function(candiErr, candidate) {
                    if (candidate) {
                      response.studentStatus = 'Shortlisted Students status updated as Interview in progress';
                      var driveObj = {};
                      driveObj['companyId'] = eventResponseData.companyId;
                      driveObj['empEventId'] = eventResponseData.empEventId;
                      updateEmployerDriveCampuses(driveObj, 'In Progress', function(driveErr, driveOut) {
                        cb(null, response);
                      });
                    } else {
                      cb(candiErr, null);
                    }
                  });
                }
              });
            }
          });
        }
      });
      //end update student list
    } else {
      var error = new Error();
      error.message = 'Cant change the status';
      error.status = 'failure';
      throwError(error, cb);
    }
  });
}
//Employer event reject action
function eventActionRejectStudent(eventResponseData, inputData, cb) {
  lookup('EMPLOYER_EVENT_STATUS_CODE', function(err, response) {
    var eventStatusValueSchedule = response.find(findIndicatior);
    var eventStatusValueReject = response.find(findPublishIndicatior);

    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Published';
    }

    function findPublishIndicatior(publish) {
      return publish.lookupValue === 'Rejected';
    }
    if (eventResponseData.eventStatusValueId == eventStatusValueSchedule.lookupValueId) {
      var eventStudentListModel = server.models.EventStudentList;
      var inputRejectStudentObject = {};
      inputRejectStudentObject['studentId'] = inputData.studentId;
      //find student in event student list
      eventStudentListModel.findOne({
        'where': {
          'and': [inputRejectStudentObject],
        },
      }, function(err, eventStudentListResp) {
        //
        if (err) {
          cb(err, null);
        } else {
          var eventStudentListObj = {};
          eventStudentListObj = eventStudentListResp;
          eventStudentListObj.offerStatusValueId = eventStatusValueReject.lookupValueId;
          //updating event student list
          eventStudentListResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
            if (err) {
              cb(err, null);
            } else {
              var response = {};
              response.message = 'student rejected';
              cb(null, response);
            }
          });
        }
      });
      //end update student list
    } else {
      var error = new Error();
      error.message = 'Cant Offer the students';
      error.status = 'failure';
      throwError(error, cb);
    }
  });
}

//employer event close action
function eventActionClose(eventResponseData, inputData, cb) {
  // console.log('closedddddddddddddddddddd ', eventResponseData);
  lookup('EMPLOYER_EVENT_STATUS_CODE', function(err, response) {
    var eventStatusValueClosed = response.find(findIndicatior);

    function findIndicatior(campusVal) {
      return campusVal.lookupValue === 'Closed';
    }
    var updateEmpEvent = {};
    updateEmpEvent = eventResponseData;
    updateEmpEvent.eventStatusValueId = eventStatusValueClosed.lookupValueId;
    eventResponseData.updateAttributes(updateEmpEvent, function(err, info) {
      if (err) {
        cb(err, null);
      } else {
        var response = {};
        response.message = 'event closed';
        var inputObject = {};
        inputObject['employerEventId'] = inputData.employerEventId;
        changeCandidateStatus(inputObject, 'Interview in progress', 'Rejected', function(candiErr, candidate) {
          if (candidate) {
            response.studentStatus = 'Interview in progress Students status updated as Rejected';
            var driveObj = {};
            driveObj['companyId'] = eventResponseData.companyId;
            driveObj['empEventId'] = inputData.employerEventId;
            // console.log('objjjjjjjjjjjjjjjjjjjjj ', driveObj);
            updateEmployerDriveCampuses(driveObj, 'Closed', function(driveErr, driveOut) {
              var campusEvent = server.models.CampusEvent;
              campusEvent.findOne({
                'where': {
                  'employerEventId': inputData.employerEventId,
                },
              }, function(campusErr, campusOut) {
                if (campusOut) {
                  // Students Status shortlisted to interview inprogress
                  var eventStudentList = server.models.EventStudentList;
                  eventStudentList.find({
                    'where': {
                      'and': [{
                          'companyId': eventResponseData.companyId,
                        }, {
                          'employerEventId': inputData.employerEventId,
                        },
                        {
                          'candidateStatusValueId': 378,
                        },
                      ],
                    },
                  }, function(empErr, empResp) {
                    // console.log('emprespppppppppppppppp ', empResp);
                    if (empErr) {
                      cb(empErr, null);
                    } else if (empResp.length > 0) {
                      //----remove duplicates
                      var studentArray = [];
                      for (var i = 0; i < empResp.length; i++) {
                        studentArray.push(empResp[i].studentId);
                      }
                      var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                        return index == self.indexOf(elem);
                      });
                      async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                        var pushInput = {
                          'userId': inputData.userId,
                          'role': 'RECDIR',
                          'companyId': inputData.companyId,
                          'employerPersonId': inputData.employerPersonId,
                          // 'empEventId': CampusEventData.empEventId,
                          'campusEventId': campusOut.campusEventId,
                          'notificationName': 12,
                          'studentList': studentList,
                        };
                        var notificationTemplate = server.models.NotificationMessageTemplates;
                        var pushNotification = notificationTemplate.pushNotification;
                        pushNotification(pushInput, function(pushErr, pushOut) {
                          if (pushErr) {
                            cb(pushErr, null);
                            studentArray = [];
                          } else {
                            response.Notification = 'Notificaton Sent';
                            cb(null, response);
                            studentArray = [];
                          }
                        });
                      });
                    } else {
                      cb(null, response);
                    }
                  });
                  //----------------------------//
                } else {
                  cb(null, response);
                }
              });
              // cb(null, response);
            });
          } else {
            cb(candiErr, null);
          }
        });
        // cb(null, response);
      }
    });
  });
}

function changeCandidateStatus(inputObject, existVal, changedVal, cb) {
  var eventStudentList = server.models.EventStudentList;
  eventStudentList.find({
    'where': {
      'and': [inputObject],
    },
  }, function(err, eventStudentArray) {
    if (err) {
      cb(err, null);
    } else {
      async.map(eventStudentArray, updateCandidateStatus, function(candiErr, candidate) {
        if (candiErr) {
          cb(candiErr, null);
        } else {
          var response = {};
          response.studentStatus = 'Shortlisted Students status updated as Interview in progress';
          cb(null, response);
        }
      });
    }
  });



  function updateCandidateStatus(obj, callBC) {
    lookup('CANDIDATE_STATUS_TYPE', function(err, response) {
      var candidateShortlisted = response.find(findShort);
      var candidateInprogress = response.find(findInprogress);

      function findShort(shortVal) {
        return shortVal.lookupValue === existVal;
      }

      function findInprogress(progressVal) {
        return progressVal.lookupValue === changedVal;
      }
      if (candidateShortlisted.lookupValueId == obj.candidateStatusValueId) {
        var object = {};
        object = obj;
        object.candidateStatusValueId = candidateInprogress.lookupValueId;
        obj.updateAttributes(object, function(err, resp) {
          if (err) {
            callBC(err, null);
          } else {
            callBC(null, resp);
          }
        });
      } else {
        callBC(null, null);
      }
    });
  }
}

function updateCampusEvent(obj, callBc) {
  lookup('EDUCATION_EVENT_STATUS_CODE', function(error, Campusresponse) {
    var eduStatusValueSchedule = Campusresponse.find(findCampusIndicatior);

    function findCampusIndicatior(eduVal) {
      return eduVal.lookupValue === 'In Progress';
    }
    var campusEvent = server.models.CampusEvent;
    campusEvent.findOne({
      'where': {
        'campusEventId': obj,
      },
    }, function(findErr, findResp) {
      var eventObj = {};
      eventObj = findResp;
      eventObj.eventStatusValueId = eduStatusValueSchedule.lookupValueId;
      findResp.updateAttributes(eventObj, function(updateErr, updated) {
        callBc(null, updated);
      });
    });
  });
}

function updateEmployerDriveCampuses(obj, changedVal, cb) {
  lookup('EMPLOYER_EVENT_STATUS_CODE', function(error, Campusresponse) {
    var eventStatusValueId = Campusresponse.find(findCampusIndicatior);

    function findCampusIndicatior(eduVal) {
      return eduVal.lookupValue == changedVal;
    }
    var employerDriveCampus = server.models.EmployerDriveCampuses;
    employerDriveCampus.findOne({
      'where': {
        'and': [obj],
      },
    }, function(err, driveCampuses) {
      var eventObj = {};
      eventObj = driveCampuses;
      eventObj.empDriveStatusValueId = eventStatusValueId.lookupValueId;
      eventObj.updateAttributes(eventObj, function(updateErr, update) {
        // console.log('--------------------------------------- ', eventObj, updateErr, update);
        if (update) {
          cb(null, update);
        } else {
          cb(updateErr, null);
        }
      });
    });
  });
}

function eventActionScheduled(eventResponseData, inputData, callBack) {
  var empObj = {};
  empObj = eventResponseData;
  empObj.eventStatusValueId = 319;
  eventResponseData.updateAttributes(empObj, function(empErr, empResp) {
    if (empResp) {
      callBack(null, empResp);
    } else {
      callBack(null, null);
    }
  });
}

function sendStudent(obj, callBc) {
  if (obj) {
    // campusArray.push({'campusId': obj.campusId});
    var objj = {};
    objj.studentId = obj;
    callBc(null, objj);
  } else {
    callBc('studentId is null', null);
  }
}

function updateEmpDrive(eventResponseData, cb) {
  var empDrive = server.models.EmployerDrive;
  empDrive.findOne({
    'where': {
      'empDriveId': eventResponseData.empDriveId,
    },
  }, function(err, resp) {
    var driveObj = {};
    driveObj = resp;
    driveObj.driveStatusValueId = 306; // changind emp drive status to inprogress
    resp.updateAttributes(driveObj, function(driveErr, driveOut) {
      cb(null, driveOut);
    });
  });
}

function updateStudentInfo(empEventId, companyId, cb) {
  var eventStudentList = server.models.EventStudentList;
  eventStudentList.find({
    'where': {
      'and': [{
        'companyId': companyId
      }, {
        'employerEventId': empEventId
      }, {
        'candidateStatusValueId': 376
      }]
    }
  }, function(eventErr, eventOut) {
    if (eventOut.length > 0) {
      async.map(eventOut, updateCandidate, function(candidateErr, candidateOut) {
        cb(null, candidateOut);
      });
    } else {
      cb(null, null);
    }
  });

  function updateCandidate(obj, callBack) {
    var eventStudentList = server.models.EventStudentList;
    eventStudentList.findOne({
      'where': {
        'and': [{
          'studentId': obj.studentId
        }, {
          'companyId': companyId
        }, {
          'employerEventId': empEventId
        }, {
          'candidateStatusValueId': 376
        }]
      }
    }, function(err, resp) {
      var screeningObj = {};
      screeningObj = resp;
      screeningObj.candidateStatusValueId = 378;
      resp.updateAttributes(screeningObj, function(updateErr, updateOut) {
        cb(null, updateOut);
      });
    });
  }
}



exports.onCampusActions = onCampusActions;
exports.changeCandidateStatus = changeCandidateStatus;
exports.updateEmployerDriveCampuses = updateEmployerDriveCampuses;

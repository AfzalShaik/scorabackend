/**
 * Created by Yogi on 09-08-2017.
 */
'use strict';
/**
   *persistAddressDataInForeignEntity-function deals with getting addressid from array of objects
   *@constructor
   * @param {object} lengthOfSavedAddress - contains the length of address and used to compare with the count
   * @param {object} savedAddressInfo - contains the address with array of objects
   * @param {object} foreignEntity - contains the model where to create
   * @param {object} inputObjToPersist - inputObjToPersist contains the object need to get created
   * @param {function} cb - deals with the response
   */
function persistAddressDataInForeignEntity(lengthOfSavedAddresses,
              savedAddressesInfo, foreignEntiy, inputObjToPersist, callBack) {
  var count = 0; // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- variable count doesnt indicate what its incrementing. Also use === and !==

  savedAddressesInfo.map(function(addressData) {
    console.log('this is data....', addressData);
    inputObjToPersist['addressId'] = addressData.addressId;
    inputObjToPersist['primaryInd'] = addressData.primaryInd;
    foreignEntiy.create(inputObjToPersist, function(error, savedEntityResp) {
      console.log('ressssssssssssssssssssss', savedEntityResp);
      if (error) {
        error.requestStatus = false;
        callBack(error, null);
      } else {
        console.log('address foreign entity record created.....');
        count++;
        if (lengthOfSavedAddresses == count) {
          savedAddressesInfo.requestStatus = true;
          console.log('ressssssssssssssssssssss', savedEntityResp);
          callBack(null, savedEntityResp);
        }
      }
    });
  });
}
exports.persistAddressDataInForeignEntity = persistAddressDataInForeignEntity;

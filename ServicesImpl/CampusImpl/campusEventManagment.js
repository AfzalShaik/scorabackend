'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var request = require('request');
var getDetailsById = require('../CommonImpl/getEntityDetails.js');
var loopbackValidation = require('../../commonValidation/lookupMethods.js');
var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
var employerEvent = server.models.EmployerEvent;
var lookup = server.models.LookupType;
var lookupvalue = server.models.LookupValue;
var eventCampus = server.models.CampusEvent;
var campusModel = server.models.CampusDrive;
var EmployerDriveCampuses = server.models.EmployerDriveCampuses;
var accpect = 'Accepted';
var async = require('async');
var notificationTemplate = server.models.NotificationMessageTemplates;
var pushNotification = notificationTemplate.pushNotification;
var updateEmployerDriveCampuses = require('../EmployerEventActionImpl/employer-event-oncampus-actions').updateEmployerDriveCampuses;
/**
 * campusEvent-this function deals with checking action
 * @constructor
 * @param {object} CampusEventData-contains companyid and employer event id
 * @param {function} cb-callback
 */
function campusBussinessEvent(CampusEventData, cb) {
  if (CampusEventData.Action == 'Accepted') {
    // if (CampusEventData.campusDriveId) {
    // if (CampusEventData.employerEventId) {
    companyAccpectEvent(CampusEventData, function(err, AcceptedResponse,
      code) {
      // console.log('33333333333333333333333333333333333 ', err, AcceptedResponse, code);
      if (AcceptedResponse) {
        var sucess = {};
        sucess.status = 'Sucessfully you accpected the event';
        sucess.campusEventId = null;
        sucess.statusCode = 200;
        cb(null, sucess);
      } else {
        throwError('event Not Created', cb);
      }
    });
    // } else {
    //   throwError('employerEventId Required', cb);
    // }
    // } else {
    //   throwError('campusDriveId Required', cb);
    // }
  } else if (CampusEventData.Action == 'Published') {
    if (CampusEventData.campusEventId) {
      CampusPublishEvent(CampusEventData, function(err, publishEvent) {
        if (err) {
          throwError('Error Occured', cb);
        } else {
          var publish = {};
          publish.status = 'sucess';
          cb(null, publish);
        }
      });
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Share Student List with Company') {
    if (CampusEventData.campusEventId) {
      ShareStudentList(CampusEventData, function(err, shortListResponse) {
        if (err) {
          throwError('cant update student Response', cb);
        } else {
          var status = {};
          status.status = 'sucessfully done';
          status.statusCode = 200;
          cb(null, status);
        }
      });
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Scheduled') {
    if (CampusEventData.campusEventId) {
      if (CampusEventData.scheduledDate) {
        if (CampusEventData.scheduledStartTime) {
          scheduledEvent(CampusEventData, function(err, sheduledRes) {
            // if (err) {
            //   throwError('event id you provided was not under required status', cb);
            // } else {
            var status = {};
            status.status = 'Sheduled Event Sucessully';
            cb(null, status);
            // }
          });
        } else {
          throwError('scheduledStartTime Required', cb);
        }
      } else {
        throwError('scheduledDate Required', cb);
      }
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Closed') {
    if (CampusEventData.campusEventId) {
      closeEvent(CampusEventData, function(err, closerResponse) {
        if (err) {
          throwError(err, cb);
        } else {
          var status = {};
          if (err) {
            throwError('THERE WAS AN ERROR', cb);
          } else {
            var status = {};
            status.status = 'Event Closed Sucessfully';
            cb(null, status);
          }
        }
      });
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Rejected') {
    actionRejected(CampusEventData, function(err, rejectResponse) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, rejectResponse);
      }
    });
  } else if (CampusEventData.Action == 'In Progress') {
    actionInProgress(CampusEventData, function(err, inProgressResponse) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, inProgressResponse);
      }
    });
  } else {
    throwError('Action Cant Be NULL', cb);
  }
}
/**
 * companyAccpectEvent- will work on creating am event when event was accpected
 * @constructor
 * @param {object} CampusEventData-contains Data
 * @param {function} AccpectCallback- Deals with response
 */
function companyAccpectEvent(CampusEventData, AccpectCallback) {
  // console.log('.......................... ', CampusEventData);
  var employerEvent = server.models.EmployerEvent;
  var ob = {};
  ob.empEventId = CampusEventData.employerEventId;
  getDetailsById.entityDetailsById(ob, employerEvent, function(err, Campusres) {
    if (err) {
      AccpectCallback(err, null);
    } else if (Campusres.data == 0) {
      AccpectCallback('Invalid employerEventId', null);
    } else {
      eventStatusManagment(CampusEventData, Campusres,
        function(err, eventAccpect, code) {
          // console.log('0000000000000000000000000000000000000000', err, eventAccpect, code);
          if (err) {
            throwError(err, AccpectCallback);
          } else {
            /**
             * //----------pushnotifications
             */
            var resp = {};
            resp.message = 'event Accepted';
            var notificationTemplate = server.models.NotificationMessageTemplates;
            var pushNotification = notificationTemplate.pushNotification;
            var employerDriveCampuses = server.models.EmployerDriveCampuses;
            employerDriveCampuses.find({
              'where': {
                'and': [{
                  'campusId': CampusEventData.campusId,
                }, {
                  'empEventId': CampusEventData.employerEventId,
                }],
              },
            }, function(empErr, empResp) {
              // console.log('------------------ ', empResp);
              if (empErr) {
                cb(empErr, null);
              } else {
                // resp.campusList = empResp;
                // update campus drive status to inprogress
                updateCampusDrive(CampusEventData, function(driveErr, driveOut) {
                  var companyArray = [];
                  for (var i = 0; i < empResp.length; i++) {
                    companyArray.push(empResp[i].companyId);
                  }
                  var uniqueEmpResp = companyArray.filter(function(elem, index, self) {
                    return index == self.indexOf(elem);
                  });
                  if (uniqueEmpResp.length > 0) {
                    async.map(uniqueEmpResp, getCompanyList, function(campusErr, campusResp) {
                      var pushInput = {
                        'userId': CampusEventData.userId,
                        'role': 'PLCDIR',
                        'campusId': CampusEventData.campusId,
                        'educationPersonId': CampusEventData.educationPersonId,
                        'empEventId': CampusEventData.employerEventId,
                        'notificationName': 3,
                        'companyList': campusResp,
                      };
                      pushNotification(pushInput, function(pushErr, pushOut) {
                        if (pushErr) {
                          cb(pushErr, null);
                        } else {
                          resp.Notification = 'Notificaton Sent';
                          AccpectCallback(null, resp);
                          companyArray = [];
                        }
                      });
                    });
                  } else {
                    AccpectCallback('No Records Found for given EmpEventId', null);
                  }
                });
              }
            });
            //------------pushnotification over
          }
        });
    }
  });
}
/**
 * eventStatusManagment-the function which was done after accpected company event
 * @constructor
 * @param {object} CampusEventData-contains all the data employer id and company id
 * @param {any} Campusres-contains data of employer
 */
function eventStatusManagment(CampusEventData, Campusres,
  eventAccpectCallback) {
  var lookupObject = {};
  var code = 'EMPLOYER_EVENT_STATUS_CODE';
  var accpect = 'Accepted';
  var publish = 314;
  typeCodeFunction(accpect, code, function(err, EducationEventres) {
    if (err) {
      throwError('error', eventAccpectCallback);
    } else {
      // console.log('.......................................== ', Campusres);
      if (publish == Campusres.data[0].eventStatusValueId) {
        var company = server.models.Company;
        var companyObject = {};
        companyObject.companyId = Campusres.data.companyId;
        getDetailsById.entityDetailsById(companyObject, company,
          function(err, response2) {
            if (err) {
              throwError('cant find', eventAccpectCallback);
            } else {
              var campus = {};
              campus.campusDriveId = CampusEventData.campusDriveId;
              getDetailsById.entityDetailsById(campus, campusModel,
                function(err, campusResponse) {
                  if (err) {
                    throwError('error', eventAccpectCallback);
                  } else if (campusResponse.data == 0) {
                    eventAccpectCallback('Invalid Campus Drive Id', null);
                  } else {
                    var object = {};
                    object = createObject(CampusEventData,
                      Campusres,
                      response2,
                      EducationEventres, campusResponse);
                    var lookupcodeEmployer = {};
                    var type = 'EMPLOYER_EVENT_STATUS_CODE';
                    var accpect = 'Accepted';
                    typeCodeFunction(accpect, type, function(err, re1) {
                      if (err) {
                        throwError('error', eventAccpectCallback);
                      } else {
                        var ob1 = {};
                        ob1.eventStatusValueId = re1[0].lookupValueId;
                        update(Campusres, object, ob1, CampusEventData,
                          function(err, updatedAccpect, code) {
                            if (err) {
                              throwError(err, eventAccpectCallback);
                            } else {
                              var driveObj = {};
                              driveObj['campusId'] = CampusEventData.campusId;
                              driveObj['empEventId'] = CampusEventData.employerEventId;
                              updateEmployerDriveCampuses(driveObj, 'Accepted', function(driveErr, driveOut) {
                                eventAccpectCallback(null, updatedAccpect, code);
                              });
                            }
                          }
                        );
                      }
                    });
                  }
                });
            }
          });
      } else {
        eventAccpectCallback('Cannot change the status to accept', null);
      }
    }
  });
};
/**
 * update-deals with updatind datain the database
 * @constructor
 * @param {object} res-employer event response
 * @param {object} object-coject contains all the data need to get updated
 * @param {object} ob1-contains lookupvalueId
 */
function update(res, object, ob1, CampusEventData, UpadatedResponse) {
  var id = res.data[0].empEventId;
  var employerDriveCampus = server.models.EmployerDriveCampuses;
  employerEvent.updateAll({
    'empEventId': id,
  }, ob1,
    function(err, updateResponse) {
      if (err) {
        throwError('error', UpadatedResponse);
      } else {
        // eventCampus.create(object, function(err, objectCreated) {
        //   if (err) {
        //     throwError('error', UpadatedResponse);
        //   } else {
        var sampleObject = {};
        sampleObject.empDriveStatusValueId = 243;
        employerDriveCampus.updateAll({
          'empEventId': CampusEventData.empEventId,
          'campsuId': CampusEventData.campusId,
        }, object,
          function(err, statusResponse) {
            var created = {};
            created.count = 1;
            UpadatedResponse(null, created, null);
          });
        //   } }
        // );
      }
    });
}
/**
 * CompanyPublishEvent-executes when ever event was set to public
 * @constructor
 * @param {object} CampusEventData-contains companyid
 */
function CampusPublishEvent(CampusEventData, CampuspublishCallback) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var publish = 'Published';
  var a = {};
  var initial = 'Initial';
  typeCodeFunction(publish, code, function(err, PublishEventresponse) {
    if (err) {
      throwError('error', CampuspublishCallback);
    } else {
      typeCodeFunction(initial, code, function(err, checkingResponse) {
        if (err) {
          throwError('error', CampuspublishCallback);
        } else {
          var companyObject = {};
          var eventCampus = server.models.CampusEvent;
          companyObject.eventStatusValueId = PublishEventresponse[0].lookupValueId;
          eventCampus.updateAll({
            'eventStatusValueId': checkingResponse[0].lookupValueId,
            'campusEventId': CampusEventData.campusEventId,
          },
            companyObject,
            function(err, publishResponse) {
              // console.log('3333333333333333333333333333333333 ', publishResponse);
              if (err) {
                CampuspublishCallback('error', publishResponse);
              } else {
                if (publishResponse.count == 0) {
                  CampuspublishCallback(null, publishResponse);
                } else {
                  //pushnotification
                  var resp = {};
                  //  resp.message = 'event Rejected';
                  var notificationTemplate = server.models.NotificationMessageTemplates;
                  var pushNotification = notificationTemplate.pushNotification;
                  var eventStudentList = server.models.EventStudentList;
                  eventStudentList.find({
                    'where': {
                      'and': [{
                        'campusId': CampusEventData.campusId,
                      }, {
                        'campusEventId': CampusEventData.campusEventId,
                      }, {
                        'campusPublishInd': 'Y',
                      }],
                    },
                  }, function(empErr, empResp) {
                    if (empErr) {
                      cb(empErr, null);
                    } else {
                      //----remove duplicates
                      var studentArray = [];
                      for (var i = 0; i < empResp.length; i++) {
                        studentArray.push(empResp[i].studentId);
                      }
                      var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                        return index == self.indexOf(elem);
                      });
                      async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                        var pushInput = {
                          'userId': CampusEventData.userId,
                          'role': 'PLCDIR',
                          'campusId': CampusEventData.campusId,
                          'educationPersonId': CampusEventData.educationPersonId,
                          // 'empEventId': CampusEventData.empEventId,
                          'campusEventId': CampusEventData.campusEventId,
                          'notificationName': 4,
                          'studentList': studentList,
                        };
                        pushNotification(pushInput, function(pushErr, pushOut) {
                          if (pushErr) {
                            cb(pushErr, null);
                            studentArray = [];
                          } else {
                            resp.Notification = 'Notificaton Sent';
                            CampuspublishCallback(null, resp);
                            studentArray = [];
                          }
                        });
                      });
                    }
                  });
                  //pushnotification over
                }
              }
            }
          );
        }
      });
    }
  });
}
//for Share Student List event
/**
 * ShareStudentList-executes when ever status was sharestudentList
 * @constructor
 * @param {object} CampusEventData-contains data with companyID
 */
function ShareStudentList(CampusEventData, studentshortListCallBack) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var share = 'Shared with Company';
  var publish = 'Published';
  var Studentscreening = 'Screening';
  var screeningCode = 'CANDIDATE_STATUS_TYPE';
  // var lookup = require('./lookupMethods').lookupMethod;
  // lookup('USER_STATUS_CODE', function(err, response) {
  //   var userStatusValueId = response.find(findStatus);

  //   function findStatus(statusVal) {
  //     return statusVal.lookupValue === 'Confirmed';
  //   } });
  typeCodeFunction(share, code, function(err, ShareStudentEventresponse) {
    // console.log('111111111111111111111111111111111111111 ', err, ShareStudentEventresponse);
    if (err) {
      studentshortListCallBack(err, null);
    } else {
      typeCodeFunction(publish, code, function(err, checkingResponse) {
        // console.log('222222222222222222222222222222222222222 ', err, checkingResponse);
        if (err) {
          throwError('error', CampuspublishCallback);
        } else {
          typeCodeFunction(Studentscreening, screeningCode, function(err, screeningResponse) {
            // console.log('3333333333333333333333333333333333333 ', err, screeningResponse);
            if (err) {
              throwError('error', CampuspublishCallback);
            } else {
              //   }
              // });
              var eventCampus = server.models.CampusEvent;
              var companyObject = {};
              companyObject.eventStatusValueId = 240;
              // console.log('----------------------- ', checkingResponse[0].lookupValueId, CampusEventData.campusEventId);
              eventCampus.updateAll({
                'eventStatusValueId': checkingResponse[0].lookupValueId,
                'campusEventId': CampusEventData.campusEventId,
              },
                companyObject,
                function(err, SharedStudentsResponse) {
                  // console.log('............................... ', err, SharedStudentsResponse);
                  if (err) {
                    studentshortListCallBack('error', SharedStudentsResponse);
                  } else {
                    if (SharedStudentsResponse.count == 0) {
                      studentshortListCallBack(null, SharedStudentsResponse);
                    } else {
                      var campusId = {};
                      campusId.campusEventId = CampusEventData.campusEventId;
                      getDetailsById.entityDetailsById(campusId, eventCampus,
                        function(err, eventRes) {
                          if (err) {
                            studentshortListCallBack('error', eventRes);
                          } else if (eventRes.data[0].employerEventId) {
                            var evenStudentlist = server.models.EventStudentList;
                            var EventStudentListObject = {};
                            EventStudentListObject.companyId = (eventRes.data[0].companyId) ? eventRes.data[0].companyId : null;
                            EventStudentListObject.employerEventId = (eventRes.data[0].employerEventId) ? eventRes.data[0].employerEventId : null;
                            EventStudentListObject.candidateStatusValueId = screeningResponse[0].lookupValueId;
                            // console.log(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,, ', EventStudentListObject);
                            evenStudentlist.updateAll({
                              'campusEventId': CampusEventData.campusEventId,
                            },
                              EventStudentListObject,
                              function(err, evenStudentlistResponse) {
                                // console.log('eventstudentlisttttttttttttttttt ', err, evenStudentlistResponse);
                                if (err) {
                                  studentshortListCallBack('error', evenStudentlistResponse);
                                } else {
                                  var code = 'EMPLOYER_EVENT_STATUS_CODE';
                                  var screening = 'Screening';
                                  typeCodeFunction(screening, code, function(err, employerstatus) {
                                    if (err) {
                                      studentshortListCallBack('error', employerstatus);
                                    } else {
                                      var statusId = {};
                                      statusId.eventStatusValueId = employerstatus[0].lookupValueId;
                                      employerEvent.updateAll({
                                        'empEventId': eventRes.data[0].employerEventId,
                                      },
                                        statusId,
                                        function(err, employerstatus) {
                                          // console.log('employerstatusssssssssssssssssssss ', err, employerstatus);
                                          if (err) {
                                            studentshortListCallBack('error', employerstatus);
                                          } else {
                                            //push notification
                                            var resp = {};
                                            var driveObj = {};
                                            driveObj['campusId'] = CampusEventData.campusId;
                                            driveObj['empEventId'] = eventRes.data[0].employerEventId;
                                            updateEmployerDriveCampuses(driveObj, 'Screening', function(driveErr, driveOut) {
                                              evenStudentlist.find({
                                                'where': {
                                                  'and': [{
                                                    'campusId': CampusEventData.campusId,
                                                  }, {
                                                    'campusEventId': CampusEventData.campusEventId,
                                                  }],
                                                },
                                              }, function(empErr, empResp) {
                                                if (empErr) {
                                                  cb(empErr, null);
                                                } else {
                                                  //----remove duplicates

                                                  var companyArray = [];
                                                  // Student Info
                                                  var StudentArray = [];
                                                  for (var i = 0; i < empResp.length; i++) {
                                                    StudentArray.push(empResp[i].studentId);
                                                  }
                                                  var uniqueEmpStudentResp = StudentArray.filter(function(elem, index, self) {
                                                    return index == self.indexOf(elem);
                                                  });
                                                  // Company Info
                                                  for (var i = 0; i < empResp.length; i++) {
                                                    companyArray.push(empResp[i].companyId);
                                                  }
                                                  var uniqueEmpResp = companyArray.filter(function(elem, index, self) {
                                                    return index == self.indexOf(elem);
                                                  });
                                                  // console.log('.................... ', uniqueEmpResp);
                                                  if (CampusEventData.empEventId && CampusEventData.campusEventId) {
                                                    if (uniqueEmpResp.length > 0) {
                                                      async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                                                        async.map(uniqueEmpResp, getCompanyList, function(campusErr, companyList) {
                                                          var pushInput = {
                                                            'userId': CampusEventData.userId,
                                                            'role': 'PLCDIR',
                                                            'campusId': CampusEventData.campusId,
                                                            'educationPersonId': CampusEventData.educationPersonId,
                                                            'empEventId': CampusEventData.empEventId,
                                                            'notificationName': 5,
                                                            'companyList': companyList,
                                                          };
                                                          // console.log('notificationInputtttttttttttttt ', pushInput);
                                                          pushNotification(pushInput, function(pushErr, pushOut) {
                                                            if (pushErr) {
                                                              cb(pushErr, null);
                                                            } else {
                                                              resp.Notification = 'Notificaton Sent';
                                                              notificationService(CampusEventData, studentList, 11, function(err, stResponse) {
                                                                companyArray = [];
                                                                StudentArray = [];
                                                                studentshortListCallBack(null, resp);
                                                              });
                                                            }
                                                          });
                                                        });
                                                      });
                                                    } else {
                                                      studentshortListCallBack('No Records Found', null);
                                                      companyArray = [];
                                                    }
                                                  } else {
                                                    companyArray = [];
                                                    studentshortListCallBack(null, resp);
                                                  }
                                                }
                                              });
                                            });

                                            //push notification
                                            //studentshortListCallBack(null, employerstatus);
                                          }
                                        });
                                    }
                                  });
                                }
                              });
                          } else {
                            var evenStudentlist = server.models.EventStudentList;
                            var EventStudentListObject = {};
                            EventStudentListObject.candidateStatusValueId = screeningResponse[0].lookupValueId;
                            // console.log(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,, ', EventStudentListObject);
                            evenStudentlist.updateAll({
                              'campusEventId': CampusEventData.campusEventId,
                            },
                              EventStudentListObject,
                              function(err, evenStudentlistResponse) {
                                // ------------- Notification For Student (Initial To Screening) --------
                                var eventStudentList = server.models.EventStudentList;
                                evenStudentlist.find({
                                  'where': {
                                    'and': [{
                                      'campusId': CampusEventData.campusId,
                                    }, {
                                      'campusEventId': CampusEventData.campusEventId,
                                    }],
                                  },
                                }, function(empErr, empResp) {
                                  if (empErr) {
                                    cb(empErr, null);
                                  } else if (empResp.length > 0) {
                                    //----remove duplicates

                                    var StudentArray = [];
                                    for (var i = 0; i < empResp.length; i++) {
                                      StudentArray.push(empResp[i].studentId);
                                    }
                                    var uniqueEmpResp = StudentArray.filter(function(elem, index, self) {
                                      return index == self.indexOf(elem);
                                    });
                                    // console.log('.................... ', uniqueEmpResp);
                                    if (uniqueEmpResp.length > 0) {
                                      // async.map(uniqueEmpResp, sendStudent, function(campusErr, studentList) {
                                      async.map(uniqueEmpResp, sendStudent, function(campusErr, studentList) {
                                        notificationService(CampusEventData, studentList, 11, function(notErr, notifyOut) {
                                          evenStudentlistResponse.notification = notifyOut;
                                          studentshortListCallBack(null, evenStudentlistResponse);
                                          StudentArray = [];
                                        });
                                      });
                                      // });
                                    } else {
                                      studentshortListCallBack('No Records Found', null);
                                      StudentArray = [];
                                    }
                                  } else {
                                    studentshortListCallBack(null, evenStudentlistResponse);
                                  }
                                });
                                // studentshortListCallBack(null, eventRes);
                              });
                          }
                        }
                      );
                    };
                  }
                }
              );
            }
          });
        }
      });
    }
  });
}
//Sheduled event
/**
 * scheduledEvent-for changing event status to shedule event
 * @constructor
 * @param {object} CampusEventData-contains data that need to get updated
 */
function scheduledEvent(CampusEventData, sheduledCallback) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var sheduled = 'Scheduled';
  var share = 'Students shortlisted';
  typeCodeFunction(sheduled, code, function(err, sheduledResponse) {
    if (err) {
      sheduledCallback('error', sheduledResponse);
    } else {
      typeCodeFunction(share, code, function(err, checkingResponse) {
        if (err) {
          throwError('error', sheduledCallback);
        } else {
          var sheduleCampus = {};
          sheduleCampus.campusEventId = CampusEventData.campusEventId;
          getDetailsById.entityDetailsById(sheduleCampus, eventCampus,
            function(err, sheduleRes) {
              if (err) {
                sheduledCallback('Invalid ID', sheduleRes);
              } else if (sheduleRes.data[0].employerEventId) {
                var sheduleEvent = {};
                sheduleEvent.eventStatusValueId = sheduledResponse[0].lookupValueId;
                sheduleEvent.scheduledDate = CampusEventData.scheduledDate;
                sheduleEvent.scheduledStartTime =
                  CampusEventData.scheduledStartTime;
                eventCampus.updateAll({
                  'eventStatusValueId': checkingResponse[0].lookupValueId,
                  'employerEventId': sheduleRes.data[0].employerEventId,
                },
                  sheduleEvent,
                  function(err, eventResponse) {
                    if (eventResponse.count == 0) {
                      sheduledCallback(null, eventResponse);
                    } else {
                      if (err) {
                        sheduledCallback('eror', eventResponse);
                      } else {
                        var code = 'EMPLOYER_EVENT_STATUS_CODE';
                        var Scheduled = 'Scheduled';
                        typeCodeFunction(Scheduled, code, function(err, employerstatus) {
                          if (err) {
                            sheduledCallback('error', employerstatus);
                          } else {
                            var statusId = {};
                            statusId.eventStatusValueId = employerstatus[0].lookupValueId;
                            statusId.scheduledDate = CampusEventData.scheduledDate;
                            statusId.scheduledStartTime = CampusEventData.scheduledStartTime;
                            employerEvent.updateAll({
                              'empEventId': sheduleRes.data[0].employerEventId,
                            },
                              statusId,
                              function(err, employerstatus) {
                                if (err) {
                                  sheduledCallback('error', employerstatus);
                                } else {
                                  var driveObj = {};
                                  driveObj['campusId'] = CampusEventData.campusId;
                                  driveObj['empEventId'] = sheduleRes.data[0].employerEventId;
                                  updateEmployerDriveCampuses(driveObj, 'Scheduled', function(driveErr, driveOut) {
                                    //............... notification......................
                                    var eventStudentList = server.models.EventStudentList;
                                    eventStudentList.find({
                                      'where': {
                                        'and': [{
                                          'campusId': CampusEventData.campusId,
                                        }, {
                                          'employerEventId': CampusEventData.empEventId,
                                        }],
                                      },
                                    }, function(empErr, empResp) {
                                      if (empErr) {
                                        cb(empErr, null);
                                      } else {
                                        //----remove duplicates
                                        var companyArray = [];
                                        var studentArray = [];
                                        for (var i = 0; i < empResp.length; i++) {
                                          companyArray.push(empResp[i].companyId);
                                        }
                                        for (var i = 0; i < empResp.length; i++) {
                                          studentArray.push(empResp[i].studentId);
                                        }
                                        var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                                          return index == self.indexOf(elem);
                                        });

                                        var uniqueEmpCompanyResp = companyArray.filter(function(elem, index, self) {
                                          return index == self.indexOf(elem);
                                        });
                                        if (CampusEventData.empEventId && CampusEventData.campusEventId) {
                                          if (uniqueEmpStudentResp.length > 0 && uniqueEmpCompanyResp.length > 0) {
                                            async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                                              async.map(uniqueEmpCompanyResp, getCompanyList, function(campusErr, companyList) {
                                                var pushInput = {
                                                  'userId': CampusEventData.userId,
                                                  'role': 'PLCDIR',
                                                  'campusId': CampusEventData.campusId,
                                                  'educationPersonId': CampusEventData.educationPersonId,
                                                  'empEventId': CampusEventData.empEventId,
                                                  'campusEventId': CampusEventData.campusEventId,
                                                  'notificationName': 7,
                                                  'companyList': companyList,
                                                  'studentList': studentList,
                                                };
                                                pushNotification(pushInput, function(pushErr, pushOut) {
                                                  if (pushErr) {
                                                    cb(pushErr, null);
                                                    studentArray = [];
                                                  } else {
                                                    var resp = {};
                                                    resp.Notification = 'Notificaton Sent';
                                                    sheduledCallback(null, resp);
                                                    studentArray = [];
                                                    companyArray = [];
                                                  }
                                                });
                                              });
                                            });
                                          } else {
                                            sheduledCallback('No Records Found', null);
                                            studentArray = [];
                                          }
                                        } else {
                                          sheduledCallback(null, resp);
                                          studentArray = [];
                                          companyArray = [];
                                        }
                                      }
                                    });
                                  });

                                  // sheduledCallback(null, employerstatus);
                                }
                              });
                          }
                        });
                      }
                    }
                  });
              } else {
                var sheduleEvent = {};
                sheduleEvent.eventStatusValueId = sheduledResponse[0].lookupValueId;
                sheduleEvent.scheduledDate = CampusEventData.scheduledDate;
                sheduleEvent.scheduledStartTime = CampusEventData.scheduledStartTime;
                eventCampus.updateAll({
                  'eventStatusValueId': checkingResponse[0].lookupValueId,
                  'campusEventId': CampusEventData.campusEventId,
                },
                  sheduleEvent,
                  function(err, eventResponse) {
                    // Students get notification of Schedule of a campus event
                    var eventStudentList = server.models.EventStudentList;
                    eventStudentList.find({
                      'where': {
                        'and': [{
                          'campusId': CampusEventData.campusId,
                        }, {
                          'campusEventId': CampusEventData.campusEventId,
                        }],
                      },
                    }, function(empErr, empResp) {
                      if (empErr) {
                        cb(empErr, null);
                      } else if (empResp.length > 0) {
                        //----remove duplicates
                        var studentArray = [];
                        for (var i = 0; i < empResp.length; i++) {
                          studentArray.push(empResp[i].studentId);
                        }
                        var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                          return index == self.indexOf(elem);
                        });

                        async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                          notificationService(CampusEventData, studentList, 8, function(notifyErr, notifyOut) {
                            eventResponse.notification = notifyOut;
                            sheduledCallback(null, eventResponse);
                            studentArray = [];
                          });
                          // var pushInput = {
                          //   'userId': CampusEventData.userId,
                          //   'role': 'PLCDIR',
                          //   'campusId': CampusEventData.campusId,
                          //   'educationPersonId': CampusEventData.educationPersonId,
                          //   // 'empEventId': stData.employerEventId,
                          //   'campusEventId': CampusEventData.campusEventId,
                          //   'notificationName': 8,
                          //   'studentList': studentList,
                          // };
                          // var notificationTemplate = server.models.NotificationMessageTemplates;
                          // var pushNotification = notificationTemplate.pushNotification;
                          // pushNotification(pushInput, function(pushErr, pushOut) {
                          //   // console.log(pushErr, pushOut);
                          //   if (pushErr) {
                          //     sheduledCallback(pushErr, null);
                          //   } else {
                          //     pushOut.Notification = 'Notificaton Sent';
                          //     sheduledCallback(null, pushOut);
                          //     studentArray = [];
                          //   }
                          // });
                        });
                      } else {
                        sheduledCallback(null, eventResponse);
                      }
                    });
                    // sheduledCallback(null, eventResponse);
                  });
              }
            }
          );
        }
      });
    }
  });
}
/**
 * closeEvent - function Deals with close the event
 * @constructor
 * @param {object} CampusEventData -contains Data id and action
 * @param {function} closedCallback -Deals with response
 */
function closeEvent(CampusEventData, closedCallback) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var Closed = 'Closed';
  var schedule = CampusEventData.scheduledDate;
  var scDate = new Date(schedule);
  var scTime = scDate.getTime();
  var current = new Date();
  var currentTime = current.getTime();
  // console.log(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ', scDate.getTime());
  // if (scTime < currentTime) {
  typeCodeFunction(Closed, code, function(err, closedResponse) {
    if (err) {
      closedCallback('error', closedResponse);
    } else {
      var campusObject = {};
      campusObject.eventStatusValueId = closedResponse[0].lookupValueId;
      eventCampus.updateAll({
        'campusEventId': CampusEventData.campusEventId,
      },
        campusObject,
        function(err, closeResponse) {
          if (err) {
            closedCallback('error', null);
          } else {
            var ob1 = {};
            ob1.candidateStatusValueId = 378;
            var eventStudentList = server.models.EventStudentList;
            eventStudentList.updateAll({
              'campusEventId': CampusEventData.campusEventId,
              'candidateStatusValueId': 380,
            }, ob1,
              function(err, updateResponse) {
                // Notification to the students who are In Progress to closed
                eventStudentList.find({
                  'where': {
                    'and': [{
                      'campusEventId': CampusEventData.campusEventId,
                    }, {
                      'candidateStatusValueId': 378,
                    }],
                  },
                }, function(eventErr, empResp) {
                  if (empResp.length > 0) {
                    var studentArray = [];
                    for (var i = 0; i < empResp.length; i++) {
                      studentArray.push(empResp[i].studentId);
                    }
                    var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                      return index == self.indexOf(elem);
                    });

                    async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                      notificationService(CampusEventData, studentList, 11, function(err, notifyOut) {
                        updateResponse.notification = notifyOut;
                        closedCallback(null, updateResponse);
                        studentArray = [];
                      });
                    });
                  } else {
                    closedCallback(null, updateResponse);
                  }
                });
                // closedCallback(null, closeResponse);
                // Notification to the students who are In Progress to closed
              });
          }
        });
    }
  });
  // } else {
  //   closedCallback('You Cannot Close The Event', null);
  // }
}
/**
 * createObject-for creating data with responses and inputs
 * @constructor
 * @param {object} CampusEventData-contains all the data inputs
 * @param {any} res-contains employer event response
 * @param {any} response2-contains event details
 * @param {any} resp-contains lookupvalueId
 * @returns an object which contains all the details
 */
function createObject(CampusEventData, res, response2, resp, drive) {
  var object1 = {};
  object1.campusId = drive.data.campusId;
  object1.campusDriveId = drive.data.campusDriveId;
  object1.employerEventId = CampusEventData.employerEventId;
  object1.companyId = res.data.companyId;
  object1.eventName = response2.data.name;
  object1.eventTypeValueId = res.data.eventTypeValueId;
  object1.eventStatusValueId = resp[0].lookupValueId;
  object1.duration = 2;
  object1.createDatetime = new Date();
  object1.updateDatetime = new Date();
  object1.createUserId = CampusEventData.createUserId;
  object1.updateUserId = CampusEventData.createUserId;
  return object1;
}
/**
 * typeCodeFunction-for checking lookupValue and lookupId
 * @constructor
 * @param {var} value-contains lookupValue like accpected,publish....
 * @param {any} type-contains lookupCode
 * @param {any} cb-callBack function deals with response
 */
function typeCodeFunction(value, type, lookupCallback) {
  // console.log('inputtttttttttttttttttttttttttt ', value, type);
  lookup.find({
    'where': {
      lookupCode: type,
    },
  }, function(err, re) {
    // console.log('lookipppppppppp ', err, re);
    if (err) {
      lookupCallback(err, null);
    } else {
      lookupvalue.find({
        'where': {
          lookupValue: value,
          lookupTypeId: re[0].lookupTypeId,
        },
      }, function(err, re1) {
        // console.log('outttttttttttttttttttttt ', re1, err);
        if (err) {
          lookupCallback('error', re1);
        } else {
          lookupCallback(null, re1);
        };
      });
    }
  });
}

//getStudent
var campusArray = [];

function getStudent(obj, callBc) {
  if (obj.campusId) {
    // campusArray.push({'campusId': obj.campusId});
    var objj = {};
    objj.studentId = obj.studentId;
    callBc(null, objj);
  } else {
    callBc('campusId is null', null);
  }
}

//sendStudent
var campusArray = [];

function sendStudent(obj, callBc) {
  if (obj) {
    // campusArray.push({'campusId': obj.campusId});
    var objj = {};
    objj.studentId = obj;
    callBc(null, objj);
  } else {
    callBc('studentId is null', null);
  }
}

//getCompany
var campusArray = [];

function getCompany(obj, callBc) {
  if (obj.companyId) {
    // campusArray.push({'campusId': obj.campusId});
    var objj = {};
    objj.companyId = obj.companyId;
    callBc(null, objj);
  } else {
    callBc('companyId is null', null);
  }
}

function getCompanyList(obj, cb) {
  if (obj) {
    var objj = {};
    objj.companyId = obj;
    cb(null, objj);
  } else {
    cb('companyId is null', null);
  }
}

// function getStudentList(obj, cb) {
//   if (obj) {
//     var objj = {};
//     objj.studentId = obj;
//     cb(null, objj);
//   } else {
//     cb('studentId is null', null);
//   }
// }

var sample = [];

function actionRejected(data, rejectedCallback) {
  var code = 'EMPLOYER_EVENT_STATUS_CODE';
  var value = 'Rejected';
  typeCodeFunction(value, code, function(err, lookupResponse) {
    var input = {};
    input.empEventId = data.employerEventId;
    // input.empDriveStatusValueId = 243;//lookupResponse[0].lookupValueId;
    EmployerDriveCampuses.find({
      'where': {
        'and': [input],
      },
    }, function(err, rejectEmpResponse) {
      if (err) {
        rejectedCallback(err, null);
      } else if (rejectEmpResponse == null) {
        throwError('no event with that id', rejectedCallback);
      } else {
        // var async = require('async');
        sample.push(lookupResponse[0].lookupValueId);
        sample.push(rejectEmpResponse.length);
        async.map(rejectEmpResponse, findAndUpdateStatus, function(err, finalReject) {
          if (err) {
            throwError('there was an error', rejectedCallback);
          } else {
            var resp = {};
            resp.message = 'event Rejected';
            // var notificationTemplate = server.models.NotificationMessageTemplates;
            //  var pushNotification = notificationTemplate.pushNotification;
            var employerDriveCampuses = server.models.EmployerDriveCampuses;
            employerDriveCampuses.find({
              'where': {
                'and': [{
                  'campusId': data.campusId,
                }, {
                  'empEventId': data.employerEventId,
                }],
              },
            }, function(empErr, empResp) {
              if (empErr) {
                cb(empErr, null);
              } else {
                // resp.campusList = empResp;
                var companyArray = [];
                for (var i = 0; i < empResp.length; i++) {
                  companyArray.push(empResp[i].companyId);
                }
                var uniqueEmpResp = companyArray.filter(function(elem, index, self) {
                  return index == self.indexOf(elem);
                });
                if (data.employerEventId) {
                  if (uniqueEmpResp.length > 0) {
                    async.map(uniqueEmpResp, getCompanyList, function(campusErr, campusResp) {
                      var pushInput = {
                        'userId': data.userId,
                        'role': 'PLCDIR',
                        'campusId': data.campusId,
                        'educationPersonId': data.employerPersonId,
                        'empEventId': data.employerEventId,
                        'notificationName': 2,
                        'companyList': campusResp,
                      };
                      pushNotification(pushInput, function(pushErr, pushOut) {
                        if (pushErr) {
                          cb(pushErr, null);
                        } else {
                          resp.Notification = 'Notificaton Sent';
                          rejectedCallback(null, resp);
                          companyArray = [];
                        }
                      });
                    });
                  } else {
                    rejectedCallback('No Records Found for given EmpEventId', null);
                  }
                } else {
                  rejectedCallback(null, resp);
                  companyArray = [];
                }
              }
            });
            //------------pushnotification over
          }
        });
      }
    });
  });

  var count = 0;

  function findAndUpdateStatus(obj, cb) {
    if (obj.empDriveStatusValueId == 314 || obj.empDriveStatusValueId == null) {
      count++;
      if (sample[1] === count) {
        var updateStatus = {};
        updateStatus.eventStatusValueId = sample[0];
        employerEvent.updateAll({
          'empEventId': obj.empEventId,
        }, updateStatus, function(err, updateRes) {
          if (err) {
            throwError('cant update the event status', cb);
          } else {
            cb(null, updateRes);
          }
        });
      }
    }
  }
}

function actionInProgress(CampusEventData, inpCB) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var empCode = 'EMPLOYER_EVENT_STATUS_CODE';
  var publish = 'In Progress';
  var a = {};
  var initial = 'Scheduled';
  var empStatus = 'In Progress';
  typeCodeFunction(publish, code, function(err, PublishEventresponse) {
    if (err) {
      throwError('error', inpCB);
    } else {
      typeCodeFunction(initial, code, function(err, checkingResponse) {
        if (err) {
          throwError('error', inpCB);
        } else {
          eventCampus.findOne({
            'where': {
              'campusEventId': CampusEventData.campusEventId,
            },
          }, function(error, resp) {
            if (error) {
              inpCB(error, null);
            } else {
              var companyObject = {};
              var eventCampus = server.models.CampusEvent;
              companyObject = resp;
              companyObject.eventStatusValueId = PublishEventresponse[0].lookupValueId;
              // console.log('............................ ', companyObject, checkingResponse[0].lookupValueId, CampusEventData.campusEventId);
              resp.updateAttributes(companyObject, function(err, publishResponse) {
                // console.log(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ', err, publishResponse);
                if (err) {
                  inpCB('error', publishResponse);
                } else if (resp.employerEventId == null) {
                  // console.log('insideeeeeeeeeeeeeeeee');
                  var eventStudentList = server.models.EventStudentList;
                  var ob1 = {};
                  ob1.candidateStatusValueId = 380;
                  eventStudentList.updateAll({
                    'campusEventId': CampusEventData.campusEventId,
                    'candidateStatusValueId': 377,
                  }, ob1,
                    function(err, updateResponse) {
                      // student status will be changed from Shortlisted to InterView Inprogress(Notification)
                      var eventStudentList = server.models.EventStudentList;
                      eventStudentList.find({
                        'where': {
                          'and': [{
                            'campusId': CampusEventData.campusId,
                          }, {
                            'campusEventId': CampusEventData.campusEventId,
                          },
                          {
                            'candidateStatusValueId': 380,
                          },
                          ],
                        },
                      }, function(empErr, empResp) {
                        // console.log('emprespppppppppppppppp ', empResp);
                        if (empErr) {
                          cb(empErr, null);
                        } else if (empResp.length > 0) {
                          //----remove duplicates
                          var studentArray = [];
                          for (var i = 0; i < empResp.length; i++) {
                            studentArray.push(empResp[i].studentId);
                          }
                          var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                            return index == self.indexOf(elem);
                          });

                          async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                            notificationService(CampusEventData, studentList, 11, function(err, notifyOut) {
                              updateResponse.notification = notifyOut;
                              inpCB(null, updateResponse);
                              studentArray = [];
                            });
                            // var pushInput = {
                            //   'userId': CampusEventData.userId,
                            //   'role': 'PLCDIR',
                            //   'campusId': CampusEventData.campusId,
                            //   'educationPersonId': CampusEventData.educationPersonId,
                            //   // 'empEventId': stData.employerEventId,
                            //   'campusEventId': CampusEventData.campusEventId,
                            //   'notificationName': 11,
                            //   'studentList': studentList,
                            // };
                            // // console.log(';;;;;;;;;;;;;;;;;;;;;;; ', pushInput);
                            // var notificationTemplate = server.models.NotificationMessageTemplates;
                            // var pushNotification = notificationTemplate.pushNotification;
                            // pushNotification(pushInput, function(pushErr, pushOut) {
                            //   // console.log(pushErr, pushOut);
                            //   if (pushErr) {
                            //     inpCB(pushErr, null);
                            //   } else {
                            //     pushOut.Notification = 'Notificaton Sent';
                            //     inpCB(null, pushOut);
                            //     studentArray = [];
                            //   }
                            // });
                          });
                        } else {
                          inpCB(null, updateResponse);
                        }
                      });

                      // inpCB(null, updateResponse);
                    });
                } else {
                  // console.log('outsideeeeeeeeeeeeeeee');
                  var empEvent = server.models.EmployerEvent;
                  empEvent.findOne({
                    'where': {
                      'empEventId': resp.employerEventId,
                    },
                  }, function(empErr, empResp) {
                    // console.log('.................................... ', CampusEventData, empResp);
                    if (empErr) {
                      inpCB('error', empErr);
                    } else {
                      var driveObj = {};
                      driveObj['campusId'] = CampusEventData.campusId;
                      driveObj['empEventId'] = resp.employerEventId;
                      updateEmployerDriveCampuses(driveObj, 'In Progress', function(driveErr, driveOut) {
                        typeCodeFunction(empStatus, empCode, function(err, empResponse) {
                          var empObj = {};
                          empObj = empResp;
                          empObj.eventStatusValueId = empResponse[0].lookupValueId;
                          // console.log('.........-----------------------............. ', empObj, empResponse[0].lookupValueId);
                          empResp.updateAttributes(empObj, function(err, updatedResponse) {
                            // console.log('eventresponseeeeeeeeeeeeeeeeeeee ', err, updatedResponse);
                            var response = {};
                            response.message = 'event status changed as In Progress';
                            var employerObj = {};
                            employerObj['employerEventId'] = resp.employerEventId;
                            var updateCandidate = require('../EmployerEventActionImpl/employer-event-oncampus-actions').changeCandidateStatus;
                            updateCandidate(employerObj, 'Shortlisted', 'Interview in progress', function(candiErr, candidate) {
                              if (candiErr) {
                                inpCB(null, null);
                              } else {
                                // Notification to the students who's status changed to Interview In Progress
                                var eventStudentList = server.models.EventStudentList;
                                eventStudentList.find({
                                  'where': {
                                    'and': [{
                                      'campusId': CampusEventData.campusId,
                                    }, {
                                      'campusEventId': CampusEventData.campusEventId,
                                    },
                                    {
                                      'candidateStatusValueId': 380,
                                    },
                                    ],
                                  },
                                }, function(empErr, empResp1) {
                                  // console.log('emprespppppppppppppppp ', empResp);
                                  if (empErr) {
                                    cb(empErr, null);
                                  } else if (empResp1.length > 0) {
                                    //----remove duplicates
                                    var studentArray = [];
                                    for (var i = 0; i < empResp1.length; i++) {
                                      studentArray.push(empResp1[i].studentId);
                                    }
                                    var uniqueEmpStudentResp = studentArray.filter(function(elem, index, self) {
                                      return index == self.indexOf(elem);
                                    });

                                    async.map(uniqueEmpStudentResp, sendStudent, function(campusErr, studentList) {
                                      notificationService(CampusEventData, studentList, 11, function(err, notifyOut) {
                                        response.notification = notifyOut;
                                        inpCB(null, response);
                                        studentArray = [];
                                      });
                                    });
                                  } else {
                                    inpCB(null, response);
                                  }
                                });
                                // inpCB(candiErr, null);
                              }
                            });
                          });
                        });
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function notificationService(CampusEventData, studentList, notificationName, notificationCB) {
  console.log('into the notification service');
  var pushInput = {
    'userId': CampusEventData.userId,
    'role': 'PLCDIR',
    'campusId': CampusEventData.campusId,
    'educationPersonId': CampusEventData.educationPersonId,
    // 'empEventId': stData.employerEventId,
    'campusEventId': CampusEventData.campusEventId,
    'notificationName': notificationName,
    'studentList': studentList,
  };
  var notificationTemplate = server.models.NotificationMessageTemplates;
  var pushNotification = notificationTemplate.pushNotification;
  pushNotification(pushInput, function(pushErr, pushOut) {
    // console.log(pushErr, pushOut);
    if (pushErr) {
      notificationCB(pushErr, null);
    } else {
      pushOut.Notification = 'Notificaton Sent';
      // StudentArray = [];
      // cb(null, pushOut);
      notificationCB(null, pushOut);
      // StudentArray = [];
    }
  });
}

function updateCampusDrive(CampusEventData, cb) {
  var campusEvent = server.models.CampusEvent;
  campusEvent.findOne({
    'where': {
      'and': [{
        'campusId': CampusEventData.campusId,
      }, {
        'empEventId': CampusEventData.employerEventId,
      }],
    },
  }, function(campusEventErr, campusOut) {
    var campusDrive = server.models.CampusDrive;
    campusDrive.findOne({
      'where': {
        'campusDriveId': campusOut.campusDriveId,
      },
    }, function(err, resp) {
      var driveObj = {};
      driveObj = resp;
      driveObj.driveStatusValueId = 233; // changind campus drive status to inprogress
      resp.updateAttributes(driveObj, function(driveErr, driveOut) {
        cb(null, driveOut);
      });
    });
  });
}
exports.campusBussinessEvent = campusBussinessEvent;
exports.typeCodeFunction = typeCodeFunction;

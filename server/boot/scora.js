'use strict';
module.exports = function(app) {
  /*
  Define by Vijay
  Please use this library for defining access to various models and util functions\
  Tips
    - Always make sure the path you have defined is correct
    - Make the name of variables meaningful.
    - Always segregate the paths acc to category (Define your own category in case missing)
  */

  //Add path for global defined module variable here
  var server = require('../../server/server');
  var logger = require('../../server/boot/lib/logger');

  //Add path for utility function here
  var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails').entityDetailsUsingIncludeFilter;

  //Add path for Error Handling functions here.
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;

  //Add path for models here
  var User = server.models.SCORAXCHANGE_USER;
  var CompensationPackage = server.models.CompensationPackage;
};

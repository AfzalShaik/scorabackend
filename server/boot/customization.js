/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client
/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client

// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Remove once Vijay's authorization comes into place.
'use strict';
module.exports = function(server) {

//region: Before remote method for authentication

  var remotes = server.remotes();
  
  remotes.before('**', function(ctx, next) {
    var url = ctx.req.originalUrl.toString();
    var method = ctx.req.method.toString();
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var createUserId = ctx.req.body.createUserId;
    var updateUserId = (ctx.req.body.updateUserId) ? ctx.req.updateUserId : createUserId;
    var scoraServices = server.models.ScoraServices;
    var scoraRolePrivileges = server.models.ScoraRolePrivileges;
    var searchShort = (url) ? url.indexOf('access_token') >= 0 : false;
    var headers = ctx.req.headers;
    var security = require('../config.json').security;
    // var verifyJWT = require('../../common/utils/authentication').verifyJWT;
    // var jwt = require('jsonwebtoken');
    // jwt.verify(ctx.req.token, function(err, decoded) {
    // });
    // var verifySession = require('../server').verifySession;
    // verifySession(headers, function(err, out) {
    //   next();
    // });
    var getInput = [];
    var arrayInput = [];
    var attachmentUrl = url.search('/api/attachments/');
    if (url.indexOf('/api/ScoraUsers') > -1) {
      console.log('aaaaaaaaaaaaaaaaaaaaaaaaa', url);
      loginService(method, url, headers, next);
    } else if (url.indexOf('/api/Attachments') > -1 || url.indexOf('/api/attachments') > -1 || url.indexOf('/api/Cities') > -1 || url.indexOf('/api/Currencies') > -1 || url.indexOf('/api/States') > -1 || url.indexOf('/api/Countries') > -1 || url.indexOf('/api/FieldLabels') > -1 || url.indexOf('/api/LookupValues') > -1 || url.indexOf('/api/PostalCodes') > -1 || url.indexOf('/api/Universities') > -1 || url.indexOf('/api/UserRoles/sendEmail') > -1) {
      next();
    } else if (security == false) {
      next();
      // });
    } else {
      // Check whether API call has JWT token or not.
      if (headers['x-scora-auth']) {
        verifyJWT(headers['x-scora-auth'], function(error, response) {
          if (response) {
            var serviceName = url;
            if (method == 'GET' || method == 'DELETE') {
              var serviceName = serviceName.split('?');
              serviceName = serviceName[0];
            }
            // Check whether service and method are registered in database or not
            scoraServices.findOne({
              'where': {
                'and': [{
                  'serviceName': serviceName,
                },
                {
                  'methodName': method,
                },
                ],
              },
            }, function(err, services) {
              // If service exist then we will find out service privilage
              if (services) {
                var serviceId = services.serviceId;
                var scoraServicePrivileges = server.models.ScoraServicePrivileges;
                scoraServicePrivileges.findOne({
                  'where': {
                    'serviceId': serviceId,
                  },
                }, function(privilegesErr, privileges) {
                  // Check whether we have privilage to access the Service based on accessLevel and Role
                  if (privileges) {
                    var accessLevel = privileges.accessLevel;
                    var userId = response.userId;
                    var roleCode = response.role;
                    var scoraRoleAccess = server.models.ScoraRoleAccess;
                    scoraRoleAccess.findOne({
                      'where': {
                        'and': [{
                          'roleCode': roleCode,
                        }, {
                          'accessLevel': accessLevel,
                        }],
                      },
                    }, function(roleAccessErr, roleAccess) {
                      // console.log(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,', roleAccess, serviceName);
                      if ((url.indexOf('/api/Campuses') > -1 &&  response.role == 'SYSADMIN') || (url.indexOf('/api/Companies') > -1 && response.role == 'SYSADMIN') || (url.indexOf('/api/EducationPeople') > -1 && response.role == 'SYSADMIN') || (url.indexOf('/api/EmployerPeople') > -1 && response.role == 'SYSADMIN')) {
                        next();
                      } else if (roleAccess) {
                        // If role and accesslevel are verified then check for attributes
                        checkAttributeLevel(response, ctx, accessLevel, serviceName, function(checkErr, checkResp) {
                          if (checkResp) {
                            var scoraRoleAccess = server.models.ScoraRoleAccess;
                            // var userDetails = scoraRoleAccess.getUserDetails;
                            var object = {};
                            object.userId = userId;
                            object.role = roleCode;
                            object.campusId = response.campusId;
                            object.companyId = response.companyId;
                            object.educationPersonId = response.educationpersonId;
                            object.employerPersonId = response.employerpersonId;
                            // userDetails(object, function(err, resp) {
                            next();
                            // });
                          } else {
                            unauthorizedRes('You are not Autherized to Perform the action', next);
                          }
                        });
                        // }
                      } else {
                        // You are not autherized to access this api
                        unauthorizedRes('You Dont Have a Access to Perform This Action', next);
                      }
                    });
                  } else {
                    // Authentication Failed
                    unauthorizedRes('Autherization Failed', next);
                  }
                  //
                });
              } else {
                unauthorizedRes('API Meta Data Is Not Defined', next);
              }
            });
          } else {
            unauthorizedRes('Autherization Failed', next);
          }
        });
      } else {
        unauthorizedRes('AUTHORIZATION_REQUIRED', next);
      }
    }
  });

//endregion: Before remote method for authentication

//region: Modify response before sending response to requested user/client

  remotes.after('**', function(ctx, next) {
    // console.log('helloooooooooooooooooooooooooo');
    if (ctx.req.method.toString() == 'POST') {
      ctx.result = {
        data: ctx.result,
        requestStatus: true,
      };
      next();
    } else {
      next();
    }
  });

  //endregion: Modify response before sending response to requested user/client

//region: error message if any unauthorized request comes to server
  var unauthorizedRes = function(error, next) {
    var adminAuthorizationError = new Error(error);
    adminAuthorizationError.statusCode = 401;
    // adminAuthorizationError.requestStatus = false;
    next(adminAuthorizationError, null);
  };

  //endregion: error message if any unauthorized request comes to server

//region: loginService

  var loginService = function(method, url, headers, next) {
    console.log('...............................', url, headers);
    if (url.indexOf('/api/ScoraUsers/login') > -1 || url.indexOf('/api/ScoraUsers/reset') > -1 || url.indexOf('/api/ScoraUsers/change-password') > -1 || url.indexOf('/api/ScoraUsers/reset-password') > -1) {
      // console.log('000000000000000000000000000000000000000000 ', url.indexOf('/api/ScoraUsers/change-password'));
      next();
    } else if (method == 'GET') {
      next();
    } else if (headers['x-scora-auth']) {
      verifyJWT(headers['x-scora-auth'], function(error, response) {
        if (response) {
          next();
        } else {
          console.log('hiiiiiiiiiiiiiiiiiiii');
          unauthorizedRes('You Are Not Autherized', next);
        }
      });
    } else {
      console.log('hellooooooooooooooooo');
      unauthorizedRes('You Are Not Autherized', next);
    }
  };

//endregion: loginService

//region: Check Attribute Level

  function checkAttributeLevel(headers, ctx, accessLevel, serviceName, checkCB) {
    if (headers.role === 'PLCDIR' && headers.campusId) {
      var educationPerson = server.models.EducationPerson;
      educationPerson.findOne({
        'where': {
          // 'and': [
          // {
          'id': headers.userId,
          // }, {
          // 'campusId': headers.campusId,
          // }],
        },
      }, function(campusErr, campusOut) {
        if (campusOut) {
          if (campusOut.campusId == headers.campusId) {
            var method = ctx.req.method.toString();
            var url = ctx.req.originalUrl.toString();
            var objInput = {};
            objInput = ctx.req.body;
            var campusObject = {};
            // campusObject['roleCode'] = roleCode;
            // campusObject['userId'] = userId;
            campusObject['campusId'] = campusOut.campusId;
            campusObject['educationPersonId'] = campusOut.educationPersonId;
            campusObject['method'] = method;
            campusObject['objInput'] = objInput;
            campusObject['url'] = url;
            if (method === 'GET' || method === 'DELETE') {
              campusObject['objInput'] = ctx.req.query;
            }
            // console.log('objInputttttttttt ', campusObject);
            if (serviceName == '/api/CompanySearchVws/searchCompany') {
              checkCB(null, campusOut);
            } else {
              campusAttributeLevel(campusObject, accessLevel, function(campusError, campusOutput) {
                if (campusOutput) {
                  checkCB(null, campusOut);
                } else {
                  checkCB('You are not autherized to access the data', null);
                }
              });
            }
          } else {
            checkCB('You are not autherized to access the data', null);
          }
        } else {
          checkCB('You are not autherized to access the data', null);
        }
      });
    } else if (headers.role == 'RECDIR' && headers.companyId) {
      var employerPerson = server.models.EmployerPerson;
      employerPerson.findOne({
        'where': {
          'and': [{
            'id': headers.userId,
          }],
        },
      }, function(companyErr, companyOut) {
        // console.log('-0000000000000000000000 ', companyOut, headers);
        if (companyOut) {
          // To verify whether the same company is requesting for its own resource
          if (companyOut.companyId == headers.companyId) {
            var method = ctx.req.method.toString();
            var url = ctx.req.originalUrl.toString();
            var objInput = {};
            objInput = ctx.req.body;
            var companyObject = {};
            companyObject['companyId'] = companyOut.companyId;
            companyObject['employerPersonId'] = companyOut.employerPersonId;
            // companyObject['roleCode'] = roleCode;
            // companyObject['userId'] = userId;
            companyObject['method'] = method;
            companyObject['objInput'] = objInput;
            companyObject['url'] = url;
            if (method === 'GET' || method === 'DELETE') {
              companyObject['objInput'] = ctx.req.query;
            }
            if (serviceName == '/api/CampusSearchVws/searchCampus') {
              checkCB(null, companyOut);
            } else {
              companyAttributeLevel(companyObject, accessLevel, function(companyError, companyOutput) {
                if (companyOutput) {
                  checkCB(null, companyOut);
                } else {
                  checkCB('You are not autherized to access the data', null);
                }
              });
            }
            // });
          } else {
            checkCB('You are not autherized to access the data', null);
          }
        } else {
          checkCB('Record Not Found', null);
        }
      });
    } else if (headers.role == 'STUDENT' && headers.studentId) {
      // console.log('headerssssssssssssssssssss ', headers);
      var student = server.models.Student;
      student.findOne({
        'where': {
          'and': [{
            'id': headers.userId,
          }],
        },
      }, function(studentErr, studentOut) {
        if (studentOut) {
          if (studentOut.studentId == headers.studentId) {
            var method = ctx.req.method.toString();
            var url = ctx.req.originalUrl.toString();
            var objInput = {};
            objInput = ctx.req.body;
            if (method == 'GET' || method == 'DELETE') {
              objInput =  ctx.req.query;
            }
            // console.log('odyyyyyyyyyyyyyyyyyy ', objInput);
            var studentObject = {};
            studentObject['studentId'] = studentOut.studentId;
            studentObject['method'] = method;
            studentObject['objInput'] = objInput;
            studentObject['url'] = url;
            if (method == 'GET') {
              studentObject['objInput'] = ctx.req.query;
            }
            // if (accessLevel != 7) {
            studentAttributeLevel(studentObject, accessLevel, function(studentError, studentOutput) {
              if (studentOutput) {
                checkCB(null, studentOut);
              } else {
                checkCB('You are not autherized to access the data', null);
              }
            });
            // } else {
            //   checkCB(null, studentOut);
            // }
          } else {
            checkCB('You are not autherized to access the data', null);
          }
          // checkCB(null, studentOut);
        } else {
          checkCB('Record Not Found', null);
        }
      });
    } else {
      checkCB('AUTHORIZATION_REQUIRED', null);
    }
  }

//endregion: Check Attribute Level

//region: campusAttributeLevel

  function campusAttributeLevel(campusObject, accessLevel, campusCB) {
    // console.log('0000000000000000000000000000 ', ctx.req.method.toString());
    if (campusObject.method == 'POST' || campusObject.method == 'PUT' || campusObject.method == 'DELETE' || campusObject.method == 'GET') {
      var objInput = {};
      objInput = campusObject.objInput;
      // console.log('5555555555555555555555555555555 ', objInput, campusObject);
      // if (objInput.constructor === Array) {
      //   campusCB(null, campusObject);
      // } else {
      if (objInput.campusId) {
        // console.log('444444444444444444444444444 ');
        if (objInput.campusId == campusObject.campusId) {
          campusCB(null, campusObject);
        } else {
          // console.log('3333333333333333333333333 ');
          campusCB(null, null);
        }
      } else if (objInput.educationPersonId) {
        if (objInput.educationPersonId == campusObject.educationPersonId) {
          campusCB(null, campusObject);
        } else {
          campusCB(null, null);
        }
      } else if (objInput.campusEventId) {
        var campusEvent = server.models.CampusEvent;
        campusEvent.findOne({'where': {'campusEventId': objInput.campusEventId}}, function(campusEventErr, campusEventOut) {
          if (campusEventOut) {
            if (campusEventOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (objInput.enrollmentId) {
        var enrollment = server.models.Enrollment;
        enrollment.findOne({'where': {'enrollmentId': objInput.enrollmentId}}, function(enrollmentErr, enrollmentOut) {
          if (enrollmentOut) {
            if (enrollmentOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (objInput.programId) {
        var program = server.models.Program;
        program.findOne({'where': {'programId': objInput.programId}}, function(programErr, programOut) {
          if (programOut) {
            if (programOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (objInput.departmentId) {
        var department = server.models.Department;
        department.findOne({'where': {'departmentId': objInput.departmentId}}, function(departmentErr, departmentOut) {
          if (departmentOut) {
            if (departmentOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (objInput.campusDriveId) {
        var campusDrive = server.models.CampusDrive;
        campusDrive.findOne({'where': {'campusDriveId': objInput.campusDriveId}}, function(campusDriveErr, campusDriveOut) {
          if (campusDriveOut) {
            if (campusDriveOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (objInput.assessmentId) {
        var assessment = server.models.Assessment;
        assessment.findOne({'where': {'assessmentId': objInput.assessmentId}}, function(assessErr, assessOut) {
          if (assessOut) {
            if (assessOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (accessLevel == 4 || accessLevel == 5 || accessLevel == 6 || accessLevel == 7) {
        validateAccessFromCampus(campusObject, function(validateErr, validateOut) {
          if (validateOut) {
            campusCB(null, campusObject);
          } else {
            campusCB(null, null);
          }
        });
      } else {
        campusCB(null, null);
      }
      // }
    } else {
      campusCB(null, null);
    }
  }

  //endregion: campusAttributeLevel

//region: Company Attribute Level

  function companyAttributeLevel(companyObject, accessLevel, companyCB) {
    if (companyObject.method == 'POST' || companyObject.method == 'PUT' || companyObject.method == 'DELETE' || companyObject.method == 'GET') {
      var objInput = {};
      objInput = companyObject.objInput;
      // if (objInput.constructor === Array) {
      //   companyCB(null, companyObject);
      // } else {
      if (objInput.companyId) {
        if (objInput.companyId == companyObject.companyId) {
          companyCB(null, companyObject);
        } else {
          companyCB(null, null);
        }
      } else if (objInput.employerPersonId) {
        if (objInput.employerPersonId == companyObject.employerPersonId) {
          companyCB(null, companyObject);
        } else {
          companyCB(null, null);
        }
      } else if (objInput.eventStudentListId) {
        var eventStudentList = server.models.EventStudentList;
        eventStudentList.findOne({'where': {'studentListId': objInput.eventStudentListId}}, function(eventErr, eventOut) {
          if (eventOut) {
            if (eventOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.eventTestScoreId) {
        var eventTestScore = server.models.EventTestScore;
        eventTestScore.findOne({'where': {'eventTestScoreId': objInput.eventTestScoreId}}, function(scoreErr, scoreOut) {
          if (scoreOut) {
            if (scoreOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.listCompPkgId) {
        var employerCampusListCompPkg = server.models.EmployerCampusListCompPkg;
        employerCampusListCompPkg.findOne({'where': {'listCompPkgId': objInput.listCompPkgId}}, function(listErr, listOut) {
          if (listOut) {
            if (listOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.listId) {
        var employerCampusListHdr = server.models.EmployerCampusListHdr;
        employerCampusListHdr.findOne({'where': {'listId': objInput.listId}}, function(listErr, listOut) {
          if (listOut) {
            if (listOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.empDriveId) {
        var employerDrive = server.models.EmployerDrive;
        employerDrive.findOne({'where': {'empDriveId': objInput.empDriveId}}, function(driveErr, driveOut) {
          if (driveOut) {
            if (driveOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.listCampusId) {
        var employerCampusListDtl = server.models.EmployerCampusListDtl;
        employerCampusListDtl.findOne({'where': {'listCampusId': objInput.listCampusId}}, function(listErr, listOut) {
          if (listOut) {
            if (listOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.compPackageItemId) {
        var compensationPackageItem = server.models.CompensationPackageItem;
        compensationPackageItem.findOne({'where': {'compPackageItemId': objInput.compPackageItemId}}, function(itemErr, itemOut) {
          if (itemOut) {
            if (itemOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.compPackageId) {
        var compensationPackage = server.models.CompensationPackage;
        compensationPackage.findOne({'where': {'compPackageId': objInput.compPackageId}}, function(compErr, compOut) {
          if (compOut) {
            if (compOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.jobRoleId) {
        var jobRole = server.models.JobRole;
        jobRole.findOne({'where': {'jobRoleId': objInput.jobRoleId}}, function(jobRoleErr, jobRoleOut) {
          if (jobRoleOut) {
            if (jobRoleOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.organizationId) {
        var organization = server.models.Organization;
        organization.findOne({'where': {'organizationId': objInput.organizationId}}, function(orgErr, orgOut) {
          if (orgOut) {
            if (orgOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (objInput.empEventId || objInput.employerEventId) {
        var empEventId = (objInput.empEventId) ? objInput.empEventId : objInput.employerEventId;
        var employerEvent = server.models.EmployerEvent;
        employerEvent.findOne({'where': {'empEventId': empEventId}}, function(eventErr, eventOut) {
          if (eventOut) {
            if (eventOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (accessLevel == 4 || accessLevel == 5 || accessLevel == 6 || accessLevel == 7) {
        validateAccessFromCompany(companyObject, function(validateErr, validateOut) {
          if (validateOut) {
            companyCB(null, companyObject);
          } else {
            companyCB(null, null);
          }
        });
      } else {
        companyCB(null, null);
      }
      // }
    } else {
      companyCB(null, null);
    }
  }

  //endregion: Company Attribute Level

//region: studentAttributeLevel

function studentAttributeLevel(studentObject, accessLevel, studentCB) {
  if (studentObject.method == 'POST' || studentObject.method == 'PUT' || studentObject.method == 'DELETE' || studentObject.method == 'GET') {
    var objInput = {};
    objInput = studentObject.objInput;
    // console.log('objjjjjjjjjjjjjjjjjjjjjjjjjjj ', objInput, studentObject);
    if (objInput.constructor === Array) {
      studentCB(null, studentObject);
    } else {
      if (objInput.studentId) {
        if (objInput.studentId == studentObject.studentId) {
          studentCB(null, studentObject);
        } else {
          studentCB(null, null);
        }
      } else if (objInput.enrollmentId) {
        var enrollment = server.models.Enrollment;
        enrollment.findOne({'where': {'and': [{'enrollmentId': objInput.enrollmentId}, {'studentId': studentObject.studentId}]}}, function(enrollmentErr, enrollmentOut) {
          if (enrollmentOut) {
            // if (enrollmentOut.studentId == studentObject.studentId) {
            studentCB(null, studentObject);
            // } else {
            // studentCB(null, null);
            // }
          } else {
            studentCB(null, null);
          }
        });
      } else if (objInput.assessmentId) {
        var assessment = server.models.Assessment;
        assessment.findOne({'where': {'and': [{'assessmentId': objInput.assessmentId}, {'studentId': studentObject.studentId}]}}, function(assessErr, assessOut) {
          if (assessOut) {
            // if (assessOut.studentId == studentObject.studentId) {
            studentCB(null, studentObject);
            // } else {
            // studentCB(null, null);
            // }
          } else {
            studentCB(null, null);
          }
        });
      } else {
        studentCB(null, null);
      }
    }
  } else {
    studentCB(null, null);
  }
}


//endregion: studentAttributeLevel

//region: Verify JWT

function verifyJWT(token, verifyCB) {
  // function verifyJWT(token, verifyCB) {
  var config = require('../config.json');
  var loopback = require('loopback');
  var app = module.exports = loopback();
  app.set('notASecretKey', config.secret);
  var jwt = require('jsonwebtoken');
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, app.get('notASecretKey'), function(err, decoded) {
      // console.log('decodeddddddddddddddddddd ', decoded);
      if (err) {
        var error = new Error('JWT Token Expired.');
        error.statusCode = 401;
        error.success = false;
        error.err = err;
        verifyCB(error, null);
      } else {
        // if everything is good, save to request for use in other routes
        verifyCB(null, decoded);
      }
    });
  } else {
    // if there is no token
    // return an error
    var error = new Error('No token provided.');
    error.success = false;
    verifyCB(error, null);
  }
};

//endregion: Verify JWT

//region: Validate Access From Campus

function validateAccessFromCampus(campusObject, validateCb) {
  var objInput = {};
  objInput = campusObject.objInput;
  if (objInput.studentId) {
    var enrollment = server.models.Enrollment;
    enrollment.find({'where': {'and': [{'studentId': objInput.studentId}, {'campusId': campusObject.campusId}]}}, function(enrollErr, enrollOut) {
      if (enrollOut) {
        // var studentIndicator = enrollOut.find(findStudent);
        // function findStudent(studentVal) {
        //   return studentVal.campusId == campusObject.campusId;
        // }
        // if (studentIndicator) {
        validateCb(null, campusObject);
        // } else {
        //   validateCb(null, null);
        // }
      } else {
        validateCb(null, null);
      }
    });
  } else if (objInput.companyId) {
    var campusEvent = server.models.CampusEvent;
    campusEvent.find({'where': {'and': [{'companyId': objInput.companyId}, {'campusId': campusObject.campusId}]}}, function(companyErr, companyOut) {
      if (companyOut) {
        // var campusEventIndicator = companyOut.find(findCompany);
        // function findCompany(companyVal) {
        //   return companyVal.campusId == campusObject.campusId;
        // }
        // if (campusEventIndicator) {
        validateCb(null, campusObject);
        // } else {
        //   validateCb(null, null);
        // }
      } else {
        validateCb(null, null);
      }
    });
  } else if (objInput.empEventId || objInput.employerEventId) {
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    var empEventId = (objInput.empEventId) ? objInput.empEventId : objInput.employerEventId;
    employerDriveCampuses.find({'where': {'and': [{'empEventId': empEventId}, {'campusId': campusObject.campusId}]}}, function(driveErr, driveOut) {
      if (driveOut) {
        // var empEventIndicator = driveOut.find(findEvent);
        // function findEvent(Val) {
        //   return val.campusId == campusObject.campusId;
        // }
        // if (empEventIndicator) {
        validateCb(null, campusObject);
        // } else {
        //   validateCb(null, null);
        // }
      } else {
        validateCb(null, null);
      }
    });
  } else if (objInput.compPackageId) {
    var eventStudentList = server.models.EventStudentList;
    eventStudentList.find({'where': {'and': [{'compPackageId': objInput.compPackageId}, {'campusId': campusObject.campusId}]}}, function(pkgErr, pkgOut) {
      if (pkgOut) {
        var pkgIndicator = pkgOut.find(findPkg);
        // function findPkg(Val) {
        //   return val.campusId == campusObject.campusId;
        // }
        // if (pkgIndicator) {
        validateCb(null, campusObject);
        // } else {
        //   validateCb(null, null);
        // }
      } else {
        validateCb(null, null);
      }
    });
  } else if (objInput.studentListId) {
    var eventStudentList = server.models.EventStudentList;
    eventStudentList.findOne({'where': {'and': [{'studentListId': objInput.studentListId}, {'campusId': campusObject.campusId}]}}, function(listErr, listOut) {
      if (listOut) {
        // var listIndicator = listOut.find(findList);
        // function findList(Val) {
        //   return val.campusId == campusObject.campusId;
        // }
        // if (listIndicator) {
        validateCb(null, campusObject);
        // } else {
        //   validateCb(null, null);
        // }
      } else {
        validateCb(null, null);
      }
    });
  } else {
    validateCb(null, null);
  }
}

//endregion: Validate Access From Campus

//region: Validate Access From Company

function validateAccessFromCompany(companyObject, compCB) {
  var objInput = {};
  objInput = companyObject.objInput;
  if (objInput.campusId) {
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    employerDriveCampuses.find({'where': {'and': [{'campusId': objInput.campusId}, {'companyId': companyObject.companyId}]}}, function(campusErr, campusOut) {
      if (campusOut) {
        // var campusIndicator = campusOut.find(findCampus);
        // function findCampus(Val) {
        //   return val.campusId == campusObject.campusId;
        // }
        // if (campusIndicator) {
        compCB(null, companyObject);
        // } else {
        //   compCB(null, null);
        // }
      } else {
        compCB(null, null);
      }
    });
  } else if (objInput.campusEventId) {
    var campusEvent = server.models.CampusEvent;
    campusEvent.findOne({'where': {'and': [{'campusEventId': objInput.campusEventId}, {'companyId': companyObject.companyId}]}}, function(campusErr, campusOut) {
      if (campusOut) {
        // if (campusOut.companyId == companyObject.companyId) {
        compCB(null, companyObject);
        // } else {
        //   compCB(null, null);
        // }
      } else {
        compCB(null, null);
      }
    });
  } else if (objInput.studentId) {
    var eventStudentList = server.models.EventStudentList;
    eventStudentList.find({'where': {'and': [{'studentId': objInput.studentId}, {'companyId': companyObject.companyId}]}}, function(campusErr, campusOut) {
      if (campusOut) {
        // var campusIndicator = campusOut.find(findCampus);
        // function findCampus(Val) {
        //   return val.companyId == campusObject.companyId;
        // }
        // if (campusIndicator) {
        compCB(null, companyObject);
        // } else {
        //   compCB(null, null);
        // }
      } else {
        compCB(null, null);
      }
    });
  } else if (objInput.departmentId) {
    var eventStudentList = server.models.EventStudentList;
    eventStudentList.find({'where': {'and': [{'departmentId': objInput.departmentId}, {'companyId': companyObject.companyId}]}}, function(campusErr, campusOut) {
      if (campusOut) {
        // var campusIndicator = campusOut.find(findCampus);
        // function findCampus(Val) {
        //   return val.companyId == campusObject.companyId;
        // }
        // if (campusIndicator) {
        compCB(null, companyObject);
        // } else {
        //   compCB(null, null);
        // }
      } else {
        compCB(null, null);
      }
    });
  } else {
    compCB(null, null);
  }
}

//endregion: Validate Access From Company
  


};

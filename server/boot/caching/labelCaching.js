'use strict';

var server = require('./../../server');
var nodeCache = require('node-cache');

//Definition of System Tables
var fieldLabel = server.models.FieldLabel;
var currencyModel = server.models.Currency;
var lookupType = server.models.LookupType;
var lookupValue = server.models.LookupValue;

//Definition of utility functions
var findEntityDetailsAsIs = require('./utils').findEntityDetailsAsIs;
var createCache = require('./utils').createCache;
var setCache = require('./utils').setCache;
var getCache = require('./utils').getCache;
var printEntityDetails = require('./utils').printEntitydetails;
var lookupArray = [];
var modelCache = new nodeCache();
/**
 *The function will format the Label Table as needed in Cache.
 *The function accept an error function, a json object for Label Model and a callback function
 * @param {function} error
 * @param {Object} entitydetails
 * @param {function} callback
 */
function formatLabelTable(callback, error, modelname, entitydetails) {
  var formattedObject = {};
  entitydetails.map(function (obj) {
    formattedObject[obj.fieldName] = obj.label;
  });
  callback(null, modelname, formattedObject);
}

const key = 'labelKey';

var lookUp = {};
function data() {
  // console.log(1);
  var lookupType = server.models.LookupType;
  lookupType.find({}, function(err, res) {
    var async = require('async');
    async.map(res, getAll, function(err1, res1) {
      // console.log('look', lookUp);
      modelCache.set('lookUpCache', lookUp, function(errr, ress) {
        return lookUp;
      });
    });
  });
};
function getAll(obj, cb1) {
  var lookupValue = server.models.LookupValue;
  lookupValue.find({'where': {'lookupTypeId': obj.lookupTypeId}},
  function(err2, res2) {
    var async = require('async');
    async.map(res2, formatData, function(err3, res3) {
      cb1(null, res3);
    });
  });
  var data = {};
  function formatData(object, cb2) {
    data[object.lookupValueId] = object;
    lookUp[obj.lookupCode] = data;
    cb2(null, 'done');
  }
}
function getCacheLookUp(){
  // console.log(2);
  var data = modelCache.get('lookUpCache');
  // console.log('dd', data);
  return data;
}
/**
 * 
 * 
 * @param {string} modelname 
 * @param {string} key 
 * @param {function} callback 
 */
function apiLabelset(modelname, key, callback) {
  //var setCacheCB = setCache.bind(null, cacheName, key, entityDetails);
  //var createCacheCB = createCache.bind(null, modelname, entityDetails, setCacheCB);
  //var formatLabelTableCB = formatLabelTable.bind(null, entitydetails, createCacheCB);
  findEntityDetailsAsIs(modelname,
    formatLabelTable.bind(null,
      createCache.bind(null,
        setCache.bind(null,
          getCache.bind(null, callback, key), key), key)));
}


// Lookup 
function getLookUpData(modelName, callback) {
 
    modelName.find({
      include: ['lookupValueIbfk1rel']
    }, function (error, result) {
      if (error) {
        callback(error);
      } else {
        formatLookUpData(function(res1){
          callback(res1);
        }, null, result)
      }
    });
  }


var lookUpKey = 'lookUpdata';
function getData(data, cb){
  createCache(function(err, cache, data1){
    setCache(function(cacheData){
      getCache(function(finalData){
        cb(null, finalData);
      }, lookUpKey, cacheData)
    },lookUpKey, null, cache, data)
  },null, null, null, null)
}

let lookupTypeArray = [];
function formatLookUpData(callback, error, entitydetails) {
  let lookupValueArray = [];
  let formattedObject = {};

  const lookupValueId = 'lookupValueId';
  const lookupTypeId = 'lookupTypeId';
  const lookupValue = 'lookupValue';

  var checkSameTypeCode;
  var tempTypeCode;

  let flag = false;
  entitydetails.map(function (lookupRecord) {
    const lookupValueObject = {};
    var tempObject = JSON.stringify(lookupRecord);
    var lookupRecord = JSON.parse(tempObject);
    lookupValueObject[lookupValueId] = lookupRecord.lookupValueId;
    lookupValueObject[lookupTypeId] = lookupRecord.lookupTypeId;
    lookupValueObject[lookupValue] = lookupRecord.lookupValue;

    if(flag === false){
      checkSameTypeCode = lookupRecord.lookupValueIbfk1rel.lookupCode;
      tempTypeCode = lookupRecord.lookupValueIbfk1rel.lookupCode;
      flag = true;
    }
    if (checkSameTypeCode !== lookupRecord.lookupValueIbfk1rel.lookupCode) {
      formattedObject[checkSameTypeCode] = lookupValueArray;
      tempTypeCode = checkSameTypeCode;
      lookupValueArray = [];
    }
      lookupValueArray.push(lookupValueObject);
      checkSameTypeCode = lookupRecord.lookupValueIbfk1rel.lookupCode;
  })
  lookupTypeArray.push(formattedObject);
  getData(lookupTypeArray, function(err, res){
      callback(res);
      // getD(lookupTypeArray);
      lookupTypeArray = [];
  });
}
function getDataOfAnyModel(modelName, Cb){
modelName.find({}, function(err, res){
  if(err){
    Cb(err, null);
  } else {
    getData(res, function(err, finalCache){
      Cb(finalCache);
    })
  }
})
}
function getAllData(callBc){
var State = server.models.State;
getDataOfAnyModel(State, function(stateObject){
  var country = server.models.Country;
  getDataOfAnyModel(country, function(countryObject){
    var currency = server.models.Currency;
    getDataOfAnyModel(currency, function(currencyObject){
        var city = server.models.City;
        getDataOfAnyModel(city, function(cityData){
          var univ = server.models.University;
          getDataOfAnyModel(univ, function(univData){
            var finalCacheObject = {};
            finalCacheObject.Country = countryObject;
            finalCacheObject.State = stateObject;
            finalCacheObject.Currency = currencyObject;
            finalCacheObject.City = cityData;
            finalCacheObject.University = univData;
            callBc(finalCacheObject);
          })
        })
    })
  })
})
}
getAllData(function(allDataObject){
  return allDataObject;
})
getLookUpData(lookupValue, function(object){
    return object;
});
data();
// getCacheLookUp();
//How to declare the function.
apiLabelset(fieldLabel, 'LabelKey', function (obj) {
  return obj;
});
exports.apiLabelset = apiLabelset;
exports.formatLookUpData = formatLookUpData;
exports.getLookUpData = getLookUpData;
exports.getAllData = getAllData;
exports.getCacheLookUp = getCacheLookUp;
exports.data = data;
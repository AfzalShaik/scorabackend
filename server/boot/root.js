'use strict';
var dsConfig = require('../datasources.json');
var configFile = require('../config.json');
var path = require('path');
var nodeCache = require('node-cache');
module.exports = function(server) {
  var ScoraUser = server.models.ScoraUser;
  var fieldLabel = server.models.FieldLabel;
  var lookupValue = server.models.LookupValue;
  var caching = require('./caching/labelCaching').apiLabelset;
  var lookup = require('./caching/labelCaching').getLookUpData;
  var getCacheLookUp = require('./caching/labelCaching').getCacheLookUp;
  var getAllData = require('./caching/labelCaching').getAllData;
  var data = require('./caching/labelCaching').data;
  var formatLookUpData = require('./caching/labelCaching').formatLookUpData;
  var getLookUpData = require('./caching/labelCaching').getLookUpData;
  var modelCache = new nodeCache();
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  //login page
  router.get('/', function(req, res) {
    var credentials = configFile.adminEmail;
    res.render('login', {
      email: credentials.user,
      password: credentials.pass,
    });
  });
  router.get('/getLookUpData', function(req, res) {
    var lookupValue = server.models.LookupValue;
    lookup(lookupValue, function(obj) {
      // var lookupData = {};
      // lookupData.data = obj;
      res.send(obj);
    });
  });
  router.get('/getCacheLookUp', function(req, res) {
    var data = getCacheLookUp();
    // var c = data.ATTACH_CAT_CODE;
    // console.log('data', c['35']);
    res.send(data);
  });
  router.get('/data', function(req, res) {
    // var lookupValue = server.models.LookupValue;
    // lookup(lookupValue, function(obj) {
      // var lookupData = {};
      // lookupData.data = obj;
    var datar = data();
    var final = 'done';
    res.send(final);
    // });
  });
// to get all data
  router.get('/getAllData', function(req, res) {
    getAllData(function(obj) {
      var allData = {};
      allData.data = obj;
      res.send(obj);
    });
  });
  router.get('/apiLabelset', function(req, res) {
    caching(fieldLabel, 'LabelKey', function(obj) {
      res.send(obj);
    });
  });

  //Lookup values
  router.get('/lookupAPI', function(req, res) {
    getLookUpData(lookupValue, formatLookUpData);
    // console.log('ddddddd');
    res.send('lookupAPI');
  });
  //verified
  router.get('/verified', function(req, res) {
    res.render('verified');
  });

  //log a ScoraUser in
  router.post('/login', function(req, res) {
    // console.log('req.bodyyyyyyyyyyyyyyy ', req.body.email);
    // console.log('req.bodyyyyyyyyyyyyyyy ', req.body.password);
    ScoraUser.login({
      email: req.body.email,
      password: req.body.password,
    }, 'ScoraUser', function(err, token) {
      if (err) {
        if (err.details && err.code === 'LOGIN_FAILED_EMAIL_NOT_VERIFIED') {
          res.render('reponseToTriggerEmail', {
            title: 'Login failed',
            content: err,
            redirectToEmail: '/api/ScoraUsers/' + err.details.userId + '/verify',
            redirectTo: '/',
            redirectToLinkText: 'Click here',
            userId: err.details.userId,
          });
        } else {
          res.render('response', {
            title: 'Login failed. Wrong username or password',
            content: err,
            redirectTo: '/',
            redirectToLinkText: 'Please login again',
          });
        }
        return;
      }
      res.render('home', {
        email: req.body.email,
        accessToken: token.id,
        redirectUrl: '/api/ScoraUsers/change-password?access_token=' + token.id,
      });
    });
  });

  //log a user out
  router.get('/logout', function(req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);
    ScoraUser.logout(req.accessToken.id, function(err) {
      if (err) return next(err);
      res.redirect('/');
    });
  });
  router.get('/verfyAccount/:userId/:token', function(req, res, next) {
    console.log('aa', req.params.userId);
    var userId = req.params.userId;
    var token = req.params.token;
    if (userId && token) {
      var scoraUser = server.models.ScoraUser;
      scoraUser.findOne({'where': {'id': userId, 'verificationToken': token}},
      function(err, response) {
        if (err) {
          res.sendStatus(401);
        } else if (response === null) {
          res.sendStatus(401);
        } else {
          var bcrypt = require('bcrypt');
          var encryptPass = bcrypt.hashSync(response.password, 10);
          response.updateAttributes(
          {'emailVerified': 1, 'verificationToken': null, 'password': encryptPass},
        function(err, responseDone) {
          if (err) {
            res.sendStatus(401);
          } else {
            res.render('verified');
          }
        });
        }
      });
    } else {
      res.sendStatus(401);
    }
  });
  //send an email with instructions to reset an existing ScoraUser's password
  router.post('/request-password-reset', function(req, res, next) {
    // console.log('in reset password reset routeeeeeeeeeeeeeeeeeeeeeee');
    ScoraUser.resetPassword({
      email: req.body.email,
    }, function(err) {
      if (err) return res.status(401).send(err);

      res.render('response', {
        title: 'Password reset requested',
        content: 'Check your email for further instructions',
        redirectTo: '/',
        redirectToLinkText: 'Log in',
      });
    });
  });

  //show password reset form
  router.get('/reset-password', function(req, res, next) {
    // console.log('reset passwordddddddddddddddddddddddddddddddddddddddddddddddddddddddd');
    if (!req.accessToken) return res.sendStatus(401);
    res.render('password-reset', {
      redirectUrl: '/api/ScoraUsers/reset-password?access_token=' +
        req.accessToken.id,
    });
  });
  server.use(router);
  // server.use(caching());
};
